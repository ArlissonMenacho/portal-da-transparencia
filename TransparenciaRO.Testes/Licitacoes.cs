﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;

namespace TransparenciaRO.Testes
{
    [TestClass]
    public class Licitacoes
    {


        private Guid InserirLicitacaoTeste()
        {
            var db = new DbTransparencia();

            //Inserir o arquivo temporário no BD
            var novoArquivo = new Arquivo { ArquivoId = Guid.NewGuid(), Temporario = true, Descricao = "Teste inserção licitação", PastaId = new Guid("0920468b-8bd4-47a8-8b99-f30587e2021e") };
            db.Arquivos.Add(novoArquivo);
            db.SaveChanges();

            var novaLicitacao = new Licitacao
            {
                Ano = 2017,
                DataHoraAbertura = DateTime.Now,
                EnderecoEletronico = "teste@teste.com.br",
                Local = "Porto Velho",
                //LicitacaoModalidadeId = new Guid("faf57c3e-72c1-4aed-83ea-e24420b00b05"),
                MaisInformacoes = "Teste de inserção",
                UnidadeGestoraId = new Guid("A8C7D47E-E393-467F-AD58-0098391A807C"),
                Arquivos = new List<Arquivo>() { novoArquivo }
            };

            var licitacaoDAL = new LicitacaoDAL(db);

            var msgErro = "";
            //Inserir a licitação no BD e vincular arquivo
            var guidNovaLicitacao = licitacaoDAL.SalvarDados(novaLicitacao, out msgErro);
            db.SaveChanges();

            return guidNovaLicitacao.Value;

        }

        private void LimparDadosTeste()
        {

            var db = new DbTransparencia();

            //Remove os dados de teste do BD
            db.Database.ExecuteSqlCommand("delete from Arquivos where licitacaoid is not null; delete from Licitacoes;");
            db.SaveChanges();
        }

        /// <summary>
        /// Teste de inserção de licitação com 1 arquivo
        /// </summary>
        [TestMethod]        
        public void Insercao1Licitacao()
        {

            var db = new DbTransparencia();

            var guidNovaLicitacao = InserirLicitacaoTeste();
            var licitacaoBD = db.Licitacoes.FirstOrDefault(l => l.LicitacaoId == guidNovaLicitacao);
            
            /*
            Verifica as seguintes condições:
            1) Licitação foi inserida no BD
            2) Arquivo foi vinculado à esta licitação
            3) Arquivo foi alterado para temporario = false
            */
            Assert.IsTrue(licitacaoBD != null && licitacaoBD.Arquivos.Where(a => !a.Temporario).Count() > 0);

            LimparDadosTeste();
        }

        [TestMethod]
        public void TesteAtualizacaoLicitacao()
        {
            //Insere nova licitação de teste
            var db = new DbTransparencia();
            var guidNovaLicitacao = InserirLicitacaoTeste();
            //Recupera a licitação inserida do BD
            var licitacaoBD = db.Licitacoes.Include(l => l.Arquivos).FirstOrDefault(l => l.LicitacaoId == guidNovaLicitacao);
            //Adiciona um arquivo temporária atribuída à licitação no BD            
            var novoArquivo = new Arquivo { ArquivoId = Guid.NewGuid(), Temporario = true, Descricao = "Teste inserção licitação (update)", PastaId = new Guid("0920468b-8bd4-47a8-8b99-f30587e2021e"), LicitacaoId = guidNovaLicitacao };
            db.Arquivos.Add(novoArquivo);
            db.SaveChanges();
            licitacaoBD.Arquivos.Add(novoArquivo);
            //Chama o método de salvar a licitação
            var msgErro = "";
            new LicitacaoDAL(db).SalvarDados(licitacaoBD, out msgErro);

            /* Verifica a seguinte condição:            
            Dois arquivos com temporario = false vinculados à esta licitação
            */
            var licitacaoBD2 = db.Licitacoes.Include(l => l.Arquivos).FirstOrDefault(l => l.LicitacaoId == guidNovaLicitacao);
            Assert.IsTrue(licitacaoBD2.Arquivos.Where(a => !a.Temporario).Count() == 2);
            LimparDadosTeste();

        }
    }
}
