# Portal de Transparência - CGE/RO 2016 #



## Passos para instalação do ambiente ##

* Baixar esta solução no bitbucket
* Restaurar as bases DBTCE e DBPLL do SQL Server
* Criar o banco DbTransparencia
* Rodar o script de criação de views em /TransparenciaRO.Infra/Scripts/Views
* Gerar script de Update-Database (Update-Database -script)
* Executar os scripts de criação de tabela (*excluir os scripts de criação de tabelas, uma vez que as views foram criadas manualmente*)
* Executar novamente um Update-Database (sem o -script) para que a rotina de seed seja executada (inserirá diversos dados padrões como referência)
* Executar a solução e verificar se erros de sincronização entre BD <-> Entidades ocorrem
* Se tudo correr bem, o preparo do ambiente está feito