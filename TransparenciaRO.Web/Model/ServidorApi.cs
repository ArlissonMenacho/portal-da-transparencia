﻿using System;

namespace TransparenciaRO.Web.Model
{
    public class ServidorApi
    {
        public Guid Id { get; set; }
    
        public string Cpf { get; set; }
      
        public string Matricula { get; set; }
      
        public string Nome { get; set; }
        
        public DateTime DataAdmissao { get; set; }
        
        public DateTime DataNomeacao { get; set; }
        public string NumeroDecreto { get; set; }
        public string NumeroDiof { get; set; }
      
        public string OrgaoOrigem { get; set; }
    
        public string Lotacao { get; set; }
        public string Setor { get; set; }
    
        public bool Cedido { get; set; }

        public int UnidadeGestoraId { get; set; }
        public virtual UnidadeGestoraApi UnidadeGestora { get; set; }
    }
}