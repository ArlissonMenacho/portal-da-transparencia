﻿using System;
using Microsoft.AspNet.Identity;

namespace TransparenciaRO.Web.Model
{
    public class Usuario : IUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public long CPF { get; set; }
        public string UserName { get; set; }
    }
}