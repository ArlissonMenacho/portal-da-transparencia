﻿using System.Collections.Generic;

namespace TransparenciaRO.Web.Model
{
    public class UnidadeGestoraApi
    {
        public int Id { get; set; }
     
        public string Codigo { get; set; }
        public string Sigla { get; set; }
        public string Descricao { get; set; }
        public bool Enviado { get; set; }
        public bool Aprovado { get; set; }
        public virtual ICollection<ServidorApi> Servidores { get; set; }
    }
}