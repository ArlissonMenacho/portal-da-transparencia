﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.AspNet.Identity;
using System;

[assembly: OwinStartupAttribute(typeof(TransparenciaRO.Web.Startup))]
namespace TransparenciaRO.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Login/"),
                ExpireTimeSpan = TimeSpan.FromMinutes(30)
            });
        }
    }
}