﻿using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers
{
    public class IndiceController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly IndiceDAL _indiceDal;

        public IndiceController()
        {
            _indiceDal = new IndiceDAL(_dbTransparencia);
        }

        // GET: Indice
        public ActionResult Index(string pTermosBusca)
        {
            return View(_indiceDal.Buscar(pTermosBusca ?? "").ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}