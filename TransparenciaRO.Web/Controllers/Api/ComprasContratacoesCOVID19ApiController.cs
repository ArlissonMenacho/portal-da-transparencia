﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class ComprasContratacoesCOVID19ApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ViewEmpenhosDal _viewEmpenhosDal;

        public ComprasContratacoesCOVID19ApiController()
        {
            _viewEmpenhosDal = new ViewEmpenhosDal(_dbTransparencia);
        }
        [Obsolete("Oculto para o Swagger")]
        public HttpResponseMessage Get()
        {
            var dados = _viewEmpenhosDal.GetComprasContratacoesCOVID19ParaAPI();

            var response = Request.CreateResponse(HttpStatusCode.OK, dados);
            string uri = Url.Link("DefaultApi", new { id = response });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}