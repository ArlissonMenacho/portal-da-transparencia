﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers.Api
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ComprasContratacoesApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly OrgaoDAL _orgaoDal;
        private readonly ModalidadeLicitacaoDAL _modalidadeLicitacaoDal;
        private readonly ViewEmpenhosDal _viewEmpenhosDal;

        public ComprasContratacoesApiController()
        {
            _orgaoDal = new OrgaoDAL(_dbTransparencia);
            _modalidadeLicitacaoDal = new ModalidadeLicitacaoDAL(_dbTransparencia);
            _viewEmpenhosDal = new ViewEmpenhosDal(_dbTransparencia);
        }
        public HttpResponseMessage Get()
        {
            var unidadesGestoras = _orgaoDal
                    .GetOrgaos()
                    .Select(org => new { NomOrgao = org.NomOrgao.Trim(), CodOrgao = org.CodOrgao })
                    .Distinct().GroupBy(org => org.NomOrgao)
                    .Select(org => new SelectOptionModel(String.Join(",", org.Select(o => o.CodOrgao).ToArray()), org.Key))
                    .Combify("TODAS")
                    .ToList();

            var modalidadesCompras = _modalidadeLicitacaoDal
                .GetModalidades()
                .Select(m => new SelectOptionModel(m.Codigo.ToString(), m.Descricao))
                .Combify("TODAS")
                .ToList();
            var dados = new { unidadesGestoras, modalidadesCompras };
            var response = Request.CreateResponse(HttpStatusCode.OK, dados);
            string uri = Url.Link("DefaultApi", new { id = dados });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        //public HttpResponseMessage Get(string dataInicial, string dataFinal, string unidadeGestora, string modalidadeCompra, string CNPJ, int pagina)
        //{
        //    var dtInicial = DateTime.Parse(dataInicial).Date;
        //    var dtFinal = DateTime.Parse(dataFinal).Date;
        //    var codModalidadeCompra = Convert.ToInt32(modalidadeCompra.ToDecrypted());
        //    var listaOrgaos = unidadeGestora.ToDecrypted().Split(',').ToList();
        //    const int maxRegistros = 1000;

        //    if (listaOrgaos.Count == 1 && listaOrgaos.First() == "0") // Caso o usuário tenha selecionado TODAS
        //        listaOrgaos = null;

        //    var data = _viewEmpenhosDal.GetEmpenhos(codModalidadeCompra == 0
        //             ? null
        //             : (int?)codModalidadeCompra, listaOrgaos ?? new List<string>(), dtInicial, dtFinal, "", CNPJ);

        //    var dataARetornar = data.Select(d => new
        //    {
        //        d.Credor,
        //        d.DocCredor,
        //        DataArquivo = d.DataArquivo.Value.ToString("dd/MM/yyyy"),
        //        DataEmpenho = d.DataEmpenho.Value.ToString("dd/MM/yyyy"),
        //        d.Descricao,
        //        d.Exercicio,
        //        d.Item,
        //        d.ModalidadeCompra,
        //        d.NumeroEmpenho,
        //        d.NumeroProcesso,
        //        d.Processo,
        //        d.Quantidade,
        //        d.TipoEmpenho,
        //        d.UnidadeGestora,
        //        d.UnidadeMedida,
        //        ValorItem = d.ValorItem?.ToString("c2")
        //    })
        //    .Distinct()
        //    .Take(maxRegistros)
        //    .ToList();

        //    var response = Request.CreateResponse(HttpStatusCode.OK, dataARetornar);
        //    string uri = Url.Link("DefaultApi", new { id = response });
        //    response.Headers.Location = new Uri(uri);
        //    response.Headers.Add("Access-Control-Allow-Origin", "*");
        //    return response;
        //}


        /// <summary>
        /// Pesquisar compras e contratações por unidades gestora, modalidade de compra, data inicial, data final, cnpj,quantidade de registros por página e escolha da página.
        /// </summary>
        /// <param name="dataInicial">A data inicial para qual o detalhamento de compras e contratações deverá ser retornado.	</param>
        /// <param name="dataFinal">A data final para qual o detalhamento de compras e contratações deverá ser retornado.	</param>
        /// <param name="unidadeGestora">O código da unidade gestora para qual o detalhamento de compras e contratações deverá ser retornado.	</param>
        /// <param name="modalidadeCompra">O código da modalidade de compra para qual o detalhamento de compras e contratações deverá ser retornado.	</param>
        /// <param name="cnpj">O CNPJ do credor para qual o detalhamento de compras e contratações deverá ser retornado.	</param>
        /// <param name="qtdRegistros">A quantidade de registros por página para qual o detalhamento de compras e contratações deverá ser retornado. Valor padrão = 100.	</param>
        /// <param name="pagina">A página para qual o detalhamento de compras e contratações deverá ser retornado. Valor padrão = 1.	</param>
        /// <response code="200">A lista de Orgãos foi obtida com sucesso.</response> 
        /// <response code="500">Ocorreu um erro ao obter a lista de orgãos.</response> 
        [Route("Api/Compras/{dataInicial}/{dataFinal}/{unidadeGestora}/{modalidadeCompra}/{cnpj}/{qtdRegistros}")]
        [HttpGet]
        public HttpResponseMessage GetComParametros(string dataInicial, string dataFinal, string unidadeGestora, string modalidadeCompra, string cnpj, int? qtdRegistros = 100, int? pagina = 1)
        {
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date;
            var codModalidadeCompra = Convert.ToInt32(modalidadeCompra.ToDecrypted());
            var listaOrgaos = unidadeGestora.ToDecrypted().Split(',').ToList();

            if (listaOrgaos.Count == 1 && listaOrgaos.First() == "0") // Caso o usuário tenha selecionado TODAS
                listaOrgaos = null;

            var data = _viewEmpenhosDal.GetEmpenhosAPI(codModalidadeCompra == 0
                     ? null
                     : (int?)codModalidadeCompra, listaOrgaos ?? new List<string>(), dtInicial, dtFinal, "", cnpj, pagina ?? 1, qtdRegistros ?? 100);


            var response = Request.CreateResponse(HttpStatusCode.OK, new { data });
            string uri = Url.Link("DefaultApi", new { id = response });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}