﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class ContratosApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ContratoConvenioDAL _contratoConvenioDal;

        public ContratosApiController()
        {
            _contratoConvenioDal = new ContratoConvenioDAL(_dbTransparencia);
        }

        /// <summary>
        /// Informações em formato aberto sobre Contratos firmados no âmbito do Poder Executivo do Governo do Estado de Rondônia.
        /// A API abaixo apresenta as seguintes informações: Unidade gestora, número do documento, número do processo, licitação origem, data de elaboração, data de assinatura, data de vigência descritiva, empresa, CNPJ, objeto e descrição do valor.
        /// </summary>
        /// <response code="200">Consulta realizada com sucesso.</response> 
        /// <response code="500">Ocorreu um erro.</response> 
        //GET: Contratos e Contratos Emergenciais COVID-19
        public HttpResponseMessage Get()
        {
            var dados = _contratoConvenioDal.GetContratoConvenioTipoContratoEContratoEmergencialCOVIDParaAPI(null, null, true, true, true, true, new List<Guid>(), "", "", "", "", "").ToList().Select(d => new
            {
                UnidadeGestora = d.UnidadeGestora.sigla,
                d.NumeroDocumento,
                d.NumeroProcesso,
                d.LicitacaoOrigem,
                DataElaboracao = d.DataElaboracao.ToString("dd/MM/yyyy"),
                DataAssinatura = d.DataElaboracao.ToString("dd/MM/yyyy"),
                d.DataVigenciaDescritiva,
                d.Empresa,
                d.Cnpj,
                d.Objeto,
                DescricaoValor = $"Valor {d.Valor:c2} {(d.ValorEmpenhado != null ? "VALOR EMPENHADO " + d.ValorEmpenhado : "")}{(d.ValorAContraPartida != null ? "VALOR A CONTRA PARTIDA " + d.ValorAContraPartida : "")}"
                //Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new { NomeArquivo = a.Descricao + "." + a.Extensao, LinkArquivo = "https://transparencia.ro.gov.br/Arquivo/VisualizarArquivo/pEncArquivoId=" + a.ArquivoId.ToString().ToEncrypted() }),
            });

            var response = Request.CreateResponse(HttpStatusCode.OK, dados);
            string uri = Url.Link("DefaultApi", new { id = response });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}
