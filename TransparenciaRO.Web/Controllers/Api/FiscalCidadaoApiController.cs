﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class FiscalCidadaoApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia;
        private readonly FiscalCidadaoDAL _fiscalCidadaoDal;

        public FiscalCidadaoApiController()
        {
            _dbTransparencia = new DbTransparencia();
            _fiscalCidadaoDal = new FiscalCidadaoDAL(_dbTransparencia);
        }

        //GET api
        [Obsolete("Oculto para o Swagger")]
        public HttpResponseMessage Get(string email)
        {
            var protocolo = _fiscalCidadaoDal.BuscarProtocoloPorEmail(email).Select(x => new { protocolo = x.ProtocoloId });
            var response = Request.CreateResponse(HttpStatusCode.Created, protocolo);
            string uri = Url.Link("DefaultApi", new { id = protocolo });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        // POST api/<controller>
        [Obsolete("Oculto para o Swagger")]
        public HttpResponseMessage Post(FiscalCidadao fiscalCidadao)
        {
            fiscalCidadao = _fiscalCidadaoDal.Salvar(fiscalCidadao);
            var response = Request.CreateResponse<FiscalCidadao>(HttpStatusCode.Created, fiscalCidadao);
            string uri = Url.Link("DefaultApi", new { id = fiscalCidadao.ProtocoloId });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}