﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using TransparenciaRO.Business;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class ReceitaApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();

        public ReceitaApiController()
        {

        }

        /// <summary>
        /// Método para detalhar as receitas do governo entre intervalo de anos
        /// </summary>
        /// <param name="anoInicial">Ano inicial para qual o detalhamento da receita deverá ser retornado.</param>
        /// <param name="anoFinal">Ano final para qual o detalhamento da receita deverá ser retornado. </param>
        /// <param name="consolidacao">Período de consolidação de receita, pode ser feita por Mês, quadrimestre ou ano. </param>
        /// <param name="ug">Unidade Gestora a ser consultada para detalhamento.</param>
        /// <returns></returns>
        [HttpGet]
        public List<ReceitaModel> Get(int anoInicial, int anoFinal, int consolidacao, string ug)
        {
            var tipoconsolidacao = (ETipoConsolidacao)consolidacao;

            var receitas = tipoconsolidacao == ETipoConsolidacao.PorAno ? ReceitaBusiness.GetReceitaAno(anoInicial, anoFinal, tipoconsolidacao, ETipoConsultaReceita.Total, ug, true) :
                           ReceitaBusiness.GetReceitaMes(anoInicial, anoFinal, tipoconsolidacao, ETipoConsultaReceita.Total, ug, true);

            return receitas;
        }
        /// <summary>
        /// Método para detalhar as receitas do governo referentes a um ano específico
        /// </summary>
        /// <param name="ano">Ano para qual o detalhamento da receita deverá ser retornado.</param>
        /// <param name="tipoconsulta">0: Total - A somatória de todas as receitas, ignorando os descontos 1: Líquida - A somatória de todas as receitas, contemplando também os descontos 2: Receita corrente 3: Receita capital                4: Receita intra-orçamentária</param>
        /// <param name="ug">Unidade Gestora a ser consultada para detalhamento.</param>
        /// <returns></returns>
        [Route("api/receitaapi/Ano/{ano}/{tipoconsulta}/{ug}")]
        [HttpGet]
        public List<ReceitaModel> Ano(int ano, int tipoconsulta, string ug)
        {
            return ReceitaBusiness.DetalharAno(ano, ug, (ETipoConsultaReceita)tipoconsulta);
        }
        /// <summary>
        /// Método para detalhar as receitas do governo referentes a um ano e mês específico
        /// </summary>
        /// <response code="200">A lista de Orgãos foi obtida com sucesso.</response> 
        /// <response code="500">Ocorreu um erro ao obter a lista de orgãos.</response> 
        [Route("api/receitaapi/mes/{mes}/{ano}/{tipoconsulta}/{ug}")]
        [HttpGet]
        public List<ReceitaModel> Mes(int mes, int ano, int tipoconsulta, string ug)
        {
            return ReceitaBusiness.DetalharMes(mes, ano, ug, (ETipoConsultaReceita)tipoconsulta);
        }
        /// <summary>
        /// Método para detalhar as receitas do governo referentes a um ano e quadrimestre específico
        /// </summary>
        /// <response code="200">A lista de Orgãos foi obtida com sucesso.</response> 
        /// <response code="500">Ocorreu um erro ao obter a lista de orgãos.</response> 
        [Route("api/receitaapi/Quadrimestre/{quadrimestre}/{ano}/{ug}/{tipoConsulta}")]
        [HttpGet]
        public List<ReceitaModel> Quadrimestre(string quadrimestre, int ano, string ug, int tipoConsulta)
        {
            return ReceitaBusiness.DetalharQuadrimestre(quadrimestre, ano, ug, (ETipoConsultaReceita)tipoConsulta);
        }

        public class PostInput
        {
            public int pAnoInicial { get; set; }
            public int pAnoFinal { get; set; }
            public int pConsolidarPor { get; set; }
            public int pTipoConsulta { get; set; }
            public string pUG { get; set; }
            public bool pIncluirReceitaPrevista { get; set; }
        }
        [Obsolete("Oculto para o Swagger")]
        [HttpPost]
        public List<ReceitaModel> Post([FromBody] PostInput pDados)
        {
            var consolidacao = (ETipoConsolidacao)pDados.pConsolidarPor;
            var tipoConsulta = (ETipoConsultaReceita)pDados.pTipoConsulta;

            var receitas =
                consolidacao == ETipoConsolidacao.PorQuadrimestre ? ReceitaBusiness.GetReceitaQuadrimestre(pDados.pAnoInicial, pDados.pAnoFinal, consolidacao, tipoConsulta, pDados.pUG, pDados.pIncluirReceitaPrevista) :
                consolidacao == ETipoConsolidacao.PorMes ? ReceitaBusiness.GetReceitaMes(pDados.pAnoInicial, pDados.pAnoFinal, consolidacao, tipoConsulta, pDados.pUG, pDados.pIncluirReceitaPrevista) :
                ReceitaBusiness.GetReceitaAno(pDados.pAnoInicial, pDados.pAnoFinal, consolidacao, tipoConsulta, pDados.pUG, pDados.pIncluirReceitaPrevista);

            return receitas;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}