﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class ContratosEmergenciaisCOVID19ApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ContratoConvenioDAL _contratoConvenioDal;

        public ContratosEmergenciaisCOVID19ApiController()
        {
            _contratoConvenioDal = new ContratoConvenioDAL(_dbTransparencia);
        }
        [Obsolete("Oculto para o Swagger")]
        // GET: ContratosEmergenciaisCOVID19
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var dados = _contratoConvenioDal.GetContratoConvenio(ETipoDocumento.ContratoEmergencialCOVID19, null, null, true, true, true, true, new List<Guid>(), "", "", "", "", "").ToList().Select(d => new
            {
                d.NumeroDocumento,
                d.NumeroProcesso,
                DataElaboracao = d.DataElaboracao.ToString("dd/MM/yyyy"),
                d.DataVigenciaDescritiva,
                d.Empresa,
                d.Cnpj,
                d.Objeto,
                DescricaoValor = $"Valor {d.Valor:c2} {(d.ValorEmpenhado != null ? "VALOR EMPENHADO " + d.ValorEmpenhado : "")}{(d.ValorAContraPartida != null ? "VALOR A CONTRA PARTIDA " + d.ValorAContraPartida : "")}",
                Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new { NomeArquivo = a.Descricao + "." + a.Extensao, LinkArquivo = "https://transparencia.ro.gov.br/Arquivo/VisualizarArquivo/pEncArquivoId=" + a.ArquivoId.ToString().ToEncrypted() }),
            }).Take(1000);

            var response = Request.CreateResponse(HttpStatusCode.OK, dados);
            string uri = Url.Link("DefaultApi", new { id = response });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        //public HttpResponseMessage Get(string dataInicial = null, string dataFinal = null, string cnpj = null, string numeroProcesso = null, string numeroDocumento = null, string razaoSocial = null)
        //{
        //    var tipoDoc = (ETipoDocumento)Convert.ToInt32(ETipoDocumento.ContratoEmergencialCOVID19);
        //    var dtInicial = DateTime.Parse(dataInicial).Date;
        //    var dtFinal = DateTime.Parse(dataFinal).Date;
        //    var unidadesGestoras = new List<Guid>();
        //    var limiteQtdeDocumentos = 1000;

        //    var dadosRetorno = _contratoConvenioDal.GetContratoConvenio(tipoDoc, dtInicial, dtFinal, true, true, true, true, unidadesGestoras, cnpj, numeroProcesso, numeroDocumento, razaoSocial, null).ToList().Select(d => new
        //    {
        //        Origem = d.LicitacaoOrigem,
        //        NumeroLivroEFolha = d.NumeroLivroEspecial?.ToString("00") + "/" + d.NumeroFolha?.ToString("00"),
        //        d.NumeroDocumento,
        //        d.NumeroProcesso,
        //        DataElaboracao = d.DataElaboracao.ToString("dd/MM/yyyy"),
        //        DataVigencia = d.DataVigencia?.ToString("dd/MM/yyyy"),
        //        DataRetorno = d.DataRetorno?.ToString("dd/MM/yyyy"),
        //        d.NumeroDoe,
        //        d.Empresa,
        //        d.Cnpj,
        //        d.Objeto,
        //        DescricaoValor = $"Valor {d.Valor:c2} {(d.ValorEmpenhado != null ? "VALOR EMPENHADO " + d.ValorEmpenhado : "")}{(d.ValorAContraPartida != null ? "VALOR A CONTRA PARTIDA " + d.ValorAContraPartida : "")}",
        //        Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new { LinkArquivo = "VisualizarArquivo/Arquivo/pEncArquivoId=" + a.ArquivoId.ToString().ToEncrypted(), NomeArquivo = a.Descricao + "." + a.Extensao }),
        //    }).Take(limiteQtdeDocumentos);

        //    var dados = new { dadosRetorno };

        //    var response = Request.CreateResponse(HttpStatusCode.OK, dados);
        //    string uri = Url.Link("DefaultApi", new { id = response });
        //    response.Headers.Location = new Uri(uri);
        //    response.Headers.Add("Access-Control-Allow-Origin", "*");
        //    return response;
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}
