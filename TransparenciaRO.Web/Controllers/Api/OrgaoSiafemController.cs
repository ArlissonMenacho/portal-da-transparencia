﻿using System;
using System.Linq;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class OrgaoSiafemController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();

        [Obsolete("Oculto para o Swagger")]
        public object Get()
        {
            return new
            {
                dados = _dbTransparencia.OrgaosSiafem.Select(org => new
                {
                    Codigo = org.ORGAO,
                    Nome = org.NOMEORGAO,
                    CNPJ = org.CGC.Substring(0, 2) + "." + org.CGC.Substring(2, 3) + "." + org.CGC.Substring(5, 3) + "/" + org.CGC.Substring(8, 4) + "-" + org.CGC.Substring(12, 2)
                }).ToList()
            };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}