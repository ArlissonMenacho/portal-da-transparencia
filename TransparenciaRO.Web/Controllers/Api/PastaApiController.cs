﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using System.Security.Claims;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class PastaApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly PastaDAL _pastaDal;

        public PastaApiController()
        {
            _pastaDal = new PastaDAL(_dbTransparencia);
        }

        public class MenuEntry
        {
            public Guid Id { get; set; }
            public string Descricao { get; set; }
            public string faIcon { get; set; }
            public string imgIcon { get; set; }
            public string tipoImg { get; set; }
            public string onClick { get; set; }
            public string url { get; set; }
            public bool botaoVoltar { get; set; }
            public string temaCss { get; set; }
            public int? Ordem { get; set; }
            public string target { get; set; }
            public int? Agrupamento { get; set; }
            public string Note { get; set; }
        }

        public class BreadCrumbEntry
        {
            public string Descricao { get; set; }
            public string OnClick { get; set; }
        }

        [Obsolete("Oculto para o Swagger")]
        private string getFaIconPorExtensao(string pExtensao)
        {
            switch (pExtensao.Replace(".", ""))
            {
                case "pdf":
                    return "file-pdf-o";
                case "xls":
                case "xlsx":
                case "csv":
                    return "file-excel-o";
                case "doc":
                case "docx":
                    return "file-word-o";
                default:
                    return "file";
            }
        }

        [Obsolete("Oculto para o Swagger")]
        [Route("api/GetVisualizarPastaLink/{pPastaId}")]
        public string GetVisualizarPastaLink(Guid pPastaId)
        {
            return $"/api/PastaApi?pEncPastaId={pPastaId.ToString("N").ToEncrypted()}";
        }
        [Obsolete("Oculto para o Swagger")]
        [Route("api/GetVisualizarArquivoLink/{pGuidArquivo}")]
        public string GetVisualizarArquivoLink(Guid pGuidArquivo)
        {
            var controllerArquivos = new ArquivoController();
            Func<string, System.Web.Mvc.ActionResult> func = controllerArquivos.VisualizarArquivo;

            return "https://transparencia.ro.gov.br/Arquivo/VisualizarArquivo?pEncArquivoId=" + pGuidArquivo.ToString("N").ToEncrypted();

            //return Url.Link("Default", new
            //{
            //    Controller = controllerArquivos.GetType().Name.Replace("Controller", ""),
            //    Action = func.Method.Name,
            //    pEncArquivoId = pGuidArquivo.ToString("N").ToEncrypted()
            //});
        }

        [HttpGet]
        public object Get([FromUri] string pEncPastaId)
        {
            var pastaId = new Guid();
            if (string.IsNullOrEmpty(pEncPastaId) || pEncPastaId == "null\n" || pEncPastaId == "null")
            {
                pEncPastaId = GuidUtils.GuidMenuPrincipal().ToString("N").ToEncrypted();
                pastaId = new Guid(pEncPastaId.ToDecrypted());

            }
            else
            {
                pastaId = new Guid(pEncPastaId.ToDecrypted());
            }

            var pastaDb = _pastaDal.GetPasta(pastaId);

            var retorno = pastaDb.SubPastas
                .Where(p => p.PerfilPasta == null)
                .Select(pasta => new MenuEntry
                {
                    Id = pasta.PastaId,
                    botaoVoltar = false,
                    Descricao = pasta.Nome,
                    Note = pasta.Descricao,
                    faIcon = pasta.IconeFa?.ToString().Replace("_", "-").Replace("fa-", "") ?? "",
                    imgIcon = pasta.IconeImagem != null ? Convert.ToBase64String(pasta.IconeImagem) : null,
                    tipoImg = pasta.TipoImagem != null ? (pasta.TipoImagem == ".svg" ? "svg+xml" : pasta.TipoImagem.Replace(".", "")) : null,
                    onClick = !string.IsNullOrEmpty(pasta.Href) ? null : $"requestFolderList('{GetVisualizarPastaLink(pasta.PastaId)}');",
                    url = pasta.Href != "" ? pasta.Href : null,
                    Agrupamento = pasta.Agrupamento ?? 0,
                    Ordem = pasta.Ordem,
                    temaCss = BuscarTema(pasta.PastaOrigem.PastaId)
                }).OrderBy(p => p.Ordem).ThenBy(p => p.Descricao).ToList();

            retorno.AddRange(pastaDb.Arquivos.Select(arq => new MenuEntry
            {
                Id = arq.PastaId,
                Descricao = arq.Descricao,
                botaoVoltar = false,
                faIcon = getFaIconPorExtensao(arq.Extensao),
                url = GetVisualizarArquivoLink(arq.ArquivoId),
                target = "_blank",
                onClick = null,
                temaCss = BuscarTema(arq.PastaId),
                Agrupamento = 0,
                Ordem = arq.Ordem
            }).OrderBy(a => a.Ordem).ThenBy(a => a.Descricao));

            retorno = retorno.OrderBy(x => x.Ordem).ThenBy(y => y.Descricao).ToList();

            var hierarquiaPastas = _pastaDal.GetHierarquiaPastas(pastaId);

            if (hierarquiaPastas.Count() >= 2) // Se for >= 2, significa que há a pasta atual e pelo menos uma pasta pai, ou seja, pode retornar
            {
                retorno.Insert(0, new MenuEntry
                {
                    botaoVoltar = true,
                    Descricao = "Voltar",
                    faIcon = "backward",
                    temaCss = BuscarTema(pastaId),
                    Agrupamento = 0,
                    onClick = $"requestFolderList('{GetVisualizarPastaLink(hierarquiaPastas.Last().PastaOrigemId.Value)}');"
                });
            }

            return new
            {
                dados = retorno.GroupBy(p => p.Agrupamento).Select(p => new { Agrupamento = p.Key, Pastas = p }),
                breadCrumb = hierarquiaPastas.Select(b => new BreadCrumbEntry
                {
                    Descricao = b.Nome,
                    OnClick = b.PastaId == pastaId ? "" : $"requestFolderList('{GetVisualizarPastaLink(b.PastaId)}');"
                }),
                imagens = pastaDb.Imagens.OrderBy(i => i.Ordem).Select(img => new { Titulo = img.Titulo, URLImagem = img.URLImagem, NumeroColunas = img.NumeroColunas, Descricao = img.Descricao })
            };
        }
        [HttpGet]
        [Route("api/BuscarTema/{pastaId}")]
        private string BuscarTema(Guid pastaId)
        {
            var possuiTema = false;
            var classTemaCss = "";
            var pastas = _pastaDal.GetHierarquiaPastas(pastaId);
            foreach (var pasta in pastas.Reverse())
            {
                if (!possuiTema)
                    classTemaCss = pasta.TemaCss;

                possuiTema = !pasta.TemaCss.IsNullOrEmpty();
            }

            return classTemaCss;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}