﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class ComprasMateriaisApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly MateriaisDAL _materiaisDAL;

        public ComprasMateriaisApiController()
        {
            _materiaisDAL = new MateriaisDAL(_dbTransparencia);
        }
        /// <summary>
        /// Informações sobre compras de materiais de consumo e permamente para atender a demanda causada pelo corona vírus(COVID-19) realizadas no âmbito do Poder Executivo do Governo do Estado de Rondônia para os Municípios. 
        /// </summary>
        /// <response code="200">Consulta realizada com sucesso.</response> 
        /// <response code="500">Ocorreu um erro.</response> 
        public HttpResponseMessage Get()
        {
            var dados = _materiaisDAL.ObterComprasCOVID19ParaAPI(1000);

            var response = Request.CreateResponse(HttpStatusCode.OK, dados);
            string uri = Url.Link("DefaultApi", new { id = dados });
            response.Headers.Location = new Uri(uri);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}
