﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class FornecedoresApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia;
        private readonly FornecedoresImpedidosLicitarDAL _fornecedoresImpedidosLicitarDal;

        public FornecedoresApiController()
        {
            _dbTransparencia = new DbTransparencia();
            _fornecedoresImpedidosLicitarDal = new FornecedoresImpedidosLicitarDAL(_dbTransparencia);
        }

        [Obsolete("Oculto para o Swagger")]
        public IList<FornecedoresImpedidosLicitar> GetFornecedoresImpedidosLicitar(string cpfCnpj)
        {
            if (string.IsNullOrEmpty(cpfCnpj))
            {
                return _fornecedoresImpedidosLicitarDal.GetlByCpfCnpj(cpfCnpj);
            }
            else
            {
                return _fornecedoresImpedidosLicitarDal.GetlAll();
            }
        }
    }
}