﻿using System.Collections.Generic;
using System.Web.Http;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System;

namespace TransparenciaRO.Web.Controllers.Api
{
    public class DespesaApiController : ApiController
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DespesaDAL _despesaDal;

        public DespesaApiController()
        {
            _despesaDal = new DespesaDAL(_dbTransparencia);
        }
        /// <summary>
        /// Pesquisa despesas por empenho, CNPJ, nome do credor, ano ou mês
        /// </summary>
        /// <param name="ano">O ano para qual o detalhamento da despesa deverá ser retornado.</param>
        /// <param name="mes">O mês para qual o detalhamento da despesa deverá ser retornado.</param>
        /// <param name="empenho">O número de empenho para qual o detalhamento da despesa deverá ser retornado (a consulta é canse-insensitive)</param>
        /// <param name="credor">O nome do credor para qual o detalhamento da despesa deverá ser retornado. Poderá ser preechido parcialmente.</param>
        /// <param name="cnpj">O CNPJ do credor para qual o detalhamento da despesa deverá ser retornado. Poderá ser preenchido ou não com pontos e traços, o sistema ignorará tudo o que não for numérico.</param>
        /// <response code="200">Consulta realizada com sucesso.</response> 
        /// <response code="500">Ocorreu um erro.</response> 
        public List<DespesaPortal> Get(string ano = "", string mes = "", string empenho = "", string credor = "", string cnpj = "", string ug = "")
        {
            var paramAno = string.IsNullOrWhiteSpace(ano) ? (short?)null : Convert.ToInt16(ano);
            var paramMes = string.IsNullOrWhiteSpace(mes) ? (short?)null : Convert.ToInt16(mes);

            return _despesaDal.Api(paramAno, paramMes, empenho, credor, cnpj, ug);
        }

        /// <summary>
        /// Pesquisa despesas por intervalo de anos
        /// </summary>
        /// <response code="200">Consulta realizada com sucesso.</response> 
        /// <response code="500">Ocorreu um erro.</response>
        [Route("Api/DespesaApi/GetIntervaloAno")]
        [HttpGet]
        public List<DespesaPortal> GetIntervaloAno(string anoIni = "2010", string anoFim = "2011")
        {
            var paramAnoIni = string.IsNullOrWhiteSpace(anoIni) ? (short?)null : Convert.ToInt16(anoIni);
            var paramAnoFim = string.IsNullOrWhiteSpace(anoFim) ? (short?)null : Convert.ToInt16(anoFim);

            return _despesaDal.ApiIntervaloAno(paramAnoIni, paramAnoFim);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}
