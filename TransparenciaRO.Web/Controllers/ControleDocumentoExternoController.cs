﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security.Provider;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class ControleDocumentoExternoController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly UnidadeGestoraDAL _unidadeGestora;
        private readonly ControleDocumentoExternoDAL _controleDocumentoExternoDal;
        private readonly ArquivoDAL _arquivoDal;
        public ControleDocumentoExternoController()
        {
            _unidadeGestora = new UnidadeGestoraDAL(_dbTransparencia);
            _controleDocumentoExternoDal = new ControleDocumentoExternoDAL(_dbTransparencia);
            _arquivoDal = new ArquivoDAL(_dbTransparencia);
        }


        // GET: ControleDocumentoExterno

        [Authorize(Roles = nameof(AdministradorControleDocumentoExterno))]
        public ActionResult Index()
        {
            ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
            return View();
        }

        [Authorize(Roles = nameof(AdministradorControleDocumentoExterno))]
        public ActionResult Novo()
        {
            ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
            return View(new ControleDoumentoExterno());
        }

        [Authorize(Roles = nameof(AdministradorControleDocumentoExterno))]
        public ActionResult Editar(string documentoId)
        {
            using (var dbTransparencia = new DbTransparencia())
            {
                ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
                var controleDocumentoExterno = _controleDocumentoExternoDal.Buscar(new Guid(documentoId));
                if (controleDocumentoExterno == null)
                {
                    MensagemErro = "Número de processo / documento informado inválido";
                }
                return View(controleDocumentoExterno);
            }
        }
        [Authorize(Roles = nameof(AdministradorControleDocumentoExterno))]
        public ActionResult Detalhar(string documentoId)
        {
            using (var dbTransparencia = new DbTransparencia())
            {
                ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
                var controleDocumentoExterno = _controleDocumentoExternoDal.Buscar(new Guid(documentoId));
                if (controleDocumentoExterno == null)
                {
                    MensagemErro = "Número de processo / documento informado inválido";
                }
                return View(controleDocumentoExterno);
            }
        }

        public ActionResult AdicionarArquivo(ControleDoumentoExterno dados)
        {
            var msgErrosArquivos = new List<String>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(Request.Files[i]))
                {
                    //Novos arquivos (salva no disco e BD)
                    var novoArquivo = new Arquivo();
                    novoArquivo.ArquivoId = Guid.NewGuid();
                    novoArquivo.ControleDoumentoExternoId = dados.ControleDoumentoExternoId == new Guid() ? null : (Guid?)dados.ControleDoumentoExternoId;
                    novoArquivo.UsuarioIdUpload = AuthenticationManager.User.IdUsuario();
                    novoArquivo.Extensao = Path.GetExtension(Request.Files[i].FileName);
                    novoArquivo.Descricao = Request.Files[i].FileName;
                    novoArquivo.PastaId = GuidUtils.GuidPastaContratosEConvenios();
                    novoArquivo.Temporario = true; //Arquivos com esta propriedade serão excluídos automaticamente por uma rotina de varredura diária no servidor
                    if (ArquivoUtils.SalvarArquivo(Request.Files[i], novoArquivo.ArquivoId))
                    {
                        _arquivoDal.Adicionar(novoArquivo);
                        dados.Arquivos.Add(novoArquivo);
                    }
                    else
                    {
                        msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                    }
                }
                else
                {
                    msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo {Request.Files[i].FileName}. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
                }
            }
            ViewBag.MensagensErrosArquivos = msgErrosArquivos;
            return PartialView("_ListaArquivos", dados);
        }

        public ActionResult RemoverArquivo(ControleDoumentoExterno dados, string guidArquivo)
        {
            if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)) && dados.ControleDoumentoExternoId != new Guid())
            {
                //Foi solicitada a exclusão de um arquivo de uma licitação já cadastrada no BD. Nada será feito agora. Apenas na hora de salvar a portaria, onde será comparado o BD com a lista atual
            }
            else if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)))
            {
                //Arquivo ainda é temporário, poderá ser excluído fisicamente do disco (ainda não existe na base)
                ArquivoUtils.ExcluirArquivo(new Guid(guidArquivo));
            }
            //Remove do array de arquivos que está sendo utilizado pela view
            dados.Arquivos.Remove(dados.Arquivos.FirstOrDefault(a => a.ArquivoId == new Guid(guidArquivo)));
            //Devolve à view  o modelo atualizado (apenas a Partial _ListaArquivos.cshtml será renderizada novamente, e apenas a propriedade Arquivos acessada)
            return PartialView("_ListaArquivos", dados);
        }


        //POST
        [HttpPost]
        public ActionResult Novo(ControleDoumentoExterno dados)
        {
            dados.ETipoStatus = ETipoStatus.EmPrazo;
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                    ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
                    return View(dados);
                }

                var msgErro = "";
                var guid = _controleDocumentoExternoDal.AdicionarEditar(dados, out msgErro);
                if (msgErro != "")
                {
                    MensagemErro = msgErro;
                    return RedirectToAction("Index");
                }

                MensagemSucesso = $"Documento salvo com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                MensagemErro = $"Ocorreu um erro inesperado: ''. Caso o problema persista, entre em contato com a CGE";
                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult Finalizar(string documentoExternoId)
        {
            var documentoGuid = new Guid();
            documentoGuid = new Guid(documentoExternoId);

            ControleDoumentoExterno controleDoumentoExterno = new ControleDoumentoExterno();
            controleDoumentoExterno = _controleDocumentoExternoDal.BuscarPorId(documentoGuid);
            controleDoumentoExterno.DataFinalizacao = DateTime.Now;
            controleDoumentoExterno.ETipoStatus = ETipoStatus.Finalizado;
            _controleDocumentoExternoDal.Update(controleDoumentoExterno);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult ProrrogarPrazo(string documentoExternoIdProrrogado, int diasProrrogado)
        {
            var documentoGuid = new Guid();
            documentoGuid = new Guid(documentoExternoIdProrrogado);

            ControleDoumentoExterno controleDoumentoExterno = new ControleDoumentoExterno();
            controleDoumentoExterno = _controleDocumentoExternoDal.BuscarPorId(documentoGuid);
            controleDoumentoExterno.ProrrogacaoDePrazo = diasProrrogado;
            _controleDocumentoExternoDal.Update(controleDoumentoExterno);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Editar(ControleDoumentoExterno dados)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                    ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
                    return View(dados);
                }

                var msgErro = "";
                var guid = _controleDocumentoExternoDal.AdicionarEditar(dados, out msgErro);
                if (msgErro != "")
                {
                    MensagemErro = msgErro;
                    return RedirectToAction("Index");
                }

                MensagemSucesso = $"Documento salvo com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                MensagemErro = $"Ocorreu um erro inesperado: ''. Caso o problema persista, entre em contato com a CGE";
                return View("Index");
            }
        }

        //JSON

        public JsonResult Consultar(string numeroDocumento, string unidadeGestora)
        {
            var tabela = _controleDocumentoExternoDal.BuscarDocumento(numeroDocumento, unidadeGestora)
                .Select(x => new
                {
                    documentoId = x.ControleDoumentoExternoId,
                    NumeroProcessoDocumento = x.NumeroProcessoDocumento,
                    OrgaoOrigem = x.UnidadeGestora.sigla ?? x.UnidadeGestora.nomeUG,
                    DataAbertura = x.DataOrigem.Date.ToString("d"),
                    DataFinalizacao = x.DataFinalizacao?.ToString("d") ?? "Não Finalizado",
                    ProrrogacaoPrazo = x.ProrrogacaoDePrazo ?? 0,
                    Status = VerificarStatus(x.ETipoStatus, x.Prazo, x.ProrrogacaoDePrazo,
                        CalcularDiasPassado(x.DataOrigem, x.DataFinalizacao, x.ETipoStatus)),
                    Prazo = CalcularDiasPassado(x.DataOrigem, x.DataFinalizacao, x.ETipoStatus) + "/" + x.Prazo,
                    CorLinha = VerificarCorLinha(VerificarStatus(x.ETipoStatus, x.Prazo, x.ProrrogacaoDePrazo,
                        CalcularDiasPassado(x.DataOrigem, x.DataFinalizacao, x.ETipoStatus)))
                }).ToList();


            var grafico = new[]
            {
                new { Descricao = ETipoStatus.EmAtraso.GetDescricao(), Qtd = tabela.Count(x => x.Status == ETipoStatus.EmAtraso.GetDescricao()), color = "red" },
                new { Descricao = ETipoStatus.Expirando.GetDescricao(), Qtd = tabela.Count(x => x.Status == ETipoStatus.Expirando.GetDescricao()), color = "yellow" },
                new { Descricao = ETipoStatus.Prorrogado.GetDescricao(), Qtd = tabela.Count(x => x.Status == ETipoStatus.Prorrogado.GetDescricao()), color = "blue" },
                new { Descricao = ETipoStatus.EmPrazo.GetDescricao(), Qtd = tabela.Count(x => x.Status == ETipoStatus.EmPrazo.GetDescricao()), color = "green" },
                new { Descricao = ETipoStatus.Finalizado.GetDescricao(), Qtd = tabela.Count(x => x.Status == ETipoStatus.Finalizado.GetDescricao()), color = "gray" }
            };

            return Json(new { tabela, grafico }, JsonRequestBehavior.AllowGet);
        }
        //Metodos
        private int CalcularDiasPassado(DateTime dataOrigem, DateTime? dataFinalizacao, ETipoStatus status)
        {
            if (status == ETipoStatus.Finalizado)
            {
                TimeSpan? totalDias = (dataFinalizacao - dataOrigem);
                int dias = totalDias.Value.Days;

                return dias;
            }
            else
            {
                TimeSpan dias = DateTime.Now - dataOrigem;
                return dias.Days;

            }
        }
        private string VerificarStatus(ETipoStatus status, int prazo, int? prorrogacaoDePrazo, int diasPassado)
        {
            if (status == ETipoStatus.Finalizado)
            {
                return ETipoStatus.Finalizado.GetDescricao();
            }
            else
            {
                if (prorrogacaoDePrazo != null)
                {
                    int diasProrrogado = prorrogacaoDePrazo ?? 0;
                    int total = (prazo + diasProrrogado) - diasPassado;
                    if (total < 0)
                    {

                        return ETipoStatus.EmAtraso.GetDescricao();
                    }

                    return ETipoStatus.Prorrogado.GetDescricao();
                }
                else
                {
                    int dias = prazo - diasPassado;
                    if (dias < 0)
                    {
                        return ETipoStatus.EmAtraso.GetDescricao();
                    }
                    else
                    {
                        if (dias <= 5)
                        {
                            return ETipoStatus.Expirando.GetDescricao();
                        }
                    }

                    return ETipoStatus.EmPrazo.GetDescricao();
                }
            }
        }
        private string VerificarCorLinha(string verificarStatus)
        {
            switch (verificarStatus)
            {
                case "Em Atraso":
                    return "fb-danger";
                case "Em Prazo":
                    return "fb-success";
                case "Expirando":
                    return "fb-warning";
                case "Prorrogado":
                    return "fb-primary";
                default:
                    return "";
            }
        }

    }
}