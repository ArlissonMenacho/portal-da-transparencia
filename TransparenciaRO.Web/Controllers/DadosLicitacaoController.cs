﻿using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View.DadosLicitacao;

namespace TransparenciaRO.Web.Controllers
{
    public class DadosLicitacaoController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DadosLicitacaoDAL _dadosLicitacaoDAL;

        public DadosLicitacaoController()
        {
            _dadosLicitacaoDAL = new DadosLicitacaoDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            var indexDadosLicitacaoViewModel = _dadosLicitacaoDAL.GetDataToLoadIndexView();

            return View(indexDadosLicitacaoViewModel);
        }        

        [HttpPost]
        public ActionResult Filtrar(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _dadosLicitacaoDAL.Filter(filtro);

            return PartialView("Partials\\_Tabela", licitacoes);
        }

        #region contratos aditivados

        public ActionResult Aditivados()
        {
            var indexDadosLicitacaoViewModel = _dadosLicitacaoDAL.GetDataToLoadAditivadosView();

            return View(indexDadosLicitacaoViewModel);
        }

        [HttpPost]
        public ActionResult FiltrarAditivados(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _dadosLicitacaoDAL.FilterAditivados(filtro);

            return PartialView("Partials\\_TabelaAditivados", licitacoes);
        }

        #endregion

        #region contratos pendentes

        public ActionResult Pendentes()
        {
            var indexDadosLicitacaoViewModel = _dadosLicitacaoDAL.GetDataToLoadPendentesView();

            return View(indexDadosLicitacaoViewModel);
        }

        [HttpPost]
        public ActionResult FiltrarPendentes(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _dadosLicitacaoDAL.FilterPendentes(filtro);

            return PartialView("Partials\\_TabelaPendentes", licitacoes);
        }
        #endregion
    }
}