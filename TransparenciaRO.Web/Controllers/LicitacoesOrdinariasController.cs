﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL;
using TransparenciaRO.Infra.Model.View.ProcessosAPISUPEL;
using TransparenciaRO.Web.ServiceSEIRO;

namespace TransparenciaRO.Web.Controllers
{
    public class LicitacoesOrdinariasController : ControllerBase
    {
        public async Task<ActionResult> Index()
         {
            try
            {
                var filtro = new FiltroLicitacaoAPISUPELViewModel()
                {
                    EnfrentamentoAoCovid = true,
                    Modalidade = "Pregão Eletrônico",
                    QuantidadeResultados = 15
                };

                var licitacaoAPISUPELVM = new LicitacaoAPISUPELViewModel() { pagina = new Pagina(), itens = new List<ItemLicitacaoAPISUPEL>() };

                var pagina = 1;

                do
                {
                    var urlFiltro = $"http://www.rondonia.ro.gov.br/supel/licitacoes/?" +
                                                $"json=true" +
                                                $"&paged={pagina}" +
                                                $"&modalidade={filtro.Modalidade ?? "" }" +
                                                $"&unidade_administrativa={filtro.UnidadeAdministrativa ?? "" }" +
                                                $"&ano={(filtro.Ano == null ? "" : filtro.Ano.ToString()) }" +
                                                $"&situacao={filtro.Situacao ?? "" }" +
                                                $"&covid={ (filtro.EnfrentamentoAoCovid == true ? "1" : "")}";

                    string dados = await GetJsonAPI(urlFiltro);

                    var dadosDeserializado = JsonConvert.DeserializeObject<LicitacaoAPISUPELViewModel>(dados);

                    licitacaoAPISUPELVM.pagina = dadosDeserializado.pagina;

                    foreach (var item in dadosDeserializado.itens)
                    {
                        if (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados)
                            licitacaoAPISUPELVM.itens.Add(item);
                    }

                    pagina++;

                } while (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados &&
                          licitacaoAPISUPELVM.pagina.total > licitacaoAPISUPELVM.pagina.atual);

                var indexLicitacaoAPISUPELVM = new IndexLicitacaoAPISUPELViewModel { Filtro = new FiltroLicitacaoAPISUPELViewModel(), LicitacaoAPISUPELViewModel = licitacaoAPISUPELVM,};

                indexLicitacaoAPISUPELVM.LicitacaoAPISUPELViewModel.Sucesso = true;

                return View(indexLicitacaoAPISUPELVM);
            }
            catch
            {
                return View(new IndexLicitacaoAPISUPELViewModel() { Filtro = new FiltroLicitacaoAPISUPELViewModel(), LicitacaoAPISUPELViewModel = new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Houve uma falha ao conectar com a fonte de dados, tente novamente mais tarde." } }); ;
            }
        }

        public async Task<ActionResult> Filtrar(FiltroLicitacaoAPISUPELViewModel filtro)
        {
            //paged = Número da página(inteiro);
            //modalidade = (string);
            //unidade_administrativa = (string);
            //ano = (inteiro);
            //situacao = (string);
            //covid = (booleano)[0]não ou[1]sim;

            try
            {
                var licitacaoAPISUPELVM = new LicitacaoAPISUPELViewModel() { pagina = new Pagina(), itens = new List<ItemLicitacaoAPISUPEL>() };

                var pagina = 1;

                do
                {
                    var urlFiltro = $"http://www.rondonia.ro.gov.br/supel/licitacoes/?" +
                                                $"json=true" +
                                                $"&paged={pagina}" +
                                                $"&modalidade={filtro.Modalidade ?? "" }" +
                                                $"&unidade_administrativa={filtro.UnidadeAdministrativa ?? "" }" +
                                                $"&ano={(filtro.Ano == null ? "" : filtro.Ano.ToString()) }" +
                                                $"&situacao={filtro.Situacao ?? "" }" +
                                                $"&covid={ (filtro.EnfrentamentoAoCovid == true ? "1" : "")}";

                    string dados = await GetJsonAPI(urlFiltro);                   

                    var dadosDeserializado = JsonConvert.DeserializeObject<LicitacaoAPISUPELViewModel>(dados);

                    licitacaoAPISUPELVM.pagina = dadosDeserializado.pagina;                   

                    if(dadosDeserializado.itens == null)
                    {
                        return PartialView("Partials\\_Tabela", new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Nenhum resultado encontrado para esta consulta." });
                    }

                    foreach (var item in dadosDeserializado.itens)
                    {
                        if (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados)
                            licitacaoAPISUPELVM.itens.Add(item);
                    }

                    pagina++;

                } while (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados &&
                          licitacaoAPISUPELVM.pagina.total > licitacaoAPISUPELVM.pagina.atual);

                licitacaoAPISUPELVM.Sucesso = true;

                return PartialView("Partials\\_Tabela", licitacaoAPISUPELVM);
            }
            catch
            {
                return PartialView("Partials\\_Tabela", new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Houve uma falha ao conectar com a fonte de dados, tente novamente mais tarde." });
            }

        }

        public async Task<ActionResult> Resultado(string processo)
        {
            try
            {
                string dados = await GetJsonAPI($"http://qa.soluctions.supel.ro.gov.br/cronos/api/processos");

                var jsonResult = JsonConvert.DeserializeObject(dados).ToString();

                var processosAPISUPELVM = JsonConvert.DeserializeObject<ProcessosAPISUPELViewModel>(jsonResult);

                //if(processosAPISUPELVM.erros = "false")

                var processoVM = processosAPISUPELVM.processos.FirstOrDefault(p => p.NumeroProcesso.Replace(".", "").Replace("/", "").Replace("-", "").Contains(processo.Replace(".", "").Replace("/", "").Replace("-", "")));

                return View(new ResultadoViewModel() { Sucesso = true, Processo = processoVM });
            }
            catch
            {
                return View(new ResultadoViewModel() { Sucesso = false });
            }
        }
       
        public ActionResult HistoricoDoProcesso(string processo)
        {
            try
            {
                var serviceSEI = new ServiceSEIRO.SeiService();
                var andamentos = new string[0];                
                var modulos = new string[0];

                //ver tabela de tarefas do SEI.
                //o arquivo está em \Web References\ServiceSEIRO\TarefasSEI.xlsx
                var tarefas = new string[11] { "1", "7", "12", "13", "18", "28", "30", "32", "48", "62", "63" };

                var retornoAndamentos = serviceSEI.listarAndamentos(
                    "PDT",
                    "PortaldaTransparencia",
                    null,
                    processo,
                    "S",
                    andamentos,
                    tarefas,
                    modulos);
                
                if(retornoAndamentos.Length > 0)
                {
                    ViewBag.Sucesso = true;

                    return View(retornoAndamentos);
                }

                ViewBag.Sucesso = false;
                ViewBag.Mensagem = "Nenhum resultado encontrado para este processo";
                return View();

            }
            catch(Exception e)
            {
                ViewBag.Sucesso = false;
                ViewBag.Mensagem = e.Message.Contains("não encontrado") ? e.Message : "Houve uma falha ao conectar com a fonte de dados, tente novamente mais tarde.";
                return View();
            }
        }

        #region métodos privados
        private async Task<string> GetJsonAPI(string path)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return await client.GetStringAsync(path);
        }
        #endregion
    }
}