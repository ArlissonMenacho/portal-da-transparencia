﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using TransparenciaRO.Infra.Utils;
using System.Web.Mvc;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using TransparenciaRO.Infra.Constantes;


namespace TransparenciaRO.Web.Controllers
{
    public class NoticiaController : CRUDControllerBase<AdicionarEditarNoticiaViewModel>
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly IndiceDAL _indiceDal;
        private readonly NoticiaDAL _noticiaDal;

        public NoticiaController()
        {
            _indiceDal = new IndiceDAL(_dbTransparencia);
            _noticiaDal = new NoticiaDAL(_dbTransparencia);
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        [ValidateInput(false)]
        public override ActionResult AdicionarEditar(AdicionarEditarNoticiaViewModel pDados)
        {
            pDados.UsuarioCriadorId = (User as ClaimsPrincipal).IdUsuario();
            var noticiaId = new NoticiaDAL(_dbTransparencia).SalvarDados(pDados.ToDBModel());
            var indice = new Indice
            {
                Agrupamento = Infra.Enum.EAgrupamentoIndice.Noticia,
                IndiceId = noticiaId,
                Href = Url.Action("Visualizar", new {pEncId = noticiaId.ToString("n").ToEncrypted()}),
                Titulo = $"Notícia (data: {DateTime.Now:dd/MM/yyyy}): '{pDados.TituloNoticia}'",
                PalavrasChave = String.Join(" ", pDados.HTMLNoticia.RemoveEntries(ConstantesDiversas.TermosComuns)
                    .Split(' ')
                    .GroupBy(palavra => palavra)
                    .Select(palavra => new {palavra = palavra.FirstOrDefault(), incidencias = palavra.Count()})
                    .OrderByDescending(palavra => palavra.incidencias)
                    .Take(40)
                    .Select(palavra => palavra.palavra))
            };

            indice.PalavrasChave = Regex.Replace(indice.PalavrasChave, "<img\\s[^>]*>(?:\\s*?</img>)?", " ", RegexOptions.IgnoreCase); //Removendo a tag img caso exista
            indice.PalavrasChave = indice.PalavrasChave.Length > 8000
                ? indice.PalavrasChave.Substring(8000)
                : indice.PalavrasChave; //Limitando o tamanho do string em 8000 caracteres

            _indiceDal.SalvarDados(indice);
            return RedirectToAction(nameof(this.Index));
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        private AdicionarEditarNoticiaViewModel ObterPorEncId(string pEncId)
        {
            return (pEncId != null 
                ? _noticiaDal.ObterPorId(new Guid(pEncId.ToDecrypted())).ToAdicionarEditarNoticiaViewModel() 
                : new AdicionarEditarNoticiaViewModel());
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        public override ActionResult AdicionarEditar(string pEncId)
        {
            return View(ObterPorEncId(pEncId));
        }

        public ActionResult Visualizar(string pEncId)
        {
            return View(ObterPorEncId(pEncId));
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        public override ActionResult ConfirmaRemocao(string pEncId)
        {
            throw new NotImplementedException();
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        public override ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        [HttpPost]
        public JsonResult Index(string dataInicial, string dataFinal)
        {
            var dInicial = DateTime.Parse(dataInicial).Date;
            var dFinal = DateTime.Parse(dataFinal).Date.EndOfTheDay();
            return Json(new
            {
                dados = _noticiaDal
                .ObterComUsuarios()
                .Where(n => n.DataCriacao >= dInicial && n.DataCriacao <= dFinal)
                .ToList()
                .Select(n => n.ToListarNoticiasViewModel())
                .OrderByDescending(n => n.DataUltimaAtualizacao)
            });
        }

        [Authorize(Roles = nameof(AdministradorNoticias))]
        public override ActionResult Remover(string pEncId)
        {
            _noticiaDal.Remover(new Guid(pEncId.ToDecrypted()));
            return RedirectToAction(nameof(this.Index));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbTransparencia.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}