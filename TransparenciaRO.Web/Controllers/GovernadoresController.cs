﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Web.Controllers
{
    public class GovernadoresController : Controller
    {
        private readonly RemuneracaoServidorDAL _remuneracaoServidor;

        public GovernadoresController()
        {
            _remuneracaoServidor = new RemuneracaoServidorDAL(new DbTransparencia());
        }

        // GET: GaleriaGovernador
        public ActionResult Index()
        {
            ViewBag.Ano = _remuneracaoServidor.Ano();
            ViewBag.Mes = _remuneracaoServidor.Mes(ViewBag.Ano);
            return View();
        }

    }
}