﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using static System.Convert;

namespace TransparenciaRO.Web.Controllers
{
    public class LegislacaoEspecificaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly PortariaResolucaoDAL _portariaResolucaoDal;
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly ArquivoDAL _arquivoDal;

        public LegislacaoEspecificaController()
        {
            _portariaResolucaoDal = new PortariaResolucaoDAL(_dbTransparencia);
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _arquivoDal = new ArquivoDAL(_dbTransparencia);
            _portariaResolucaoDal = new PortariaResolucaoDAL(_dbTransparencia);
        }

        private void PopularViewBagUnidadesGestoras()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasDictionary()
                .Union(new Dictionary<string, string> { })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        public ActionResult Index()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            //Verifica se possui arquivos temporarios excluindo os mesmos
            _arquivoDal.RemoverTemporario(_arquivoDal.BuscarArquivoTemporarios());

            return View();
        }

        [Authorize(Roles = nameof(AdministradorPortariaResolucaoNormativa))]
        public ActionResult AdicionarEditar(string pEncPortariaResolucao)
        {
            try
            {
                PopularViewBagUnidadesGestoras();
                if (pEncPortariaResolucao == null)
                {
                    return View(new PortariaResolucao());
                }
                using (var dbTransparencia = new DbTransparencia())
                {
                    var portariaResolucao = _portariaResolucaoDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncPortariaResolucao)));
                    if (portariaResolucao == null)
                    {
                        MensagemErro = "Número de legislação informado inválido";
                    }
                    return View(portariaResolucao);
                }
            }
            catch (Exception e)
            {
                MensagemErro = e.Message;
                return View("Index");
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AdicionarEditar(PortariaResolucao dados)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                    PopularViewBagUnidadesGestoras();
                    return View(dados);
                }

                var msgErro = "";
                dados.DataPublicacao = DateTime.Now;
                var guid = _portariaResolucaoDal.AdicionarEditar(dados, out msgErro);
                if (msgErro != "")
                {
                    MensagemErro = msgErro;
                    return RedirectToAction("Index");
                }

                MensagemSucesso = $"Documento salvo com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                MensagemErro = $"Ocorreu um erro inesperado: ''. Caso o problema persista, entre em contato com a CGE";
                return View("Index");
            }
        }

        [Authorize(Roles = nameof(AdministradorPortariaResolucaoNormativa))]
        public ActionResult AdicionarDocumento(ETipoDocumento tipoDoc)
        {
            PopularViewBagUnidadesGestoras();
            ViewBag.PossuiArquivo = false;
            return View("AdicionarEditar", new PortariaResolucao() { ETipoDocumento = tipoDoc });
        }

        public ActionResult AdicionarArquivo(PortariaResolucao dados)
        {
            var msgErrosArquivos = new List<String>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(Request.Files[i]))
                {
                    //Novos arquivos (salva no disco e BD)
                    var novoArquivo = new Arquivo();
                    novoArquivo.ArquivoId = Guid.NewGuid();
                    novoArquivo.PortariaResolucaoId = dados.PortariaResolucaoId == new Guid() ? null : (Guid?)dados.PortariaResolucaoId;
                    novoArquivo.UsuarioIdUpload = AuthenticationManager.User.IdUsuario();
                    novoArquivo.Extensao = Path.GetExtension(Request.Files[i].FileName);
                    novoArquivo.Descricao = Request.Files[i].FileName;
                    novoArquivo.PastaId = GuidUtils.GuidPastaContratosEConvenios();
                    novoArquivo.Temporario = true; //Arquivos com esta propriedade serão excluídos automaticamente por uma rotina de varredura diária no servidor
                    if (ArquivoUtils.SalvarArquivo(Request.Files[0], novoArquivo.ArquivoId))
                    {
                        _arquivoDal.Adicionar(novoArquivo);
                        dados.Arquivos.Add(novoArquivo);
                    }
                    else
                    {
                        msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                    }
                }
                else
                {
                    msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo {Request.Files[i].FileName}. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
                }
            }
            ViewBag.MensagensErrosArquivos = msgErrosArquivos;
            return PartialView("_ListaArquivos", dados);
        }

        public ActionResult RemoverArquivo(PortariaResolucao dados, string guidArquivo)
        {
            if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)) && dados.PortariaResolucaoId != new Guid())
            {
                //Foi solicitada a exclusão de um arquivo de uma licitação já cadastrada no BD. Nada será feito agora. Apenas na hora de salvar a portaria, onde será comparado o BD com a lista atual
            }
            else if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)))
            {
                //Arquivo ainda é temporário, poderá ser excluído fisicamente do disco (ainda não existe na base)
                ArquivoUtils.ExcluirArquivo(new Guid(guidArquivo));
            }
            //Remove do array de arquivos que está sendo utilizado pela view
            dados.Arquivos.Remove(dados.Arquivos.FirstOrDefault(a => a.ArquivoId == new Guid(guidArquivo)));
            //Devolve à view  o modelo atualizado (apenas a Partial _ListaArquivos.cshtml será renderizada novamente, e apenas a propriedade Arquivos acessada)
            return PartialView("_ListaArquivos", dados);
        }

        //Json
        public JsonResult Lotacoes()
        {
            var lotacoes = _unidadeGestoraDal.UnidadesGestorasDictionary().Select(l => new
            {
                Id = l.Key.ToUpper(),
                Descricao = l.Value
            });

            return Json(lotacoes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Index(string dataInicial, string dataFinal, Guid? ug, int? numeroDocumento, string tipoDocumento)
        {
            var tipoDoc = (ETipoDocumento)ToInt32(tipoDocumento);
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date.AddDays(1);
            var dados = _portariaResolucaoDal.BuscarPortariaResolucao(dtInicial, dtFinal, ug, numeroDocumento, tipoDoc);

            return Json(dados.Select(d => new
            {
                TipoDocumento = d.ETipoDocumento.GetDescricao(),
                OrgaoSigla = d.UnidadeGestora.sigla,
                Orgao = d.UnidadeGestora.nomeUG,
                Assunto = d.Assunto,
                DataDocumento = d.Data.ToString("dd MMMM yyyy"),
                LinkParaEdicao = this.Url.Action("AdicionarEditar", new { pEncPortariaResolucao = d.PortariaResolucaoId.ToString().ToEncrypted() }),
                NumeroDocumento = d.Numero,
                Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new
                {
                    LinkArquivo = Url.Action("VisualizarArquivo", "Arquivo", new { pEncArquivoId = a.ArquivoId.ToString().ToEncrypted() }),
                    NomeArquivo = a.Descricao + "." + a.Extensao
                })
            }), JsonRequestBehavior.AllowGet);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}