﻿using System;
using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    [Authorize(Roles = nameof(VisualizadorLog))]
    public class LogController : Controller
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly LogDAL _logDal;


        public LogController()
        {
            _logDal = new LogDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult Index(string dataInicial, string dataFinal)
        {
            var dInicial = DateTime.Parse(dataInicial).Date;
            var dFinal = DateTime.Parse(dataFinal).Date.EndOfTheDay();

            return Json(new
            {
                dados = _logDal.ObterPorPeriodo(dInicial, dFinal).OrderByDescending(l => l.DataHoraEvento).Select(l => new
                {
                    DataHoraEvento = l.DataHoraEvento.ToString("dd/MM/yyyy HH:mm:ss"),
                    l.Descricao,
                    Usuario = l.Usuario != null ? l.Usuario.Nome : "N/D"
                })
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}