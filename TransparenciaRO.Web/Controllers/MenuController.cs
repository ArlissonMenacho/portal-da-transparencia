﻿using CaptchaMvc.HtmlHelpers;
using reCAPTCHA.MVC;
using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using TransparenciaRO.Business;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;
using static System.Configuration.ConfigurationManager;

namespace TransparenciaRO.Web.Controllers
{
    public class MenuController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ContatoInstitucionalDAL _contatoInstitucionalDal;

        public MenuController()
        {
            _contatoInstitucionalDal = new ContatoInstitucionalDAL(_dbTransparencia);
        }

        public ActionResult Institucional()
        {
            var orgaos = new TbPllOrgaoSiafemBusiness().getOrgaos();
            return View(orgaos);
        }

        public ActionResult OPortal()
        {
            return View();
        }

        public ActionResult Legislacao()
        {
            return View();
        }

        public ActionResult InformacoesGerais()
        {
            return View();
        }

        public ActionResult PerguntasFrequentes()
        {
            return View();
        }

        public PartialViewResult FaleConosco()
        {
            return PartialView();
        }

        public ActionResult Acessibilidade()
        {
            return View();
        }

        [CaptchaValidator, HttpPost, ValidateAntiForgeryToken]
        public ActionResult FaleConosco(FaleConoscoViewModel faleConosco, bool captchaValid)
        {
            if (this.IsCaptchaValid("Captcha is not valid"))
            {

                var de = new MailAddress(AppSettings["mailContatoTo"], "Controladoria Geral do Estado / CGE");
                var para = new MailAddress(faleConosco.Email, faleConosco.Nome);
                var resposta = new MailMessage(de, para)
                {
                    Subject = "Contato",
                    Body = @"Prezado cidadão, agradecemos o seu contato, e caso for necessário, entraremos em contato em breve."
                };

                var msn = $"Nome: {faleConosco.Nome} \nEmail: {faleConosco.Email} \nFone: {faleConosco.Telefone} \nAssunto: {faleConosco.Assunto} \nMensagem: {faleConosco.Mensagem}";

                MailUtils.SendMail(msn);

                var smtp = new SmtpClient();
                smtp.Send(resposta);


                MensagemSucesso = "E-mail enviado com sucesso";
                return RedirectToAction("Index", "Home");

            }
            MensagemErro = "reCAPTCHA inválido";
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ContatoInstitucional()
        {
            return View(_contatoInstitucionalDal.BuscarInstituicoes());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}