﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class RemuneracaoEmpresaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly RemuneracaoEmpresaDAL _remuneracaoEmpresaDal;
        private readonly RemuneracaoSophDAL _remuneracaoSophDal;
        private readonly EmpresaRemuneracaoDAL _empresaRemuneracao;
        private int cont = 1;

        public RemuneracaoEmpresaController()
        {
            _remuneracaoEmpresaDal = new RemuneracaoEmpresaDAL(_dbTransparencia);
            _remuneracaoSophDal = new RemuneracaoSophDAL(_dbTransparencia);
            _empresaRemuneracao = new EmpresaRemuneracaoDAL(_dbTransparencia);
        }

        //Actions
        [Authorize(Roles = nameof(AdministradorRemuneracaoServidoresEmpresaSoph) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCaerd) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCmr) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaEmater))]
        public ActionResult ImportarDados(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file, EEmpresa empresa, int ano, int pMes)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    CapturarDadosDoExcelESalvarNoBanco(empresa, ano, pMes, path);

                    LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados RPV de fornecedor:", AuthenticationManager.User.IdUsuario());

                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    ViewBag.Empresa = empresa.ToString().ToUpper();
                    return RedirectToAction("ImportarDados", "RemuneracaoEmpresa", new { empresa });
                }
                ViewBag.Empresa = empresa.ToString().ToUpper();
                MensagemErro = "Arquivo inválido";
                return View();
            }
            catch
            {
                MensagemErro = "Ocorreu um erro na linha " + cont + ", caso o erro persista entre em contato com o suporte: transparencia.cge.ro@gmail.com";
                ViewBag.Empresa = empresa.ToString().ToUpper();
                return RedirectToAction("ImportarDados", "RemuneracaoEmpresa", new { empresa });
            }
        }

        public ActionResult Grafico(EEmpresa empresa)
        {
            if (_empresaRemuneracao.AnoMax(empresa) == 0)
            {
                if (empresa == EEmpresa.Soph)
                {
                    ViewBag.AnoMax = _remuneracaoSophDal.AnoMax(empresa);
                    ViewBag.AnoMin = _remuneracaoSophDal.AnoMin(empresa);
                }
                else
                {
                    ViewBag.AnoMax = _remuneracaoEmpresaDal.AnoMax(empresa);
                    ViewBag.AnoMin = _remuneracaoEmpresaDal.AnoMin(empresa);
                }
            }
            else
            {
                ViewBag.AnoMax = _empresaRemuneracao.AnoMax(empresa);


                int anoMin = _empresaRemuneracao.AnoMin(empresa);
                if (empresa == EEmpresa.Soph)
                {
                    if (anoMin > _remuneracaoSophDal.AnoMin(empresa))
                    {
                        anoMin = _remuneracaoSophDal.AnoMin(empresa);
                    }
                }
                else
                {
                    if (anoMin > _remuneracaoEmpresaDal.AnoMin(empresa))
                    {
                        anoMin = _remuneracaoEmpresaDal.AnoMin(empresa);
                    }
                }

                ViewBag.AnoMin = anoMin;

            }

            ViewBag.Empresa = empresa.ToString().ToUpper();


            return View();
        }

        //Json
        public JsonResult Meses(int ano, EEmpresa empresa)
        {
            object retorno = null;
            if (!_empresaRemuneracao.Meses(ano, empresa).Any())
            {
                if (empresa == EEmpresa.Soph)
                {

                    var buscarMeses = _remuneracaoSophDal.Meses(ano, empresa);
                    foreach (var i in _remuneracaoEmpresaDal.Meses(ano, empresa))
                    {
                        buscarMeses.Add(i);
                    }


                    retorno = buscarMeses.Select(m => new
                    {
                        MesInt = m,
                        MesString = m.NomeMes()
                    }).OrderByDescending(m => m.MesInt);
                }
                else
                {
                    retorno = _remuneracaoEmpresaDal.Meses(ano, empresa).Select(m => new
                    {
                        MesInt = m,
                        MesString = m.NomeMes()
                    }).OrderByDescending(m => m.MesInt);
                }
            }
            else
            {
                retorno = _empresaRemuneracao.Meses(ano, empresa).Select(m => new
                {
                    MesInt = m,
                    MesString = m.NomeMes()
                }).OrderByDescending(m => m.MesInt);
            }

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoCargo(int ano, int mes, EEmpresa empresa, string tipoCargo)
        {
            object retorno = null;
            if (!_empresaRemuneracao.RelacaoCargo(ano, mes, empresa, tipoCargo).Any())
            {

                if (empresa == EEmpresa.Soph && ano == 2017 && mes >= 10 || empresa == EEmpresa.Soph && ano >= 2018)
                {
                    retorno = _remuneracaoSophDal.RelacaoCargo(ano, mes, empresa, tipoCargo).Select(r => new
                    {
                        Cargo = r.Key.Trim(),
                        QtdServidores = r.Value
                    }).OrderByDescending(r => r.QtdServidores);
                }
                else
                {
                    retorno = _remuneracaoEmpresaDal.RelacaoCargo(ano, mes, empresa, tipoCargo).Select(r => new
                    {
                        Cargo = r.Key.Trim(),
                        QtdServidores = r.Value
                    }).OrderByDescending(r => r.QtdServidores);
                }
            }
            else
            {
                retorno = _empresaRemuneracao.RelacaoCargo(ano, mes, empresa, tipoCargo).Select(r => new
                {
                    Cargo = r.Key.Trim(),
                    QtdServidores = r.Value
                }).OrderByDescending(r => r.QtdServidores);
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoDepartamento(int ano, int mes, EEmpresa empresa)
        {
            object retorno = null;
            if (!_empresaRemuneracao.RelacaoDepartamento(ano, mes, empresa).Any())
            {
                if (empresa == EEmpresa.Soph && ano == 2017 && mes >= 10 || empresa == EEmpresa.Soph && ano >= 2018)
                {
                    retorno = _remuneracaoSophDal.RelacaoDepartamento(ano, mes, empresa).Select(r => new
                    {
                        Classificacao = r.Key.Trim(),
                        QtdServidores = r.Value
                    }).OrderByDescending(r => r.QtdServidores);
                }
                else
                {
                    retorno = _remuneracaoEmpresaDal.RelacaoDepartamento(ano, mes, empresa).Select(r => new
                    {
                        Classificacao = r.Key.Trim(),
                        QtdServidores = r.Value
                    }).OrderByDescending(r => r.QtdServidores);
                }
            }
            else
            {
                retorno = _empresaRemuneracao.RelacaoDepartamento(ano, mes, empresa).Select(r => new
                {
                    Classificacao = r.Key.Trim(),
                    QtdServidores = r.Value
                }).OrderByDescending(r => r.QtdServidores);

            }

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoTipoCargo(int ano, int mes, EEmpresa empresa)
        {
            object retorno = null;
            if (!_empresaRemuneracao.RelacaoTipoCargo(ano, mes, empresa).Any())
            {
                retorno = _remuneracaoEmpresaDal.RelacaoTipoCargo(ano, mes, empresa).Select(r => new
                {
                    TipoCargo = r.Key.Trim(),
                    QtdPorCargo = r.Value
                }).OrderByDescending(r => r.QtdPorCargo);
            }
            else
            {
                retorno = _empresaRemuneracao.RelacaoTipoCargo(ano, mes, empresa).Select(r => new
                {
                    TipoCargo = r.Key.Trim(),
                    QtdPorCargo = r.Value
                }).OrderByDescending(r => r.QtdPorCargo);
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FaixasSalariais(int ano, int mes, EEmpresa empresa)
        {
            object faixasSalariais = null;

            if (!_empresaRemuneracao.FaixasSalariais(ano, mes, empresa).Any())
            {
                if (empresa == EEmpresa.Soph && ano == 2017 && mes >= 10 || empresa == EEmpresa.Soph && ano >= 2018)
                {
                    faixasSalariais = _remuneracaoSophDal.FaixasSalariais(ano, mes, empresa).Select(f => new
                    {
                        FaixaSalarial = f.Key,
                        QtdServidores = f.Value
                    })
                        .OrderBy(fs => fs.FaixaSalarial == "Acima de 16 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 8 até 16 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 4 até 8 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 2 até 4 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 1 até 2 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "Até 1 Salário Mínimo Federal");
                }
                else
                {
                    faixasSalariais = _remuneracaoEmpresaDal.FaixasSalariais(ano, mes, empresa).Select(f => new
                    {
                        FaixaSalarial = f.Key,
                        QtdServidores = f.Value
                    })
                        .OrderBy(fs => fs.FaixaSalarial == "Acima de 16 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 8 até 16 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 4 até 8 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 2 até 4 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "De 1 até 2 Salários Mínimos Federais")
                        .ThenBy(fs => fs.FaixaSalarial == "Até 1 Salário Mínimo Federal");
                }
            }
            else
            {
                faixasSalariais = _empresaRemuneracao.FaixasSalariais(ano, mes, empresa).Select(f => new
                {
                    FaixaSalarial = f.Key,
                    QtdServidores = f.Value
                })
                    .OrderBy(fs => fs.FaixaSalarial == "Acima de 16 Salários Mínimos Federais")
                    .ThenBy(fs => fs.FaixaSalarial == "De 8 até 16 Salários Mínimos Federais")
                    .ThenBy(fs => fs.FaixaSalarial == "De 4 até 8 Salários Mínimos Federais")
                    .ThenBy(fs => fs.FaixaSalarial == "De 2 até 4 Salários Mínimos Federais")
                    .ThenBy(fs => fs.FaixaSalarial == "De 1 até 2 Salários Mínimos Federais")
                    .ThenBy(fs => fs.FaixaSalarial == "Até 1 Salário Mínimo Federal");
            }
            return Json(faixasSalariais, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Funcionarios(int ano, int mes, EEmpresa empresa, string cargo, string tipoCargo, string departamento, string faixaSalarial)
        {
            object retorno = null;
            if (!_empresaRemuneracao.Funcionarios(ano, mes, empresa, cargo, tipoCargo, departamento, faixaSalarial)
                .Any())
            {
                if (empresa == EEmpresa.Soph && ano == 2017 && mes >= 10 || empresa == EEmpresa.Soph && ano >= 2018)
                {
                    retorno = _remuneracaoSophDal.Funcionarios(ano, mes, empresa, cargo, departamento, faixaSalarial)
                        .Select(f => new
                        {
                            NomeEmpregado = f.Nome,
                            Cargo = f.Cargo,
                            Cpf = f.Cpf,
                            Departamento = f.Departamento,
                            TotalProvento = f.TotalProventos,
                            TotalDesconto = f.TotalDescontos,
                            Salario = f.Salario,
                            OutroProvento = f.OutrosProventos,
                            INSS = f.Inss,
                            IRRF = f.Irrf,
                            FGTS = f.Fgts,
                            OutroDesconto = f.OutrosDescontos,
                            TotalLiquido = f.TotalLiquido
                        }).OrderByDescending(f => f.TotalProvento);
                }
                else
                {
                    retorno = _remuneracaoEmpresaDal
                        .Funcionarios(ano, mes, empresa, cargo, tipoCargo, departamento, faixaSalarial).Select(f => new
                        {
                            NomeEmpregado = f.NomeEmpregado,
                            Cargo = f.Cargo,
                            Cpf = f.Cpf,
                            Departamento = f.Departamento,
                            TipoCargo = f.TipoCargo,
                            TotalProvento = f.TotalProvento,
                            TotalDesconto = f.TotalDesconto,
                            TotalLiquido = f.TotalLiquido
                        }).OrderByDescending(f => f.TotalProvento);
                }
            }
            else
            {
                retorno = _empresaRemuneracao.Funcionarios(ano, mes, empresa, cargo, tipoCargo, departamento, faixaSalarial).Select(f => new
                {
                    NomeEmpregado = f.Nome,
                    Cargo = f.Cargo,
                    Cpf = f.Cpf,
                    Departamento = f.Departamento,
                    TotalProvento = f.TotalProventos,
                    Salario = f.Salario,
                    OutroProvento = f.OutrosProventos,
                    TotalLiquido = f.TotalLiquido,
                    Ano = f.Ano,
                    Mes = f.Mes,
                    Matricula = f.Matricula
                }).OrderByDescending(f => f.TotalProvento);
            }

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetalheServidor(int ano, int mes, string matricula)
        {
            return View(_empresaRemuneracao.RemuneracaoServidor(ano, mes, matricula));
        }

        //Metodos 
        private void CapturarDadosDoExcelESalvarNoBanco(EEmpresa empresa, int ano, int pMes, string path)
        {
            //Rotina de importação dos dados
            using (var db =
                new OleDbConnection(
                    $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';")
            )
            {
                db.Open();
                var remuneracoesEmpresa = new List<EmpresaRemuneracao>();

                var dtSchema =
                    db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })
                        ?.Rows[0]["TABLE_NAME"].ToString();
                using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                {
                    var dr = cmd.ExecuteReader();
                    while (dr != null && dr.Read())
                    {
                        cont++;
                        var empresaRemuneracao = new EmpresaRemuneracao()
                        {
                            Nome = dr.GetValue(dr.GetOrdinal("NomeServidor")).ToString(),
                            Cpf = dr.GetValue(dr.GetOrdinal("CPF")).ToString(),
                            Matricula = dr.GetValue(dr.GetOrdinal("Matricula")).ToString(),
                            PisPasep = dr.GetValue(dr.GetOrdinal("PisPasep")).ToString(),
                            DataNascimento = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataNascimento")).ToString()),
                            Sexo = dr.GetValue(dr.GetOrdinal("Sexo")).ToString(),
                            EstadoCivil = dr.GetValue(dr.GetOrdinal("EstadoCivil")).ToString(),
                            Cargo = dr.GetValue(dr.GetOrdinal("Cargo")).ToString(),
                            RequisitoCargo = dr.GetValue(dr.GetOrdinal("RequisitoCargo")).ToString(),
                            Departamento = dr.GetValue(dr.GetOrdinal("Departamento")).ToString(),
                            Admissao = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("Admissao")).ToString()),
                            Demissao = dr.GetValue(dr.GetOrdinal("Demissao")) == null
                                ? Convert.ToDateTime(dr.GetString(dr.GetOrdinal("Demissao")))
                                : new DateTime?(),
                            TipoJornada = dr.GetValue(dr.GetOrdinal("TipoJornada")).ToString(),
                            CargaHoraria = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("CargaHoraria")).ToString()),
                            CategoriaSituacao = dr.GetValue(dr.GetOrdinal("CategoriaSituacao")).ToString(),
                            FormaComissao = dr.GetValue(dr.GetOrdinal("FormaComissao")).ToString(),
                            TipoCargo = dr.GetValue(dr.GetOrdinal("TipoCargo")).ToString(),
                            Salario = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Salario")).ToString()),
                            TotalProventos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("TotalProvento")).ToString()),
                            OutrosProventos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("OutroProvento")).ToString()),
                            Inss = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("INSS")).ToString()),
                            Irrf = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("IRRF")).ToString()),
                            Fgts = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("FGTS")).ToString()),
                            TotalDescontos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("TotalDesconto")).ToString()),
                            OutrosDescontos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("OutroDesconto")).ToString()),
                            TotalLiquido = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("TotalLiquido")).ToString()),
                            EEmpresa = empresa,
                            Mes = pMes,
                            Ano = ano
                        };
                        remuneracoesEmpresa.Add(empresaRemuneracao);
                    }
                    _empresaRemuneracao.VerficarRemoverDados(ano, pMes, empresa);
                    _empresaRemuneracao.AdicionarDados(remuneracoesEmpresa);
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}
