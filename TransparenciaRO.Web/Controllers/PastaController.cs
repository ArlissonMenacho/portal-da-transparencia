﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Enum;
using System.Linq;

namespace TransparenciaRO.Web.Controllers
{
    public class PastaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly PastaDAL _pastaDal;

        public PastaController()
        {
            _pastaDal = new PastaDAL(_dbTransparencia);
        }

        public ActionResult Index(string pEncPastaId)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(pEncPastaId)) return RedirectToAction("Index", "Home");
                var pastaGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                var pastaBD = _pastaDal.BuscarPastaComSubPastasEArquivos(pastaGuid);
                var hierarquia = _pastaDal.GetHierarquiaPastas(pastaGuid);
                
                if (pastaBD.Publica)
                {
                    ViewBag.PastaGuid = pastaGuid.ToString();
                    /*
                     * Usuário somente poderá alterar dados da pasta se as duas condições forem verdadeiras:
                     * 1) Possui Role de administrador de pastas
                     * 2) Possui acesso à pasta atual ou à uma pasta ancestral
                    */
                    ViewBag.Administravel = GetPermissoesUsuario().Contains(EPerfilUsuario.AdministradorPastas) && // Condição 1
                        GetPastasUsuario().Any(pu => hierarquia.Any(h => h.PastaId == pu)); // Condição 2
                    return View("IndexPasta", pastaBD);
                }
                else
                {
                    //Pasta não é pública, não pode ser visualizada, nem gerenciada
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult Adicionar(string pEncPastaId)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                if (pEncPastaId == null) return View();

                var pastaOrigemGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                var pasta = new Pasta { PastaOrigemId = pastaOrigemGuid };

                return View(pasta);
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult Adicionar(HttpPostedFileBase imagem, Pasta pasta)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                if (!ModelState.IsValid) return View(pasta);

                pasta.Administravel = false;
                pasta.Publica = true;

                if (imagem != null)
                {
                    var tipoArquivo = Path.GetExtension(imagem.FileName);
                    if (tipoArquivo == ".gif" || tipoArquivo == ".png" || tipoArquivo == ".jpg" || tipoArquivo == ".svg")
                    {
                        pasta.TipoImagem = tipoArquivo;
                        pasta.IconeImagem = ImageToByteArray(imagem);
                        _pastaDal.Adicionar(pasta);
                        TempData["Sucesso"] = "Pasta adicionado com sucesso";
                        return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pasta.PastaOrigemId.ToString()) });
                    }

                    TempData["Erro"] = "O Icone está em um formato inválido, os formatos aceitos são GIF, PNG, JPG";
                    return View(pasta);
                }

                _pastaDal.Adicionar(pasta);
                TempData["Sucesso"] = "Pasta adicionado com sucesso";
                return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pasta.PastaOrigemId.ToString()) });
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult Editar(string pEncPastaId)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                if (pEncPastaId != null)
                {
                    var pastaGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                    var pasta = _pastaDal.BuscarPasta(pastaGuid);
                    if (pasta != null) return View(pasta);

                    TempData["Erro"] = "Pasta não encontrada";
                    return RedirectToAction("Index", _pastaDal.BuscarPastas());
                }

                TempData["Erro"] = "Pasta não encontrada";
                return RedirectToAction("Index", _pastaDal.BuscarPastas());
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult Editar(HttpPostedFileBase imagem, Pasta pasta)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                if (!ModelState.IsValid) return View(pasta);

                pasta.Administravel = false;
                pasta.Publica = true;
                if (imagem != null)
                {
                    var tipoArquivo = Path.GetExtension(imagem.FileName);
                    if (tipoArquivo == ".gif" || tipoArquivo == ".png" || tipoArquivo == ".jpg" || tipoArquivo == ".svg")
                    {
                        pasta.TipoImagem = tipoArquivo;
                        pasta.IconeImagem = ImageToByteArray(imagem);
                        _pastaDal.Editar(pasta);
                        TempData["Sucesso"] = "Pasta editado com sucesso";
                        return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pasta.PastaOrigemId.ToString()) });
                    }

                    TempData["Erro"] = "O Icone está em um formato inválido, os formatos aceitos são GIF, PNG, JPG";
                    return View(pasta);
                }

                _pastaDal.Editar(pasta);
                TempData["Sucesso"] = "Pasta editado com sucesso";
                return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pasta.PastaOrigemId.ToString()) });
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult Remover(string pEncPastaId)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                if (pEncPastaId != null)
                {
                    var pastaGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                    var pasta = _pastaDal.BuscarPasta(pastaGuid);

                    if (pasta == null)
                    {
                        TempData["Erro"] = "Pasta não encontrada";
                        return RedirectToAction("Index", _pastaDal.BuscarPastas());
                    }

                    return View(pasta);
                }

                TempData["Erro"] = "Pasta não encontrada";
                return RedirectToAction("Index", _pastaDal.BuscarPastas());
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost, ValidateAntiForgeryToken, ActionName("Remover")]
        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorPastas))]
        public ActionResult ConfirmaRemocao(string pEncPastaId)
        {
            try
            {
                //TODO: Verificar se usuário possui permissão para a pasta na hierarquia
                Guid pastaGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                var pastaOrigem = _pastaDal.BuscarPasta(pastaGuid).PastaOrigemId ?? null;

                if (_pastaDal.Remover(pastaGuid))
                {
                    TempData["Sucesso"] = "Pasta removida com sucesso";
                    return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pastaOrigem.ToString()) });
                }

                TempData["Erro"] = "A pasta contem arquivos e não pode ser removida";
                return RedirectToAction("Index", new { pEncPastaId = CriptografiaUtils.Encrypt(pastaOrigem.ToString()) });
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        public ActionResult Breadcrumbs(string pEncPastaId)
        {
            try
            {
                var pastaGuid = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                IEnumerable<Pasta> pastas = new List<Pasta>();
                pastas = _pastaDal.GetHierarquiaPastas(pastaGuid);
                return PartialView("_Breadcrumbs", pastas);
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return View("Index", _pastaDal.BuscarPastas());
            }
        }

        protected byte[] ImageToByteArray(HttpPostedFileBase imagem)
        {
            var retorno = new MemoryStream();
            imagem.InputStream.CopyTo(retorno);
            return retorno.ToArray();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();
            
            base.Dispose(disposing);
        }
    }
}