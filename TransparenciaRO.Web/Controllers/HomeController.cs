﻿using System;
using System.Web.Mvc;
using System.Linq;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using TransparenciaRO.Infra.Constantes;
using System.Runtime.InteropServices.WindowsRuntime;

namespace TransparenciaRO.Web.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly NoticiaDAL _noticiaDal;
        private readonly PastaDAL _pastaDal;
        private readonly UsuarioDAL _usuarioDal;
        private readonly EmailNotificacaoDAL _emailNotificacaoDAL;

        public HomeController()
        {
            _noticiaDal = new NoticiaDAL(_dbTransparencia);
            _pastaDal = new PastaDAL(_dbTransparencia);
            _usuarioDal = new UsuarioDAL(_dbTransparencia);
            _emailNotificacaoDAL = new EmailNotificacaoDAL(_dbTransparencia);
        }

        public ActionResult Index(string pEncPastaId)
        {
            var url = HttpContext.Request.Url.AbsoluteUri;

            if (url.Contains("comprasemergenciais-covid19"))
                return RedirectToAction(nameof(CovidCombate));

            ViewBag.PastaId = pEncPastaId.IsNullOrEmpty() ? new Guid() : new Guid(pEncPastaId.ToDecrypted());

            ViewBag.PaginaInicial = true;
            ViewBag.Noticias = _noticiaDal
                .ObterComUsuarios()
                .OrderByDescending(n => n.DataUltimaAtualizacao)
                .Where(n => n.Publicado)
                .Take(3)
                .ToList()
                .Select(n => n.ToListarNoticiasViewModel())
                .ToList();

            var x = _noticiaDal.GetNoticiaPoPup().ToListarNoticiasViewModel();
            ViewBag.NoticiaPopUp = x;
            ViewBag.MapaSite = _pastaDal.DetalharPasta(GuidUtils.GuidMenuPrincipal());

            return View();
        }

        public ActionResult NotificarEmail()
        {
            var emailNotificacao = new EmailNotificacao();

            return View(emailNotificacao);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult NotificarEmail(EmailNotificacao emailNotificacao)
        {
            if (ModelState.IsValid)
            {
                _emailNotificacaoDAL.Save(emailNotificacao);

                var message = $"Olá, cidadão!<br /></br />" +
                    $"Obrigado por se cadastrar na TRANSPARÊNCIA PROATIVA, uma ação vinculada ao " +
                    $"Programa Rondoniense de Fortalecimento ao Controle Social - PROFOCOS.<br /><br />" +
                    $"A partir de agora você receberá, em tempo real, informações sobre as " +
                    $"despesas governamentais destinadas ao enfrentamento da COVID-19 no Estado.<br /><br/>" +
                    $"A Cidadania e o Controle Social não estão de quarentena!<br /><br />" +
                    $"Conheça mais sobre o PROFOCOS <a href='http://comprasemergenciais-covid19.ro.gov.br/Arq/PROFOCOS_CGE-RO.pdf'>clicando aqui<a/>. " +
                    $"Ou, se preferir, basta copiar o link abaixo em uma nova aba: <br/>" +
                    $"http://comprasemergenciais-covid19.ro.gov.br/Arq/PROFOCOS_CGE-RO.pdf";

                var subject = $"Portal da Transparência | Seja bem-vindo!";

                MailUtils.SendMail(message, subject, true, emailNotificacao.Email);

                MensagemSucesso = "E-mail cadastrado com sucesso!";

                return View();
            }

            return View(emailNotificacao);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult NotificarEmailModal(EmailNotificacao emailNotificacao)
        {
            try
            {
                bool possuiAlgumCheckBoxSelecionado = emailNotificacao.NotificarBoletim == true ||
                                                      emailNotificacao.NotificarIntegraProcesso == true ||
                                                      emailNotificacao.NotificarDispensaLicitacao ?
                                                      true : false;

                if (ModelState.IsValid && possuiAlgumCheckBoxSelecionado)
                {
                    _emailNotificacaoDAL.Save(emailNotificacao);

                    var message = $"Olá, cidadão!<br /><br />" +
                        $"Obrigado por se cadastrar na TRANSPARÊNCIA PROATIVA, uma ação vinculada ao " +
                        $"Programa Rondoniense de Fortalecimento ao Controle Social - PROFOCOS.<br /><br />" +
                        $"A partir de agora você receberá, em tempo real, informações sobre as " +
                        $"despesas governamentais destinadas ao enfrentamento da COVID-19 no Estado.<br /><br/>" +
                        $"A Cidadania e o Controle Social não estão de quarentena!<br /><br />" +
                        $"Conheça mais sobre o PROFOCOS <a href='http://comprasemergenciais-covid19.ro.gov.br/Arq/PROFOCOS_CGE-RO.pdf'>clicando aqui<a/>." +
                        $"Ou se preferir, basta copiar o link abaixo em uma nova aba: <br/>" +
                        $"http://comprasemergenciais-covid19.ro.gov.br/Arq/PROFOCOS_CGE-RO.pdf";

                    var subject = $"Portal da Transparência | Seja bem-vindo!";

                    MailUtils.SendMail(message, subject, true, emailNotificacao.Email);

                    return Json(new { sucesso = true, mensagem = "E-mail cadastrado com sucesso!" });
                }

                return Json(new { sucesso = false, mensagem = !possuiAlgumCheckBoxSelecionado ? "Por favor, selecione pelo menos algum tipo de notificação que deseja receber." : "Por favor, preencha todos os campos." });
            }
            catch (Exception)
            {
                return Json(new { sucesso = false, mensagem = "Houve algum problema ao realizar o cadastro." });
            }
        }

        public ActionResult DesabilitarNotificacaoPorEmail(Guid emailId)
        {
            _emailNotificacaoDAL.DisableNotificationsById(emailId);

            MensagemSucesso = "Notificações desativadas com sucesso!";

            return RedirectToAction(nameof(CovidCombate));
        }

        public ActionResult Login(string pMensagem)
        {
            if (pMensagem != "")
                MensagemErro = pMensagem;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return View();
        }

        public ActionResult RecuperarSenha()
        {

            return View();
        }

        [HttpPost]
        public ActionResult RecuperarSenha(UsuarioRecuperarSenha pModel)
        {
            long cpf = 0;
            if (!Int64.TryParse(pModel.CPF.Replace("-", "").Replace(".", ""), out cpf))
            {
                MensagemErro = "Por favor, informe um CPF válido";
                return View(pModel);
            }

            var dadosUsuario = _usuarioDal.ObterPorCpf(cpf);
            if (dadosUsuario == null)
            {
                MensagemErro = "CPF inválido ou usuário inativo";
                return View(pModel);
            }

            var senhaTemporaria = "123456"; //StringUtils.GenerateRandomString(6);
            _usuarioDal.AtualizarSenha(dadosUsuario.UsuarioId, senhaTemporaria, true);

            MailUtils.SendMail($"Prezado usuário {dadosUsuario.Nome}, segue abaixo seus dados para <a href='http://transparencia.ro.gov.br/Home/Login'>acesso</a>:<hr />Usuário: {dadosUsuario.CPF.ToString("00000000000")}<br />Senha: {senhaTemporaria}<hr />Governo do estado de Rondônia", "Dados para acesso ao portal de transparência RO", true, dadosUsuario.Email);
            MensagemSucesso = "Uma senha temporária foi enviado ao seu email com sucesso!";
            return RedirectToAction(nameof(this.Login));
        }

        [Authorize]
        public ActionResult AreaAdministrativa()
        {
            return View();
        }

        #region COVID-19

        public ActionResult CovidCombate()
        {
            return View();
        }
        public ActionResult DespesasPowerBi()
        {
            return View();
        }
        public ActionResult Boletim()
        {
            return View();
        }
        public ActionResult LegislacaoCovid()
        {
            return View();
        }
        public ActionResult AcordosCooperacao()
        {
            return View();
        }

        public ActionResult MedidasEconomicas()
        {
            return View();
        }

        #endregion

        [Authorize]
        public PartialViewResult Permissoes()
        {
            var permissoes = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
            return PartialView("_Permissoes", _pastaDal.BuscarPastasAcessoRestrito(permissoes).ToList());
        }

        public PartialViewResult PastasUsuario()
        {
            var guids = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimsCustomizadas.Pastas).Select(c => c.Value).ToList();
            return PartialView("_PastasUsuario", _pastaDal.BuscarPastasDoUsuario(guids).ToList());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel pModel)
        {
            if (ModelState.IsValid)
            {
                long cpf = 0;
                if (!Int64.TryParse(pModel.CPF.Replace("-", "").Replace(".", ""), out cpf))
                {
                    MensagemErro = "Por favor, informe um CPF válido";
                    return View(pModel);
                }

                var dadosUsuario = _usuarioDal.ObterPorCpf(cpf);
                if (dadosUsuario == null || !dadosUsuario.Senha.SequenceEqual(pModel.Senha.GenerateHash()) || !dadosUsuario.Ativo)
                {
                    MensagemErro = "CPF/Senha inválidos ou usuário inativo";
                    return View(pModel);
                }

                //Se cursor passar por aqui, usuário e senha foram informados corretamente            
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, dadosUsuario.Nome));
                claims.Add(new Claim(ClaimsCustomizadas.Administrador, dadosUsuario.Permissoes.Count(p => p.PermissaoId == Infra.Enum.EPerfilUsuario.AdministradorUsuarios) > 0 ? "1" : "0"));
                claims.Add(new Claim(ClaimsCustomizadas.PrecisaTrocarSenhaNoProximoLogin, dadosUsuario.AlterarSenhaProximoLogin ? "1" : "0"));
                claims.Add(new Claim(ClaimTypes.Email, dadosUsuario.Email));
                var strCPF = dadosUsuario.CPF.ToString("00000000000"); // Formata o CPF do usuário para 11 dígitos
                claims.Add(new Claim(ClaimsCustomizadas.CPF, String.Join("", strCPF.Take(3).Concat(".").Concat(strCPF.Skip(3).Take(3)).Concat(".").Concat(strCPF.Skip(6).Take(3)).Concat("-").Concat(strCPF.Skip(9)))));
                claims.Add(new Claim(ClaimsCustomizadas.IdUsuario, dadosUsuario.UsuarioId.ToString("N")));
                claims.Add(new Claim(ClaimsCustomizadas.UgID, dadosUsuario.UnidadeGestoraID.ToString("N")));
                if (dadosUsuario.AlterarSenhaProximoLogin)
                {
                    claims.Add(new Claim(ClaimTypes.Role, EPerfilUsuario.PrecisaTrocarSenha.ToString()));
                }
                else
                {
                    claims.AddRange(dadosUsuario.Permissoes.Select(permissao => new Claim(ClaimTypes.Role, permissao.PermissaoId.ToString())));
                    claims.AddRange(dadosUsuario.Pastas.Select(pasta => new Claim(ClaimsCustomizadas.Pastas, pasta.PastaId.ToString("N"))));
                }
                var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(id);
                dadosUsuario.DataUltimoLogin = DateTime.Now;
                _dbTransparencia.SaveChanges();
                if (dadosUsuario.AlterarSenhaProximoLogin)
                {
                    return RedirectToAction(nameof(UsuarioController.TrocarSenha), "Usuario");
                }
                return RedirectToAction(nameof(this.AreaAdministrativa));
            }
            else
            {
                return View();
            }
        }

        public ActionResult Logoff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction(nameof(this.Index));
        }

        public ActionResult TesteEmail()
        {
            var msg = MailUtils.SendMail("Teste envio e-mail", "Teste");

            if (String.IsNullOrWhiteSpace(msg))
            {
                msg = "E-mail enviado com sucesso!";
            }

            var conteudoDinamico = new ConteudoDinamico
            {
                ConteudoDinamicoId = Guid.NewGuid(),
                DataCriacao = DateTime.Now,
                Descricao = "Teste de envio de e-mail",
                ConteudoHTML = $"<p><b>Status de envio de e-mail:</b> {msg}<p>",
                Titulo = "Teste de envio de e-mail"
            };

            return View(conteudoDinamico);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}