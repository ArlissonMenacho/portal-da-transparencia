﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using static TransparenciaRO.Infra.Enum.ETipoDocumento;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using static System.Convert;
using System.IO;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using System.Text;
using System.Web;

namespace TransparenciaRO.Web.Controllers
{
    public class ContratoConvenioController : CRUDControllerBase<ContratoConvenio>
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly ArquivoDAL _arquivoDal;
        private readonly ContratoConvenioDAL _contratoConvenioDal;
        private readonly DadosLicitacaoDAL _dadosLicitacaoDAL;
        List<RemuneracaoEquipe> remuneracaoEquipes = new List<RemuneracaoEquipe>();

        public ContratoConvenioController()
        {
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _arquivoDal = new ArquivoDAL(_dbTransparencia);
            _contratoConvenioDal = new ContratoConvenioDAL(_dbTransparencia);
            _dadosLicitacaoDAL = new DadosLicitacaoDAL(_dbTransparencia);
        }

        #region COVID-19

        public ActionResult ContratosEmergenciaisCOVID19()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            return View();
        }

        [HttpPost]
        public JsonResult ContratosEmergenciaisCOVID19(string dataInicial, string dataFinal, bool cancelado, bool sancionado, bool comArquivos, bool semArquivos, string unidadeGestora, string cnpj, string numeroProcesso, string numeroDocumento, string razaoSocial, string objeto)
        {
            var tipoDoc = (ETipoDocumento)ToInt32(ETipoDocumento.ContratoEmergencialCOVID19);
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date;
            var unidadesGestoras = unidadeGestora.IsNullOrEmpty() ? new List<Guid>() : unidadeGestora.Split(',').Select(ug => new Guid(ug)).ToList();
            var limiteQtdeDocumentos = 25;

            var dadosRetorno = _contratoConvenioDal.GetContratoConvenio(tipoDoc, dtInicial, dtFinal, cancelado, sancionado, comArquivos, semArquivos, unidadesGestoras, cnpj, numeroProcesso, numeroDocumento, razaoSocial, objeto).ToList().Select(d => new
            {
                Origem = d.LicitacaoOrigem,
                NumeroLivroEFolha = d.NumeroLivroEspecial?.ToString("00") + "/" + d.NumeroFolha?.ToString("00"),
                d.NumeroDocumento,
                d.NumeroProcesso,
                DataElaboracao = d.DataElaboracao.ToString("dd/MM/yyyy"),
                d.DataVigenciaDescritiva,
                DataRetorno = d.DataRetorno?.ToString("dd/MM/yyyy"),
                d.NumeroDoe,
                d.Empresa,
                d.Cnpj,
                d.Objeto,
                DescricaoValor = $"Valor {d.Valor:c2} {(d.ValorEmpenhado != null ? "VALOR EMPENHADO " + d.ValorEmpenhado : "")}{(d.ValorAContraPartida != null ? "VALOR A CONTRA PARTIDA " + d.ValorAContraPartida : "")}",
                Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new { LinkArquivo = Url.Action("VisualizarArquivo", "Arquivo", new { pEncArquivoId = a.ArquivoId.ToString().ToEncrypted() }), NomeArquivo = a.Descricao + "." + a.Extensao }),
                LinkParaEdicao = this.Url.Action("AdicionarEditar", new { pEncContratoConvenioId = d.ContratoConvenioId.ToString().ToEncrypted() })
            }).Take(limiteQtdeDocumentos);

            return Json(new
            {
                tipoDoc = tipoDoc.GetDescricao(),
                dados = dadosRetorno,
                msgAlerta = dadosRetorno.Count() == limiteQtdeDocumentos ? $"O número de registros foi limitado à {limiteQtdeDocumentos} documentos. Caso não encontre o registro encontrado, especifique critérios de busca nos filtros acima" : null
            });
        }

        public void ContratosEmergenciaisCOVID19ToCSV()
        {
            var contratosConveniosCOVID19 = _dadosLicitacaoDAL.GetAllForCSV();

            var sb = new StringBuilder();

            sb.AppendFormat(
                "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                "Modadlidade",
                "Status",
                "Unidade Gestora",
                "Número processo administrativo",
                "Número da licitação (certame)",
                "Ano",
                "Contrato",
                "CNPJ",
                "Razão social",
                "Data assinatura",
                "Vigência",
                "Local execução",
                "Quantidade",
                "Unidade",
                "Valor unitário",
                "Valor global",
                "Arquivo Íntegra do processo",
                "Arquivo Instrumento contratual",
                "Objeto"
            );

            sb.Append(Environment.NewLine);

            foreach (var item in contratosConveniosCOVID19)
            {
                sb.AppendFormat(
                    "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                    item.ModalidadeLicitacao.GetDescription(),
                    item.Status.GetDescription(),
                    item.UnidadeGestora,
                    item.NumeroProcessoAdministrativo,
                    item.NumeroLicitacao,
                    item.Ano,
                    item.Contrato,
                    item.CNPJ,
                    item.RazaoSocial,
                    item.DataAssinatura.Date,
                    item.Vigencia,
                    item.LocalExecucao,
                    item.Quantidade,
                    item.Unidade,
                    item.ValorUnitario,
                    item.ValorGlobal,
                    Request.Url.Scheme + "://" + Request.Url.Host + "/Arquivo/VisualizarArquivo?pEncArquivoId=" + item.LicitacaoArquivoId.ToString().ToEncrypted(),
                    Request.Url.Scheme + "://" + Request.Url.Host + "/Arquivo/VisualizarArquivo?pEncArquivoId=" + item.FornecedorArquivoId.ToString().ToEncrypted(),
                    item.Objeto
                );

                sb.Append(Environment.NewLine);
            }

            var response = System.Web.HttpContext.Current.Response;
            response.BufferOutput = true;
            response.Clear();
            response.ClearHeaders();
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("content-disposition", "attachment;filename=ContratosEmergenciaisCOVID19.CSV ");
            response.ContentType = "text/plain";
            response.Write(sb.ToString());
            response.End();
        }
        #endregion

        // GET: Contrato
        public override ActionResult Index()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            //Verifica se possui arquivos temporarios excluindo os mesmos
            _arquivoDal.RemoverTemporario(_arquivoDal.BuscarArquivoTemporarios());

            return View();
        }
        public ActionResult IndexParcerias()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            //Verifica se possui arquivos temporarios excluindo os mesmos
            _arquivoDal.RemoverTemporario(_arquivoDal.BuscarArquivoTemporarios());

            return View("IndexParceria");
        }

        [HttpPost]
        public JsonResult Index(string tipoDocumento, string dataInicial, string dataFinal, bool cancelado, bool sancionado, bool comArquivos, bool semArquivos, string unidadeGestora, string cnpj, string numeroProcesso, string numeroDocumento, string razaoSocial, string objeto)
        {
            var tipoDoc = (ETipoDocumento)ToInt32(tipoDocumento);
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date;
            var unidadesGestoras = unidadeGestora.IsNullOrEmpty() ? new List<Guid>() : unidadeGestora.Split(',').Select(ug => new Guid(ug)).ToList();
            var limiteQtdeDocumentos = 25;

            var dadosRetorno = _contratoConvenioDal.GetContratoConvenio(tipoDoc, dtInicial, dtFinal, cancelado, sancionado, comArquivos, semArquivos, unidadesGestoras, cnpj, numeroProcesso, numeroDocumento, razaoSocial, objeto).ToList().Select(d => new
            {
                Origem = d.LicitacaoOrigem,
                NumeroLivroEFolha = d.NumeroLivroEspecial?.ToString("00") + "/" + d.NumeroFolha?.ToString("00"),
                d.NumeroDocumento,
                d.NumeroProcesso,
                DataElaboracao = d.DataElaboracao.ToString("dd/MM/yyyy"),
                DataVigencia = d.DataVigencia?.ToString("dd/MM/yyyy"),
                DataRetorno = d.DataRetorno?.ToString("dd/MM/yyyy"),
                d.NumeroDoe,
                d.Empresa,
                d.Cnpj,
                d.Objeto,
                DescricaoValor = $"Valor {d.Valor:c2} {(d.ValorEmpenhado != null ? "VALOR EMPENHADO " + d.ValorEmpenhado : "")}{(d.ValorAContraPartida != null ? "VALOR A CONTRA PARTIDA " + d.ValorAContraPartida : "")}",
                Arquivos = d.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId)).Select(a => new { LinkArquivo = Url.Action("VisualizarArquivo", "Arquivo", new { pEncArquivoId = a.ArquivoId.ToString().ToEncrypted() }), NomeArquivo = a.Descricao + "." + a.Extensao }),
                LinkParaEdicao = this.Url.Action("AdicionarEditar", new { pEncContratoConvenioId = d.ContratoConvenioId.ToString().ToEncrypted() }),
                LinkParaVisualizacao = this.Url.Action("VisualizarContratoConvenio", new { pEncContratoConvenioId = d.ContratoConvenioId.ToString().ToEncrypted() })
            }).Take(limiteQtdeDocumentos);

            return Json(new
            {
                tipoDoc = tipoDoc.GetDescricao(),
                dados = dadosRetorno,
                msgAlerta = dadosRetorno.Count() == limiteQtdeDocumentos ? $"O número de registros foi limitado à {limiteQtdeDocumentos} documentos. Caso não encontre o registro encontrado, especifique critérios de busca nos filtros acima" : null
            });
        }

        public ActionResult VisualizarContratoConvenio(string pEncContratoConvenioId)
        {
            var contratoConvenio = _contratoConvenioDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncContratoConvenioId)));

            if (contratoConvenio == null)
                MensagemErro = "Número de contrato/convênio informado inválido";

            if (contratoConvenio.TermoFomento == null)
            {
                PopularViewBagUnidadesGestoras();
                return View("VisualizarContratos", contratoConvenio);
            }

            return View("VisualizarContratoConvenio", contratoConvenio);

        }
        [Authorize(Roles = nameof(AdministradorContratosConvenios))]
        public ActionResult AdicionarDocumento(ETipoDocumento tipoDoc)
        {
            if (tipoDoc == ETipoDocumento.TermoFomento)
            {
                PopularViewBagUnidadesGestoras();
                ViewBag.PossuiArquivo = false;
                var contrato = new ContratoConvenio
                {
                    TermoFomento = new TermoFomento(),
                    TipoDocumento = tipoDoc
                };
                return View("AdicionarFomento", contrato);
            }
            if (tipoDoc == ETipoDocumento.TermoDeCooperacao)
            {
                PopularViewBagUnidadesGestoras();
                ViewBag.PossuiArquivo = false;
                var contrato = new ContratoConvenio
                {
                    TermoFomento = new TermoFomento(),
                    TipoDocumento = tipoDoc
                };
                return View("AdicionarTermoDeCooperacao", contrato);
            }
            else
            {

                PopularViewBagUnidadesGestoras();
                ViewBag.PossuiArquivo = false;
                return View("AdicionarEditar", new ContratoConvenio { TipoDocumento = tipoDoc });
            }
        }

        //[Authorize(Roles = nameof(AdministradorContratosConvenios))]

        public override ActionResult AdicionarEditar(string pEncContratoConvenioId)
        {
            try
            {
                PopularViewBagUnidadesGestoras();

                var contratoConvenio = _contratoConvenioDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncContratoConvenioId)));

                if (contratoConvenio == null)
                    MensagemErro = "Número de contrato/convênio informado inválido";

                if (contratoConvenio.TipoDocumento == ETipoDocumento.TermoFomento || contratoConvenio.TipoDocumento == ETipoDocumento.TermoDeCooperacao)
                {
                    if (contratoConvenio.TermoFomento != null)
                    {
                        foreach (var item in contratoConvenio.TermoFomento.RemuneracaoEquipe)
                        {
                            item.TermoFomento = null;
                        }
                        return View("AdicionarFomento", contratoConvenio);
                    }
                }
                return View("AdicionarEditar", contratoConvenio);
            }
            catch (Exception e)
            {
                MensagemErro = e.Message;
                return View("Index");
            }
        }

        public ActionResult AdicionarArquivo(ContratoConvenio contrato)
        {


            var arquivos = contrato.Arquivos.Where(a => a.DataRemocao == null && ArquivoUtils.ArquivoExiste(a.ArquivoId));
            var temporarios = _arquivoDal.BuscarArquivoTemporarios();
            var tipoAnexo = contrato.ArquivoTemporario == null? null: contrato.ArquivoTemporario.TipoAnexoTermoFomento;
            var msgErrosArquivos = new List<String>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(Request.Files[i]))
                {
                    //Novos arquivos (salva no disco e BD)
                    var novoArquivo = new Arquivo();
                    novoArquivo.ArquivoId = Guid.NewGuid();
                    novoArquivo.UsuarioIdUpload = AuthenticationManager.User.IdUsuario();
                    novoArquivo.Extensao = Path.GetExtension(Request.Files[i].FileName);
                    novoArquivo.Descricao = Request.Files[i].FileName;
                    novoArquivo.TipoAnexoTermoFomento = tipoAnexo;
                    novoArquivo.PastaId = GuidUtils.GuidPastaContratosEConvenios();
                    novoArquivo.Temporario = true; //Arquivos com esta propriedade serão excluídos automaticamente por uma rotina de varredura diária no servidor


                    switch (Request.Files.AllKeys[i])
                    {
                        case "files":
                            if (ArquivoUtils.SalvarArquivo(Request.Files[i], novoArquivo.ArquivoId))
                            {
                                novoArquivo.ContratoConvenioId = contrato.ContratoConvenioId == new Guid() ? null : (Guid?)contrato.ContratoConvenioId;
                                _arquivoDal.Adicionar(novoArquivo);
                                contrato.Arquivos.Add(novoArquivo);
                                return PartialView("_ListaArquivosContratos", contrato);

                            }
                            break;
                        case "Arquivo":
                            if (ArquivoUtils.SalvarArquivo(Request.Files[i], novoArquivo.ArquivoId))
                            {
                                novoArquivo.ContratoConvenioId = contrato.ContratoConvenioId == new Guid() ? null : (Guid?)contrato.ContratoConvenioId;
                                _arquivoDal.Adicionar(novoArquivo);
                                contrato.Arquivos.Add(novoArquivo);
                                return PartialView("_ListaArquivosContratos", contrato);

                            }
                            break;
                        case "filesPrestacaoConta":
                            if (ArquivoUtils.SalvarArquivo(Request.Files[i], novoArquivo.ArquivoId))
                            {
                                novoArquivo.PrestacaoDeContasId = contrato.TermoFomento.PrestacaoDeContasId == new Guid() ? null : (Guid?)contrato.TermoFomento.PrestacaoDeContasId;
                                _arquivoDal.Adicionar(novoArquivo);
                                contrato.TermoFomento.PrestacaoDeContas.ArquivoResultadoConclusivo.Add(novoArquivo);
                                //contrato.Arquivos.Add(novoArquivo);

                                return PartialView("_ListaArquivosPrestacaoContas", contrato.TermoFomento.PrestacaoDeContas);

                                //contrato.Arquivos.Add(novoArquivo);
                            }
                            break;
                        case "filesMonitoramento":
                            if (ArquivoUtils.SalvarArquivo(Request.Files[i], novoArquivo.ArquivoId))
                            {
                                novoArquivo.MonitoramentoId = contrato.TermoFomento.MonitoramentoId == new Guid() ? null : (Guid?)contrato.TermoFomento.MonitoramentoId;
                                _arquivoDal.Adicionar(novoArquivo);
                                contrato.TermoFomento.Monitoramento.ArquivoRelatorioMonitoramento.Add(novoArquivo);
                                //contrato.Arquivos.Add(novoArquivo);

                                return PartialView("_ListaArquivosMonitoramento", contrato.TermoFomento.Monitoramento);

                                //contrato.Arquivos.Add(novoArquivo);
                            }
                            break;
                        default:
                            msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                            break;

                    }
                }

                else
                {
                    msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo { Request.Files[i].FileName }. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
                }
            }
            ViewBag.MensagensErrosArquivos = msgErrosArquivos;
            return PartialView("_ListaArquivos", contrato);
        }
   
        public ActionResult RemoverArquivo(ContratoConvenio dados, string guidArquivo)
        {
            if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)) && dados.ContratoConvenioId != new Guid())
            {
                //Foi solicitada a exclusão de um arquivo de um contrato/convênio já cadastrado no BD. Nada será feito                
            }
            else if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)))
            {
                //Arquivo ainda é temporário, poderá ser excluído fisicamente do disco (ainda não existe na base)
                ArquivoUtils.ExcluirArquivo(new Guid(guidArquivo));
            }
            //Remove do array de arquivos que está sendo utilizado pela view
            dados.Arquivos.Remove(dados.Arquivos.FirstOrDefault(a => a.ArquivoId == new Guid(guidArquivo)));

            //Devolve à view  o modelo atualizado (apenas a Partial _ListaArquivos.cshtml será renderizada novamente, e apenas a propriedade Arquivos acessada)
            return PartialView("_ListaArquivos", dados);
        }

        [ValidateAntiForgeryToken]
        public override ActionResult AdicionarEditar(ContratoConvenio pContratoConvenio)
        {
            try
            {
                var adicionando = pContratoConvenio.ContratoConvenioId == new Guid();

                pContratoConvenio.DataElaboracao = pContratoConvenio.DataAssinatura ?? DateTime.Now;

                if (!ModelState.IsValid)
                {
                    MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                    PopularViewBagUnidadesGestoras();
                    return View(pContratoConvenio);
                }

                var msgErro = "";
                var guid = _contratoConvenioDal.AdicionarEditar(pContratoConvenio, out msgErro); // Insere/atualiza o contrato/convênio no BD
                if (guid == null)
                {
                    MensagemErro = msgErro;
                    return RedirectToAction("Index");
                }

                MensagemSucesso = $"{(pContratoConvenio.TipoDocumento == Contrato ? "Contrato" : "Convênio")} {(adicionando ? "inserido" : "alterado")} com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um erro inesperado. Caso o problema persista entre em contato com a CGE";
                return View("Index");
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult AdicionarFomento(ContratoConvenio fomento)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                PopularViewBagUnidadesGestoras();
                return View(fomento);
            }
            var msgErro = "";
            var guid = _contratoConvenioDal.AdicionarEditar(fomento, out msgErro);
            if (guid == null)
            {
                MensagemErro = msgErro;
                return Json(new { Sucesso = false, Mensagem = "Ocorreu um erro inesperado. Caso o problema persista, entre em contato com a CGE" });
            }

            return Json(new { Sucesso = true, Mensagem = "Licitação salva com sucesso!" });


        }
        [HttpPost]
        public ActionResult AdicionarRemuneracao(RemuneracaoEquipe remuneracaoEquipe)
        {

            remuneracaoEquipes.Add(remuneracaoEquipe);

            return PartialView("_TabelaRemuneracao", remuneracaoEquipes);
        }
        public override ActionResult Remover(string pEncId)
        {
            throw new NotImplementedException();
        }

        public override ActionResult ConfirmaRemocao(string pEncId)
        {
            throw new NotImplementedException();
        }

        public ActionResult Grafico()
        {
            ViewBag.AnoFinal = _contratoConvenioDal.AnosFinalContratosConvenios();
            ViewBag.Ano = _contratoConvenioDal.AnosDosContratosConvenios();
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid();

            return View();
        }

        public JsonResult DadosAnuais(string tipoDocumento)
        {
            var dados = _contratoConvenioDal.DadosAnuais((ETipoDocumento)ToInt32(tipoDocumento)).Select(c => new
            {
                Ano = c.Key,
                Valor = c.Value
            });

            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DadosMensais(string tipoDocumento, int ano)
        {
            var dados = _contratoConvenioDal.DadosMensais((ETipoDocumento)ToInt32(tipoDocumento), ano).Select(c => new
            {
                Mes = c.Key.NomeMes(),
                Valor = c.Value
            });

            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DadosAnuaisPorUnidadeGestora(string tipoDocumento, string unidadeGestora)
        {
            var dados = _contratoConvenioDal.DadosAnuaisPorUnidadeGestora((ETipoDocumento)ToInt32(tipoDocumento), unidadeGestora.Split(',').Select(ug => new Guid(ug)).ToList()).Select(c => new
            {
                Ano = c.Key,
                Valor = c.Value
            });

            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DadosMensaisPorUnidadeGestora(string tipoDocumento, int ano, string unidadeGestora)
        {
            var dados = _contratoConvenioDal.DadosMensaisPorUnidadeGestora((ETipoDocumento)ToInt32(tipoDocumento), ano, unidadeGestora.Split(',').Select(ug => new Guid(ug)).ToList()).Select(c => new
            {
                Mes = c.Key.NomeMes(),
                Valor = c.Value
            });

            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        private void PopularViewBagUnidadesGestoras()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasDictionary()
                .Union(new Dictionary<string, string> { })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}