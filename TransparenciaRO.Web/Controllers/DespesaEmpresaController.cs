﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class DespesaEmpresaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DespesaEmpresaDAL _despesaEmpresaDal;
        private int cont = 1;

        public DespesaEmpresaController()
        {
            _despesaEmpresaDal = new DespesaEmpresaDAL(_dbTransparencia);
        }

        //Actions
        [Authorize(Roles = nameof(AdministradorDespesasReceitasSoph) + "," + nameof(AdministradorDespesasReceitasCaerd) + "," + nameof(AdministradorDespesasReceitasCrm))]
        public ActionResult ImportarDados(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(AdministradorDespesasReceitasSoph) + "," + nameof(AdministradorDespesasReceitasCaerd) + "," + nameof(AdministradorDespesasReceitasCrm))]
        public ActionResult ImportarDados(HttpPostedFileBase file, EEmpresa empresa)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    //Rotina de importação dos dados
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';"))
                    {
                        db.Open();
                        var despesasEmpresas = new List<DespesaEmpresa>();

                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                                if (cont == 164)
                                {
                                    Console.Write("pare");
                                }
                                var despesaTerceirizada = new DespesaEmpresa
                                {
                                    FonteRecurso = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("FONT# RECURSO"))),
                                    PagamentoRecebimento = dr.GetDateTime(dr.GetOrdinal("DATA PAGAMENTO/RECEBIMENTO")),
                                    CredorDevedor = dr.GetValue(dr.GetOrdinal("CREDOR/DEVEDOR")).ToString().Trim(),
                                    Outros = dr.GetValue(dr.GetOrdinal("OUTROS")).ToString().Trim(),
                                    NotaFiscal = dr.GetValue(dr.GetOrdinal("NOTA FISCAL")).ToString().Trim(),
                                    CpfCnpj = dr.GetValue(dr.GetOrdinal("CNPJ CPF")).ToString().Trim(),
                                    Valor = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("VALOR")).ToString().Replace("-", "")),
                                    Empresa = empresa
                                };
                                cont++;
                                despesasEmpresas.Add(despesaTerceirizada);
                            }
                            _despesaEmpresaDal.VerficarRemoverDados(despesasEmpresas.First().PagamentoRecebimento, empresa);
                            _despesaEmpresaDal.Adicionar(despesasEmpresas);
                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    ViewBag.Empresa = empresa.ToString().ToUpper();
                    return RedirectToAction("ImportarDados", "DespesaEmpresa", new { empresa = empresa });
                }
                MensagemErro = "O arquivo deve está no formato xls ou xlsx";
                return View();
            }
            catch
            {
                MensagemErro = "Ocorreu um erro na linha "+cont+", caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                ViewBag.Empresa = empresa.ToString().ToUpper();
                return RedirectToAction("ImportarDados", "DespesaEmpresa", new { empresa = empresa });
            }
        }

        public ActionResult Grafico(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            ViewBag.AnoMax = _despesaEmpresaDal.AnoMax(empresa);
            ViewBag.AnoMin = _despesaEmpresaDal.AnoMin(empresa);

            return View();
        }

        //Json
        public JsonResult Meses(int ano, EEmpresa empresa)
        {
            var retorno = _despesaEmpresaDal.Meses(ano, empresa).Select(m => new
            {
                MesInt = m,
                MesString = m.NomeMes()
            }).OrderByDescending(m => m.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DespesaAnual(EEmpresa empresa)
        {
            var retorno = _despesaEmpresaDal.DespesasAnual(empresa).Select(r => new
            {
                Ano = r.Key,
                Valor = r.Value,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Value).Replace("R$", "")
            }).OrderByDescending(r => r.Ano);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DespesaMensal(int ano, EEmpresa empresa)
        {
            var retorno = _despesaEmpresaDal.DespesaMensal(ano, empresa).Select(r => new
            {
                Ano = ano,
                MesInt = r.Key,
                MesString = r.Key.NomeMes(),
                Valor = r.Value,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Value).Replace("R$", "")
            }).OrderBy(r => r.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DespesaDetalhada(int ano, int mes, EEmpresa empresa)
        {
            var retorno = _despesaEmpresaDal.DespesaDetalhada(ano, mes, empresa).Select(r => new
            {
                FonteRecurso = r.FonteRecurso,
                PagamentoRecebimento = r.PagamentoRecebimento.ToString("d"),
                CredorDevedor = r.CredorDevedor,
                Outros = r.Outros,
                NotaFiscal = r.NotaFiscal,
                CpfCnpj = r.CpfCnpj,
                Valor = r.Valor,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Valor).Replace("R$", "")
            }).OrderByDescending(r => r.Valor);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}