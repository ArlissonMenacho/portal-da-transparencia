﻿using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers
{
    public class GlossarioController : ReadOnlyControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly GlossarioDAL _glossarioDal;

        public GlossarioController()
        {
            _glossarioDal = new GlossarioDAL(_dbTransparencia);
        }

        public override ActionResult Index()
        {
            return View(_glossarioDal.ObterTodos().OrderBy(g => g.Termo).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}