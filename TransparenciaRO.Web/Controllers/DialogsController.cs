﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    
    public class DialogsController : ControllerBase
    {
        private DbTransparencia db = new DbTransparencia();
        private DialogDAL _dialogDal;

        public DialogsController()
        {
            _dialogDal = new DialogDAL(db);
        }
        [Authorize(Roles = nameof(AdministradorDialog))]
        public ActionResult Index()
        {
            return View(_dialogDal.List());
        }
        [Authorize(Roles = nameof(AdministradorDialog))]
        public ActionResult Novo()
        {
            return View();
        }

        [Authorize(Roles = nameof(AdministradorDialog))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Novo(Dialog dialog)
        {
            if (ModelState.IsValid)
            {
                _dialogDal.Add(dialog);
                MensagemSucesso = "Dialog adicionado com sucesso";
                return RedirectToAction("Index");
            }

            MensagemErro = "Ocorreu um erro, por favor entrar em contato com administrador";
            return View(dialog);
        }
        [Authorize(Roles = nameof(AdministradorDialog))]
        public ActionResult Atualizar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dialog dialog = db.Dialog.Find(id);
            if (dialog == null)
            {
                return HttpNotFound();
            }
            return View(dialog);
        }
        [Authorize(Roles = nameof(AdministradorDialog))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Atualizar(Dialog dialog)
        {
            if (ModelState.IsValid)
            {
                _dialogDal.Update(dialog);
                MensagemSucesso = "Dialog atualizado com sucesso";
                return RedirectToAction("Index");
            }
            MensagemErro = "Ocorreu um erro, por favor entrar em contato com administrador";
            return View(dialog);
        }

        [Authorize(Roles = nameof(AdministradorDialog))]
        public ActionResult Delete(int id)
        {
            Dialog dialog = db.Dialog.Find(id);
            db.Dialog.Remove(dialog);
            db.SaveChanges();
            MensagemSucesso = "Dialog removido com sucesso";
            return RedirectToAction("Index");
        }

        public JsonResult List()
        {
            return Json(_dialogDal.List(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
