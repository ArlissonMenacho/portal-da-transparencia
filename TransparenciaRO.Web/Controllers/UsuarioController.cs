﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Utils;
using System.Web.Mvc;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.DAL;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace TransparenciaRO.Web.Controllers
{
    public class UsuarioController : CRUDControllerBase<AdicionarEditarUsuarioViewModel>
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly UsuarioDAL _usuarioDal;
        private readonly PermissaoDAL _permissaoDal;
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly PastaDAL _pastaDal;

        public UsuarioController()
        {
            _usuarioDal = new UsuarioDAL(_dbTransparencia);
            _permissaoDal = new PermissaoDAL(_dbTransparencia);
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _pastaDal = new PastaDAL(_dbTransparencia);
        }

        [Authorize(Roles = nameof(PrecisaTrocarSenha))]
        public ActionResult TrocarSenha()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = nameof(PrecisaTrocarSenha))]
        public ActionResult TrocarSenha(UsuarioTrocarSenhaViewModel pDados)
        {
            if (ModelState.IsValid)
            {
                _usuarioDal.AtualizarSenha((this.User as ClaimsPrincipal).IdUsuario(), pDados.Senha);
                MensagemSucesso = "A senha foi alterada com sucesso! Por favor, informe suas novas credenciais nos campos abaixo";
                return RedirectToAction("Login", "Home");
            }
            MensagemErro = "A senha e a confirmação de senha não são iguais. Por favor, tente novamente";
            return View();
        }

        [Authorize(Roles = nameof(AdministradorUsuarios))]
        public JsonResult ResetarSenha(string pEncEmail)
        {
            var usuarioBD = _usuarioDal.ObterPorEmail(pEncEmail.ToDecrypted());

            if (usuarioBD == null)
                return Json(false, JsonRequestBehavior.AllowGet); // Usuário não encontrado na base

            var senhaTemporaria = "123456"; //StringUtils.GenerateRandomString(6);
            _usuarioDal.AtualizarSenha(usuarioBD.UsuarioId, senhaTemporaria, true);

            MailUtils.SendMail($"Prezado usuário {usuarioBD.Nome}, segue abaixo seus dados para <a href='http://transparencia.ro.gov.br/Home/Login'>acesso</a>:<hr />Usuário: {usuarioBD.CPF}<br />Senha: {senhaTemporaria}<hr />Governo do estado de Rondônia", "Dados para acesso ao portal de transparência RO", true, pEncEmail.ToDecrypted());
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        [Authorize(Roles = nameof(AdministradorUsuarios))]
        public override ActionResult AdicionarEditar(AdicionarEditarUsuarioViewModel pDados)
        {
            if (ModelState.IsValid)
            {
                var usuarioAtualBD = _usuarioDal.ObterPorId(pDados.ToDBModel().UsuarioId);
                //var senhaTemporaria = StringUtils.GenerateRandomString(6); //Caso o usuário precise trocar a senha no próximo login
                var senhaTemporaria = "123456"; //Caso o usuário precise trocar a senha no próximo login
                pDados.Senha = senhaTemporaria;
                pDados.Pastas = pDados.PastasGuids.Select(pastaId => _pastaDal.BuscarPasta(pastaId)).ToList();
                if (usuarioAtualBD != null && usuarioAtualBD.Email != pDados.Email)
                {
                    // Usuário trocou o e-mail, por questões de segurança, uma nova senha temporária deverá ser enviada para o destino
                    usuarioAtualBD.AlterarSenhaProximoLogin = false;
                    pDados.AlterarSenhaProximoLogin = true;
                }
                _usuarioDal.SalvarDados(pDados.ToDBModel());
                if ((usuarioAtualBD != null && usuarioAtualBD.AlterarSenhaProximoLogin == false && pDados.AlterarSenhaProximoLogin) || (usuarioAtualBD == null))
                {
                    // Caso o usuário seja novo, ou precisa alterar a senha no próximo login, o mesmo deverá receber por e-mail uma senha temporária para primeiro acesso                                                            
                    MailUtils.SendMail($"Prezado usuário {pDados.Nome}, segue abaixo seus dados para acesso:<hr />Usuário: {pDados.CPF}<br />Senha: {pDados.Senha}<hr />Governo do estado de Rondônia", "Dados para acesso ao portal de transparência RO", true, pDados.Email);
                    MensagemSucesso = $"Usuário salvo com sucesso! Um e-mail foi enviado ao endereço {pDados.Email} com a senha temporária";
                }
                else
                {
                    MensagemSucesso = "Usuário salvo com sucesso!";
                }
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Permissoes = _permissaoDal.ObterTodos().ToList();
            ViewBag.Pastas = _pastaDal.ObterPastasAdministraveis();
            MensagemErro = "Preencha os campos corretamente";
            return View(pDados);
        }

        [Authorize(Roles = nameof(AdministradorUsuarios))]
        public override ActionResult AdicionarEditar(string pEncId)
        {
            ViewBag.Permissoes = _permissaoDal.ObterTodos().OrderBy(x => x.Descricao).ToList();
            ViewBag.Pastas = _pastaDal.ObterPastasAdministraveis().OrderBy(x => x.Nome).ToList();
            UnidadesGestoras();
            return View(pEncId != null
                ? new AdicionarEditarUsuarioViewModel(_usuarioDal.ObterPorId(new Guid(pEncId.ToDecrypted())))
                : new AdicionarEditarUsuarioViewModel());
        }

        // Usuário não é excluído. Apenas inativado
        [Authorize(Roles = nameof(AdministradorUsuarios))]
        public override ActionResult ConfirmaRemocao(string pEncId)
        {
            throw new NotImplementedException();
        }

        [Authorize(Roles = nameof(AdministradorUsuarios) + "," + nameof(AdministradorReadOnly))]
        public override ActionResult Index()
        {
            return View(_usuarioDal.ObterTodosComPermissoes().ToList().Select(u => new AdicionarEditarUsuarioViewModel(u)).OrderBy(u => u.Nome).ToList());
        }

        [Authorize(Roles = nameof(AdministradorUsuarios) + "," + nameof(AdministradorReadOnly))]
        public override ActionResult Remover(string pEncId)
        {
            throw new NotImplementedException();
        }

        private void UnidadesGestoras()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasDictionary()
                .Union(new Dictionary<string, string> { })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}