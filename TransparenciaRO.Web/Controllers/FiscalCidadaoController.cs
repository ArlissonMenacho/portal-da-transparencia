﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class FiscalCidadaoController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly FiscalCidadaoDAL _fiscalCidadaoDal;
        private readonly FluxoFiscalCidadaoDAL _fluxoFiscalCidadaoDal;
        private readonly UnidadeGestoraDAL _unidadeGestora;
        private readonly RespostaFiscalCidadaoDAL _respostaFiscalCidadaoDal;
        private readonly TextoFiscalCidadaoDAL _textoFiscalCidadaoDal;

        public FiscalCidadaoController()
        {
            _fiscalCidadaoDal = new FiscalCidadaoDAL(_dbTransparencia);
            _fluxoFiscalCidadaoDal = new FluxoFiscalCidadaoDAL(_dbTransparencia);
            _unidadeGestora = new UnidadeGestoraDAL(_dbTransparencia);
            _respostaFiscalCidadaoDal = new RespostaFiscalCidadaoDAL(_dbTransparencia);
            _textoFiscalCidadaoDal = new TextoFiscalCidadaoDAL(_dbTransparencia);
        }

        // GET: FiscalCidadao
        public ActionResult ConsultarProtocolo(long id)
        {
            return View(_fiscalCidadaoDal.ConsultarFluxo(id));
        }

        [Authorize(Roles = nameof(AdministradorFiscalCidadao))]
        public ActionResult Visualizar(long id)
        {
            if (AuthenticationManager.User.UgID() == UgGuidUtils.CGE)
            {
                ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
            }
            else
            {
                ViewBag.UnidadeGestora =
                    _unidadeGestora.BuscarUgPorGuid(UgGuidUtils.CGE);
            }
            return View(_fiscalCidadaoDal.BuscarProtocolo(id));
        }
        
        [Authorize(Roles = nameof(AdministradorFiscalCidadao))]
        public ActionResult Listar()
        {
            if (AuthenticationManager.User.UgID() == UgGuidUtils.CGE)
            {
                ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
            }
            else
            {
                ViewBag.UnidadeGestora =
                    _unidadeGestora.BuscarUgPorGuid(AuthenticationManager.User.UgID());
            }
            return View();
        }
        
        // POST: FiscalCidadao
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Resposta(RespostaFiscalCidadao respostaFiscalCidadao)
        {
            respostaFiscalCidadao.DataResposta = DateTime.Now;
            respostaFiscalCidadao.UsuarioId = AuthenticationManager.User.IdUsuario();
            _respostaFiscalCidadaoDal.SalvarResposta(respostaFiscalCidadao);
            return RedirectToAction("Listar");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Encaminhar(string ugDestino, string observacao, string protocoloId, string ugOrigem)
        {
            _fluxoFiscalCidadaoDal.SalvarFluxo(new FluxoFiscalCidadao
            {
                ProtocoloId = Convert.ToInt64(protocoloId),
                DataMovimentacao = DateTime.Now,
                Observacao = observacao,
                OrgaoOrigemId = new Guid(ugOrigem),
                OrgaoDestinoId = new Guid(ugDestino),
                UsuarioId = AuthenticationManager.User.IdUsuario()
            });
            return RedirectToAction("Listar");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Finalizar(FiscalCidadao fiscalCidadao)
        {
            if (_fiscalCidadaoDal.VerificarSePossuiResposta(fiscalCidadao.ProtocoloId))
            {
                fiscalCidadao.DataDaFinaliacao = DateTime.Now;
                fiscalCidadao.UsuarioId = AuthenticationManager.User.IdUsuario();
                fiscalCidadao.EStatus = ETipoStatus.Finalizado;
                _fiscalCidadaoDal.Update(fiscalCidadao);
                return RedirectToAction("Listar");
            }
            MensagemErro = "Para finalizar está fiscalização, é necessario ter uma resposta";
            ViewBag.UnidadeGestora = _unidadeGestora.UnidadesGestorasDictionary();
            return View("Visualizar", _fiscalCidadaoDal.BuscarProtocolo(fiscalCidadao.ProtocoloId));
        }

        [HttpPost]
        public ActionResult ModeloResposta(string texto)
        {
            _textoFiscalCidadaoDal.Save(new TextoFiscalCidadao
            {
                Texto = texto,
                UnidadeGestoraId = AuthenticationManager.User.UgID()
            });
            return PartialView("_ModelosRespostas", _textoFiscalCidadaoDal.ListaDeModeloResposta(AuthenticationManager.User.UgID()));
        }

        //Retorno Json
        public JsonResult ConsultarFiscalizacao(string categoria, string dateInicial, string dateFinal, string unidadeGestora)
        {
            return Json(_fiscalCidadaoDal.BuscarDemandaCidadao(categoria, Convert.ToDateTime(dateInicial), Convert.ToDateTime(dateFinal).AddDays(1), UnidadeGestora(unidadeGestora))
                .Select(x => new
                {
                    Protocolo = x.ProtocoloId.ToString(),
                    Categoria = x.Categoria,
                    OrgaoVinculado = x.UnidadeGestora.nomeUG,
                    DataAbertura = x.DataFiscalizacao.ToString("d"),
                    Prazo = CalcularDias(DateTime.Now.Date, x.DataFiscalizacao.Date),
                    CorLinha = CorDaLinhaTable(CalcularDias(DateTime.Now.Date, x.DataFiscalizacao.Date), x.EStatus),
                    Status = x.EStatus.GetDescricao()
                }).OrderByDescending(x => x.Protocolo), JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult DadosGrafico(string categoria, string dateInicial, string dateFinal, string unidadeGestora)
        {
            return Json(_fiscalCidadaoDal.CarregarGrafico(categoria, Convert.ToDateTime(dateInicial),
                Convert.ToDateTime(dateFinal).AddDays(1), UnidadeGestora(unidadeGestora)).OrderByDescending(x => x.Value).ToList(), JsonRequestBehavior.AllowGet);
           
        }

        //Partial Views
        public ActionResult ModeloResposta()
        {
            return PartialView("_ModelosRespostas", _textoFiscalCidadaoDal.ListaDeModeloResposta(AuthenticationManager.User.UgID()));
        }
        
        //Metodos
        private string CorDaLinhaTable(int dias, ETipoStatus? eTipo)
        {
            if (dias > 20)
            {
                if (eTipo == ETipoStatus.Finalizado)
                {
                    return "success";
                }
                return "danger";
            }
            if (eTipo == ETipoStatus.Finalizado)
            {
                return "success";
            }
            else
            {
                if (eTipo == ETipoStatus.EmAndamento)
                {
                    return "warning";
                }
            }
            return "";
        }

        private int CalcularDias(DateTime dataAtual, DateTime dataFiscalizacao)
        {
            return (dataAtual - dataFiscalizacao).Days;
        }
        
        private string UnidadeGestora(string unidadeGestora)
        {
            return unidadeGestora.IsNullOrEmpty()
                ? AuthenticationManager.User.UgID() == UgGuidUtils.CGE
                    ? ""
                    : AuthenticationManager.User.UgID().ToString()
                : unidadeGestora;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}