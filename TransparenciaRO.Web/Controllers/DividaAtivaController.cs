﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Web.Services.DividaAtiva;

namespace TransparenciaRO.Web.Controllers
{
    public class DividaAtivaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DividaAtivaDAL _dividaAtivaDal;

        public DividaAtivaController()
        {
            _dividaAtivaDal = new DividaAtivaDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            return View();
        }

        //public JsonResult ResultadoDividaAtiva(string nomeRazaoSocial, string cpfCnpj)
        //{
        //    var cpfCnpjSomenteNumeros = cpfCnpj != ""
        //        ? Convert.ToInt64(cpfCnpj.Replace(".", "").Replace("-", "").Replace("/", "")).ToString()
        //        : cpfCnpj;

        //    var dados = _dividaAtivaDal.BuscarDividaAtiva(nomeRazaoSocial, cpfCnpjSomenteNumeros.ToString()).Select(d => new
        //    {
        //        Id = d.Id,
        //        NomeRazaoSocial = d.Nome,
        //        CpfCnpj = d.CpfCnpj.Length <= 11
        //            ? Convert.ToUInt64(d.CpfCnpj).ToString(@"000\.000\.000\-00")
        //            : Convert.ToUInt64(d.CpfCnpj).ToString(@"00\.000\.000\/0000\-00"),
        //        ValorTotal = d.ValorTotal.ToString("N"),
        //        ValorPago = d.ValorPago.ToString("N"),
        //        Valor = d.ValorAPagar.ToString("N")
        //    }).ToList();

        //    return Json(dados, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult Detalhar(int? Id)
        //{
        //    if (Id == null)
        //    {
        //        MensagemErro = "ID da dívida ativa não passada como parâmetro na URI. Por favor, corrija a questão e tente novamente.";

        //        return View(nameof(Index));
        //    }

        //    var dividaAtiva = _dividaAtivaDal.Detalhar(Id);

        //    if (dividaAtiva == null)
        //    {
        //        MensagemErro = "Nenhuma dívida ativa encontrada com este ID. Verifique se o parâmetro passado está correto e tente novamente.";

        //        return View(nameof(Index));
        //    }

        //    return View(dividaAtiva);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();
            base.Dispose(disposing);
        }

        //divida ativa API

        public async Task<ActionResult> Detalhes(int? id)
        {
            if(id == null)
            {
                MensagemErro = "Id inválido";

                return View(nameof(Index));
            }

            try
            {
                var dividaAtivaVM = await DividaAtivaAPI.ObterDetalheDividaAtiva((int)id);

                return View(dividaAtivaVM);
            }
            catch (Exception)
            {
                MensagemErro = "Ocorreu um problema ao buscar os detalhes";

                return View(nameof(Index));
            }
        }

        public async Task<ActionResult> Filtrar(string nomeRazaoSocial, string cpfCnpj)
        {
            var retornoVM = new RetornoViewModel();

            try
            {
                retornoVM.Dados = await DividaAtivaAPI.ObterDividasAtivas(cpfCnpj, nomeRazaoSocial);
                retornoVM.Sucesso = true;
            }
            catch (Exception e)
            {
                retornoVM.Sucesso = false;
                retornoVM.Mensagem = e.Message;
            }

            return Json(retornoVM, JsonRequestBehavior.AllowGet);
        }
    }
}