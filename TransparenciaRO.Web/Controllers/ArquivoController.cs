﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers
{
    public class ArquivoController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ArquivoDAL _arquivoDal;
        private readonly PastaDAL _pastaDal;

        public ArquivoController()
        {
            _arquivoDal = new ArquivoDAL(_dbTransparencia);
            _pastaDal = new PastaDAL(_dbTransparencia);
        }

        public ActionResult Adicionar(string pEncPastaId)
        {
            try
            {
                var pastaId = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                ViewBag.PastaGuid = pastaId;
                return View();
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Adicionar(HttpPostedFileBase file, Arquivo arquivo, string pEncPastaId)
        {
            try
            {
                if (!ModelState.IsValid) return View(arquivo);

                if (file == null)
                {
                    TempData["Erro"] = "Selecione um arquivo";
                    return View(arquivo);
                }

                var tipoArquivo = Path.GetExtension(file.FileName).ToLower();
                if (ArquivoUtils.ArquivoValido(file))
                {
                    var guidArquivo = Guid.NewGuid();
                    var caminho = Path.Combine(Server.MapPath("~/App_Data/Arquivos"), guidArquivo.ToString());
                    file.SaveAs(caminho);

                    arquivo.ArquivoId = guidArquivo;
                    arquivo.Extensao = tipoArquivo;
                    arquivo.PastaId = new Guid(CriptografiaUtils.Decrypt(pEncPastaId));
                    arquivo.UsuarioIdUpload = AuthenticationManager.User.IdUsuario();

                    _arquivoDal.Adicionar(arquivo);
                    TempData["Sucesso"] = "Arquivo adicionado com sucesso";
                    return RedirectToAction("Index", "Pasta", new { pEncPastaId = CriptografiaUtils.Encrypt(arquivo.PastaId.ToString()) });
                }

                ViewBag.PastaGuid = pEncPastaId.ToDecrypted();
                TempData["Erro"] = "Tipo de arquivo inválido, verique se o arquivo esta no formato PDF ou EXCEL e se possui menos 30MB";
                return View(arquivo);
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }

        public ActionResult Editar(string pEncArquivoID)
        {
            try
            {
                var arquivoGuid = new Guid(CriptografiaUtils.Decrypt(pEncArquivoID));
                var upload = _arquivoDal.BuscarArquivo(arquivoGuid);
                return View(upload);
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(Guid arquivoId, string descricao, int ordem)
        {
            try
            {
                var arquivo = _dbTransparencia.Arquivos.First(x => x.ArquivoId == arquivoId);
                arquivo.Descricao = descricao;
                arquivo.Ordem = ordem;

                _arquivoDal.Editar(arquivo);
                TempData["Sucesso"] = "Arquivo Alterado com sucesso";
                return RedirectToAction("Index", "Pasta", new { pEncPastaId = CriptografiaUtils.Encrypt(arquivo.PastaId.ToString()) });
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }

        public ActionResult Remover(string pEncArquivoId)
        {
            try
            {
                var arquivoGuid = new Guid(CriptografiaUtils.Decrypt(pEncArquivoId));
                var upload = _arquivoDal.BuscarArquivo(arquivoGuid);
                return View(upload);
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }

        [HttpPost, ActionName("Remover")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmarRemocao(string pEncArquivoId)
        {
            try
            {
                var arquivoGuid = new Guid(CriptografiaUtils.Decrypt(pEncArquivoId));
                var arquivo = _dbTransparencia.Arquivos.First(x => x.ArquivoId == arquivoGuid);
                arquivo.UsuarioIdRemocao = AuthenticationManager.User.IdUsuario();
                _arquivoDal.Remover(arquivo);

                TempData["Sucesso"] = "Arquivo removido com sucesso";
                return RedirectToAction("Index", "Pasta", new { pEncPastaId = CriptografiaUtils.Encrypt(arquivo.PastaId.ToString()) });
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }

        }

        public ActionResult VisualizarArquivo(string pEncArquivoId)
        {
            try
            {
                var arquivoGuid = new Guid(CriptografiaUtils.Decrypt(pEncArquivoId));
                var arquivo = _arquivoDal.BuscarArquivo(arquivoGuid);
                var bytesArquivo = System.IO.File.ReadAllBytes(ArquivoUtils.CaminhoArquivo(arquivoGuid));
                var mimeType = "application/" + arquivo.Extensao.Replace(".", "");
                Response.AppendHeader("Content-Disposition", "inline; filename=" + arquivo.Descricao.Replace(",", " ") + "." + (arquivo.Extensao.Replace(".", "")));
                return File(bytesArquivo, mimeType);
            }
            catch (Exception exception)
            {
                TempData["Erro"] = exception.Message;
                return RedirectToAction("Index", "Pasta", _pastaDal.BuscarPastas());
            }
        }        

        public FileResult DownloadFile()
        {


            //"~/App_Data/Arquivos"
            string filePath = Server.MapPath("~/App_Data/Arquivos/Licitacoes");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = "documento.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();
            
            base.Dispose(disposing);
        }
    }
}