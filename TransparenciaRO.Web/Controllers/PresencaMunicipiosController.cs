﻿using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Utils;
using System.Linq;
using System;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Web.Controllers
{
    public class PresencaMunicipiosController : Controller
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly PresencaMunicipiosDAL _presencaMunicipiosDal;

        public PresencaMunicipiosController()
        {
            _presencaMunicipiosDal = new PresencaMunicipiosDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            var DataInicial = DateTime.Now.AddMonths(-1).ToString("MM-yyyy");
            var DataFinal = DateTime.Now.ToString("MM-yyyy");
            ViewBag.DataInicial = DataInicial;
            ViewBag.DataFinal = DataFinal;
            using (var db = new DbTransparencia())
            {
                ViewBag.DataAtualizacao = new ParametrosDAL(db).GetParametro(ETipoParametro.DataAtualizacaoDespesas);
            }
            return View();
        }

        [HttpPost]
        public JsonResult Index(string pExercicio)
        {
            //Retorna todas as funções/administrações
            //PresencaMunicipiosModel
            return Json(pExercicio.ToInt() != null 
                ? new { dados = _presencaMunicipiosDal.GetPresencaMunicipiosPorExercicio(pExercicio.ToInt().Value).ToList() } 
                : null);
        }

        [HttpPost]
        public JsonResult DetalharMunicipio(string pExercicio, string pEncCodMunicipio)
        {
            // Retorna municípios

            if (pExercicio.ToInt() != null && pEncCodMunicipio.ToDecrypted().ToInt() != null)
                return Json(new { dados = _presencaMunicipiosDal.GetPresencaMunicipiosPorFuncao(pExercicio.ToInt().Value, pEncCodMunicipio.ToDecrypted().ToInt().Value).ToList() });

            return Json(null);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}