﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class DiariasEmpresaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DiariasEmpresasDAL _diariasEmpresasDal;

        public DiariasEmpresaController()
        {
            _diariasEmpresasDal = new DiariasEmpresasDAL(_dbTransparencia);
        }

        [Authorize(Roles = nameof(AdministradorRemuneracaoServidoresEmpresaSoph) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCmr) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCaerd))]
        public ActionResult ImportarDados(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file, int ano, int mes, EEmpresa empresa)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    //Importar Dados para o aplicaçao

                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0; Data Source = {path};Extended Properties = 'Excel 12.0; HDR = YES; ReadOnly = true; IMEX = 1';"))
                    {
                        db.Open();
                        var diariasEmpresa = new List<DiariasEmpresa>();

                        var dtSchema =
                            db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })
                                ?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                                var diariaEmpresa = new DiariasEmpresa
                                {
                                    FonteRecurso = dr.GetValue(dr.GetOrdinal("FonteRecurso")).ToString(),
                                    NumeroProcesso = dr.GetString(dr.GetOrdinal("NumeroProcesso")).Trim(),
                                    Nome = dr.GetString(dr.GetOrdinal("Nome")).Trim(),
                                    OrdemBancaria = dr.GetValue(dr.GetOrdinal("OrdemBancaria")).ToString().Trim(),
                                    Cargo = dr.GetString(dr.GetOrdinal("Cargo")).Trim(),
                                    Destino = dr.GetString(dr.GetOrdinal("Destino")).Trim(),
                                    Ida = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataSaida"))),
                                    DataPublicacao = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataPublicacao"))),
                                    DataPagamento = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataPagamento"))),
                                    Volta = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataRetorno"))),
                                    Motivo = dr.GetString(dr.GetOrdinal("Motivo")).Trim(),
                                    Transporte = dr.GetString(dr.GetOrdinal("TipoTransporte")).Trim(),
                                    DadosVeiculo = dr.GetValue(dr.GetOrdinal("DadosVeiculo")).ToString(),
                                    ValorPassagem = dr.GetValue(dr.GetOrdinal("ValorPassagem")).ToString(),
                                    ValorDiaria = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("ValorDiaria"))),
                                    TotalDiarias = Convert.ToString(dr.GetValue(dr.GetOrdinal("TotalDiarias"))),
                                    ValorTotal = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("ValorTotal"))),
                                    Ano = ano,
                                    //Mes = Convert.ToInt16(dr.GetValue(dr.GetOrdinal("Mes"))),
                                    Mes = mes,
                                    Empresa = empresa
                                };
                                diariasEmpresa.Add(diariaEmpresa);
                            }
                            _diariasEmpresasDal.VerificarRemoverDadosExistente(ano, mes, empresa);
                            _diariasEmpresasDal.AdicionarDados(diariasEmpresa);

                            LogUtils.Logar($"usuário: {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados de Diarias e Viagens de Servidores {empresa} -:", AuthenticationManager.User.IdUsuario());
                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    ViewBag.Empresa = empresa.ToString().ToUpper();
                    return RedirectToAction("ImportarDados", "DiariasEmpresa", new { empresa });
                }
                ViewBag.Empresa = empresa.ToString().ToUpper();
                MensagemErro = "Arquivo Inválido";
                return View();
            }
            catch
            {    
                MensagemErro = "Ocorreu um erro ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                ViewBag.Empresa = empresa.ToString().ToUpper();
                return RedirectToAction("ImportarDados", "DiariasEmpresa", new { empresa });
            }
        }

        public ActionResult Index(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            ViewBag.Ano = _diariasEmpresasDal.Ano();
            ViewBag.Mes = _diariasEmpresasDal.Mes(ViewBag.Ano);
            return View();
        }

        public ActionResult CarregarMes(int ano)
        {
            ViewBag.Mes = _diariasEmpresasDal.Mes(ano);
            return PartialView("_CarregarMes");
        }

        //Json
        public JsonResult ResultadoFiltro(int ano, int? mes, string nome, EEmpresa empresa)
        {
            return Json(_diariasEmpresasDal.BuscarDiariasEmpresas(ano, mes, nome, empresa).Select(x => new
            {
                NumeroProcesso = x.NumeroProcesso,
                Nome = x.Nome,
                Cargo = x.Cargo,
                DestinoViagem = x.Destino,
                TotalDiarias = x.TotalDiarias,
                ValorTotal = x.ValorTotal.ToString("N")

            }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DetalharDiarias(string numeroProcesso, string nome, EEmpresa empresa)
        {
            var servidor = _diariasEmpresasDal.DetalharDiariaEmpresa(numeroProcesso, nome, empresa);
            var dadosRetorno = new
            {
                NumeroProcesso = servidor.NumeroProcesso,
                Nome = servidor.Nome,
                Cargo = servidor.Cargo,
                Destino = servidor.Destino,
                Ida = servidor.Ida.ToString("d"),
                Volta = servidor.Volta.ToString("d"),
                Motivo = servidor.Motivo,
                Transporte = servidor.Transporte,
                DadosVeiculo = servidor.DadosVeiculo,
                ValorPassagem = servidor.ValorPassagem,
                ValorDiaria = servidor.ValorDiaria,
                TotalDiarias = servidor.TotalDiarias,
                ValorTotal = servidor.ValorTotal,
                FonteRecurso = servidor.FonteRecurso,
                DataPublicacao= servidor.DataPublicacao?.ToString("d"),
                OrdemBancaria = servidor.OrdemBancaria,
                DataPagamento = servidor.DataPagamento?.ToString("d"),
                Servidores = _diariasEmpresasDal.BuscarServidoresDiariasEmpresas(numeroProcesso, nome, empresa),

            };
            return Json(dadosRetorno, JsonRequestBehavior.AllowGet);
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}