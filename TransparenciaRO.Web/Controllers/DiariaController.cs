﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Web.Controllers
{
    public class DiariaController : ReadOnlyControllerBase
    {
        private readonly DbTransparencia _dbTransparenciab = new DbTransparencia();
        private readonly OrgaoDAL _orgaoDal;
        private readonly DiariaDAL _diariaDal;
        private readonly DiariasSugespeDAL _diariasSugespeDAL;

        public DiariaController()
        {
            _orgaoDal = new OrgaoDAL(_dbTransparenciab);
            _diariaDal = new DiariaDAL(_dbTransparenciab);
            _diariasSugespeDAL = new DiariasSugespeDAL(_dbTransparenciab);
        }

        public override ActionResult Index()
        {
            ViewBag.UnidadesGestoras = _orgaoDal.GetOrgaosConsolidadosPorNome().Union(new Dictionary<string, string> { { "0", "TODOS" } }).OrderBy(o => o.Value).ToDictionary(k => k.Key, v => v.Value);
            return View();
        }

        [HttpPost]
        public JsonResult Index(string dataInicial, string dataFinal, string UG, string Nome)
        {
            var ugs = UG == "0" ? new List<string>() : UG.Split(',').ToList();
            var dataInicialConvertida = DateTime.Parse(dataInicial).Date;
            var dataFinalConvertida = DateTime.Parse(dataFinal).Date;

            return Json(new
            {
                dados = _diariaDal.ObterDiariasPorFiltro(dataInicialConvertida, dataFinalConvertida, Nome, ugs).ToList()
                .Select(d => d.ToListarDiariasViewModel())

            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Detalhes(string cpf) => View(_diariasSugespeDAL.ObterPorCPF(cpf));

        public ActionResult ObterDiariasPorAnoFormatoCSV(int ano)
        {
            var diarias = _diariasSugespeDAL.ObterPorAno(ano);

            var sb = new System.Text.StringBuilder();

            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                            "Solicitacao",
                            "Matricula",
                            "Nome",
                            "Cargo",
                            "UnidadeGestora",
                            "DataIda",
                            "DataVolta",
                            "QuantidadeDiarias",
                            "ValorDiarias",
                            "TotalDiarias",
                            "TipoViagem",
                            "Objetivo"
                            );

            sb.Append(Environment.NewLine);

            foreach (var diaria in diarias)
            {

                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                           diaria.Solicitacao,
                           diaria.Matricula,
                           diaria.Nome,
                           diaria.Cargo,
                           diaria.UnidadeGestora,
                           diaria.DataIda,
                           diaria.DataVolta,
                           diaria.QuantidadeDiarias,
                           diaria.ValorDiarias,
                           diaria.TotalDiarias,
                           diaria.TipoViagem,
                           TratarStringComQuebrasDeLinha(diaria.Objetivo)
               );


                sb.Append(Environment.NewLine);
            }

            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"Diarias-{ano}.csv");
        }

        private string TratarStringComQuebrasDeLinha(string objetivo)
        {
            var texto = objetivo.Replace("\n", "")
                                .Replace("\r\n", "")
                                .Replace("\n\r", "")
                                .Replace("\r", "");

            return texto;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparenciab.Dispose();

            base.Dispose(disposing);
        }
    }
}