﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.ComiteTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers
{
    public class ComiteTransparenciaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ComiteTransparenciaDAL _comiteTransparenciaDAL;
        private readonly ArquivoDAL _arquivoDAL;

        public ComiteTransparenciaController()
        {
            _comiteTransparenciaDAL = new ComiteTransparenciaDAL(_dbTransparencia);
            _arquivoDAL = new ArquivoDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Integrantes()
        {
            return View();
        }

        public ActionResult Publicacoes()
        {
            return View();
        }

        public ActionResult Normativos()
        {
            return View();
        }

        public ActionResult EditalInscricoes()
        {
            return View();
        }
        public ActionResult RelatorioCovid()
        {
            return View();
        }

        public ActionResult Inscricao()
        {
            var inscricaoVM = new InscricaoComiteTransparenciaViewModel();

            return View(inscricaoVM);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Inscricao(InscricaoComiteTransparenciaViewModel inscricaoVM)
        {
            var documentos = Request.Files;

            if (ModelState.IsValid)
            {
                var protocolo = _comiteTransparenciaDAL.AdicionarInscricao(inscricaoVM);

                var mensagemErros = _arquivoDAL.AdicionarArquivosInscricaoComiteTransparencia(inscricaoVM.InscricaoId, documentos);

                if (mensagemErros.Count > 0)
                {
                    _comiteTransparenciaDAL.ReomverInscrito(inscricaoVM.InscricaoId);

                    inscricaoVM.Mensagem = "Ooops... \n\nOcorreu um problema ao realizar sua inscrição." +
                        "\n\nCaso o problema persista entrar em contato com : transparencia.cge.ro@gmail.com";

                    return Json(new { Sucess = false, inscricaoVM.Mensagem });
                }
                else
                {
                    inscricaoVM.Mensagem = "Inscrição realizada com sucesso! Seu número de protocolo é: " + protocolo;

                    return Json(new { Success = true, inscricaoVM.Mensagem });
                }
            }

            inscricaoVM.Mensagem = "Por favor, preencha todos os campos e tente novamente.";

            return Json(new { Success = false, inscricaoVM.Mensagem });
        }

        [HttpPost]
        public void AdicionarArquivo(InscricaoComiteTransparenciaViewModel inscricaoVM, string tipoArquivo, dynamic file)
        {
            var erros = new List<string>();

            if (ArquivoUtils.ArquivoValido(Request.Files[0]))
            {
                var arquivo = new Arquivo()
                {
                    ArquivoId = Guid.NewGuid(),
                    InscricaoComiteTransparenciaId = inscricaoVM.InscricaoId,
                    TipoAnexo = (ETipoAnexo)Enum.Parse(typeof(ETipoAnexo), tipoArquivo),
                    Extensao = Path.GetExtension(Request.Files[0].FileName),
                    Descricao = Request.Files[0].FileName,
                    PastaId = GuidUtils.GuidPastaComiteTransparencia(),
                    Temporario = true
                };

                if (ArquivoUtils.SalvarArquivo(Request.Files[0], arquivo.ArquivoId))
                {
                    _arquivoDAL.Adicionar(arquivo);
                }
                else
                    erros.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
            }
            else
                erros.Add($"Não foi possível fazer o upload do arquivo {Request.Files[0].FileName}. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");

            ViewBag.MensagensErrosArquivos = erros;
        }

        [Authorize]
        public ActionResult Inscritos()
        {
            var inscricoesComiteTransparenciaVM = _comiteTransparenciaDAL.BuscarInscricoes();

            return View(inscricoesComiteTransparenciaVM);
        }

        public ActionResult FiltrarInscricoes(string Cnpj = null, string RazaoSocial = null, string RepresentanteLegal = null, string Telefone = null)
        {
            var inscritos = _comiteTransparenciaDAL.FiltrarInscricoes(i =>
                (String.IsNullOrEmpty(Cnpj) || i.Cnpj.Contains(Cnpj))
                && (String.IsNullOrEmpty(RazaoSocial) || i.RazaoSocial.Contains(RazaoSocial))
                && (String.IsNullOrEmpty(RepresentanteLegal) || i.RazaoSocial.Contains(RepresentanteLegal))
                && (String.IsNullOrEmpty(Telefone) || i.RazaoSocial.Contains(Telefone))
            );

            return View("Partials\\_TabelaInscritos", inscritos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}