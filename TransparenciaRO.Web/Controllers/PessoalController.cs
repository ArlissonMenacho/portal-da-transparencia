﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Web.Model;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class PessoalController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly RemuneracaoServidorDAL _remuneracaoServidorDal;
        private readonly AcrescimoRendimentosDAL _acrescimoRendimentosDAL;

        public PessoalController()
        {
            _remuneracaoServidorDal = new RemuneracaoServidorDAL(_dbTransparencia);
            _acrescimoRendimentosDAL = new AcrescimoRendimentosDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            ViewBag.Ano = _remuneracaoServidorDal.Ano();
            ViewBag.Mes = _remuneracaoServidorDal.Mes(ViewBag.Ano);

            return View();
        }

        public ActionResult DetalheServidor(int ano, int mes, string matricula)
        {
            return View(_remuneracaoServidorDal.RemuneracaoServidor(ano, mes, matricula));
        }

        [Authorize(Roles = nameof(AdministradorRenumeracaoServidor))]
        public ActionResult ImportarDados()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file, int ano, int mes)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';"))
                    {
                        db.Open();
                        var renumeracaoServidores = new List<RemuneracaoServidor>();
                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                                var renumeracaoServidor = new RemuneracaoServidor
                                {
                                    Ano = (short)ano,
                                    Mes = (byte)mes,
                                    Matricula = dr.GetValue(dr.GetOrdinal("Matricula")).ToString().Trim(),
                                    Sequencia = Convert.ToInt16(dr.GetValue(dr.GetOrdinal("Sequencia"))),
                                    Nome = dr.GetString(dr.GetOrdinal("Nome")).Trim(),
                                    Cpf = dr.GetValue(dr.GetOrdinal("CPF")).ToString().Trim(),
                                    Cargo = dr.GetString(dr.GetOrdinal("Cargo/Funcao")).Trim(),
                                    Classificacao_Func = dr.GetString(dr.GetOrdinal("Classificacao_Func")).Trim(),
                                    Lotacao = dr.GetString(dr.GetOrdinal("Lotacao")).Trim(),
                                    Vencimentos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Vencimentos"))),
                                    Auxilios = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Auxilios"))),
                                    Vantagens = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Vantagens"))),
                                    VbaTemporarias = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("VbaTemporarias"))),
                                    VbaProdutividade = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("verbasprodutividade"))),
                                    VbaPrevidencias = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("verbasprevidencias"))),
                                    VbaImpostorenda = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("verbasimpostorenda"))),
                                    VbaDescontosDiversos = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("verbasdescontosdiversos"))),
                                    RendimentosTributaveis = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Rendi_Tributavel"))),
                                    TotalDescontosFinanceiros = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("TotDscFin"))),
                                    Liquido = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Liquido")))
                                };

                                renumeracaoServidores.Add(renumeracaoServidor);

                                LogUtils.Logar($"usuário: {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados da remuneração do servidor {renumeracaoServidor.Nome} -:", AuthenticationManager.User.IdUsuario());
                            }

                            _remuneracaoServidorDal.VerificarRemoverDados(ano, mes);
                            _remuneracaoServidorDal.AdicionarDados(renumeracaoServidores);


                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    return RedirectToAction("ImportarDados", "Pessoal");
                }
                MensagemErro = "Arquivo inválido, o tipo do arquivo deve ser xls ou xlsx";
                return View();
            }
            catch
            {
                MensagemErro = "Ocorreu um erro ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return RedirectToAction("ImportarDados", "Pessoal");
            }
        }

        public ActionResult Grafico()
        {
            ViewBag.Ano = _remuneracaoServidorDal.Ano();
            ViewBag.Mes = _remuneracaoServidorDal.Mes(ViewBag.Ano);

            return View();
        }

        public ActionResult RelacaoCdsFg()
        {
            ViewBag.Ano = _remuneracaoServidorDal.Ano();
            ViewBag.Mes = _remuneracaoServidorDal.Mes(ViewBag.Ano);
            return View();
        }

        public ActionResult RelacaoCedencia()
        {
            return View(UgsApi());
        }

        public ActionResult DownloadSalariosDosServidoresCSV()
        {
            var ano = 2021;

            var servidores = _remuneracaoServidorDal.ObterTodosOsServidoresPorAno(ano);

            var sb = new StringBuilder();

            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20}",
                            "Ano",
                            "Mes",
                            "Matricula",
                            "Nome",
                            "Cargo",
                            "Classificacao_Func",
                            "Lotacao",
                            "Vencimentos",
                            "Auxilios",
                            "Vantagens",
                            "VbaTemporarias",
                            "VbaProdutividade",
                            "VbaPrevidencias",
                            "VbaImpostorenda",
                            "VbaDescontosDiversos",
                            "RendimentosTributaveis",
                            "TotalDescontosFinanceiros",
                            "Liquido",
                            "CargaHoraria",
                            "CDSFG",
                            "SubLotacao"
                            );

            sb.Append(Environment.NewLine);

            foreach (var servidor in servidores)
            {
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20}",
                                servidor.Ano,
                                servidor.Mes,
                                servidor.Matricula,
                                servidor.Nome,
                                servidor.Cargo,
                                servidor.Classificacao_Func,
                                servidor.Lotacao,
                                servidor.Vencimentos,
                                servidor.Auxilios,
                                servidor.Vantagens,
                                servidor.VbaTemporarias,
                                servidor.VbaProdutividade,
                                servidor.VbaPrevidencias,
                                servidor.VbaImpostorenda,
                                servidor.VbaDescontosDiversos,
                                servidor.RendimentosTributaveis,
                                servidor.TotalDescontosFinanceiros,
                                servidor.Liquido,
                                servidor.CargaHoraria,
                                servidor.CDSFG,
                                servidor.SubLotacao
                                );
                sb.Append(Environment.NewLine);
            }

            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"SalarioServidores{ano}.csv");
        }
        //Json

        public JsonResult ResultadoCdsFg(int ano, int mes, string lotacao)
        {
            var dados = _remuneracaoServidorDal.RelacaoCdsFg(ano, mes, lotacao)
                .GroupBy(x => new { x.CDSFG })
                .Select(r => new
                {
                    Simbolo = r.Key.CDSFG,
                    Total = r.Count()
                });

            return Json(dados, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResultadoFiltro(int ano, int mes, string nome, string cpf, string lotacao)
        {
            var cpfSoNumeros = !cpf.IsNullOrEmpty()
                ? Convert.ToInt64(cpf.Replace(".", "").Replace("-", "")).ToString()
                : cpf;

            var resultado = Json(_remuneracaoServidorDal.FolhasPagamentos(ano, mes, nome, cpfSoNumeros, lotacao), JsonRequestBehavior.AllowGet);
            resultado.MaxJsonLength = int.MaxValue;

            return resultado;
        }

        public JsonResult MesesAno(int ano)
        {
            var meses = _remuneracaoServidorDal.MesesAno(ano).Select(m => new
            {
                mesInt = m,
                mesString = m == 13 ? "Décimo Terceiro" : m.NomeMes()
            });

            return Json(meses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Lotacoes(int ano, int mes)
        {
            var lotacoes = _remuneracaoServidorDal.Lotacoes(ano, mes).Select(l => new
            {
                Id = l,
                Descricao = l
            });

            return Json(lotacoes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Classificacoes(int ano, int mes, string faixaSalarial)
        {

            var classificacoes = _remuneracaoServidorDal.Classificacoes(ano, mes, faixaSalarial).Select(c => new
            {
                Classificacao = c.Key.Trim(),
                Qtd = c.Value
            });

            return Json(classificacoes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FaixasSalariais(int ano, int mes)
        {
            var faixasSalariais = _remuneracaoServidorDal.FaixasSalariais(ano, mes).Select(f => new
            {
                FaixaSalarial = f.Key,
                QtdServidores = f.Value
            })
            .OrderBy(fs => fs.FaixaSalarial == "Acima de 16 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 8 até 16 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 4 até 8 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 2 até 4 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 1 até 2 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "Até 1 Salário Mínimo Federal");

            return Json(faixasSalariais, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FaixaSalarialLotacoes(int ano, int mes, string faixaSalarial)
        {
            return Json(new
            {
                FaixaSalarial = faixaSalarial,
                data = _remuneracaoServidorDal.FaixasSalariaisLotacoes(ano, mes, faixaSalarial).Select(rs => new object[]
                {
                    rs.Key,
                    rs.Value
                })
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FaixaSalarialFuncionarios(int ano, int mes, string faixaSalarial, string classificacao, string lotacao)
        {
            var faixasSalariais = _remuneracaoServidorDal.FaixasSalariaisFuncionarios(ano, mes, faixaSalarial, classificacao, lotacao).Select(f => new
            {
                Servidor = f.Nome,
                Cargo = f.Cargo,
                Ano = f.Ano,
                Mes = f.Mes,
                Matricula = f.Matricula,
                UnidadeGestora = f.Lotacao,
                Classificacao = f.Classificacao_Func
            });

            return Json(faixasSalariais, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LotacaoFaixasSalariais(int ano, int mes, string lotacao)
        {
            var faixasSalariais = _remuneracaoServidorDal.LotacaoFaixasSalariais(ano, mes, lotacao).Select(f => new
            {
                FaixaSalarial = f.Key,
                QtdServidores = f.Value
            })
            .OrderBy(fs => fs.FaixaSalarial == "Acima de 16 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 8 até 16 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 4 até 8 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 2 até 4 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "De 1 até 2 Salários Mínimos Federais")
            .ThenBy(fs => fs.FaixaSalarial == "Até 1 Salário Mínimo Federal");

            return Json(faixasSalariais, JsonRequestBehavior.AllowGet);
        }



        public JsonResult ResultadoCedencia(int id)
        {
            var resultado = ServidoresApi(id).GroupBy(x => new
            {
                x.Nome,
                x.Matricula,
                x.DataNomeacao,
                x.DataAdmissao,
                x.NumeroDecreto,
                x.NumeroDiof,
                x.OrgaoOrigem,
                x.Lotacao,
                x.Setor
            }).Select(r => new
            {
                Nome = r.Key.Nome,
                Matricula = r.Key.Matricula,
                DataNomeacao = r.Key.DataNomeacao.ToString("d"),
                DataAdmissao = r.Key.DataAdmissao.ToString("d"),
                NumeroDecreto = r.Key.NumeroDecreto == "0" || r.Key.NumeroDecreto == "" || r.Key.NumeroDecreto == null ? "Não informado pela unidade" : r.Key.NumeroDecreto,
                NumeroDiof = r.Key.NumeroDiof == "0" || r.Key.NumeroDiof == "" || r.Key.NumeroDiof == null ? "Não informado pela unidade" : r.Key.NumeroDiof,
                OrgaoOrigem = r.Key.OrgaoOrigem,
                Lotacao = r.Key.Lotacao,
                Setor = r.Key.Setor
            });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DetalheRendimentosTemporarios(string cpf, short ano, short mes)
        {
            var resultado = _acrescimoRendimentosDAL.ObterTemporariasPorCpf(cpf, ano, mes);
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DetalheRendimentosVantagens(string cpf, short ano, short mes)
        {
            var resultado = _acrescimoRendimentosDAL.ObterVantagensPorCpf(cpf, ano, mes);
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        //Conection Api
        private List<UnidadeGestoraApi> UgsApi()
        {
            var ugs = new List<UnidadeGestoraApi>();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://is.cge.ro.gov.br/api/unidadegestoraapi");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var retorno = JsonConvert.DeserializeObject<List<UnidadeGestoraApi>>(streamReader.ReadToEnd());

                    if (retorno != null)
                        ugs = retorno;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return ugs;
        }
        private List<ServidorApi> ServidoresApi(int id)
        {
            var servidores = new List<ServidorApi>();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://is.cge.ro.gov.br/api/servidorapi/getservidor?id=" + id);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var retorno = JsonConvert.DeserializeObject<List<ServidorApi>>(streamReader.ReadToEnd());

                    if (retorno != null)
                        servidores = retorno;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return servidores;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}