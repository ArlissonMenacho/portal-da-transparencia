﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Claims;
using TransparenciaRO.Infra.Constantes;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Web.Controllers
{
    public abstract class ControllerBase : Controller
    {
        public string MensagemErro { get { return TempData["Erro"].ToString(); } set { TempData["Erro"] = value; } }
        public string MensagemSucesso { get { return TempData["Sucesso"].ToString(); } set { TempData["Sucesso"] = value; } }

        internal IAuthenticationManager AuthenticationManager => System.Web.HttpContext.Current.GetOwinContext().Authentication;

        public List<Guid> GetPastasUsuario()
        {
            if (!User.Identity.IsAuthenticated)
                return null;
            return ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimsCustomizadas.Pastas).Select(c => new Guid(c.Value)).ToList();
        }

        public List<EPerfilUsuario> GetPermissoesUsuario()
        {
            if (!User.Identity.IsAuthenticated)
                return new List<EPerfilUsuario>();
            return ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => (EPerfilUsuario)Enum.Parse(typeof(EPerfilUsuario), c.Value)).ToList();
        }
    }

    public abstract class ReadOnlyControllerBase : ControllerBase
    {
        public abstract ActionResult Index();
    }

    public abstract class CRUDControllerBase<T> : ReadOnlyControllerBase
    {
        public abstract ActionResult AdicionarEditar(string pEncId);
        public abstract ActionResult Remover(string pEncId);

        [HttpPost, ActionName(nameof(CRUDControllerBase<T>.Remover)), ValidateAntiForgeryToken]
        public abstract ActionResult ConfirmaRemocao(string pEncId);

        [HttpPost]
        public abstract ActionResult AdicionarEditar(T pDados);
    }
}