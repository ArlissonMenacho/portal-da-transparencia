﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TransparenciaRO.Infra.Model.View.OrnanogramaAPISETIC;
using TransparenciaRO.Infra.Model.View.SkalaAPISETIC;
using TransparenciaRO.Web.servi;
using TransparenciaRO.Web.Services.Ornanograma;

namespace TransparenciaRO.Web.Controllers
{
    public class SkalaController : Controller
    {        
        public async Task<ActionResult> Index()
        {
            var skalaAPI = new Skala();

            var ornanogramaAPI = new OrnanogramaAPI();

            var filtro = new FiltroSkalaViewModel();

            var retornoVM = await skalaAPI.ObterDados(ESkalaTipoFiltro.DataPlantao, null, DateTime.Now.Date.ToShortDateString() + " - " + DateTime.Now.Date.ToShortDateString(),"",0);

            var retornoOrgaosSESAU = await ornanogramaAPI.ObterUnidadesSESAU();

            filtro.Unidades = retornoOrgaosSESAU.Sucesso == true ? retornoOrgaosSESAU.Dados : new List<Unidade>();

            var tabelaVM = new TabelaSkalaViewModel
            {
                Sucesso = retornoVM.Sucesso,
                Servidores = retornoVM.Dados,
                Mensagem = retornoVM.Mensagem
            };

            var indexSkalaVM = new IndexSkalaViewModel()
            {
                Filtro = filtro,
                TabelaSkalaViewModel = tabelaVM
            };

            return View(indexSkalaVM);
        }

        [HttpPost]
        public async Task<ActionResult> Filtrar(ESkalaTipoFiltro ESkalaTipoFiltro, string Descricao, string Data,string Unidade, ETipoPlatao tipoPlantao)
        {
            var skala = new Skala();
            var tabelaSkalaVM = new TabelaSkalaViewModel();

            var retornoVM = await skala.ObterDados(ESkalaTipoFiltro, Descricao, Data,Unidade,tipoPlantao);

            tabelaSkalaVM.Sucesso = retornoVM.Sucesso;
            tabelaSkalaVM.Servidores = retornoVM.Dados;
            tabelaSkalaVM.Mensagem = retornoVM.Mensagem;

            return PartialView("Partials\\_Tabela", tabelaSkalaVM);
        }
    }
}