﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class RelacaoPensionistaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly RelacaoPensionistaDAL _relacaoPensionistaDal;
        private readonly LotacaoDAL _lotacaoDal;

        public RelacaoPensionistaController()
        {
            _relacaoPensionistaDal = new RelacaoPensionistaDAL(_dbTransparencia);
            _lotacaoDal = new LotacaoDAL(_dbTransparencia);
        }

        //Actions
        [Authorize(Roles = nameof(AdministradorRelacaoServidor))]
        public ActionResult ImportarDados()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file, int ano, int mes)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    //Rotina de importação dos dados
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';"))
                    {
                        db.Open();
                        var lotacoesBD = _lotacaoDal.ObterTodos().ToList();
                        var lotacoes = new List<Lotacao>();
                        var relacaoPensionistas = new List<RelacaoPensionista>();

                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            
                            while (dr != null && dr.Read())
                            {
                                var lotacao = new Lotacao
                                {
                                    Lot = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("Lot"))),
                                    Descricao = dr.GetString(dr.GetOrdinal("Lotacao")).Trim(),
                                };

                                if (lotacoes.Any(x => x.Lot == lotacao.Lot) || lotacoesBD.Any(x => x.Lot == lotacao.Lot))
                                    lotacao = lotacoesBD.FirstOrDefault(x => x.Lot == lotacao.Lot) ?? lotacoes.FirstOrDefault(x => x.Lot == lotacao.Lot);

                                var relacaoServidor = new RelacaoPensionista
                                {
                                    Matricula = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("Matrícula"))),
                                    Nome = dr.GetString(dr.GetOrdinal("Nome")).Trim(),
                                    Cpf = Convert.ToInt64(dr.GetValue(dr.GetOrdinal("CPF")).ToString()),
                                    Cargo = dr.GetString(dr.GetOrdinal("Cargo")).Trim(),
                                    DataNascimento = dr.GetDateTime(dr.GetOrdinal("Dt#Nasc#")),
                                    Admissao = dr.GetDateTime(dr.GetOrdinal("Admissão")),
                                    CargaHoraria = (int)dr.GetValue(dr.GetOrdinal("Carga Horaria")).ToString().Remove(2).ToInt(),
                                    Classificacao = dr.GetString(dr.GetOrdinal("Classificação")).Trim(),
                                    LocalTrabalho = dr.GetString(dr.GetOrdinal("LocalTrab")).Trim(),
                                    Vinculo = dr.GetString(dr.GetOrdinal("vinculo")).Trim(),
                                    Ano = ano,
                                    Mes = mes,
                                    LotacaoId = lotacao.LotacaoId
                                };

                                relacaoPensionistas.Add(relacaoServidor);

                                LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados da relação pensionista {relacaoServidor.Nome} -:", AuthenticationManager.User.IdUsuario());

                                if (lotacoes.All(x => x.Lot != lotacao.Lot) && lotacoesBD.All(x => x.Lot != lotacao.Lot))
                                    lotacoes.Add(lotacao);
                            }
                            _relacaoPensionistaDal.VerificarDadosRemover(ano, mes);
                            _lotacaoDal.AdicionarDados(lotacoes);
                            _relacaoPensionistaDal.AdicionarDados(relacaoPensionistas);
                            _lotacaoDal.Salvar();             
                        }
                    }
                    System.IO.File.Delete(Server.MapPath("~/App_Data/Arquivos/" + "Servidores.csv"));
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {(DateTime.Now - tempoInicial).TotalSeconds} segundos";
                    return RedirectToAction("ImportarDados");
                }
                MensagemErro = "Arquivo inválido";
                return View();
            }
            catch
            {
                MensagemErro = "Ocorreu um erro ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return RedirectToAction("ImportarDados");
            }
        }

        public ActionResult Grafico()
        {
            ViewBag.Ano = _relacaoPensionistaDal.Ano();
            ViewBag.Mes = _relacaoPensionistaDal.Mes(ViewBag.Ano);

            return View();
        }

        //Json
        public JsonResult Meses(int ano)
        {
            var retorno = _relacaoPensionistaDal.Meses(ano).Select(m => new
            {
                MesInt = m,
                MesString = m.NomeMes()
            }).OrderByDescending(m => m.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnidadesGestoras(int ano, int mes)
        {
            var retorno = _lotacaoDal.BuscarLotacaoesPorAnoMes(ano, mes).Select(l => new
            {
                UnidadeGestoraId = l.LotacaoId,
                Descricao = l.Descricao.Trim()
            });

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoUnidadeGestora(int ano, int mes)
        {
            var retorno = _relacaoPensionistaDal.RelacaoLotacao(ano, mes).Select(l => new
            {
                UnidadeGestora = l.Key,
                QtdLotado = l.Value
            }).OrderByDescending(l => l.QtdLotado);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoCargo(int ano, int mes, string unidadeGestoraId)
        {
            var retorno = _relacaoPensionistaDal.RelacaoCargo(String.IsNullOrEmpty(unidadeGestoraId) ? null : unidadeGestoraId, ano, mes).Select(r => new
            {
                UnidadeGestoraId = unidadeGestoraId,
                Cargo = r.Key.Trim(),
                QtdServidores = r.Value
            }).OrderByDescending(r => r.QtdServidores);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelacaoClassificacao(int ano, int mes, string unidadeGestoraId)
        {
            var retorno = _relacaoPensionistaDal.RelacaoClassificacao(String.IsNullOrEmpty(unidadeGestoraId) ? null : unidadeGestoraId, ano, mes).Select(r => new
            {
                UnidadeGestoraId = unidadeGestoraId,
                Classificacao = r.Key.Trim(),
                QtdServidores = r.Value
            }).OrderByDescending(r => r.QtdServidores);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MediaIdadeUnidadeGestora(int ano, int mes)
        {
            var retorno = _relacaoPensionistaDal.MediaIdadeUnidadeGestora(ano, mes).Select(r => new
            {
                UnidadeGestora = r.Key.Trim(),
                MediaIdade = Convert.ToInt32(r.Value)
            }).OrderByDescending(r => r.MediaIdade);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MediaIdadeCargo(int ano, int mes, string unidadeGestoraId)
        {
            var retorno = _relacaoPensionistaDal.MediaIdadeCargo(String.IsNullOrEmpty(unidadeGestoraId) ? null : unidadeGestoraId, ano, mes).Select(r => new
            {
                UnidadeGestoraId = unidadeGestoraId,
                Cargo = r.Key.Trim(),
                MediaIdade = Convert.ToInt32(r.Value)
            }).OrderByDescending(r => r.MediaIdade);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MediaIdadeClassificacao(int ano, int mes, string unidadeGestoraId)
        {
            var retorno = _relacaoPensionistaDal.MediaIdadeClassificacao(String.IsNullOrEmpty(unidadeGestoraId) ? null : unidadeGestoraId, ano, mes).Select(r => new
            {
                UnidadeGestoraId = unidadeGestoraId,
                Classificacao = r.Key.Trim(),
                MediaIdade = Convert.ToInt32(r.Value)
            }).OrderByDescending(r => r.MediaIdade);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Funcionarios(int ano, int mes, string unidadeGestora, string cargo, string classificacao)
        {
            var retorno = _relacaoPensionistaDal.Funcionarios(ano, mes, unidadeGestora, cargo, classificacao).ToList().Select(f => new
            {
                Ano = f.Ano,
                Mes = f.Mes,
                Matricula = f.Matricula,
                Nome = f.Nome,
                Cargo = f.Cargo,
                Classificacao = f.Classificacao
            }).Take(100);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();
            
            base.Dispose(disposing);
        }
    }
}