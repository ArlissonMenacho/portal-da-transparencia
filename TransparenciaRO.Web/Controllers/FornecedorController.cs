﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using System.Web.Script.Serialization;
using CaptchaMvc.HtmlHelpers;
using Newtonsoft.Json;
using reCAPTCHA.MVC;
using TransparenciaRO.Infra.Constantes;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Web.Controllers
{
    public class FornecedorController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia;
        private readonly FornecedorDAL _fornecedorDal;
        private readonly CidadeDAL _cidadeDal;
        private readonly ImpedirFornecedorDAL _impedirFornecedorDal;
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly SocioFornecedorDAL _socioFornecedorDal;
        private readonly CertidaoDAL _certidaoDal;
        private readonly OrgaoDAL _orgaoDal;
        private readonly ParametrosDAL _parametrosDal;
        private readonly PagamentoFornecedorDAL _pagamentoFornecedorDal;
        private readonly PagamentoFornecedorRPVsDAL _pagamentoFornecedorRpvsDal;

        private readonly ValidarCpfCnpjUtils _validarCpfCnpjUtils;

        public FornecedorController()
        {
            _dbTransparencia = new DbTransparencia();
            _impedirFornecedorDal = new ImpedirFornecedorDAL(_dbTransparencia);
            _fornecedorDal = new FornecedorDAL(_dbTransparencia);
            _cidadeDal = new CidadeDAL(_dbTransparencia);
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _socioFornecedorDal = new SocioFornecedorDAL(_dbTransparencia);
            _certidaoDal = new CertidaoDAL(_dbTransparencia);
            _orgaoDal = new OrgaoDAL(_dbTransparencia);
            _parametrosDal = new ParametrosDAL(_dbTransparencia);
            _pagamentoFornecedorDal = new PagamentoFornecedorDAL(_dbTransparencia);
            _pagamentoFornecedorRpvsDal = new PagamentoFornecedorRPVsDAL(_dbTransparencia);

            _validarCpfCnpjUtils = new ValidarCpfCnpjUtils();
        }

        public ActionResult AdicionarFornecedor()
        {
            ViewBag.EditarFornecedores = GetPermissoesUsuario().Contains(AdministradorFornecedoresImpedimentos);

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AdicionarFornecedor(FornecedorViewModel fornecedor)
        {
            ViewBag.EditarFornecedores = GetPermissoesUsuario().Contains(AdministradorFornecedoresImpedimentos);

            MensagemErro = "";

            if (!ModelState.IsValid)
                return View(fornecedor);

            if (_fornecedorDal.BuscarFornecedorPorCpfCnpj(fornecedor.CpfCnpj) != null)
                MensagemErro = "O CPF / CNPJ informado já está cadastrado!";

            var validarCpfCnpj = fornecedor.TipoPessoa == ETipoPessoa.Fisica
                ? _validarCpfCnpjUtils.ValidarCpf(fornecedor.CpfCnpj)
                : _validarCpfCnpjUtils.ValidarCnpj(fornecedor.CpfCnpj);

            if (!validarCpfCnpj)
                MensagemErro = "O CPF / CNPJ informadado não é valido";

            if (!MensagemErro.IsNullOrEmpty())
                return View(fornecedor);

            _fornecedorDal.Adicionar(fornecedor.ModelFornecedor());
            MensagemSucesso = "Fornecedor adionado com sucesso";

            var usuarioAutenticado = AuthenticationManager.User.Identity.IsAuthenticated;
            return RedirectToAction(usuarioAutenticado ? "AdicionarFornecedor" : "EmitirCertidao", "Fornecedor");
        }

        public ActionResult EditarFornecedor(string fornecedorId)
        {
            var fornecedorGuid = CriptografiaUtils.Decrypt(fornecedorId);
            var fornecedor = _fornecedorDal.BuscarFornecedor(fornecedorGuid.ToGuid());

            fornecedor.CpfCnpj = fornecedor.CpfCnpj.Length <= 11
                    ? Convert.ToUInt64(fornecedor.CpfCnpj).ToString(@"000\.000\.000\-00")
                    : Convert.ToUInt64(fornecedor.CpfCnpj).ToString(@"00\.000\.000\/0000\-00");

            fornecedor.TipoPessoa = fornecedor.CpfCnpj.Length <= 11
                ? ETipoPessoa.Fisica
                : ETipoPessoa.Juridica;

            return View(new FornecedorViewModel
            {
                CpfCnpj = fornecedor.CpfCnpj,
                RazaoSocial = fornecedor.RazaoSocial,
                CidadeId = fornecedor.CidadeId ?? new Guid(),
                Cidade = fornecedor.Cidade,
                TipoPessoa = fornecedor.TipoPessoa,
                Endereco = fornecedor.Endereco,
                FornecedorId = fornecedor.FornecedorId,
                Uf = fornecedor.Uf.Value
            });
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult EditarFornecedor(FornecedorViewModel fornecedor)
        {
            if (!ModelState.IsValid)
                return View(fornecedor);

            _fornecedorDal.Editar(fornecedor.ModelFornecedor());

            MensagemSucesso = "Os dados do fornecedor foram atualizados com sucesso";
            var usuarioAutenticado = AuthenticationManager.User.Identity.IsAuthenticated;
            return RedirectToAction(usuarioAutenticado ? "AdicionarFornecedor" : "EmitirCertidao", "Fornecedor");
        }

        [Authorize(Roles = nameof(AdministradorFornecedoresImpedimentos))]
        public ActionResult AdicionarImpedimento()
        {
            ViewBagUnidadeGestora();

            return View(new FornecedorImpedidoViewModel());
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AdicionarImpedimento(FornecedorImpedidoViewModel fornecedorImpedido)
        {
            ViewBagUnidadeGestora();

            if (!ModelState.IsValid)
                return View(fornecedorImpedido);

            if (_impedirFornecedorDal.VerificarSeContemImpedimento(fornecedorImpedido.NumeroProcessoSemFormatacao, fornecedorImpedido.Fornecedor.CpfCnpj, fornecedorImpedido.FornecedorImpedidoId))
            {
                MensagemErro = "Este impedimento está cadastrado!";
                return View(fornecedorImpedido);
            }

            fornecedorImpedido.DataInclusao = DateTime.Now;
            fornecedorImpedido.UsuarioId = new Guid(AuthenticationManager.User.IdUsuario().ToString());
            fornecedorImpedido.Fornecedor = null;

            _impedirFornecedorDal.Adicionar(fornecedorImpedido.ConverteModel());
            MensagemSucesso = "Adicionado com sucesso";
            return RedirectToAction("AdicionarImpedimento", "Fornecedor");
        }

        [Authorize(Roles = nameof(AdministradorFornecedoresImpedimentos))]
        public ActionResult EditarImpedimento(Guid fornecedorImpedidoId)
        {
            ViewBagUnidadeGestora();

            var fornecedorImpedido = _impedirFornecedorDal.BuscarFornecedor(fornecedorImpedidoId);
            return View(new FornecedorImpedidoViewModel().ConverteViewModel(fornecedorImpedido));
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult EditarImpedimento(FornecedorImpedidoViewModel fornecedorImpedido)
        {
            ViewBagUnidadeGestora();

            if (!ModelState.IsValid)
                return View(fornecedorImpedido);

            if (_impedirFornecedorDal.VerificarSeContemImpedimento(fornecedorImpedido.NumeroProcessoSemFormatacao, fornecedorImpedido.Fornecedor.CpfCnpj, fornecedorImpedido.FornecedorImpedidoId))
            {
                MensagemErro = "Este impedimento está cadastrado!";
                return View(fornecedorImpedido);
            }

            fornecedorImpedido.Fornecedor = null;
            fornecedorImpedido.DataUltimaAlteracao = DateTime.Now;
            fornecedorImpedido.UsuarioId = new Guid(AuthenticationManager.User.IdUsuario().ToString());

            _impedirFornecedorDal.Editar(fornecedorImpedido.ConverteModel());
            MensagemSucesso = "Alterado com sucesso";
            return RedirectToAction("AdicionarImpedimento");
        }

        [Authorize(Roles = nameof(AdministradorFornecedoresImpedimentos))]
        public ActionResult AdicionarSocio()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AdicionarSocio(SocioFornecedor socioFornecedor)
        {
            TempData["CodigoImpedimento"] = socioFornecedor.ImpedirFornecedorCodigo;

            if (!ModelState.IsValid) return View(socioFornecedor);

            _socioFornecedorDal.Adicionar(socioFornecedor);
            MensagemSucesso = "Sócio adicionado com sucesso";
            return RedirectToAction("AdicionarSocio");
        }

        [Authorize(Roles = nameof(AdministradorFornecedoresImpedimentos))]
        public ActionResult RemoverSocio(Guid socioFornecedorId)
        {
            var socio = _socioFornecedorDal.BuscarSocioFornecedor(socioFornecedorId);
            TempData["CodigoImpedimento"] = socio.ImpedirFornecedorCodigo;

            return View(socio);
        }

        [HttpPost, ActionName("RemoverSocio")]
        public ActionResult ConfirmaRemocao(Guid socioFornecedorId)
        {
            _socioFornecedorDal.BuscarSocioFornecedor(socioFornecedorId);
            TempData["CodigoImpedimento"] = _socioFornecedorDal.Remover(socioFornecedorId);

            return RedirectToAction("AdicionarSocio");
        }

        public ActionResult EmitirCertidao()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken, CaptchaValidator]
        public ActionResult EmitirCertidao(string cpfCnpj, bool captchaValid)
        {
            //if (captchaValid)
            if (this.IsCaptchaValid("Captcha is not valid"))
            {
                if (cpfCnpj.IsNullOrEmpty())
                {
                    MensagemErro = "Informe o CPF ou CNPJ";
                    return View("EmitirCertidao");
                }

                var fornecedor = _fornecedorDal.BuscarFornecedorPorCpfCnpj(cpfCnpj);
                if (fornecedor == null)
                {
                    return RedirectToAction("AdicionarFornecedor", "Fornecedor");
                }

                if (fornecedor.CidadeId == null || fornecedor.Endereco == null || fornecedor.Uf == null)
                {
                    return RedirectToAction("EditarFornecedor", "Fornecedor", new { fornecedorId = CriptografiaUtils.Encrypt(fornecedor.FornecedorId.ToString()) });
                }

                var impedido = _impedirFornecedorDal.VerificarSeFornecedorEstaImpedido(fornecedor.FornecedorId);
                var certidao = new Certidao
                {
                    Fornecedor = fornecedor,
                    Impedido = impedido
                };

                _certidaoDal.Adicionar(certidao);

                certidao.Fornecedor.CpfCnpj = certidao.Fornecedor.CpfCnpj.Length <= 11
                    ? Convert.ToUInt64(fornecedor.CpfCnpj).ToString(@"000\.000\.000\-00")
                    : Convert.ToUInt64(fornecedor.CpfCnpj).ToString(@"00\.000\.000\/0000\-00");

                return View("ModeloCertidao", certidao);
            }

            MensagemErro = "reCAPTCHA inválido";
            return View("EmitirCertidao");
        }

        public ActionResult DetalhesFornecedoresImpedidos()
        {
            return View(_impedirFornecedorDal.BuscarFornecedoresImpedidos());
        }

        public ActionResult HistoricoFornecedoresImpedidos()
        {
            return View(_impedirFornecedorDal.BuscarHistoricoDeEncerramentoFornecedoresImpedidos());
        }

        public ActionResult DetalharImpedimento(Guid impedimentoFornecedorId)
        {
            return View(_impedirFornecedorDal.BuscarImpedimento(impedimentoFornecedorId));
        }

        public ActionResult AutenticarCertidao()
        {
            return View("EmitirAutenticacaoDaCertidao");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AutenticarCertidao(string guid, DateTime dataEmissao, string cpfCnpj)
        {
            try
            {
                var autenticacaoId = Guid.Parse(guid.Replace("-", ""));
                var cpfCnpjSemFormatacao = cpfCnpj.Replace(".", "").Replace("/", "").Replace("-", "");

                var certidao = _certidaoDal.VerificarAutenticidadeDaCertidao(autenticacaoId, dataEmissao, cpfCnpjSemFormatacao);
                certidao.Fornecedor.CpfCnpj = certidao.Fornecedor.CpfCnpj.Length <= 11
                    ? Convert.ToUInt64(certidao.Fornecedor.CpfCnpj).ToString(@"000\.000\.000\-00")
                    : Convert.ToUInt64(certidao.Fornecedor.CpfCnpj).ToString(@"00\.000\.000\/0000\-00");

                return View("ModeloAutenticarCertidao", certidao);
            }
            catch (Exception)
            {
                MensagemErro = "O CPF/CNPJ, Código ou Data e Hora informado está inválido";
                return View("EmitirAutenticacaoDaCertidao");
            }
        }

        public ActionResult ListaEmpenhosFornecedores()
        {
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoDespesas);
            ViewBag.UnidadesGestoras = _orgaoDal.GetOrgaos().GroupBy(o => o.CodOrgao).Select(o => new { CodOrgao = o.Key, NomOrgao = o.Key.ToString("000000") + " - " + o.First().NomOrgao }).OrderBy(ug => ug.NomOrgao).ToDictionary(k => k.CodOrgao.ToString("000000"), v => v.NomOrgao);
            return View();
        }

        public ActionResult ListaItemEmpenhoUg(string empenho, string ug, string pDataInicial)
        {
            ViewBag.UG = _unidadeGestoraDal.GetNomeUg(ug);
            return View();
        }

        [Authorize(Roles = nameof(AdministradorPagamentoFornecedoresRpv))]
        public ActionResult ImportarDadosRpv()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDadosRpv(HttpPostedFileBase file)
        {
            var contadorDeLinhas = 1;

            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';"))
                    {
                        db.Open();
                        var pagamentoFornecedorRpv = new PagamentoFornecedor();
                        var pagamentoFornecedorRpvs = new List<PagamentoFornecedorRPV>();
                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                                contadorDeLinhas++;

                                string documentOb = dr.GetValue(dr.GetOrdinal("PREPARAÇÃO PAGAMENTO")).ToString().Trim();

                                if (!documentOb.IsNullOrEmpty())
                                {
                                    pagamentoFornecedorRpvs.Add(new PagamentoFornecedorRPV
                                    {
                                        DocumentoOB = dr.GetValue(dr.GetOrdinal("PREPARAÇÃO PAGAMENTO")).ToString().Trim(),
                                        DocCredor = dr.GetValue(dr.GetOrdinal("CPF/CNPJ")).ToString().Trim().Replace(".","").Replace("-",""),
                                        Credor = dr.GetValue(dr.GetOrdinal("CREDOR")).ToString().Trim(),
                                        NumeroProcesso = dr.GetValue(dr.GetOrdinal("PROCESSO SEI")).ToString().Trim(),
                                        DataOb = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DATA PAGAMENTO")).ToString()),
                                        Finalidade = dr.GetValue(dr.GetOrdinal("FINALIDADE")).ToString().Trim(),
                                        StatusOb = dr.GetValue(dr.GetOrdinal("STATUS")).ToString().Trim(),
                                        ValorPaga = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("VALOR"))),
                                        Movimentacao = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("MOVIMENTAÇÃO"))),
                                        CpfCnpjCessionario = dr.GetValue(dr.GetOrdinal("CPF/CNPJ CESSIONÁRIO")).ToString().Trim(),
                                        Cessionario = dr.GetValue(dr.GetOrdinal("CESSIONÁRIO")).ToString().Trim(),                                        
                                        DataAtualizacao = DateTime.Now
                                    });
                                }                                
                            }

                            contadorDeLinhas = 0;

                            _pagamentoFornecedorRpvsDal.AddList(pagamentoFornecedorRpvs);
                            _pagamentoFornecedorRpvsDal.UpdateList(pagamentoFornecedorRpvs);

                            LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados RPV de fornecedor:", AuthenticationManager.User.IdUsuario());
                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: { Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1) } segundos";
                    return RedirectToAction("ImportarDadosRpv", "Fornecedor");
                }
                MensagemErro = "Arquivo inválido, o tipo do arquivo deve ser xls ou xlsx";
                return View();
            }
            catch(Exception e)
            {
                MensagemErro = "Ocorreu um erro " + (contadorDeLinhas > 1 ? "na linha " + contadorDeLinhas.ToString() + " "  : "") + "ao tentar importar o arquivo, caso o erro persista entre em contato com o suporte: " + ConstantesDiversas.EmailCge;
                return RedirectToAction("ImportarDadosRpv", "Fornecedor");
            }
        }

        public ActionResult PagamentoFornecedoresRPV()
        {
            ViewBag.DataAtualizacao = _pagamentoFornecedorRpvsDal.RPVDataAtualizacao().DataAtualizacao;
            return View();
        }

        //ViewBag
        private void ViewBagUnidadeGestora()
        {
            var unidadeGestoras = _unidadeGestoraDal.GetUnidadesGestoras().Select(u => new
            {
                Text = $"{u.unidadeGestoraIDSIAFEM} - {u.nomeUG}",
                Value = u.unidadeGestoraID
            }).ToList();

            ViewBag.UnidadeGestoraList = new SelectList(unidadeGestoras, "Value", "Text");
        }

        //Json
        public JsonResult ListaCidade(int estadoId)
        {
            EEstado estado = (EEstado)Enum.ToObject(typeof(EEstado), estadoId);
            List<object> resultado = new List<object>();
            foreach (var cidade in _cidadeDal.BuscarCidadePorEstado(estado).OrderBy(x => x.Nome))
            {
                resultado.Add(new
                {
                    CidadeId = cidade.CidadeId,
                    Nome = cidade.Nome,
                    EstadoId = cidade.EstadoId
                });
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FornecedorImpedido(int codigoImpedimento)
        {
            var impedirFornecedor = _impedirFornecedorDal.BuscarFornecedorPorCodigo(codigoImpedimento);
            var dados = new
            {
                RazaoSocial = impedirFornecedor.Fornecedor.RazaoSocial,
                FornecedorImpedidoId = impedirFornecedor.FornecedorImpedidoId,
                Codigo = impedirFornecedor.Codigo
            };
            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Fornecedor(string cpfCnpj)
        {
            var fornecedor = _fornecedorDal.BuscarFornecedorPorCpfCnpjJson(cpfCnpj);
            return Json(fornecedor, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmpenhosFornecedores(string dataInicial, string dataFinal, string pCredor, string pDocCredor, string pUG)
        {
            var datInicial = DateTime.Parse(dataInicial).Date;
            var datFinal = DateTime.Parse(dataFinal).Date;

            var orgaos = _orgaoDal.GetOrgaos().ToList();
            return Json(new
            {
                dados = _pagamentoFornecedorDal.GetEmpenhosFornecedores(datInicial, datFinal, pCredor, pDocCredor, pUG).Select(d => new
                {
                    NumeroEmpenho = d.NumEmpenho,
                    Documento = d.DocumentoNE,
                    Credor = d.Credor,
                    Processo = d.Processo,
                    Data = d.DataDocumento.ToString("dd/MM/yyyy"),
                    Valor = d.ValorPaga?.ToString("C2"),
                    UnidadeGestora = orgaos.FirstOrDefault(o => o.CodOrgao.ToString("000000") == d.CodUnidadeGestora)?.NomOrgao,
                    LinkDetalhamento = new JavaScriptSerializer().Serialize(new LinkDetalhamentoEmpenhoFornecedor
                    {
                        DocCredor = d.DocCredor,
                        DocumentoNE = d.DocumentoNE,
                        EspecificacaoDespesa = d.EspecificacaoDespesa,
                        NumEmpenho = d.NumEmpenho,
                        Processo = d.Processo,
                        ValorDespesa = d.ValorPaga
                    }).ToEncrypted()
                })
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmpenhoUg(string pEmpenho, string pUg, string pData)
        {
            var dataFitro = DateTime.Parse(pData).Date;
            var orgaos = _orgaoDal.GetOrgaos().ToList();
            return Json(new
            {
                dados = _pagamentoFornecedorDal.GetEmpenhosUG(pEmpenho, pUg, DateTime.Parse(pData)).Select(d => new
                {
                    NumeroEmpenho = d.NumEmpenho,
                    Documento = d.DocumentoNE,
                    Credor = d.Credor,
                    Processo = d.Processo,
                    Data = d.DataDocumento.ToString("dd/MM/yyyy"),
                    Valor = d.ValorPaga?.ToString("C2"),
                    UnidadeGestora = orgaos.FirstOrDefault(o => o.CodOrgao.ToString("000000") == d.CodUnidadeGestora)?.NomOrgao,
                    LinkDetalhamento = new JavaScriptSerializer().Serialize(new LinkDetalhamentoEmpenhoFornecedor
                    {
                        DocCredor = d.DocCredor,
                        DocumentoNE = d.DocumentoNE,
                        EspecificacaoDespesa = d.EspecificacaoDespesa,
                        NumEmpenho = d.NumEmpenho,
                        Processo = d.Processo,
                        ValorDespesa = d.ValorPaga
                    }).ToEncrypted()
                })
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DetalhaEmpenhoFornecedor(string pEncLinkDetalhamento)
        {
            var parametros = new JavaScriptSerializer().Deserialize<LinkDetalhamentoEmpenhoFornecedor>(pEncLinkDetalhamento.ToDecrypted());
            var pagamentoFornecedor = _pagamentoFornecedorDal.DetalhaEmpenhoFornecedor(parametros.DocCredor, parametros.NumEmpenho, parametros.DocumentoNE, parametros.EspecificacaoDespesa, parametros.Processo, parametros.ValorDespesa);

            return Json(new
            {
                Processo = pagamentoFornecedor.Processo,
                Evento = pagamentoFornecedor.Evento,
                UnidadeGestora = pagamentoFornecedor.CodUnidadeGestora + " - " + pagamentoFornecedor.UnidadeGestora,
                NaturezaDespesa = pagamentoFornecedor.NaturezaDespesa,
                EspecificacaoDespesa = pagamentoFornecedor.EspecificacaoDespesa,
                CategoriaDespesa = pagamentoFornecedor.CodCategoriaDespesa, // + " - " + retorno.CategoriaDespesa,
                GrupoDespesa = pagamentoFornecedor.CodGrupoDespesa, // + " - " + retorno.GrupoDespesa,
                ModalidadeAplicacao = pagamentoFornecedor.CodModalidadeAplicacaoDespesa, // + " - " + retorno.ModAplicacao,
                ElementoDespesa = pagamentoFornecedor.CodNomeDespesa.ToString("0000"), //+ " - " + retorno.NomDespesa,
                Projeto = pagamentoFornecedor.CodProjeto + " - " + pagamentoFornecedor.Projeto,
                Programa = pagamentoFornecedor.CodPrograma + " - " + pagamentoFornecedor.Programa,
                Acao = pagamentoFornecedor.CodAcao + " - " + pagamentoFornecedor.Acao,
                Funcao = pagamentoFornecedor.CodFuncao + " - " + pagamentoFornecedor.Funcao,
                SubFuncao = pagamentoFornecedor.CodSubFuncao + " - " + pagamentoFornecedor.SubFuncao,
                FonteDeRecursos = pagamentoFornecedor.CodFonte + " - " + pagamentoFornecedor.Fonte,
                Credor = pagamentoFornecedor.DocCredor.Length == 11
                    ? Convert.ToUInt64(pagamentoFornecedor.DocCredor).ToString(@"000\.000\.000\-00")
                    : Convert.ToUInt64(pagamentoFornecedor.DocCredor).ToString(@"00\.000\.000\/0000\-00")
                    + " - " + pagamentoFornecedor.Credor,
                ModalidadeLicitacao = pagamentoFornecedor.CodModlidadeLicitacao + " - " + pagamentoFornecedor.ModalidadeLicitacao,
                Documento = pagamentoFornecedor.Documento + " Realizado em " + pagamentoFornecedor.DataDocumento.ToString("d"),
                DocumentoNE_DocumentoOB = pagamentoFornecedor.NumEmpenho + " - " + pagamentoFornecedor.DocumentoOB,
                TipoEmpenho = pagamentoFornecedor.CodTipoEmpennho + " - " + pagamentoFornecedor.TipoEmpenho,
                Empenho = pagamentoFornecedor.NumEmpenho + " Realizado em " + pagamentoFornecedor.DataEmpenho.ToString("d"),
                ValorPaga = pagamentoFornecedor.ValorPaga?.ToString("c2"),
                NumeroDocumentoLiquidacao = pagamentoFornecedor.NumeroDocumentoLiquidacao,
                DataDocumentoLiquidacao = pagamentoFornecedor.DataDocumentoLiquidacao == null ? "" : ((DateTime)pagamentoFornecedor.DataDocumentoLiquidacao).ToString("d"),
                ObjetivoDocumentoLiquidacao = pagamentoFornecedor.ObjetivoDocumentoLiquidacao
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Fornecedores(string razaoSocial, string cpfCnpj)
        {
            var fornecedores = _fornecedorDal.BuscarFornecedoresPorRazaoSocialCpfCnpj(razaoSocial, cpfCnpj).Select(f => new
            {
                RazaoSocial = f.RazaoSocial,
                CpfCnpj = f.CpfCnpj,
                Cidade = f.Cidade?.Nome,
                Uf = f.Uf.ToString(),
                Endereco = f.Endereco,
                FornecedorId = f.FornecedorId.ToString().ToEncrypted()
            });

            return Json(fornecedores, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerificarSeContemImpedimento(string numeroProcecesso, string cpfCnpj, string fornecedorImpedidoId)
        {
            var numeroProcessoSemFormatacao = numeroProcecesso.Replace(".", "").Replace("-", "").Replace("/", "");
            var retorno = _impedirFornecedorDal.VerificarSeContemImpedimento(numeroProcessoSemFormatacao, cpfCnpj, new Guid(fornecedorImpedidoId));
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPagamentoFornecedoresRpv(string dataInicial, string dataFinal, string pCredor, string pDocCredor)
        {
            var datInicial = DateTime.Parse(dataInicial).Date;
            var datFinal = DateTime.Parse(dataFinal).Date;

            return Json(new
            {
                dados = _pagamentoFornecedorRpvsDal.PagamentoFornecedorRpvs(datInicial, datFinal, pCredor, pDocCredor).OrderByDescending(f => f.DataOb).Select(d => new
                {
                    DocumentoOB = d.DocumentoOB,
                    Credor = d.Credor,
                    NumeroProcesso = d.NumeroProcesso,
                    Data = d.DataOb.ToString("dd/MM/yyyy"),
                    Finalidade = d.Finalidade,
                    StatusOb = d.StatusOb,
                    Valor = d.ValorPaga.ToString("C2"),
                    Movimentacao = d.Movimentacao.ToString("C2"),
                    Cessionario = d.Cessionario
                })
            }, JsonRequestBehavior.AllowGet);
        }

        //PartialView
        public PartialViewResult ListaSocios(int codigoImpedimento)
        {
            return PartialView("_ListaSocios", _socioFornecedorDal.BuscarSocioFornecedorPorCodigoImpedimento(codigoImpedimento));
        }

        public PartialViewResult ListaFornecedorImpedido()
        {
            return PartialView("_EditarFornecedoresImpedidos", _impedirFornecedorDal.BuscarFornecedores());
        }

        public PartialViewResult ListarFonecedores()
        {
            return PartialView("_ListaFornecedores", _fornecedorDal.BuscarFornecedores());
        }

        //Private
        private class LinkDetalhamentoEmpenhoFornecedor
        {
            public string DocCredor { get; set; }
            public string NumEmpenho { get; set; }
            public string DocumentoNE { get; set; }
            public long? EspecificacaoDespesa { get; set; }
            public string Processo { get; set; }
            public decimal? ValorDespesa { get; set; }
        }

        private class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}