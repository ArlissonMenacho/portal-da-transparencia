﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.DAL;
using System.Globalization;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Text;

namespace TransparenciaRO.Web.Controllers
{
    public class GraficoController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly DespesaDAL _despesaDal;
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly ParametrosDAL _parametrosDal;
        private readonly OrgaoDAL _orgaoDal;
        private readonly ModalidadeLicitacaoDAL _modalidadeLicitacaoDal;
        private readonly ViewEmpenhosDal _viewEmpenhosDal;
        private readonly MateriaisDAL _materiaisDAL;

        public GraficoController()
        {
            _despesaDal = new DespesaDAL(_dbTransparencia);
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _parametrosDal = new ParametrosDAL(_dbTransparencia);
            _orgaoDal = new OrgaoDAL(_dbTransparencia);
            _modalidadeLicitacaoDal = new ModalidadeLicitacaoDAL(_dbTransparencia);
            _viewEmpenhosDal = new ViewEmpenhosDal(_dbTransparencia);
            _materiaisDAL = new MateriaisDAL(_dbTransparencia);
        }

        #region Receita

        public ActionResult Receita()
        {
            // Por padrão, irá listar os últimos 5 anos, consolidado por ano, e a receita total (sem tributos).
            // Estes parâmetros poderão ser mudados diretamente na tela
            //var dadosReceitaArrecadada = ReceitaBusiness.GetReceita(DateTime.Now.AddYears(-4).Year, DateTime.Now.Year, ETipoConsolidacao.PorAno, ETipoConsultaReceita.Total, null, true);
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras().Where(ug => ug.unidadeGestoraIDSIAFEM != null).ToList();
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoReceita);

            return View();
        }

        #endregion

        #region Despesas
        [HttpPost]
        public JsonResult ExibirDespesas(int pCodUnidadeGestora, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            var mostrarTodosAnos = GetPermissoesUsuario().Contains(EPerfilUsuario.VisualizacaoCompletaDespesas);
            return Json(_despesaDal.GetDespesasPorAno(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pDocCredor.Replace("-", "").Replace("/", "").Replace(".", ""), !mostrarTodosAnos));
        }

        public JsonResult DetalharDespesa(string pEncParametros)
        {
            var parametros = pEncParametros.ToParametrosDetalharDespesaModel();

            if (parametros == null)
                return Json(null);
            switch (parametros.TipoDetalhamentoDespesa)
            {
                case ETipoDetalhamentoDespesa.DespesasMeses:
                    return Json(_despesaDal.GetDespesasMeses(parametros.CodUnidadeGestora, parametros.Exercicio, parametros.NumeroEmpenho, parametros.NomeCredor, parametros.DocCredor), JsonRequestBehavior.AllowGet);
                case ETipoDetalhamentoDespesa.DespesasProgramaAcao:
                    return Json(_despesaDal.GetDespesasProgramaAcao(parametros.MesAno, parametros.CodUnidadeGestora, parametros.NumeroEmpenho, parametros.NomeCredor, parametros.DocCredor), JsonRequestBehavior.AllowGet);
                case ETipoDetalhamentoDespesa.DespesasFuncoes:
                    return Json(_despesaDal.GetDespesasFuncoes(parametros.MesAno, parametros.CodUnidadeGestora, parametros.CodPrograma, parametros.CodAcao, parametros.CodProjeto, parametros.NumeroEmpenho, parametros.NomeCredor, parametros.DocCredor), JsonRequestBehavior.AllowGet);
                case ETipoDetalhamentoDespesa.DespesasNaturezas:
                    return Json(_despesaDal.GetDespesasNaturezas(parametros.MesAno, parametros.CodUnidadeGestora, parametros.CodPrograma, parametros.CodAcao, parametros.CodProjeto, parametros.CodFonteRecursos, parametros.NumeroEmpenho, parametros.NomeCredor, parametros.DocCredor), JsonRequestBehavior.AllowGet);
                case ETipoDetalhamentoDespesa.DespesasEmpenhos:
                    return Json(_despesaDal.GetDespesasEmpenhos(parametros.MesAno, parametros.CodUnidadeGestora, parametros.CodPrograma, parametros.CodAcao, parametros.CodProjeto, parametros.CodFonteRecursos, parametros.CodEspecificacaoDespesa, parametros.NumeroEmpenho, parametros.NomeCredor, parametros.DocCredor), JsonRequestBehavior.AllowGet);
                default:
                    return Json(null);
            }
        }

        public ActionResult Despesa()
        {
            var DataInicial = DateTime.Now.AddMonths(-1).ToString("MM-yyyy");
            var DataFinal = DateTime.Now.ToString("MM-yyyy");
            ViewBag.DataInicial = DataInicial;
            ViewBag.DataFinal = DataFinal;
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras();
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoDespesas);
            return View();
        }
        #endregion

        #region Dotação Inicial
        public ActionResult DotacaoInicial()
        {
            ViewBag.UnidadesGestoras = _orgaoDal.GetOrgaosConsolidadosPorNome()
                   .Union(new Dictionary<string, string> { { "0", "TODOS" } }).OrderBy(o => o.Key).ToDictionary(k => k.Key, v => v.Value);

            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoDotacaoInicial);

            return View();
        }

        [HttpPost]
        public JsonResult DotacaoInicial(int pExercicio, string pUG)
        {
            var ugs = pUG.Split(',').Distinct().Select(ug => ug.PadLeft(6, '0'));

            var retorno = _dbTransparencia.DotacaoInicial
                 .Where(di => di.Exercicio == pExercicio && (pUG == "0" || ugs.Contains(di.CodigoOrgao)))
                 .Take(1000)
                 .ToList()
                 .Select(di => new { di.CodigoOrgao, di.Exercicio, di.NaturezaDespesa, di.DescricaoDespesa, di.Orgao, di.ProgramaTrabalho, SaldoInicial = di.SaldoInicial.ToString("c2") })
                 .ToList();

            return Json(new DadosTabuladosGenericosModel { dados = retorno, msgInformacao = retorno.Count == 1000 ? "Apenas os 1000 primeiros registros serão exibidos" : null });
        }
        #endregion

        #region Compras e contratações
        public class ParametrosComprasContratacoesModel
        {
            public DateTime pDataInicial { get; set; }
            public DateTime pDataFinal { get; set; }
            public string CodUnidadeGestora { get; set; }


            private int _codModalidadeCompra;
            public int? CodModalidadeCompra
            {
                get => _codModalidadeCompra == 0 ? null : (int?)_codModalidadeCompra;
                set => _codModalidadeCompra = value ?? 0;
            }

        }

        public ActionResult DetalhesEmpenho(string numeroEmpenho, string unidadeGestora)
        {
            ViewBag.UnidadesGestora = _orgaoDal.GetOrgaos().Where(o => o.CodOrgao == unidadeGestora.ToDecrypted().PadLeft(6, '0').ToInt()).FirstOrDefault();
            return View(_viewEmpenhosDal.GetDetalhesEmpenho(numeroEmpenho, unidadeGestora.ToDecrypted().PadLeft(6, '0')));
        }

        public ActionResult DetalhesEmpenhoCOVID19(string numeroEmpenho, string unidadeGestora)
        {
            ViewBag.UnidadesGestora = _orgaoDal.GetOrgaos().Where(o => o.CodOrgao == unidadeGestora.ToInt()).FirstOrDefault();

            return View("DetalhesEmpenho", _viewEmpenhosDal.GetDetalhesEmpenho(numeroEmpenho, unidadeGestora));
        }

        public ActionResult ComprasContratacoes()
        {
            var parametros = new ParametrosComprasContratacoesModel();
            ViewBag.UnidadesGestoras = _orgaoDal.GetOrgaos().Select(org => new { NomOrgao = org.NomOrgao.Trim(), CodOrgao = org.CodOrgao }).Distinct().GroupBy(org => org.NomOrgao).Select(org => new SelectOptionModel(String.Join(",", org.Select(o => o.CodOrgao).ToArray()), org.Key)).Combify("Selecione...").ToList();
            ViewBag.ModalidadesCompra = _modalidadeLicitacaoDal.GetModalidades().Select(m => new SelectOptionModel(m.Codigo.ToString(), m.Descricao)).Combify("Selecione...").ToList();
            ViewBag.Parametros = parametros;
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);
            return View();
        }


        [HttpPost]
        public JsonResult ComprasContratacoes(string pEncListaUnidadeGestora, string pEncCodModalidadeCompra, string pEmpenho, string pDataInicial, string pDataFinal)
        {
            if (pEncListaUnidadeGestora == null || pEncCodModalidadeCompra == null)
            {
                return Json(new { msgInfo = "Por favor, preencha os campos obrigatórios" });
            }

            var pCodModalidadeCompra = Convert.ToInt32(pEncCodModalidadeCompra.ToDecrypted());
            var lstOrgaos = pEncListaUnidadeGestora.ToDecrypted().Split(',').ToList();
            var numEmpenho = pEmpenho;
            const int MaxRegistros = 1000;
            string codUG;

            codUG = pEncListaUnidadeGestora.ToDecrypted();

            var dtInicial = (string.IsNullOrEmpty(pDataInicial) ? (DateTime?)null : Convert.ToDateTime(pDataInicial).Date);
            var dtFinal = (string.IsNullOrEmpty(pDataFinal) ? (DateTime?)null : Convert.ToDateTime(pDataFinal).Date);
            int codMod = pCodModalidadeCompra == 0 ? 0 : pCodModalidadeCompra;

            var data = _viewEmpenhosDal
                .GetComprasContratacoes(pCodModalidadeCompra, codUG, pDataInicial, pDataFinal, numEmpenho)
                .ToList();

            return Json(new
            {
                msgInfo = data.Count() == 1000 ? $"Esta consulta teve o seu resultado limitado em {MaxRegistros} registros" : null,
                dados = data
            });
        }

        #endregion

        #region compras e contratações COVID-19

        public ActionResult ComprasContratacoesCOVID19()
        {
            var parametros = new ParametrosComprasContratacoesModel();
            ViewBag.UnidadesGestoras = _orgaoDal.GetOrgaos().Select(org => new { NomOrgao = org.NomOrgao.Trim(), CodOrgao = org.CodOrgao }).Distinct().GroupBy(org => org.NomOrgao).Select(org => new SelectOptionModel(String.Join(",", org.Select(o => o.CodOrgao).ToArray()), org.Key)).Combify("Selecione...").ToList();
            ViewBag.ModalidadesCompra = _modalidadeLicitacaoDal.GetModalidades().Select(m => new SelectOptionModel(m.Codigo.ToString(), m.Descricao)).Combify("Selecione...").ToList();
            ViewBag.Parametros = parametros;
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);
            return View();
        }


        [HttpPost]
        public JsonResult ComprasContratacoesCOVID19(string pEncListaUnidadeGestora, string pDataInicial, string pDataFinal, string pCredor)
        {                             
            const int MaxRegistros = 1000;
            
            string codUG;

            codUG = pEncListaUnidadeGestora.ToDecrypted();

            var dtInicial = (string.IsNullOrEmpty(pDataInicial) ? (DateTime?)null : Convert.ToDateTime(pDataInicial).Date);
            var dtFinal = (string.IsNullOrEmpty(pDataFinal) ? (DateTime?)null : Convert.ToDateTime(pDataFinal).Date);

            var data = _viewEmpenhosDal
                .GetComprasContratacoesCOVID19(codUG, pDataInicial, pDataFinal, pCredor)
                .ToList();

            return Json(new
            {
                msgInfo = data.Count() == MaxRegistros ? $"Esta consulta teve o seu resultado limitado em {MaxRegistros} registros" : null,
                dados = data
            });
        }

        #endregion
        
        #region Gasto de Material de consumo e Permanente

        public ActionResult ComprasMateriais()
        {
            var parametros = new ParametrosComprasContratacoesModel { CodModalidadeCompra = null, pDataFinal = DateTime.Now, pDataInicial = DateTime.Now.AddDays(-60) };
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras();

            ViewBag.Parametros = parametros;
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);
            return View();
        }

        [HttpPost]
        public ActionResult ComprasMateriais(string codUnidadeGestora, string tipoMaterial, string dataInicial, string dataFinal)
        {
            var dtInicial = (string.IsNullOrEmpty(dataInicial) ? (DateTime?)null : Convert.ToDateTime(dataInicial));
            var dtFinal = (string.IsNullOrEmpty(dataFinal) ? (DateTime?)null : Convert.ToDateTime(dataFinal));
            var qtdBusca = 500;

            if (dtInicial != null && dtFinal != null && dtInicial.Value.Year != dtFinal.Value.Year)
                return Json(new { msgErro = $"Por favor, selecione o período dentro do mesmo ano", dados = "" });

            if (dtInicial == null && dtFinal != null)
                return Json(new { msgErro = $"Por favor, selecione a data de início do período", dados = "" });

            if (dtInicial != null && dtFinal == null)
                return Json(new { msgErro = $"Por favor, selecione a data final do período", dados = "" });

            var dados = _materiaisDAL.ObterPorFiltro(codUnidadeGestora, tipoMaterial, dataInicial, dataFinal, dtInicial, dtFinal, qtdBusca);

            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras();
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);

            return Json(new
            {
                msgInfo = dados.Count() == qtdBusca ? $"Esta consulta teve o seu resultado limitado em {dados.Count()} registros" : null,
                dados = dados
            });
        }

        public ActionResult DetalhesCompra(short exercicio, string ug, string numEmpenho, long especificacaoDespesa, string valorDespesa, string documento)
        {
            ViewBag.UgNome = _unidadeGestoraDal.GetNomeUg(ug);
            return View(_materiaisDAL.ObterPorFiltroDetalhado(exercicio, ug, numEmpenho, especificacaoDespesa, decimal.Parse(valorDespesa, CultureInfo.InvariantCulture), documento).ToList());
        }

        #endregion

        #region Gasto de Material de consumo e Permanente COVID-19

        public ActionResult DespesasCOVID19()
        {
            var parametros = new ParametrosComprasContratacoesModel { CodModalidadeCompra = null, pDataFinal = DateTime.Now, pDataInicial = DateTime.Now.AddDays(-60) };
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras();

            ViewBag.Parametros = parametros;
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);
            return View();
        }

        [HttpPost]
        public ActionResult DespesasCOVID19(string codUnidadeGestora, string tipoMaterial, string dataInicial, string dataFinal)
        {
            var dtInicial = (string.IsNullOrEmpty(dataInicial) ? (DateTime?)null : Convert.ToDateTime(dataInicial));
            var dtFinal = (string.IsNullOrEmpty(dataFinal) ? (DateTime?)null : Convert.ToDateTime(dataFinal));
            var qtdBusca = 1000;

            if (dtInicial != null && dtFinal != null && dtInicial.Value.Year != dtFinal.Value.Year)
                return Json(new { msgErro = $"Por favor, selecione o período dentro do mesmo ano", dados = "" });

            if (dtInicial == null && dtFinal != null)
                return Json(new { msgErro = $"Por favor, selecione a data de início do período", dados = "" });

            if (dtInicial != null && dtFinal == null)
                return Json(new { msgErro = $"Por favor, selecione a data final do período", dados = "" });

            var dados = _materiaisDAL.ObterPorFiltroCOVID19(codUnidadeGestora, tipoMaterial, dataInicial, dataFinal, dtInicial, dtFinal, qtdBusca).ToList();

            ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestoras();
            ViewBag.DataAtualizacao = _parametrosDal.GetParametro(ETipoParametro.DataAtualizacaoComprasContratacoes);

            return Json(new
            {
                msgInfo = dados.Count() == qtdBusca ? $"Esta consulta teve o seu resultado limitado em {dados.Count()} registros" : null,
                dados = dados
            });
        }

        public ActionResult DetalhesCompraCOVID19(short exercicio, string ug, string numEmpenho, long especificacaoDespesa, string valorDespesa, string documento)
        {
            ViewBag.UgNome = _unidadeGestoraDal.GetNomeUg(ug);
            return View("DetalhesCompra", _materiaisDAL.ObterPorFiltroDetalhadoCOVID19(exercicio, ug, numEmpenho, especificacaoDespesa, decimal.Parse(valorDespesa, CultureInfo.InvariantCulture), documento).ToList());
        }

        public void DespesasDiretasToCSV()
        {
            var despesasDiretas = _despesaDal.GetDespesasDiretas();

            var sb = new StringBuilder();
            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};{21};{22};{23};{24};{25};{26};{27};{28};{29};{30};{31}",
                            "Exercicio", "CodProjeto", "EVENTO", "NumEmpenho", "DataDocumento", "UG", "N_PROCESSO_NE", "CodPrograma", "Programa", "CodAco", "Acao", "CodFuncao", "NomFuncao", "CodSubFuncao", "CodEspecificacaoDespesa", "NomDespesa", "Fonte", "DesFonteRecurso", "CodOrgao", "NomOrgao", "NomSigla", "documento", "DOCUMENT_NE", "VLR_EMPENHO", "DocCredor", "Credor", "DescricaoSubfuncao", "ValorEmpenhada", "ValorLiquidada", "ValorPaga", "valorDespesa", "Status");
            sb.Append(Environment.NewLine);
            foreach (var item in despesasDiretas)
            {
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};{21};{22};{23};{24};{25};{26};{27};{28};{29};{30};{31}",
                                 item.Exercicio, item.CodProjeto, item.EVENTO, item.NumEmpenho, item.DataDocumento, item.UG, item.N_PROCESSO_NE, item.CodPrograma, item.Programa, item.CodAco, item.Acao, item.CodFuncao, item.NomFuncao, item.CodSubFuncao, item.CodEspecificacaoDespesa, item.NomDespesa, item.Fonte, item.DesFonteRecurso, item.CodOrgao, item.NomOrgao, item.NomSigla, item.documento, item.DOCUMENT_NE, item.VLR_EMPENHO, item.DocCredor, item.Credor, item.DescricaoSubfuncao, item.ValorEmpenhada, item.ValorLiquidada, item.ValorPaga, item.valorDespesa, item.Status);
                sb.Append(Environment.NewLine);
            }

            var response = System.Web.HttpContext.Current.Response;
            response.BufferOutput = true;
            response.Clear();
            response.ClearHeaders();
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("content-disposition", "attachment;filename=Despesas.CSV ");
            response.ContentType = "text/plain";
            response.Write(sb.ToString());
            response.End();
        }

        public void DespesasDetalhadasCOVID19()
        {
            var comprasMateriais = _materiaisDAL.ObterTodasComprasMateriaisCOVID19();

            var sb = new StringBuilder();

            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                            "Exercicio", "UG", "NomeUG", "NumEmpenho", "DataDocumento", "CodEspecificacaoDespesa", "EspecificacaoDespesa", "NomDespesa", "Credor", "ValorEmpenhada", "ValorLiquidada", "ValorPaga", "valorDespesa", "Qtde", "UnidadeMedida", "VlrUnitario", "Documento", "Status", "Descricao");
            
            sb.Append(Environment.NewLine);

            foreach (var item in comprasMateriais)
            {
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                                 item.Exercicio, item.UG, item.NomeUG, item.NumEmpenho, item.DataDocumento, item.CodEspecificacaoDespesa, item.EspecificacaoDespesa, item.NomDespesa, item.Credor, item.ValorEmpenhada, item.ValorLiquidada, item.ValorPaga, item.valorDespesa, item.Qtde, item.UnidadeMedida, item.VlrUnitario, item.Documento, item.Status, item.Descricao);
                sb.Append(Environment.NewLine);
            }

            var response = System.Web.HttpContext.Current.Response;
            response.BufferOutput = true;
            response.Clear();
            response.ClearHeaders();
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("content-disposition", "attachment;filename=DespesasDetalhadasCOVID19.CSV ");
            response.ContentType = "text/plain";
            response.Write(sb.ToString());
            response.End();
        }

        #endregion

        #region Highcharts Helpers

        public class HighchartsSeriesEntry
        {
            public string name { get; set; }
            public List<HighchartsDataEntry> data { get; set; }
            public HighchartsSeriesEntry()
            {
                data = new List<HighchartsDataEntry>();
            }
        }

        public class HighchartsDataEntry
        {
            public string name { get; set; }
            public decimal y { get; set; }
            public string drilldown { get; set; }
        }

        public class HighchartsDrilldownEntry
        {
            public string id { get; set; }
            public List<HighchartsDataEntry> data { get; set; }
            public HighchartsDrilldownEntry()
            {
                data = new List<HighchartsDataEntry>();
            }
        }
        #endregion 

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}