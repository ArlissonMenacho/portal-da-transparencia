﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using TransparenciaRO.Infra.DAL;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using TransparenciaRO.Infra.Utils;
using System.Data.OleDb;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Web.Controllers
{
    public class ServidoresDesligadosExoneradosController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ServidoresDesligadosExoneradosDAL _servidoresDesligadosExoneradosDal;
        private readonly LotacaoDAL _lotacaoDal;

        public ServidoresDesligadosExoneradosController()
        {
            _servidoresDesligadosExoneradosDal = new ServidoresDesligadosExoneradosDAL(_dbTransparencia);
            _lotacaoDal = new LotacaoDAL(_dbTransparencia);
        }
        
        public ActionResult Index()
        {
            ViewBag.Ano = _servidoresDesligadosExoneradosDal.Ano();
            ViewBag.Mes = _servidoresDesligadosExoneradosDal.Mes(ViewBag.Ano);
            return View();

        }

        //Actions
        [Authorize(Roles = nameof(AdministradorRelacaoServidor))]
        public ActionResult ImportarDados()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file, int ano, int mes)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path}; Extended Properties = 'Excel 12.0;HDR = YES;ReadOnly = true;IMEX=1';"))
                    {
                        db.Open();
                        var lotacoesBD = _lotacaoDal.ObterTodos().ToList();
                        var lotacoes = new List<Lotacao>();
                        var servidoresDesligadosExonerados = new List<ServidoresDesligadosExonerados>();

                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();                         

                            while (dr != null && dr.Read())
                            {
                                var lotacao = new Lotacao
                                {
                                    Lot = Convert.ToInt32(dr.GetValue(dr.GetOrdinal("LOTAÇÃO"))),
                                    Descricao = dr.GetString(dr.GetOrdinal("NOME DA LOTAÇÃO")).Trim(),
                                };

                                if (lotacoes.Any(x => x.Lot == lotacao.Lot) || lotacoesBD.Any(x => x.Lot == lotacao.Lot))
                                    lotacao = lotacoesBD.FirstOrDefault(x => x.Lot == lotacao.Lot) ?? lotacoes.FirstOrDefault(x => x.Lot == lotacao.Lot);
                                var servidorDesligadoExonerado = new ServidoresDesligadosExonerados
                                {

                                    Mes = mes,
                                    Ano = ano,
                                    LotacaoId = lotacao.LotacaoId,
                                    Matricula = dr.GetValue(dr.GetOrdinal("MATRICULA")).ToString(),
                                    Nome = dr.GetString(dr.GetOrdinal("SERVIDOR")).Trim(),
                                    Admissao = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("ADMISSÃO"))),
                                    Desligamento = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DESLIGAMENTO"))),
                                    Cpf = dr.GetValue(dr.GetOrdinal("CPF")).ToString(),
                                    CausaDesligamento = dr.GetString(dr.GetOrdinal("NOMCAUDSL")).Trim(),
                                };
                                servidoresDesligadosExonerados.Add(servidorDesligadoExonerado);

                                LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados da relação do servidor {servidorDesligadoExonerado.Nome} -:", AuthenticationManager.User.IdUsuario());
                            }                            
                            _servidoresDesligadosExoneradosDal.VerficarRemoverDados(ano, mes);
                            _servidoresDesligadosExoneradosDal.Adicionar(servidoresDesligadosExonerados);
                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds,1)} segundos";
                    return RedirectToAction("ImportarDados", "ServidoresDesligadosExonerados");
                }
                MensagemErro = "Arquivo inválido";
                return View();

            }
            catch
            {
                MensagemErro = "Ocorreu um erro ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                return RedirectToAction("ImportarDados", "ServidoresDesligadosExonerados");
            }
        }

        //Json
        public JsonResult ResultadoFiltro(int ano, int mes, string nome, string cpf)
        {
            var cpfSoNumeros = !cpf.IsNullOrEmpty()
                ? Convert.ToInt64(cpf.Replace(".", "").Replace("-", "")).ToString()
                : cpf;
            var retorno = _servidoresDesligadosExoneradosDal.BuscarServidoresDesligadosExonerados(ano, mes, nome, cpfSoNumeros).Select(f => new
            {
                Nome = f.Nome,
                Matricula = f.Matricula,
                Admissao = f.Admissao,
                Desligamento = f.Desligamento,
                Cpf = f.Cpf,
                Causa = f.CausaDesligamento,
                Lotacaox = f.Lotacao.Descricao
            }).OrderBy(f => f.Nome);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Meses(int ano)
        {
            var retorno = _servidoresDesligadosExoneradosDal.Meses(ano).Select(m => new
            {
                MesInt = m,
                MesString = m.NomeMes()
            }).OrderByDescending(m => m.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}