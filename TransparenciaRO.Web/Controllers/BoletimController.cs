﻿using System;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers
{
    public class BoletimController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly RegistroBoletimDal _registroBoletimDal;
        private readonly ArquivoDAL _arquivoDAL;
        private readonly EmailNotificacaoDAL _emailNotificacaoDAL;

        public BoletimController()
        {
            _registroBoletimDal = new RegistroBoletimDal(_dbTransparencia);
            _arquivoDAL = new ArquivoDAL(_dbTransparencia);
            _emailNotificacaoDAL = new EmailNotificacaoDAL(_dbTransparencia);
        }

        public ActionResult Index()
        {
            var boletinsVM = _registroBoletimDal.ObterTodos();

            return View(boletinsVM);
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorControleDocumentoExterno))]
        public ActionResult GerenciarBoletins()
        {
            var boletinsVM =_registroBoletimDal.ObterTodos();

            return View(boletinsVM);
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorControleDocumentoExterno))]
        public ActionResult RegistrarBoletim()
        {
            var inscricaoVM = new RegistroBoletimViewModel() { BoletimId = Guid.NewGuid() };

            return View(inscricaoVM);
        }

        [Authorize(Roles = nameof(EPerfilUsuario.AdministradorControleDocumentoExterno))]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult RegistrarBoletim(RegistroBoletimViewModel registroVM)
        {
            var documentos = Request.Files;

            if (ModelState.IsValid && documentos.Count == 1)
            {
                 _registroBoletimDal.AdicionarBoletim(registroVM);

                var mensagemErros = _arquivoDAL.AdicionarArquivosBoletim(registroVM.BoletimId, documentos);

                if (mensagemErros.Count > 0)
                {          
                    MensagemErro = "Ooops... \n\nOcorreu um problema ao realizar sua inscrição." +
                        "\n\nCaso o problema persista entrar em contato com : transparencia.cge.ro@gmail.com";

                    return View(registroVM);
                }
                var link = "http://comprasemergenciais-covid19.ro.gov.br/Home/Boletim";

                var fileName = documentos[0].FileName;

                var emailsToNotify = _emailNotificacaoDAL.GetEmailsToNotifyBoletimAdded();

                emailsToNotify.ForEach(emailToNotify =>
                {
                    var message = $"Olá, cidadão!.<br /><br />" +
                        $"Os seguintes conteúdos do seu interesse  sobre os gastos governamentais destinados ao " +
                        $"enfrentamento da COVID 19 no Estado de Rondônia foram Inseridos/Alterados:<br />" +
                        $"Boletim de Controle Covid : { fileName }, você pode acessa-lo <a href='{ link }'>clicando aqui</a>.<br /><br />" +
                        $"Caso você não queira mais receber notificações como esta, <a href='http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id }'>clique aqui</a>, " +
                        $"ou copie o link abaixo e cole em uma nova aba.<br />" +
                        $"http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id } <br /><br />" +
                        $"Este é um e-mail automático, portanto, não é necessário respondê-lo.";

                    var subject = $"Transparência Proativa | Controladoria Geral do Estado de Rondônia";

                    MailUtils.SendMail(message, subject, true, emailToNotify.Email);
                });

                MensagemSucesso = "Boletim Registrado com Sucesso";

                return RedirectToAction(nameof(GerenciarBoletins));
            }

            MensagemErro = "Por favor, preencha todos os campos e tente novamente.";

            return View(registroVM);
        }
    }
}