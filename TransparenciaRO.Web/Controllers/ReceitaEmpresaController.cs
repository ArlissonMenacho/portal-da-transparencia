﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class ReceitaEmpresaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly ReceitaEmpresaDAL _receitaEmpresaDal;
        private int cont = 1;

        public ReceitaEmpresaController()
        {
            _receitaEmpresaDal = new ReceitaEmpresaDAL(_dbTransparencia);
        }

        [Authorize(Roles = nameof(AdministradorDespesasReceitasSoph) + "," + nameof(AdministradorDespesasReceitasCaerd) + "," + nameof(AdministradorDespesasReceitasCrm))]
        public ActionResult ImportarDados(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(AdministradorDespesasReceitasSoph) + "," + nameof(AdministradorDespesasReceitasCaerd) + "," + nameof(AdministradorDespesasReceitasCrm))]
        public ActionResult ImportarDados(HttpPostedFileBase file, EEmpresa empresa)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);

                    //Rotina de importação dos dados
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties ='Excel 12.0;HDR=YES;ReadOnly=true;IMEX=1';"))
                    {
                        db.Open();
                        var receitasEmpresas = new List<ReceitaEmpresa>();

                        var dtSchema = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                                cont++;
                                if (!dr.GetValue(dr.GetOrdinal("CNPJ/CPF")).ToString().Trim().IsNullOrEmpty())
                                {
                                    var receitaEmpresa = new ReceitaEmpresa
                                    {
                                        Data = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("Data"))),
                                        NotaFiscal = dr.GetValue(dr.GetOrdinal("NotaFiscal")).ToString().Trim(),
                                        CpfCnpj = dr.GetValue(dr.GetOrdinal("CNPJ/CPF")).ToString().Trim(),
                                        Cliente = dr.GetValue(dr.GetOrdinal("Cliente")).ToString().Trim(),
                                        Receita = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("Receita")).ToString()),
                                        Chave = dr.GetValue(dr.GetOrdinal("Chave")).ToString().Trim(),
                                        Empresa = empresa
                                    };

                                    receitasEmpresas.Add(receitaEmpresa);
                                }
                            }
                            _receitaEmpresaDal.VerficarRemoverDados(receitasEmpresas.First().Data, empresa);
                            _receitaEmpresaDal.Adicionar(receitasEmpresas);

                            LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados das receitas da empresa: {empresa} -:", AuthenticationManager.User.IdUsuario());
                        }
                    }
                    System.IO.File.Delete(path);
                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    ViewBag.Empresa = empresa.ToString().ToUpper();
                    return RedirectToAction("ImportarDados", "ReceitaEmpresa", new { empresa = empresa });
                }
                MensagemErro = "O arquivo deve está no formato xls ou xlsx";
                return View();
            }
            catch
            {       
                MensagemErro = $"Ocorreu um erro na linha {cont}, ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                ViewBag.Empresa = empresa.ToString().ToUpper();
                return RedirectToAction("ImportarDados", "ReceitaEmpresa", new { empresa = empresa });
            }
        }

        public ActionResult Grafico(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            ViewBag.AnoMax = _receitaEmpresaDal.AnoMax(empresa);
            ViewBag.AnoMin = _receitaEmpresaDal.AnoMin(empresa);

            return View();
        }

        //Json
        public JsonResult Meses(int ano, EEmpresa empresa)
        {
            var retorno = _receitaEmpresaDal.Meses(ano, empresa).Select(m => new
            {
                MesInt = m,
                MesString = m.NomeMes()
            }).OrderByDescending(m => m.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReceitaAnual(EEmpresa empresa)
        {
            var retorno = _receitaEmpresaDal.ReceitaAnual(empresa).Select(r => new
            {
                Ano = r.Key,
                Valor = r.Value,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Value).Replace("R$", "")
            }).OrderBy(r => r.Ano);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReceitaMensal(int ano, EEmpresa empresa)
        {
            var retorno = _receitaEmpresaDal.ReceitaMensal(ano, empresa).Select(r => new
            {
                Ano = ano,
                MesInt = r.Key,
                MesString = r.Key.NomeMes(),
                Valor = r.Value,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Value).Replace("R$", "")
            }).OrderBy(r => r.MesInt);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReceitaDetalhada(int ano, int mes, EEmpresa empresa)
        {
            var retorno = _receitaEmpresaDal.ReceitaDetalhada(ano, mes, empresa).Select(r => new
            {
                Data = r.Data.ToString("d"),
                NotaFiscal = r.NotaFiscal,
                CpfCnpj = r.CpfCnpj,
                Cliente = r.Cliente,
                Receita = r.Receita,
                ValorFormatado = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", r.Receita).Replace("R$", "")
            }).OrderByDescending(r => r.Receita);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}