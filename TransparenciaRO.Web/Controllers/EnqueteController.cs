﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Controllers
{
    public class EnqueteController : ControllerBase
    {
        private DbTransparencia db = new DbTransparencia();
        private readonly EnqueteDAL _enqueteDal;

        public EnqueteController()
        {
            _enqueteDal = new EnqueteDAL(db);
        }

        // GET: Enquete
        public ActionResult Index()
        {
            return View(_enqueteDal.Enquetes());
        }

        public ActionResult Novo()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Novo(Enquete enquete)
        {
            if (ModelState.IsValid)
            {
                _enqueteDal.Add(enquete);
                MensagemSucesso = "Enquete registrado com sucesso";
                return RedirectToAction("Index");
            }
            MensagemErro = "Ocorreu um erro, por favor verifique se os campos estão sendo preenchidos";
            return View(enquete);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAvaliacao(EnqueteResultado enqueteResultado)
        {
            ViewBag.Enquete = _enqueteDal.GetById(enqueteResultado.EnqueteId);
            _enqueteDal.AddResult(enqueteResultado);
            return PartialView("_resultadoAvaliacao");
        }

        public JsonResult ResultadoAvaliacao(int id)
        {
            var resultado = _enqueteDal.EnqueteResultados(id)
                .Select(r => new
                {
                    Avaliacao = r.Key,
                    Qtd = r.Value
                })
                .OrderBy(x => x.Avaliacao == "Péssima")
                .ThenBy(x => x.Avaliacao == "Ruim")
                .ThenBy(x => x.Avaliacao == "Indiferente")
                .ThenBy(x => x.Avaliacao == "Bom")
                .ThenBy(x => x.Avaliacao == "Ótimo");
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Atualizar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Enquete enquete = _enqueteDal.GetById(id);
            if (enquete == null)
            {
                return HttpNotFound();
            }
            return View(enquete);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Atualizar(Enquete enquete)
        {
            if (ModelState.IsValid)
            {
                _enqueteDal.Update(enquete);
                MensagemSucesso = "Enquete atualizado com sucesso";
                return RedirectToAction("Index");
            }

            MensagemErro = "Ocorreu um erro, por favor verifique se os campos estão sendo preenchidos";
            return View(enquete);
        }


        public ActionResult Delete(int id)
        {
            Enquete enquete = _enqueteDal.GetById(id);
            if (enquete.EnqueteResultados.Count > 0)
            {
                MensagemErro = "Enquete já possui avaliações, impossivel remover";
                return RedirectToAction("Index");
            }
            db.Enquetes.Remove(enquete);
            db.SaveChanges();
            MensagemSucesso = "Enquete removido com sucesso";
            return RedirectToAction("Index");
        }


        public ActionResult GetEnquete()
        {
            var enquete = _enqueteDal.Enquetes().Randomize().FirstOrDefault();

            ViewBag.Enquete = enquete;

            return PartialView("_Enquete");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
