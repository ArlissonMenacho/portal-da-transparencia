﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;

namespace TransparenciaRO.Web.Controllers
{
    public class FundoEmpresaController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly FundoEmpresaDAL _fundoEmpresa;

        public FundoEmpresaController()
        {
            _fundoEmpresa = new FundoEmpresaDAL(_dbTransparencia);
        }


        public ActionResult Index(EEmpresa empresa)
        {
            return View(_fundoEmpresa.BuscarTodos(empresa));
        }
        

        [Authorize(Roles = nameof(AdministradorRemuneracaoServidoresEmpresaSoph) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCmr) + "," + nameof(AdministradorRemuneracaoServidoresEmpresaCaerd))]
        public ActionResult ImportarDados(EEmpresa empresa)
        {
            ViewBag.Empresa = empresa.ToString().ToUpper();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportarDados(HttpPostedFileBase file,  EEmpresa empresa)
        {
            try
            {
                if (file.FileName.EndsWith("xls") || file.FileName.EndsWith("xlsx"))
                {
                    var tempoInicial = DateTime.Now;
                    var path = Server.MapPath("~/App_Data/Arquivos/" + file.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);

                    file.SaveAs(path);
                    
                    using (var db = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0; Data Source = {path};Extended Properties = 'Excel 12.0; HDR = YES; ReadOnly = true; IMEX = 1';"))
                    {
                        db.Open();
                        var fundosEmpresa = new List<FundoEmpresa>();

                        var dtSchema =
                            db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" })
                                ?.Rows[0]["TABLE_NAME"].ToString();
                        using (var cmd = new OleDbCommand($"SELECT * FROM [{dtSchema}]", db))
                        {
                            var dr = cmd.ExecuteReader();
                            while (dr != null && dr.Read())
                            {
                               var fundoEmpresa = new FundoEmpresa
                               {
                                   Processo = dr.GetValue(dr.GetOrdinal("NumeroProcesso")).ToString(),
                                   Objeto = dr.GetValue(dr.GetOrdinal("Objeto")).ToString(),
                                   FundamentoGeral = dr.GetValue(dr.GetOrdinal("FundamentoGeral")).ToString(),
                                   Favorecido = dr.GetValue(dr.GetOrdinal("Favorecido")).ToString(),
                                   CnpjCpf = dr.GetValue(dr.GetOrdinal("CnpjCpf")).ToString(),
                                   Modalidade = dr.GetValue(dr.GetOrdinal("Modalidade")).ToString(),
                                   ValorEstimado = Convert.ToDecimal(dr.GetValue(dr.GetOrdinal("ValorEstimado")).ToString()),
                                   Vigencia = dr.GetValue(dr.GetOrdinal("Vigencia")).ToString(),
                                   DataEntrada = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataEntrada")).ToString()),
                                   DataDecisao = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataDecisao")).ToString()),
                                   DataPublicacao = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataPublicacao")).ToString()),
                                   DataContabilizada = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataContabilizada")).ToString()),
                                   DataPagamento = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataPagamento")).ToString()),
                                   DataArquivamento = Convert.ToDateTime(dr.GetValue(dr.GetOrdinal("DataArquivamento")).ToString()),
                                   Situacao = dr.GetValue(dr.GetOrdinal("Situacao")).ToString(),
                                   EEmpresa = empresa
                               };
                                _fundoEmpresa.VerificarDados(fundoEmpresa.Processo, empresa);
                                fundosEmpresa.Add(fundoEmpresa);
                            }
                           
                            _fundoEmpresa.Salvar(fundosEmpresa);
                        }
                    }
                    System.IO.File.Delete(path);

                    LogUtils.Logar($"usuário:  {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), importou dados de Diarias e Viagens de Servidores {empresa} -:", AuthenticationManager.User.IdUsuario());

                    MensagemSucesso = $"Dados importados com sucesso, tempo estimado: {Math.Round((DateTime.Now - tempoInicial).TotalSeconds, 1)} segundos";
                    ViewBag.Empresa = empresa.ToString().ToUpper();
                    return RedirectToAction("ImportarDados", "FundoEmpresa", new { empresa });
                }
                ViewBag.Empresa = empresa.ToString().ToUpper();
                MensagemErro = "Arquivo Inválido";
                return View();
            }
            catch
            {
                MensagemErro = "Ocorreu um erro ao tentar importar o arquivos, caso o erro persista entre em contato com o suporte: contato@ro.gov.br";
                ViewBag.Empresa = empresa.ToString().ToUpper();
                return RedirectToAction("ImportarDados", "FundoEmpresa", new { empresa });
            }
        }


        public ActionResult Cosultar(DateTime dataInicial, DateTime dataFinal, string cnpjCpf, string favorecido)
        {
            var fundoEmpresa = _fundoEmpresa.BuscarFiltro(dataInicial, dataFinal, cnpjCpf, favorecido);
            return PartialView("_ResultadoFundo", fundoEmpresa);
        }
    }
}