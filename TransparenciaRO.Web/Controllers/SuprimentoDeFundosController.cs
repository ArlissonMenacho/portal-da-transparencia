﻿using System.Threading.Tasks;
using System.Web.Mvc;
using TransparenciaRO.DataDBPLL.Contexto;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Web.Controllers
{
    public class SuprimentoDeFundosController : ControllerBase
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly TransparenciaRO.DataDBPLL.Contexto.DbPLLContext _dbPLLContext = new DbPLLContext();
        private readonly SuprimentoDeFundosDAL _suprimentoDeFundosDAL;

        public SuprimentoDeFundosController()
        {
            _suprimentoDeFundosDAL = new SuprimentoDeFundosDAL(_dbTransparencia, _dbPLLContext);
        }

        public async Task<ActionResult> Consultar()
        {
            var suprimentoVM = await _suprimentoDeFundosDAL.BuscarDadosParaCarregamentoDaTelaDeConsulta();

            return View(suprimentoVM);
        }

        [HttpPost]
        public ActionResult Filtrar(string unidadeGestora, short? exercicio)
        {
            var suprimentos = _suprimentoDeFundosDAL.FiltrarSuprimentos(unidadeGestora, exercicio);

            return PartialView("_Tabela", suprimentos);
        }
    }
}