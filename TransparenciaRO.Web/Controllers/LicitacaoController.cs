﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.IO;
using static TransparenciaRO.Infra.Enum.EPerfilUsuario;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using Newtonsoft.Json;
using TransparenciaRO.Infra.Model.View.ProcessosAPISUPEL;
using TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL;
using TransparenciaRO.Infra.Model.View.DadosLicitacao;

namespace TransparenciaRO.Web.Controllers
{
    public class LicitacaoController : CRUDControllerBase<Licitacao>
    {
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();
        private readonly LicitacaoDAL _licitacaoDal;
        private readonly ArquivoDAL _arquivoDal;
        private readonly UnidadeGestoraDAL _unidadeGestoraDal;
        private readonly LicitacoesDAL _licitacoesDAL;
        private readonly FornecedorDispensaDAL _fornecedorDispensaDAL;
        private readonly EmailNotificacaoDAL _emailNotificacaoDAL;
        private readonly DadosLicitacaoDAL _dadosLicitacaoDAL;

        public LicitacaoController()
        {
            _licitacaoDal = new LicitacaoDAL(_dbTransparencia);
            _arquivoDal = new ArquivoDAL(_dbTransparencia);
            _unidadeGestoraDal = new UnidadeGestoraDAL(_dbTransparencia);
            _licitacoesDAL = new LicitacoesDAL(_dbTransparencia);
            _fornecedorDispensaDAL = new FornecedorDispensaDAL(_dbTransparencia);
            _emailNotificacaoDAL = new EmailNotificacaoDAL(_dbTransparencia);
            _dadosLicitacaoDAL = new DadosLicitacaoDAL(_dbTransparencia);
        }

        public override ActionResult Index()
        {

            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            //Verifica se possui arquivos temporarios excluindo os mesmos
            _arquivoDal.RemoverTemporario(_arquivoDal.BuscarArquivoTemporarios());

            return View();
        }

        public ActionResult ComprasEmergenciais(bool? exibirAlerta)
        {
            var unidadesGestornasQueNaoDevemDeverExibidas = new List<string>()
            {
                "61035efc-b006-4766-91b3-5969cbbfce29", // TRIBUNAL DE JUSTIÇA
                "6fc52dab-caef-4f5e-b3de-dcc029f45137", // TRIBUNAL DE CONTAS DO ESTADO
                "e1b71b56-7f01-4e93-a19c-cb1051654322", // UG CADASTRO
                "c10a7193-fd4d-4903-b3fe-a51f2473858b", // MINISTÉRIO PÚBLICO
                "f03a3b21-6c29-488d-b324-d36d79af1fa7" // ASSEMBLÉIA LEGISLATIVA DE RONDÔNIA
            };

            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasGuid()
                .Union(new Dictionary<string, string> { { "", "Todas" } })
                .Where(ug => !unidadesGestornasQueNaoDevemDeverExibidas.Contains(ug.Key))
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);

            //Verifica se possui arquivos temporarios excluindo os mesmos
            _arquivoDal.RemoverTemporario(_arquivoDal.BuscarArquivoTemporarios());

            if (exibirAlerta == true)
            {
                MensagemSucesso = "Dispensa de licitação salva com sucesso!";
            }

            return View();
        }

        public ActionResult Consulta()
        {
            ViewBag.Modalidades = _licitacoesDAL.ObterTodos().Select(l => l.Modalidade).Distinct().ToList();
            return View();
        }

        public JsonResult ObterLicitacoesPorFiltro(int? ano, string modalidade, int? numeroLicitacao, string numeroProcesso)
        {
            var qtdBusca = 1000;
            List<Licitacaoo> dados = new List<Licitacaoo>();

            foreach (var licitacao in _licitacoesDAL.ObterPorFiltro(ano.ToString(), modalidade, numeroLicitacao.ToString(), numeroProcesso, qtdBusca))
            {

                if (licitacao.Modalidade == "Pregão Eletrônico para Registro de Preços" || licitacao.Modalidade == "Pregão Eletrônico")
                {
                    licitacao.CaminhoArquivo = Directory
                            .GetFiles(Server.MapPath("/Licitacoes"))
                            .Where(f => f.Contains(licitacao.Pregao.Length == 7 ? "0" + licitacao.Pregao : licitacao.Pregao.Length == 6 ? "00" + licitacao.Pregao : licitacao.Pregao))
                            .Select(f => new CaminhoArquivo { Arquivo = Path.GetFileName(f) })
                            .ToList();


                    foreach (var caminhoArquivo in licitacao.CaminhoArquivo)
                    {
                        if (caminhoArquivo.Arquivo.Contains("Complementar"))
                            caminhoArquivo.DescricaoArquivo = "ata Complementar";
                        else
                            caminhoArquivo.DescricaoArquivo = "ata";
                    }
                }

                dados.Add(licitacao);
            }

            return Json(new
            {
                msgInfo = dados.Count() == qtdBusca ? $"Esta consulta teve o seu resultado limitado em {dados.Count()} registros" : null,
                dados = dados
            });
        }

        public ActionResult Resultado(string numeroLicitacao)
        {
            return View(_licitacoesDAL.ObterPorFiltro(numeroLicitacao));
        }

        public async Task<ActionResult> DownloadLicitacoesCSV(int? ano)
        {
            if (ano != 2021)
            {
                return HttpNotFound();
            }

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Accept", "application/json");

            var linkBase = $"http://www.rondonia.ro.gov.br/supel/licitacoes/?json=true&ano={ano}&paged=";

            var getDadosPagina1 = await client.GetStringAsync(linkBase + "1");

            dynamic dadosPagina1 = JToken.Parse(getDadosPagina1);

            var itens = new List<dynamic>();

            foreach (var item in dadosPagina1.itens)
                itens.Add(item);

            for (var pagina = 2; pagina <= Convert.ToInt32(dadosPagina1.pagina.total); pagina++)
            {
                var getDados = await client.GetStringAsync(linkBase + pagina.ToString());

                dynamic dadosPaginas = JToken.Parse(getDados);

                foreach (var item in dadosPaginas.itens)
                    itens.Add(item);
            }

            var sb = new StringBuilder();

            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                            "Enfrentamento ao COVID-19",
                            "N. Licitação",
                            "Ano",
                            "Modalidade",
                            "Unidade",
                            "Número do processo",
                            "Projeto/Atividade",
                            "Elemento despesa",
                            "Valor estimado",
                            "situação",
                            "Data abertura",
                            "Hora abertura",
                            "Fuso horário",
                            "Endereço eletrônico",
                            "Local",
                            "Mais informações",
                            "Pregoeiro",
                            "Arquivo",
                            "Objeto"
                            );

            sb.Append(Environment.NewLine);

            foreach (var item in itens)
            {
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                                TratarStringAPI(item.meta.enfrentamentoCovid),
                                TratarStringAPI(item.meta.numeroLicitacao),
                                TratarStringAPI(item.meta.ano),
                                TratarStringAPI(item.meta.modalidade),
                                TratarStringAPI(item.meta.unidadeAdministrativa),
                                TratarStringAPI(item.meta.numeroProcesso),
                                TratarStringAPI(item.meta.projetoAtividade),
                                TratarStringAPI(item.meta.despesa),
                                TratarStringAPI(item.meta.valor),
                                TratarStringAPI(item.meta.situacao),
                                TratarStringAPI(item.meta.dataAbertura),
                                TratarStringAPI(item.meta.horaAbertura),
                                TratarStringAPI(item.meta.fusoHorario),
                                TratarStringAPI(item.meta.url),
                                TratarStringAPI(item.meta.local),
                                TratarStringAPI(item.meta.maisInformacoes),
                                TratarStringAPI(item.meta.pregoeiro),
                                TratarStringAPI(item.anexo.url),
                                TratarStringAPI(item.conteudo)
                                );
                sb.Append(Environment.NewLine);
            }

            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"Licitacoes{ano}.csv");
        }

        public ActionResult DownloadLicitacoes()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Licitacoes/";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "licitacoes.csv");
            string fileName = "licitacoes.csv";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public async Task<ActionResult> DownloadResultadoLicitacoesCSV()
        {
            try
            {
                string dados = await GetJsonAPI($"http://qa.soluctions.supel.ro.gov.br/cronos/api/processos");

                var jsonResult = JsonConvert.DeserializeObject(dados).ToString();

                var processosAPISUPELVM = JsonConvert.DeserializeObject<ProcessosAPISUPELViewModel>(jsonResult);

                var sb = new StringBuilder();

                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                                "N. Licitação",
                                "Modalidade",
                                "Unidade",
                                "Objeto",
                                "item",
                                "Valor unitario",
                                "Valor global",
                                "CNPJ",
                                "Empresa vencedora"
                                );

                sb.Append(Environment.NewLine);

                foreach (var processo in processosAPISUPELVM.processos)
                    foreach (var certame in processo.Certames)
                        foreach (var item in certame.Itens.Where(item => (item.Situacao.Contains("Adjudicado") || item.Situacao.Contains("adjudicado")))
                                                          .OrderBy(i => i.Numero))
                        {
                            sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                                   TratarStringAPI(certame.NumeroLicitacao),
                                   TratarStringAPI(processo.ModalidadeLicitacao),
                                   TratarStringAPI(processo.Orgao.Descricao),
                                   TratarStringAPI(item.Descricao),
                                   TratarStringAPI(item.Numero),
                                   TratarStringAPI(item.ValorUnitario),
                                   TratarStringAPI(item.ValorGlobal),
                                   TratarStringAPI(item.Fornecedor.CNPJ),
                                   TratarStringAPI(item.Fornecedor.RazaoSocial)
                                   );
                            sb.Append(Environment.NewLine);
                        }

                return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", $"ResultadoLicitacoes.csv");
            }
            catch
            {
                return HttpNotFound();
            }
        }

        //public void DownloadResultadoLicitacoesCSV()
        //{
        //    var licitacoes = _licitacoesDAL.ObterTodos();

        //    var sb = new StringBuilder();
        //    sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8}", "N. Licitação", "Modalidade", "Unidade", "Objeto", "item", "Valor Estimado", "Valor adjudicado", "CNPJ", "Empresa vencedora");
        //    sb.Append(Environment.NewLine);
        //    foreach (var item in licitacoes)
        //    {
        //        sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8}", item.Pregao, item.Modalidade, item.Unidade_Gestora, item.Objeto, item.Item, item.Valor_Est, item.Valor_Adj, item.CNPJ, item.Empresa_Vencedora);
        //        sb.Append(Environment.NewLine);
        //    }

        //    var response = System.Web.HttpContext.Current.Response;
        //    response.BufferOutput = true;
        //    response.Clear();
        //    response.ClearHeaders();
        //    response.ContentEncoding = Encoding.Unicode;
        //    response.AddHeader("content-disposition", "attachment;filename=resultado_licitacoes.CSV ");
        //    response.ContentType = "text/plain";
        //    response.Write(sb.ToString());
        //    response.End();
        //}

        private string TratarStringAPI(dynamic variavel) => variavel != null ? variavel.ToString().Replace(";", " ").Replace("\r", " ").Replace("\n", " ") : variavel;

        private async Task<string> GetJsonAPI(string path)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return await client.GetStringAsync(path);
        }

        //public async Task<ActionResult> VisualizarResultado(string numeroLicitacao)
        //{
        //    var num = numeroLicitacao.Replace("-", "");
        //    dynamic dadosResultados = JToken.Parse(await GetJsonAPI($"http://compras.dados.gov.br/pregoes/v1/resultados_pregao.json?nu_pregao={num}&co_uasg=925373"));

        //    var resultados = new List<ResultadoPregaoViewModel>();

        //    foreach (var pregao in dadosResultados._embedded.pregoes)
        //    {
        //        var resultado = new ResultadoPregaoViewModel();
        //        resultado.Itens = new List<Item>();

        //        resultado.NomeFornecedor = pregao._links.self.title;
        //        string dadosItens = await GetJsonAPI($"http://compras.dados.gov.br" + pregao._links.Itens.href);
        //        dynamic dadosItensDynamic = JToken.Parse(dadosItens.Replace(@"item fornecedor", "itemFornecedor"));

        //        foreach (var item in dadosItensDynamic._embedded.itemFornecedor)
        //            resultado.Itens.Add(new Item { UnidadeFornecimento = item.ds_unidade_forn, Quantidade = item.qt_itens, Descricao = item.ds_item, DescricaoDetalhada = item.ds_item_detalhada, Marca = item.ds_marcaitem, Valor = item.vl_unitario, ValorTotal = item.vl_global });

        //        resultados.Add(resultado);
        //    }

        //    return View(resultados);
        //}

        //private async Task<string> GetJsonAPI(string path)
        //{
        //    HttpClient client = new HttpClient();
        //    client.DefaultRequestHeaders.Add("Accept", "application/json");
        //    return await client.GetStringAsync(path);
        //}

        public ActionResult VisualizarLicitacao(string pEncLicitacaoId)
        {
            if (pEncLicitacaoId == null)
            {
                MensagemErro = "Número da licitação informada inválida";
                return View("Index");
            }

            var licitacao = _licitacaoDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncLicitacaoId)));
            if (licitacao != null)
                return View("VisualizarLicitacao", licitacao);

            MensagemErro = "Número da licitação informada inválida";
            return View("Index");
        }

        public ActionResult VisualizarDispensaLicitacao(string pEncLicitacaoId)
        {
            if (pEncLicitacaoId == null)
            {
                MensagemErro = "Número da licitação informada inválida";
                return View("Index");
            }

            var licitacao = _licitacaoDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncLicitacaoId)));

            if (licitacao != null)
                return View(licitacao);

            MensagemErro = "Número da licitação informada inválida";

            return View("Index");
        }

        public ActionResult AdicionarArquivo(Licitacao licitacao)
        {
            // complemento serve para concatenar a razão social do fornecedor selecionado no modal de adicionar arquivo com o campo "descrição".
            // atualizacao: utilizando Numero de empenho na variavel razao social
            var razaoSocialFornecedor = licitacao.ArquivoTemporario.RazaoSocialFornecedor;

            var descricao = licitacao.ArquivoTemporario.Descricao + (razaoSocialFornecedor == "null" ? null : " (NE: " + razaoSocialFornecedor + ")");

            var tipoAnexo = licitacao.ArquivoTemporario.TipoAnexo;

            var fornecedorId = licitacao.ArquivoTemporario.FornecedorDispensaId;

            var msgErrosArquivos = new List<String>();

            for (var i = 0; i < Request.Files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(Request.Files[i]))
                {
                    // Novos arquivos (salva no disco e BD)
                    var arquivo = new Arquivo
                    {
                        ArquivoId = Guid.NewGuid(),
                        LicitacaoId = licitacao.LicitacaoId == new Guid() ? null : (Guid?)licitacao.LicitacaoId,
                        FornecedorDispensaId = tipoAnexo == ETipoAnexo.InstrumentoContratual ? fornecedorId : null,
                        UsuarioIdUpload = AuthenticationManager.User.IdUsuario(),
                        Extensao = Path.GetExtension(Request.Files[i].FileName),
                        Descricao = descricao,
                        PastaId = GuidUtils.GuidPastaContratosEConvenios(),
                        TipoAnexo = tipoAnexo,
                        Temporario = true //Arquivos com esta propriedade serão excluídos automaticamente por uma rotina de varredura diária no servidor
                    };

                    if (ArquivoUtils.SalvarArquivo(Request.Files[0], arquivo.ArquivoId))
                    {
                        _arquivoDal.Adicionar(arquivo);

                        licitacao.Arquivos.Add(arquivo);
                    }
                    else
                        msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                }
                else
                    msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo {Request.Files[i].FileName}. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
            }

            ViewBag.MensagensErrosArquivos = msgErrosArquivos;

            return PartialView("_ListaArquivos", licitacao);
        }

        public ActionResult AdicionarArquivoFornecedor(FornecedorDispensa fornecedor)
        {
            var teste = Request.Files[0];

            var descricao = fornecedor.ArquivoTemporario.Descricao;

            var tipoAnexo = ETipoAnexo.InstrumentoContratual;

            var msgErrosArquivos = new List<String>();

            var arquivo = new Arquivo();

            for (var i = 0; i < Request.Files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(Request.Files[i]))
                {
                    // Novos arquivos (salva no disco e BD)

                    arquivo.ArquivoId = Guid.NewGuid();
                    arquivo.FornecedorDispensaId = null;
                    arquivo.UsuarioIdUpload = AuthenticationManager.User.IdUsuario();
                    arquivo.Extensao = Path.GetExtension(Request.Files[i].FileName);
                    arquivo.Descricao = descricao;
                    arquivo.PastaId = GuidUtils.GuidPastaContratosEConvenios();
                    arquivo.TipoAnexo = tipoAnexo;
                    arquivo.Temporario = true; //Arquivos com esta propriedade serão excluídos automaticamente por uma rotina de varredura diária no servidor

                    if (ArquivoUtils.SalvarArquivo(Request.Files[0], arquivo.ArquivoId))
                    {
                        _arquivoDal.Adicionar(arquivo);

                        if (fornecedor.Arquivos == null)
                            fornecedor.Arquivos = new List<Arquivo>();

                        arquivo.FornecedorDispensaId = fornecedor.FornecedorId;

                        fornecedor.Arquivos.Add(arquivo);
                    }
                    else
                        msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                }
                else
                    msgErrosArquivos.Add($"Não foi possível fazer o upload do arquivo {Request.Files[i].FileName}. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
            }

            var sucesso = msgErrosArquivos.Count() > 0 ? false : true;

            return Json(new { Sucesso = sucesso, Dados = arquivo, Mensagem = msgErrosArquivos });
        }

        public ActionResult RemoverArquivoFornecedorDispensa(Guid arquivoId)
        {
            var arquivoExiste = ArquivoUtils.ArquivoExiste(arquivoId);

            if (arquivoExiste)
            {
                var arquivoExcluido = ArquivoUtils.ExcluirArquivo(arquivoId);

                _arquivoDal.RemoverArquivofornecedorDispensa(arquivoId);

                if (arquivoExcluido)
                    return Json(new { Sucesso = true });
            }

            return Json(new { Sucesso = false, Mensagem = "Não foi possível excluir o arquivo devido a um erro interno do sistema." });
        }

        public ActionResult RemoverArquivo(Licitacao dados, string guidArquivo)
        {
            if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)) && dados.LicitacaoId != new Guid())
            {
                //Foi solicitada a exclusão de um arquivo de uma licitação já cadastrada no BD. Nada será feito agora. Apenas na hora de salvar a licitação, onde será comparado o BD com a lista atual
            }
            else if (ArquivoUtils.ArquivoExiste(new Guid(guidArquivo)))
            {
                //Arquivo ainda é temporário, poderá ser excluído fisicamente do disco (ainda não existe na base)
                ArquivoUtils.ExcluirArquivo(new Guid(guidArquivo));
            }
            //Remove do array de arquivos que está sendo utilizado pela view
            dados.Arquivos.Remove(dados.Arquivos.FirstOrDefault(a => a.ArquivoId == new Guid(guidArquivo)));
            //Devolve à view  o modelo atualizado (apenas a Partial _ListaArquivos.cshtml será renderizada novamente, e apenas a propriedade Arquivos acessada)
            return PartialView("_ListaArquivos", dados);
        }

        [Authorize(Roles = nameof(AdministradorLicitacoes))]
        public override ActionResult AdicionarEditar(string pEncLicitacaoId)
        {
            try
            {
                PopularViewBagUnidadesGestoras();

                if (pEncLicitacaoId == null)
                {
                    return View("AdicionarEditar", new Licitacao());
                }

                var licitacao = _licitacaoDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncLicitacaoId)));
                if (licitacao == null)
                {
                    MensagemErro = "Número da licitação informada inválida";
                    return View("Index");
                }
                return View("AdicionarEditar", licitacao);
            }
            catch (Exception e)
            {
                MensagemErro = e.Message;
                return View("Index");
            }
        }

        [ValidateAntiForgeryToken]
        public override ActionResult AdicionarEditar(Licitacao dados)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagemErro = "Dados inválidos. Por favor, verifique o cadastro";
                    PopularViewBagUnidadesGestoras();
                    return View(dados);
                }

                var msgErro = "";
                dados.DataDaPublicacao = DateTime.Now;
                var guid = _licitacaoDal.AdicionarEditar(dados, out msgErro);
                if (msgErro != "")
                {
                    MensagemErro = msgErro;
                    return RedirectToAction("Index");
                }

                MensagemSucesso = $"Licitação salva com sucesso!";
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                MensagemErro = $"Ocorreu um erro inesperado: ''. Caso o problema persista, entre em contato com a CGE";
                return View("Index");
            }
        }

        [Authorize(Roles = nameof(AdministradorLicitacoes))]
        public ActionResult AdicionarEditarDispensaLicitacao(string pEncLicitacaoId)
        {
            try
            {
                PopularViewBagUnidadesGestorasParaTelaDeDispensaLicitacoes();

                Licitacao licitacao;

                if (pEncLicitacaoId == null)
                {
                    licitacao = new Licitacao
                    {
                        LicitacaoId = new Guid(),
                        Arquivos = new List<Arquivo>(),
                        Fornecedores = new List<FornecedorDispensa>(),
                        Ano = DateTime.Now.Year,
                        ModalidadeLicitacao = EModalidadeLicitacao.DispensaContratacaoDireta,
                        ItensLicitacao = new List<ItemLicitacao>()
                    };

                    return View(licitacao);
                }

                licitacao = _licitacaoDal.Buscar(new Guid(CriptografiaUtils.Decrypt(pEncLicitacaoId)));

                if (licitacao == null)
                {
                    MensagemErro = "Número da licitação informada inválida";

                    return View(nameof(ComprasEmergenciais));
                }

                TratarDadosDosFornecedores(licitacao);
                TratarDadosDosItens(licitacao);

                return View(licitacao);
            }
            catch (Exception e)
            {
                MensagemErro = e.Message;

                return View("Index");
            }
        }
        public ActionResult LicitacaoExiste(string NumeroProcesso)
        {
            var licitacao = _licitacaoDal.LicitacaoExiste(NumeroProcesso);
            if (licitacao)
            {
                return Json(new { Sucesso = true, Mensagem = $"Número de processo já cadastrado: { string.Join(" / ", NumeroProcesso) }" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Sucesso = false }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = nameof(AdministradorLicitacoes)), HttpPost]
        public ActionResult AdicionarEditarDispensaLicitacao(Licitacao dados)
        {
            VerificarCamposObrigatorios(dados);

            try
            {

                if (!ModelState.IsValid)
                {
                    var erros = new List<string>();

                    foreach (var value in ModelState.Values)
                    {
                        foreach (var erro in value.Errors)
                            erros.Add(erro.ErrorMessage);
                    }

                    PopularViewBagUnidadesGestorasParaTelaDeDispensaLicitacoes();

                    return Json(new { Sucesso = false, Mensagem = $"Dados inválidos. Erros(s): { string.Join(" / ", erros.ToArray()) }" });
                }

                var msgErro = "";

                if (dados.LicitacaoId == new Guid())
                    dados.DataDaPublicacao = DateTime.Now;

                foreach (var f in dados.Fornecedores)
                    if (f.DataAssinatura == default(DateTime))
                        f.DataAssinatura = null;

                TratarDadosDosFornecedores(dados);
                TratarDadosDosItens(dados);

                Guid? guid;

                var exist = _licitacaoDal.LicitacaoExiste(dados.NumeroProcessoAdministrativo);

                if (exist)
                {

                    LogUtils.Logar($"usuário: {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), UG: {AuthenticationManager.User.UgID() }, Editou a Licitação:  (Modadalidade de Licitação: {dados.ModalidadeLicitacao}, Ano: {dados.Ano}, Numero da Licitação: {dados.NumeroLicitacao}, Numero do Processo Administrativo: {dados.NumeroProcessoAdministrativo}, Fonte do Recurso: {dados.FonteDeRecurso}, Projeto de Atividade: {dados.ProjetoAtividade}, Elemento de Despesa: {dados.ElementoDespesa}, Objeto: {dados.Objeto}, Mais informações: {dados.MaisInformacoes})", AuthenticationManager.User.IdUsuario());

                    guid = _licitacaoDal.EditarDispensaLicitacao(dados, out msgErro);

                }
                else
                {
                    guid = _licitacaoDal.AdicionarDispensaLicitacao(dados, out msgErro);

                    LogUtils.Logar($"usuário: {User.Identity.Name} (Id: {AuthenticationManager.User.IdUsuario()}), UG: {AuthenticationManager.User.UgID() }, Cadastrou uma nova Licitação:  (Modadalidade de Licitação: {dados.ModalidadeLicitacao}, Ano: {dados.Ano}, Numero da Licitação: {dados.NumeroLicitacao}, Numero do Processo Administrativo: {dados.NumeroProcessoAdministrativo}, Fonte do Recurso: {dados.FonteDeRecurso}, Projeto de Atividade: {dados.ProjetoAtividade}, Elemento de Despesa: {dados.ElementoDespesa}, Objeto: {dados.Objeto}, Mais informações: {dados.MaisInformacoes})", AuthenticationManager.User.IdUsuario());

                    var link = "http://transparencia.ro.gov.br/Licitacao/VisualizarDispensaLicitacao?pEncLicitacaoId=" + dados.LicitacaoId.ToString().ToEncrypted();

                    var emailsToNotify = _emailNotificacaoDAL.GetEmailsToNotifyDispensaLicitacaoAdded();

                    emailsToNotify.ForEach(emailToNotify =>
                    {
                        var message = $"Olá, cidadão!.<br /><br />" +
                            $"Os seguintes conteúdos do seu interesse  sobre os gastos governamentais destinados ao " +
                            $"enfrentamento da COVID 19 no Estado de Rondônia foram Inseridos/Alterados:<br />" +
                            $"Processo de Dispensa de Licitação { dados.NumeroProcessoAdministrativo }, você pode acessa-lo <a href='{ link }'>clicando aqui</a>.<br /><br />" +
                            $"Caso você não queira mais receber notificações como esta, <a href='http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id }'>clique aqui</a>, " +
                            $"ou copie o link abaixo e cole em uma nova aba.<br />" +
                            $"http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id } <br /><br />" +
                            $"Este é um e-mail automático, portanto, não é necessário respondê-lo.";

                        var subject = $"Transparência Proativa | Controladoria Geral do Estado de Rondônia";

                        MailUtils.SendMail(message, subject, true, emailToNotify.Email);
                    });
                }

                if (msgErro != "")
                {
                    return Json(new { Sucesso = false, Mensagem = "Ocorreu um erro inesperado. Caso o problema persista, entre em contato com a CGE" });
                }

                #region Notificar usuários quando for inserido a Íntegra do Processo
                var integraProcesso = dados.Arquivos.Where(arquivo => arquivo.TipoAnexo.Equals(ETipoAnexo.IntegraProcesso)).FirstOrDefault();

                if (integraProcesso != null && integraProcesso.Temporario)
                {
                    var link = "http://transparencia.ro.gov.br/Licitacao/VisualizarDispensaLicitacao?pEncLicitacaoId=" + dados.LicitacaoId.ToString().ToEncrypted();

                    var emailsToNotify = _emailNotificacaoDAL.GetEmailsToNotifyIntegraProcessoAdded();

                    emailsToNotify.ForEach(emailToNotify =>
                    {
                        var message = $"Olá, cidadão!.<br /><br />" +
                            $"Os seguintes conteúdos do seu interesse  sobre os gastos governamentais destinados ao " +
                            $"enfrentamento da COVID 19 no Estado de Rondônia foram Inseridos/Alterados:<br />" +
                            $"Integra do Processo: { dados.NumeroProcessoAdministrativo }, você pode acessa-lo <a href='{ link }'>clicando aqui</a>.<br /><br />" +
                            $"Caso você não queira mais receber notificações como esta, <a href='http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id }'>clique aqui</a>, " +
                            $"ou copie o link abaixo e cole em uma nova aba.<br />" +
                            $"http://transparencia.ro.gov.br/Home/DesabilitarNotificacaoPorEmail?emailId={ emailToNotify.Id } <br /><br />" +
                            $"Este é um e-mail automático, portanto, não é necessário respondê-lo.";

                        var subject = $"Transparência Proativa | Controladoria Geral do Estado de Rondônia";

                        MailUtils.SendMail(message, subject, true, emailToNotify.Email);
                    });
                }
                #endregion

                return Json(new { Sucesso = true, Mensagem = "Licitação salva com sucesso!" });
            }
            catch
            {
                return Json(new { Sucesso = false, Mensagem = "Ocorreu um erro inesperado. Caso o problema persista, entre em contato com a CGE" });
            }
        }

        private void TratarDadosDosFornecedores(Licitacao licitacao)
        {
            foreach (var f in licitacao.Fornecedores)
            {
                f.CNPJ = TratarConteudoDoCampoString(f.CNPJ);
                f.LocalExecucao = TratarConteudoDoCampoString(f.LocalExecucao);
                f.NumeroEmpenho = TratarConteudoDoCampoString(f.NumeroEmpenho);
                f.RazaoSocial = TratarConteudoDoCampoString(f.RazaoSocial);
                f.Vigencia = TratarConteudoDoCampoString(f.Vigencia);
            }
        }

        private void TratarDadosDosItens(Licitacao licitacao)
        {
            if (licitacao.ItensLicitacao != null)
                foreach (var i in licitacao.ItensLicitacao)
                {
                    i.Descricao = TratarConteudoDoCampoString(i.Descricao);
                    i.NumeroEmpenho = TratarConteudoDoCampoString(i.NumeroEmpenho);
                    i.Unidade = TratarConteudoDoCampoString(i.Unidade);

                }
        }

        private string TratarConteudoDoCampoString(string conteudo)
        {
            if (conteudo == null)
                return null;

            return conteudo.Replace("\"", "").Replace("“", "").Replace("”", "").Replace("\t", " ").Replace("	", " ").Trim();
        }

        private void VerificarCamposObrigatorios(Licitacao dados)
        {
            if (dados.UnidadeGestoraId == null ||
                dados.UnidadeGestoraId == Guid.Empty ||
                string.IsNullOrEmpty(dados.NumeroProcessoAdministrativo) ||
                string.IsNullOrEmpty(dados.FonteDeRecurso) ||
                string.IsNullOrEmpty(dados.ProjetoAtividade) ||
                string.IsNullOrEmpty(dados.ElementoDespesa) ||
                string.IsNullOrEmpty(dados.Objeto) ||
                dados.Fornecedores.Count() < 1)
            {
                ModelState.AddModelError("LicitacaoId", "Por favor, preencha todos os campos");
            }
        }

        public override ActionResult Remover(string pEncId)
        {
            throw new NotImplementedException();
        }

        public override ActionResult ConfirmaRemocao(string pEncId)
        {
            throw new NotImplementedException();
        }

        private void PopularViewBagUnidadesGestoras()
        {
            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasDictionary()
                .Union(new Dictionary<string, string> { })
                .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                .ToDictionary(k => k.Key, v => v.Value);
        }

        private void PopularViewBagUnidadesGestorasParaTelaDeDispensaLicitacoes()
        {
            var unidadesGestornasQueNaoDevemDeverExibidas = new List<string>()
            {
                "61035efc-b006-4766-91b3-5969cbbfce29", // TRIBUNAL DE JUSTIÇA
                "6fc52dab-caef-4f5e-b3de-dcc029f45137", // TRIBUNAL DE CONTAS DO ESTADO
                "e1b71b56-7f01-4e93-a19c-cb1051654322", // UG CADASTRO
                "c10a7193-fd4d-4903-b3fe-a51f2473858b", // MINISTÉRIO PÚBLICO
                "f03a3b21-6c29-488d-b324-d36d79af1fa7" // ASSEMBLÉIA LEGISLATIVA DE RONDÔNIA
            };

            ViewBag.UnidadesGestoras = _unidadeGestoraDal.UnidadesGestorasDictionary()
                                                         .Where(ug => !unidadesGestornasQueNaoDevemDeverExibidas.Contains(ug.Key))
                                                         .Union(new Dictionary<string, string> { })
                                                         .OrderBy(ug => ug.Key == "" ? ug.Key : ug.Value)
                                                         .ToDictionary(k => k.Key, v => v.Value);
        }

        public JsonResult Lotacoes()
        {
            var lotacoes = _unidadeGestoraDal.UnidadesGestorasDictionary().Select(l => new
            {
                Id = l.Key,
                Descricao = l.Value
            });

            return Json(lotacoes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LotacoesParaTelaDeDispensaLicitacoes()
        {
            var unidadesGestornasQueNaoDevemDeverExibidas = new List<string>()
            {
                "61035efc-b006-4766-91b3-5969cbbfce29", // TRIBUNAL DE JUSTIÇA
                "6fc52dab-caef-4f5e-b3de-dcc029f45137", // TRIBUNAL DE CONTAS DO ESTADO
                "e1b71b56-7f01-4e93-a19c-cb1051654322", // UG CADASTRO
                "c10a7193-fd4d-4903-b3fe-a51f2473858b", // MINISTÉRIO PÚBLICO
                "f03a3b21-6c29-488d-b324-d36d79af1fa7" // ASSEMBLÉIA LEGISLATIVA DE RONDÔNIA
            };

            var lotacoes = _unidadeGestoraDal.UnidadesGestorasDictionary()
                .Where(ug => !unidadesGestornasQueNaoDevemDeverExibidas.Contains(ug.Key))
                .Select(l => new
                {
                    Id = l.Key,
                    Descricao = l.Value
                });

            return Json(lotacoes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResultadoFiltro(string dataInicial, string dataFinal, Guid? ug, EModalidadeLicitacao? modalidadeLicitacao, int? numeroLicitacao, string numeroProcesso)
        {
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date.AddDays(1);
            var licitacoes = _licitacaoDal.GetLicitacao(dtInicial, dtFinal, ug, modalidadeLicitacao, numeroLicitacao, numeroProcesso);

            return Json(licitacoes.Select(d =>
            {
                var descricaoModalidade = d.ModalidadeLicitacao.GetDescription();

                var numLicitacao = d.NumeroLicitacao != null ? " - " + d.NumeroLicitacao.Value.ToString("0000") + "/" + d.Ano : "";

                var unidadeGestora = !string.IsNullOrEmpty(d.UnidadeGestora.sigla) ? d.UnidadeGestora.sigla + " - " + d.UnidadeGestora.nomeUG : d.UnidadeGestora.nomeUG;

                if (!string.IsNullOrEmpty(d.UnidadeGestora.sigla) && d.UnidadeGestora.sigla.Equals("FES"))
                    unidadeGestora = "SESAU / " + unidadeGestora;

                var numeroProcessoSEI = d.NumeroProcessoAdministrativo;

                var licitacaoDocumento = descricaoModalidade + numLicitacao + " (" + unidadeGestora + ") - Nº processo SEI: " + d.NumeroProcessoAdministrativo;

                var dataLicitacao = d.DataDaPublicacao.ToString("dd") + " de " + d.DataDaPublicacao.ToString("MMMM") + " de " + d.DataDaPublicacao.ToString("yyyy");

                var linkParaEdicao = this.Url.Action("AdicionarEditarDispensaLicitacao", new { pEncLicitacaoId = d.LicitacaoId.ToString().ToEncrypted() });

                var linkParaVisualizacao = this.Url.Action("VisualizarDispensaLicitacao", new { pEncLicitacaoId = d.LicitacaoId.ToString().ToEncrypted() });

                var licitacao = new
                {
                    LicitacaoDocumento = licitacaoDocumento,
                    d.Objeto,
                    DataLicitacao = dataLicitacao,
                    LinkParaEdicao = linkParaEdicao,
                    LinkParaVisualizacao = linkParaVisualizacao
                };

                return licitacao;
            }).Take(250), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResultadoFiltroDispensaLicitacao(string dataInicial, string dataFinal, Guid? ug, EModalidadeLicitacao? modalidadeLicitacao, string numeroProcesso)
        {
            var dtInicial = DateTime.Parse(dataInicial).Date;
            var dtFinal = DateTime.Parse(dataFinal).Date.AddDays(1);
            var licitacoes = _licitacaoDal.GetDispensaLicitacao(dtInicial, dtFinal, ug, modalidadeLicitacao, numeroProcesso);

            return Json(licitacoes.Select(d =>
            {
                var descricaoModalidade = d.ModalidadeLicitacao.GetDescription();

                var numeroLicitacao = d.NumeroLicitacao != null ? " - " + d.NumeroLicitacao.Value.ToString("0000") + "/" + d.Ano : "";

                var unidadeGestora = !string.IsNullOrEmpty(d.UnidadeGestora.sigla) ? d.UnidadeGestora.sigla + " - " + d.UnidadeGestora.nomeUG : d.UnidadeGestora.nomeUG;

                if (!string.IsNullOrEmpty(d.UnidadeGestora.sigla) && d.UnidadeGestora.sigla.Equals("FES"))
                    unidadeGestora = "SESAU / " + unidadeGestora;

                var numeroProcessoSEI = d.NumeroProcessoAdministrativo;

                var licitacaoDocumento = descricaoModalidade + numeroLicitacao + " (" + unidadeGestora + ") - Nº processo SEI: " + d.NumeroProcessoAdministrativo;

                var dataLicitacao = d.DataDaPublicacao.ToString("dd") + " de " + d.DataDaPublicacao.ToString("MMMM") + " de " + d.DataDaPublicacao.ToString("yyyy");

                var licitacao = new
                {
                    LicitacaoDocumento = licitacaoDocumento,
                    d.Objeto,
                    DataLicitacao = dataLicitacao,
                    LinkParaEdicao = this.Url.Action("AdicionarEditarDispensaLicitacao", new { pEncLicitacaoId = d.LicitacaoId.ToString().ToEncrypted() }),
                    LinkParaVisualizacao = this.Url.Action("VisualizarDispensaLicitacao", new { pEncLicitacaoId = d.LicitacaoId.ToString().ToEncrypted() })
                };

                return licitacao;
            }).Take(250), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> IndexLicitacoesAtuais()
        {
            try
            {
                var filtro = new FiltroLicitacaoAPISUPELViewModel()
                {
                    EnfrentamentoAoCovid = true,
                    Modalidade = "Pregão Eletrônico",
                    QuantidadeResultados = 15
                };

                ViewBag.UnidadesGestoras = _unidadeGestoraDal.GetUnidadesGestorasParaLicitacao();

                var licitacaoAPISUPELVM = new LicitacaoAPISUPELViewModel() { pagina = new Pagina(), itens = new List<ItemLicitacaoAPISUPEL>() };

                var pagina = 1;

                do
                {
                    var urlFiltro = $"http://www.rondonia.ro.gov.br/supel/licitacoes/?" +
                                                $"json=true" +
                                                $"&paged={pagina}" +
                                                $"&modalidade={filtro.Modalidade ?? "" }" +
                                                $"&unidade_administrativa={filtro.UnidadeAdministrativa ?? "" }" +
                                                $"&ano={(filtro.Ano == null ? "" : filtro.Ano.ToString()) }" +
                                                $"&situacao={filtro.Situacao ?? "" }" +
                                                $"&covid={ (filtro.EnfrentamentoAoCovid == true ? "1" : "")}";

                    string dados = await GetJsonAPI(urlFiltro);

                    var dadosDeserializado = JsonConvert.DeserializeObject<LicitacaoAPISUPELViewModel>(dados);

                    licitacaoAPISUPELVM.pagina = dadosDeserializado.pagina;

                    foreach (var item in dadosDeserializado.itens)
                    {
                        if (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados)
                            licitacaoAPISUPELVM.itens.Add(item);
                    }

                    pagina++;

                } while (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados &&
                          licitacaoAPISUPELVM.pagina.total > licitacaoAPISUPELVM.pagina.atual);

                var indexLicitacaoAPISUPELVM = new IndexLicitacaoAPISUPELViewModel
                {
                    Filtro = new FiltroLicitacaoAPISUPELViewModel(),
                    LicitacaoAPISUPELViewModel = licitacaoAPISUPELVM,
                };

                indexLicitacaoAPISUPELVM.LicitacaoAPISUPELViewModel.Sucesso = true;

                return View(indexLicitacaoAPISUPELVM);
            }
            catch
            {
                return View(new IndexLicitacaoAPISUPELViewModel() { Filtro = new FiltroLicitacaoAPISUPELViewModel(), LicitacaoAPISUPELViewModel = new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Houve uma falha ao conectar com a fonte de dados, tente novamente mais tarde." } }); ;
            }
        }

        [HttpPost]
        public ActionResult FiltrarLicitacoes(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _dadosLicitacaoDAL.FilterAditivados(filtro);

            return PartialView("Partials\\_TabelaAditivados", licitacoes);
        }

        public async Task<ActionResult> FiltrarLicitacoesAtuais(FiltroLicitacaoAPISUPELViewModel filtro)
        {
            try
            {
                var licitacaoAPISUPELVM = new LicitacaoAPISUPELViewModel() { pagina = new Pagina(), itens = new List<ItemLicitacaoAPISUPEL>() };

                var pagina = 1;

                do
                {
                    var urlFiltro = $"http://www.rondonia.ro.gov.br/supel/licitacoes/?" +
                                                $"json=true" +
                                                $"&paged={pagina}" +
                                                $"&modalidade={filtro.Modalidade ?? "" }" +
                                                $"&unidade_administrativa={filtro.UnidadeAdministrativa ?? "" }" +
                                                $"&ano={(filtro.Ano == null ? "" : filtro.Ano.ToString()) }" +
                                                $"&situacao={filtro.Situacao ?? "" }" +
                                                $"&covid={ (filtro.EnfrentamentoAoCovid == true ? "1" : "")}";

                    string dados = await GetJsonAPI(urlFiltro);

                    var dadosDeserializado = JsonConvert.DeserializeObject<LicitacaoAPISUPELViewModel>(dados);

                    licitacaoAPISUPELVM.pagina = dadosDeserializado.pagina;

                    if (dadosDeserializado.itens == null)
                    {
                        return PartialView("Partials\\_Tabela", new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Nenhum resultado encontrado para esta consulta." });
                    }

                    foreach (var item in dadosDeserializado.itens)
                    {
                        if (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados)
                            licitacaoAPISUPELVM.itens.Add(item);
                    }

                    pagina++;

                } while (licitacaoAPISUPELVM.itens.Count() < filtro.QuantidadeResultados &&
                          licitacaoAPISUPELVM.pagina.total > licitacaoAPISUPELVM.pagina.atual);

                licitacaoAPISUPELVM.Sucesso = true;

                return PartialView("Partials\\_Tabela", licitacaoAPISUPELVM);
            }
            catch
            {
                return PartialView("Partials\\_Tabela", new LicitacaoAPISUPELViewModel() { Sucesso = false, Mensagem = "Houve uma falha ao conectar com a fonte de dados, tente novamente mais tarde." });
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _dbTransparencia.Dispose();

            base.Dispose(disposing);
        }
    }
}