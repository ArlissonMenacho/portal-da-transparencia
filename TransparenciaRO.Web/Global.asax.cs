﻿using System;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Security.Claims;

namespace TransparenciaRO.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {   
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Email;
            ModelBinders.Binders.Add(typeof(decimal), new CustomBinders.DecimalBinder()); // Binder para tratamento de números decimais mascarados (despreza o "." de separador de milhar e considera "," como separador decimal)

            //Registrar o job que importa o arquivo exercor
            //LogUtils.Logar("Job de importação exercor programado");
            //JobManager.Initialize(new ExercorRegistry());

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
        }
    }
}