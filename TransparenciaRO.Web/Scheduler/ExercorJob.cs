﻿using FluentScheduler;
using System;
using System.Web.Hosting;
using TransparenciaRO.Infra.Automation;
using TransparenciaRO.Infra.Utils;
using static System.Configuration.ConfigurationManager;

namespace TransparenciaRO.Web.Scheduler
{
    //public class ExercorRegistry : Registry
    //{
    //    public ExercorRegistry()
    //    {
    //        int scheduledHour;
    //        try
    //        {
    //            scheduledHour = Convert.ToInt32(AppSettings["exercor_scheduled_hour"]);
    //        }
    //        catch
    //        {
    //            scheduledHour = 22;
    //        }
    //        LogUtils.Logar($"Rotina de importação de exercor será programada para as {scheduledHour} horas");
    //        Schedule<ExercorJob>().ToRunEvery(1).Weekdays().At(scheduledHour, 0);
    //    }
    //}

    //public class ExercorJob : IJob, IRegisteredObject
    //{
    //    private readonly object _lock = new object();
    //    private bool _shuttingDown;

    //    public ExercorJob()
    //    {
    //        HostingEnvironment.RegisterObject(this);
    //    }

    //    public void Execute()
    //    {
    //        lock (_lock)
    //        {
    //            if (_shuttingDown)
    //                return;
    //            if (AppSettings["exercor_enabled"] == "1")
    //            {
    //                LogUtils.Logar("Rotina de importação de exercor iniciada");
    //                var rotinaOK = ExercorAutomation.ExecutarScript();
    //                LogUtils.Logar($"Rotina de importação exercor finalizada. Ocorreu sem erros? {(rotinaOK ? "Sim" : "Não")}");
    //            }
    //            else
    //            {
    //                LogUtils.Logar("Rotina exercor não será executada pois a mesma está desativada no arquivo .config através do parâmetro RotinaExercorAtivada");
    //            }
    //        }
    //    }

    //    public void Stop(bool immediate)
    //    {
    //        //Usando o lock aqui, irá garantir que o método execute seja finalizado antes de prosseguir
    //        lock (_lock)
    //        {
    //            _shuttingDown = true;
    //        }
    //        HostingEnvironment.UnregisterObject(this);
    //    }
    //}
}