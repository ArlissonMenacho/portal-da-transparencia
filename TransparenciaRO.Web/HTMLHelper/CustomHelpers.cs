﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Web.HTMLHelper
{
    public static class CustomHelpers
    {
        public static MvcHtmlString BreadCrumb(this HtmlHelper htmlhelper)
        {
            var url = htmlhelper.ViewContext.RequestContext.HttpContext.Request.Url.PathAndQuery;

            /*Exceção, pois o link com os parametros nao é encontrada no banco por isso nao funcionava*/
            if (url.Contains("/Pessoal/DetalheServidor"))
             url = htmlhelper.ViewContext.RequestContext.HttpContext.Request.Url.AbsolutePath;
            
            if (url.Last() == '/')
                url = url.Substring(0, url.Length - 1);

            using (var db = new DbTransparencia())
            {
                return MvcHtmlString.Create(
                    "<div class='row'><div class='breadCrumbContainer container col-md-12'><ol class='breadcrumb'>" +
                    String.Join("",
                    (new PastaDAL(db)
                    .GetHierarquiaPastasPorHref(url) ?? new List<Pasta>())
                    .Select(p => $"<li class='breadcrumb-item{(p.Href == url ? " active" : "")}'><a href='{(p.Href != url ? (p.Href.IsNullOrEmpty() ? "/?pEncPastaId=" + p.PastaId.ToString("N").ToEncrypted() : p.Href) : "#!")}'>{(p.IconeFa != null ? $"<i class='fa {p.IconeFa.ToString().Replace("_", "-")}'></i> " : "")}{p.Nome}</a></li>")
                    .ToArray())
                    + "</ol></div></div>");
            }
        }

        public static MvcHtmlString RenderizarReferenciasPorPlugin(this HtmlHelper htmlhelper, IEnumerable<EReferenciaHead> referencias)
        {
            var retorno = "";
            if (referencias.Contains(EReferenciaHead.DataTable))
            {
                retorno += @"
                    <link rel=""stylesheet"" type=""text/css"" href=""https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-colvis-1.2.1/b-html5-1.2.1/b-print-1.2.1/cr-1.3.2/fc-3.2.2/fh-3.1.2/sc-1.4.2/datatables.min.css"" />
                    <script type=""text/javascript"" src=""https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-colvis-1.2.1/b-html5-1.2.1/b-print-1.2.1/cr-1.3.2/fc-3.2.2/fh-3.1.2/sc-1.4.2/datatables.min.js""></script>
                    <script type=""text/javascript"" src=""/scripts/dataTable.es5.min.js""></script>";
            }
            if (referencias.Contains(EReferenciaHead.Mustache))
            {
                retorno += @"<script type=""text/javascript"" src=""https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.2.1/mustache.min.js""></script>";
            }
            return MvcHtmlString.Create(retorno);
        }

        public static MvcHtmlString RenderizarReferenciasPadroes(this HtmlHelper htmlhelper)
        {
            return htmlhelper.RenderizarReferenciasPorCampo(new List<CampoFormulario>());
        }

        public static MvcHtmlString RenderizarReferenciasPorCampo(this HtmlHelper htmlhelper, List<CampoFormulario> Campos)
        {
            var retorno = @"
            <script type=""text/javascript"" src=""https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.2.1/mustache.min.js""></script>
            <link rel=""stylesheet"" type=""text/css"" href=""https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-colvis-1.2.1/b-html5-1.2.1/b-print-1.2.1/cr-1.3.2/fc-3.2.2/fh-3.1.2/sc-1.4.2/datatables.min.css"" />
            <script type=""text/javascript"" src=""https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-colvis-1.2.1/b-html5-1.2.1/b-print-1.2.1/cr-1.3.2/fc-3.2.2/fh-3.1.2/sc-1.4.2/datatables.min.js""></script>
            <script type=""text/javascript"" src=""/scripts/dataTable.es5.min.js""></script>";

            if (Campos.OfType<CampoPeriodo>().Count() > 0)
                retorno += @"<link rel=""stylesheet"" href=""https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.standalone.min.css"">
                             <script type=""text/javascript"" src=""https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js""></script>
                             <script type=""text/javascript"" src=""https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.pt-BR.min.js""></script>";
            return MvcHtmlString.Create(retorno);

        }

        public static MvcHtmlString LabelBootstrap(this HtmlHelper htmlhelper, string pIdCampo, string pDescricao)
        {
            return MvcHtmlString.Create($"<label class='control-label col-md-2' for='{pIdCampo}'>{pDescricao}</label>");
        }

        public static MvcHtmlString FormGroupEditorFor<TModel, TValue>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TValue>> expression, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            if (pClassesParaInput == null)
                pClassesParaInput = new List<EClassesParaInput>();
            //var value = ModelMetadata.FromLambdaExpression(expression, htmlhelper.ViewData).Model;
            var editorHTMLString = htmlhelper.EditorFor(expression, new { htmlAttributes = new { @class = $"form-control {String.Join(" ", pClassesParaInput.Select(c => c.ToString()))}" } }).ToHtmlString();

            return FormGroupFor(htmlhelper, expression, editorHTMLString, pDescricao, pClassesParaInput, pColMD, pValidationMessage, pHtmlComplementar);
        }

        public static MvcHtmlString FormGroupLabel(this HtmlHelper htmlhelper, string pTitulo, string pTexto, int pColMD = 6, string pHtmlComplementar = "")
        {
            return MvcHtmlString.Create($@"
            <div class='form-group'>
                {LabelBootstrap(htmlhelper, "", pTitulo).ToHtmlString()}
                <div class='col-md-{pColMD}'>
                    <p>{pTexto}</p>
                </div>
                {pHtmlComplementar}
            </div>");
        }

        public static MvcHtmlString FormGroupPasswordFor<TModel, TValue>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TValue>> expression, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            if (pClassesParaInput == null)
                pClassesParaInput = new List<EClassesParaInput>();
            return FormGroupFor(htmlhelper, expression, htmlhelper.PasswordFor(expression, new { @class = $"form-control {String.Join(" ", pClassesParaInput.Select(c => c.ToString()))}" }).ToHtmlString(), pDescricao, pClassesParaInput, pColMD, pValidationMessage, pHtmlComplementar);
        }

        public static MvcHtmlString FormGroupCheckboxFor<TModel>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, bool>> expression, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            if (pClassesParaInput == null)
                pClassesParaInput = new List<EClassesParaInput>();
            return MvcHtmlString.Create($@"
            <div class='form-group'><div class='col-md-{pColMD} col-md-offset-2'>{htmlhelper.CheckBoxFor(expression).ToHtmlString()} {pDescricao}</div></div>");
            //return FormGroupFor(htmlhelper, expression, htmlhelper.CheckBoxFor(expression, new { @class = $"{String.Join(" ", pClassesParaInput.Select(c => c.ToString()))}" }).ToHtmlString(), pDescricao, pClassesParaInput, pColMD, pValidationMessage, pHtmlComplementar);
        }

        public static MvcHtmlString FormGroupFile(this HtmlHelper htmlhelper, string pIdCampo, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 3, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            return MvcHtmlString.Create($@"
            <div class='form-group'>
                {LabelBootstrap(htmlhelper, pIdCampo, pDescricao).ToHtmlString()}
                <div class='col-md-{pColMD}'>
                    <div class='fileinput fileinput-new' data-provides='fileinput'>
                        <span class=""btn btn-default btn-file"">
                            <span class=""fileinput-new"">Selecionar</span>
                            <span class=""fileinput-exists"">Alterar</span>
                            <input type='file' name='{pIdCampo}' id='{pIdCampo}' />
                        </span>
                        <span class=""fileinput-filename""></span>
                        <a href=""#"" class=""close fileinput-exists"" data-dismiss=""fileinput"" style=""float: none"">&times;</a>
                    </div>
                </div>
                {pHtmlComplementar}
            </div>");
        }

        public static MvcHtmlString FormGroupImageFile(this HtmlHelper htmlhelper, string pIdCampo, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 8, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            return MvcHtmlString.Create($@"
            <div class='form-group'>
                {LabelBootstrap(htmlhelper, pIdCampo, pDescricao).ToHtmlString()}
                <div class='col-md-{pColMD}'>
                    <div class='fileinput fileinput-new' data-provides='fileinput'>
                        <div class='fileinput-preview thumbnail' data-trigger='fileinput' style='width: 156px; height: 100px;'>
                        </div>
                        <div>
                            <span class='btn btn-default btn-file'>
                                <span class='fileinput-new'>Selecione</span>
                                <span class='fileinput-exists'>Alterar</span>
                                <input type='file' name='imagem' id='{pIdCampo}' name='{pIdCampo}' />
                            </span>
                            <a href='#' class='btn btn-default fileinput-exists' data-dismiss='fileinput'>Remover</a>
                        </div>
                    </div>
                </div>
            </div>");
        }

        public static MvcHtmlString FormGroupDropDownFor<TModel, TValue>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> pItens, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            return FormGroupFor(htmlhelper, expression, htmlhelper.DropDownListFor(expression, pItens, "Selecione", new { @class = "form-control chosen-select" }).ToHtmlString(), pDescricao, pClassesParaInput, pColMD, pValidationMessage, pHtmlComplementar);
        }
        public static MvcHtmlString FormGroupEnumDropDownFor<TModel, TValue>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TValue>> expression, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput = null, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            return FormGroupFor(htmlhelper, expression, htmlhelper.EnumDropDownListFor(expression, "Selecione", new { @class = "form-control" }).ToHtmlString(), pDescricao, pClassesParaInput, pColMD, pValidationMessage, pHtmlComplementar);
        }

        private static MvcHtmlString FormGroupFor<TModel, TValue>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, TValue>> expression, string pHTMLCampo, string pDescricao, IEnumerable<EClassesParaInput> pClassesParaInput, int pColMD = 6, string pValidationMessage = "", string pHtmlComplementar = "")
        {
            var retorno = $"<div class='form-group'>{LabelBootstrap(htmlhelper, expression.Parameters[0].Type.Name, pDescricao)}<div class='col-md-{pColMD}'>{pHTMLCampo}{htmlhelper.ValidationMessageFor(expression, pValidationMessage, new { @class = "text-danger" }) }</div>{pHtmlComplementar}</div>";
            return MvcHtmlString.Create(retorno);
        }

        public static MvcHtmlString RenderizarTemplateDadosPadrao(this HtmlHelper htmlhelper,
            Dictionary<string, string> Campos,
            bool pNomeColunasFooter,
            string pIdTemplate = "templateDados",
            string pIdTabela = "tabelaDados",
            string pMensagemNenhumRegistroEncontrado = "Nenhum registro encontrado",
            bool pWidth100 = false)
        {

            return MvcHtmlString.Create($@"
            <script id='{pIdTemplate}' type='text/html'>
            <table class='table table-striped' id='{pIdTabela}'{(pWidth100 ? " style='width:100%'" : "")}>
                <thead>
                    {{{{#msgInformacao}}}}
                    <div class='alert alert-warning' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Informação:</span>{{{{msgInformacao}}}}</div>
                    {{{{/msgInformacao}}}}
                    <tr>
                        {String.Join("", Campos.Select(c => $"<th>{c.Value}</th>"))}
                    </tr>
                </thead>
                <tbody>
                    {{{{#dados}}}}
                        <tr>
                            {String.Join("", Campos.Select(c => $"<td>{{{{{c.Key}}}}}</td>"))}
                        </tr>
                    {{{{/dados}}}}
                    {{{{^dados}}}}
                        <tr>
                            <td colspan='{Campos.Count}'>
                                <div class='alert alert-warning' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Erro:</span>{pMensagemNenhumRegistroEncontrado}</div>                
                            </td>
                        </tr>
                    {{{{/dados}}}}
                    {(pNomeColunasFooter ? $"<tfoot><tr>{String.Join("", Campos.Select(c => $"<th>{c.Value}</th>"))}</tr></tfoot>" : "")}
                </tbody>                
            </table>
            </script>
            ");
        }

        public static MvcHtmlString MontarMetodoChamadaPostComDados(this HtmlHelper htmlHelper
            , List<CampoFormulario> Campos
            , string pAction
            , string pMethod = "POST"
            , string pNomeFuncao = "consultarDados"
            , string pIdTemplate = "templateDados"
            , string pResultadosContainer = "tabelaDadosContainer"
            , string pIdTabela = "tabelaDados"
            , bool pUtilizarDataTable = true
            , bool pChamarAutomaticamente = false
            , string pParametrosDataTable = ""
            )
        {
            var htmlRetorno = $@"
            <script>
                function {pNomeFuncao}(){{
                    $('#{pResultadosContainer}').html('<i class=""fa fa-cog fa-spin""></i> Carregando dados...');
                    $('#{pResultadosContainer} > #{pIdTabela}').fadeOut('slow');
                    $.{(pMethod == "POST" ? "post" : "get")}('{pAction}'
                        {(Campos?.Count > 0 ? ", {"
                            + (String.Join(",", Campos.Where(c => !(c is CampoPeriodo)).Select(c => $"{c.NomeParametroAction}: $('#{c.Id}{((c is CampoRadio || c is CampoInlineRadio) ? ":checked" : "")}').val()")))
                            + (Campos.Where(c => c is CampoPeriodo).Count() == 1 ? (Campos.Count() > 1 ? ", " : "") + "dataInicial: $('#dataInicial').datepicker('getDate').toYMD(), dataFinal: $('#dataFinal').datepicker('getDate').toYMD()" : "")
                            + (Campos.Where(c => c is Cnpj).Count() == 2 ? (Campos.Count > 2 ? ", " : "") + "cnpj: $('#cnpj').val()" : "")
                            + "}" : "")}
                    ).done(function (data) {{
                        if (data.msgErro != null)
                        {{
                        $('#{pResultadosContainer}').html('<div class=""alert alert-warning"" role=""alert""><span class=""glyphicon glyphicon-exclamation-sign"" aria-hidden=""true""></span> ' + data.msgErro + '</div>');
                        }}                        
                        else
                        {{
                            $('#{pResultadosContainer}').html(Mustache.render($('#{pIdTemplate}').html(), data));
                            $('#{pResultadosContainer} > #{pIdTabela}').fadeIn('slow');
                            {(
                            pUtilizarDataTable ?
                            $@"
                            if ($('#{pResultadosContainer} > #{pIdTabela}').find('td[colspan]').not('td[colspan=1]').length == 0)
                            {{$('#{pResultadosContainer} > #{pIdTabela}').dataTablePadrao({pParametrosDataTable});}}
                            "
                            :
                            ""
                            )}                            
                        }}
                        if (data.msgAlerta != null)
                        {{
                            $('#{pResultadosContainer}').prepend('<div class=""alert alert-warning"" role=""alert""><span class=""glyphicon glyphicon-exclamation-sign"" aria-hidden=""true""></span> ' + data.msgAlerta + '</div>');
                        }}
                    }}).fail(function (data) {{
                        $('#{pResultadosContainer}').html('<div class=""alert alert-danger"" role=""alert""><span class=""glyphicon glyphicon-exclamation-sign"" aria-hidden=""true""></span><strong>Erro:</strong>Ocorreu um erro ao efetuar a pesquisa. Caso o problema persista, entre em contato com a CGE através do e-mail <a href=""mailto:contato@cge.ro.gov.br"">contato@cge.ro.gov.br</a>.</div>');
                    }});
                }}
                {(pChamarAutomaticamente ? " $(document).ready(function(){ " + pNomeFuncao + "();  });  " : "")}
            </script>";

            return MvcHtmlString.Create(htmlRetorno);
        }

        public static string GerarCampos(List<CampoFormulario> pCampos)
        {
            return String.Join("", pCampos
                                .GroupBy(c => c.Linha)
                                .OrderBy(c => c.Key)
                                .Select(c => $@"
                                                <div class='row'>
                                                {String.Join("", c.Select(sc => sc.toHTML()))}
                                                </div>"
                                    ));
        }

        public static MvcHtmlString FormularioParametros(this HtmlHelper htmlHelper
            , List<CampoFormulario> pCampos
            , string pTextoBotaoConsultar = "Consultar"
            , string onClickBotaoConsultar = "consultarDados()")
        {
            var htmlCampos = GerarCampos(pCampos);

            var htmlRetorno = $@"<div class=""container-fluid""><form>
                                    <div class='panel panel-default'>
                                        <div class='panel-body'>
                                            {htmlCampos}
                                        </div>
                                        [footer]
                                    </div>
                                </form>
                                </div>";

            var htmlBotoes = $@"<div class='panel-footer'>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <button type='button' class='btn btn-primary' onclick='{onClickBotaoConsultar.Replace("'", "\"")}'>
                                                        <i class=""fa fa-search""></i> {pTextoBotaoConsultar}</button>
                                                </div>
                                            </div>
                                        </div>";

            htmlRetorno = htmlRetorno.Replace("[footer]", htmlBotoes);

            //Se possui campos do tipo período, é preciso chamar a engine de datePicker para que os mesmos fiquem funcionais
            if (pCampos.OfType<CampoPeriodo>().Count() > 0)
            {
                htmlRetorno += @"   <script>
                                    $('.input-daterange').datepicker({
                                        language: ""pt-BR"",
                                        format: ""dd-mm-yyyy""
                                    });
                                    </script>";
            }

            return MvcHtmlString.Create(htmlRetorno);
        }

        public static IDisposable BeginFormGroup(this HtmlHelper htmlHelper, string pTitulo, int pColMD, string pIdCampo = "")
        {
            htmlHelper.ViewContext.Writer.Write($"<div class='form-group'>{LabelBootstrap(htmlHelper, pIdCampo == "" ? pTitulo : pIdCampo, pTitulo)}<div class='col-md-{pColMD}'>");
            return new FormGroup(htmlHelper);
        }
    }

    public class FormGroup : IDisposable
    {
        private HtmlHelper helper;
        public FormGroup(HtmlHelper pHelper)
        {
            this.helper = pHelper;
        }

        public void Dispose()
        {
            this.helper.ViewContext.Writer.Write("</div></div>");
        }
    }

    public enum EReferenciaHead
    {
        DataTable,
        Mustache
    }

    public enum EClassesParaInput
    {
        CampoCNPJ,
        CampoCPF,
        CampoDinheiro,
        CampoData,
        CampoCNPJCPF,
        CampoProcessoSEI
    }

    public abstract class CampoFormulario
    {
        public string Id { get; set; }
        public int ColMD { get; set; }
        public int Linha { get; set; }
        public bool Isolado { get; set; }
        public string NomeParametroAction { get; set; }
        public abstract string toHTML();
        public CampoFormulario()
        {
            Linha = 0;
        }
    }

    public class BotaoAcao
    {
        public string MyProperty { get; set; }
    }

    public abstract class CampoComDescricao : CampoFormulario
    {
        public string Descricao { get; set; }
    }

    public class CampoTexto : CampoComDescricao
    {
        public string Placeholder { get; set; }
        public string ValorPadrao { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" placeholder=""{this.Placeholder}"" />
                </div>
            </div>";
        }
    }

    public class CampoNumerico : CampoTexto { }

    public class CampoSelect : CampoComDescricao
    {
        public Dictionary<string, string> Valores { get; set; }
        public bool OpcaoTodos { get; set; }
        public string ValorTodos { get; set; }
        public string ValorPadrao { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <select id=""{this.Id}"" class=""form-control chosen-select"">
                        {(OpcaoTodos ? $"<option value=\"{ValorTodos}\" {(ValorPadrao == ValorTodos ? "selected" : "")}>TODOS</option>" : "")}
                        {String.Join("", (Valores ?? new Dictionary<string, string>()).Select(v => $"<option value=\"{v.Key}\"{(v.Key == ValorPadrao ? "selected" : "")}>{v.Value}</option>"))}
                    </select>
                </div>
            </div>";
        }
    }

    public class CampoRadio : CampoComDescricao
    {
        public Dictionary<string, string> Valores { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">                                    
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    {String.Join("", this.Valores.Select(v => $"<div class=\"radio\"><label><input type=\"radio\" id=\"{this.Id}\" name=\"{this.Id}\" value=\"{v.Key}\"{(this.Valores.IsFirst(v) ? " checked " : "")}>{v.Value}</label></div>"))}                    
            </div>";
        }
    }

    public class CampoInlineRadio : CampoComDescricao
    {
        public Dictionary<string, string> Valores { get; set; }
        public string ValorPadrao { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}""> 
                    <label for=""{this.Id}"">{this.Descricao}</label><br />                          
                    {String.Join("", this.Valores.Select(v => $"<label class=\"radio-inline\"><input type=\"radio\" id=\"{this.Id}\" name=\"{this.Id}\" value=\"{v.Key}\"{(this.ValorPadrao == v.Key ? " checked " : "")}>{v.Value}</label>"))}                    
            </div>";
        }
    }

    public class CampoInlineCheckbox : CampoComDescricao
    {
        public Dictionary<string, string> Valores { get; set; }
        public string ValorPadrao { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}""> 
                    <label for=""{this.Id}"">{this.Descricao}</label><br />                          
                    {String.Join("", this.Valores.Select(v => $"<label class=\"checkbox-inline\"><input type=\"checkbox\" id=\"{this.Id}\" name=\"{this.Id}\" value=\"{v.Key}\"{(this.ValorPadrao == v.Key ? " checked " : "")}>{v.Value}</label>"))}                    
            </div>";
        }
    }

    public class CampoPeriodo : CampoFormulario
    {
        public DateTime ValorDataInicial { get; set; }
        public DateTime ValorDataFinal { get; set; }
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD} margin-bottom-15px"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">Período</label><br />
                    <div class=""input-daterange input-group"" id=""{this.Id}"">
                    <input id=""dataInicial"" name=""dataInicial"" type=""text"" class=""form-control"" value=""{this.ValorDataInicial.ToString("dd-MM-yyyy")}"" />
                    <span class=""input-group-addon"">à</span>
                    <input id=""dataFinal"" name=""dataFinal"" type=""text"" class=""form-control"" value=""{this.ValorDataFinal.ToString("dd-MM-yyyy")}"" />
                    </div>
                </div>
            </div>";
        }
    }

    public class Cnpj : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" placeholder=""{this.Placeholder}"" />
                </div>
            </div>";
        }
    }
    public class CnpjCpf : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control {this.Class}"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}""  />
                </div>
            </div>";
        }

        public string Class { get; set; }
    }

    public class NumeroProcesso : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" />
                </div>
            </div>";
        }
    }

    public class NumeroDocumento : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" />
                </div>
            </div>";
        }
    }

    public class RazaoSocial : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" />
                </div>
            </div>";
        }
    }

    public class Objeto : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" placeholder=""Mínimo 10 letras"" />
                </div>
            </div>";
        }
    }

    public class NumeroLicitacao : CampoTexto
    {
        public override string toHTML()
        {
            return $@"
            <div class=""col-md-{this.ColMD}"">
                <div class=""form-group"">
                    <label for=""{this.Id}"">{this.Descricao}</label><br />
                    <input type=""text"" class=""form-control"" name=""{this.Id}"" id=""{this.Id}"" value=""{this.ValorPadrao}"" />
                </div>
            </div>";
        }
    }
}