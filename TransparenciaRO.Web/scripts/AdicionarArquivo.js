﻿$(document).ready(function () {
    $("#UnidadeGestoraId").chosen();
    $('#files').change(function () {
        enviarArquivos();
    });
    $('#filesPrestacaoConta').change(function () {
        enviarArquivosPrestacaoContas();
    });
    $('#filesMonitoramento').change(function () {
        enviarArquivosMonitoramento();
    });
    $(document).on('click', '.btn-excluir-arquivo', function () {
        excluirArquivo($(this).data('dadosArquivo').guidArquivo, $(this).data('dadosArquivo').NomeArquivo);
    });
});

function enviarArquivos() {
    postArquivos('AdicionarArquivo');
}
function enviarArquivosPrestacaoContas() {
    alert("Passou postArquivoslistaArquivosPrestacaoContas");
    postArquivoslistaArquivosPrestacaoContas('AdicionarArquivo');
}

function enviarArquivosMonitoramento() {
    alert("Passou enviarArquivosMonitoramento");
    postArquivoslistaArquivosMonitoramento('AdicionarArquivo');
}

function excluirArquivo(guidArquivo, nomeArquivo) {
    if (confirm('Deseja realmente excluir o arquivo ' + nomeArquivo + '?')) {
        postArquivos('RemoverArquivo', guidArquivo);
    }
}

function postArquivos(url, guidArquivo) {

    var data = new FormData($('form')[0]);
    if (guidArquivo != null) {
        data.append('guidArquivo', guidArquivo);
    }

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            $('#listaArquivosContainer').html(response);
        }
    });
}

function postArquivoslistaArquivosPrestacaoContas(url) {

    var dados = new FormData($('form')[0]);

    //dados.delete('files');
    //dados.delete('filesMonitoramento');

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        success: function success(response) {

            $('#listaArquivosPrestacaoContas').empty();
            //$('#listaArquivosPrestacaoContas').html("");
            $('#listaArquivosPrestacaoContas').html(response);
        }
    });
}

function postArquivoslistaArquivosMonitoramento(url) {

    var dados = new FormData($('form')[0]);

    //dados.delete('files');
    //dados.delete('filesPrestacaoConta');

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        success: function success(response) {
            $('#listaArquivosfilesMonitoramento').empty();
            $('#listaArquivosfilesMonitoramento').html(response);
        }
    });
}