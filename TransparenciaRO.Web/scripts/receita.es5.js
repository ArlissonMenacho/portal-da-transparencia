﻿'use strict';

var nomesValores = [];
var valores = [];
var valoresPrevista = [];

var sliderAno = null;

function atualizaGrafico() {
    if (Number($('input[name=optconsolidarpor]:checked', 'html').val() == '2')) inicializaGraficoColunas('Receita', 'grafico', [{ name: 'Receita arrecadada' }, { name: 'Receita prevista' }]); else inicializaGraficoColunas('Receita', 'grafico', [{ name: 'Receita arrecadada' }]);

    chart.xAxis[0].setCategories(nomesValores, true, true);
    chart.series[0].setData(valores, true, true);

    if (Number($('input[name=optconsolidarpor]:checked', 'html').val() == '2')) chart.series[1].setData(valoresPrevista, true, true);
}

var dados = null;

var chart;

function getDadosReceita() {
    $.post('/api/ReceitaApi', {
        pAnoInicial: $('#sliderAno').val().split(',')[0],
        pAnoFinal: $('#sliderAno').val().split(',')[1],
        pConsolidarPor: Number($('input[name=optconsolidarpor]:checked', 'html').val() == '2' ? '0' : $('input[name=optconsolidarpor]:checked', 'html').val() == '3' ? '2' : $('input[name=optconsolidarpor]:checked', 'html').val()),
        pTipoConsulta: Number($('input[name=opttipografico]:checked', 'html').val()),
        pIncluirReceitaPrevista: $('input[name=optconsolidarpor]:checked', 'html').val() == '2',
        pUG: $('#cboUnidadeGestora').val()
    }).done(function (data) {
        dados = data.slice();
        var optConsolidar = $('input[name=optconsolidarpor]:checked', 'html').val();
        nomesValores = dados.map(function (elm) {
            return elm.Referencia;
        });
        valores = dados.map(function (elm) {
            return elm.ValorArrecadado;
        });
        valoresPrevista = dados.map(function (elm) {
            return elm.ValorPrevisto;
        });
        var metodoonclick = Number(optConsolidar) == 1 ? 'mensal' : Number(optConsolidar) == 3 ? 'quadrimestre' : 'anual';
        $(data).each(function (i, v) {
            v.onclick = metodoonclick == 'mensal' ? 'detalharMes(' + v.Mes + ',' + v.Ano + ',\'' + $('#cboUnidadeGestora').val() + '\', this)' : metodoonclick == 'quadrimestre' ? 'detalharQuadrimestre("' + v.Referencia + '",' + v.Ano + ',\'' + $('#cboUnidadeGestora').val() + '\', this)' : 'detalharAno(' + v.Ano + ',\'' + $('#cboUnidadeGestora').val() + '\', this)';
        });
        var objetoMustache = { "dados": data, "mostrarReceitaPrevista": $('input[name=optconsolidarpor]:checked', 'html').val() == '2' };
        $('#tabelaReceitaContainer').html(Mustache.render($('#templateReceita').html(), objetoMustache));
        if (data.length == 0) {
            $('#grafico').fadeOut();
        } else {
            $('#grafico').fadeIn();
        }
        $('#tabelaReceita').dataTablePadrao({ "ordering": false });
        atualizaGrafico();
    });
}
function getLinhaDataTable(sender) {
    var tabela = $(sender).closest('table').DataTable();
    return tabela.row($(sender).closest('tr'));
}
function detalharMes(mes, ano, ug, sender) {

    var linhaDataTable = getLinhaDataTable(sender);
    let tipoconsulta = Number($('input[name=opttipografico]:checked', 'html').val());
    if (!linhaDataTable.child.isShown()) {
        $.get(`/api/ReceitaApi/Mes/${mes}/${ano}/${tipoconsulta}/${ug}`)

            .done(function (data) {
                renderizarChild(data, sender, linhaDataTable);
            });
    } else {
        linhaDataTable.child.hide();
        $(".DTFC_ScrollWrapper").css({ "height": "100%" });
    }
}

function detalharQuadrimestre(pQuadrimestre, pAno, pUg, sender) {

    var linhaDataTable = getLinhaDataTable(sender);
    var quadrimestreCorrigido = pQuadrimestre.replace("/","")
    if (!linhaDataTable.child.isShown()) {
        let tipoconsulta = Number($('input[name=opttipografico]:checked', 'html').val());
        if (!linhaDataTable.child.isShown()) {
            $.get(`/api/ReceitaApi/Quadrimestre/${quadrimestreCorrigido}/${pAno}/${tipoconsulta}/${pUg}`)
                .done(function (data) {
                    renderizarChild(data, sender, linhaDataTable);
                });
        } else {
            linhaDataTable.child.hide();
            $(".DTFC_ScrollWrapper").css({ "height": "100%" });
        }
    }
}
function detalharAno(ano, ug, sender) {
    var linhaDataTable = getLinhaDataTable(sender);
    let tipoconsulta = Number($('input[name=opttipografico]:checked', 'html').val());
    if (!linhaDataTable.child.isShown()) {
        $.get(`/api/ReceitaApi/Ano/${ano}/${tipoconsulta}/${ug}`).done(function (data) {
            renderizarChild(data, sender, linhaDataTable);
        });
    } else {
        linhaDataTable.child.hide();
        $(".DTFC_ScrollWrapper").css({ "height": "100%" });
    }
}

function renderizarChild(data, sender, linhaDataTable) {
    var guidNovaTabela = guid();
    var html = Mustache.render($('#templateDetalhe').html(), { "dados": data, "guid": guidNovaTabela });
    linhaDataTable.child(html).show();
    $('#' + guidNovaTabela).dataTablePadrao();
}

$(document).ready(function () {
    $('input:radio[name=optconsolidarpor]').change(debounce(getDadosReceita, 250));
    $('input:radio[name=opttipografico]').change(debounce(getDadosReceita, 250));
    sliderAno = $("#sliderAno").slider().on('slide', debounce(getDadosReceita, 250)).data('slider');
    $('#cboUnidadeGestora').change(debounce(getDadosReceita, 250));

    inicializaGraficoColunas('Receita', 'grafico', [{ name: 'Receita arrecadada' }, { name: 'Receita prevista' }]);

    getDadosReceita();
});