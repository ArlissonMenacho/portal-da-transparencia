﻿// Ao terminar de carregar a página
$(document).ready(function () {
    inicializaGraficoColunas('Despesas', 'grafico', [0]);
    exibirDespesas();
    $('#btnPesquisar').click(function () { exibirDespesas(); });
});

// Método utilizado na primeira chamada ao consultar despesas (recarrega o gráfico e carrega o primeiro nível de tabela, por ano, dos dados)
function exibirDespesas() {
    $('#grafico').css('opacity', '0');
    $('.chartContainer > h2').fadeIn('slow'); // Mostra o título de "carregando..." do gráfico    
    var codUnidadeGestora = Number($('#cboUnidadeGestora').val()); // Atribui o valor (numérico) da unidade gestora selecionada (0 para todas)
    var noEmpenho = $('#numeroEmpenho').val(); // Atribui o valor do número do empenho que será filtrado
    $('#tabelaDespesasContainer').html(Mustache.render($('#templateProcessando').html(), { MensagemOpcional: 'Processando despesas...' })); // Mostra o título de "carregando..." onde será a tabela
    var credor = $("#credor").val();
    var doccredor = $("#doccredor").val();
    // Faz o request dos dados que serão usados no primeiro nível da tabela e gráfico
    $.post('/Grafico/ExibirDespesas', {
        pCodUnidadeGestora: codUnidadeGestora,
        pNumeroEmpenho: noEmpenho,
        pNomeCredor: credor,
        pDocCredor: doccredor
    })
    .done(function (data) {
        if (data.msgErro) {
            $('#tabelaDespesasContainer').html('<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>' + data.msgErro + '</div>');
        }
        else {
            // Renderizar tabela
            var template = $('#templateAno').html(); // Recupera o template a ser renderizado com Mustache
            var templateRenderizado = Mustache.render(template, { "dados": data }); // Renderiza o template e guarda o resultado HTML na variável htmlRenderizado
            $('#tabelaDespesasContainer').html(templateRenderizado);
            $('#tabelaAno').dataTablePadrao();
            // Renderizar gráfico
            $('#grafico').animate({ opacity: 1 }, 300, function () { $('.chartContainer > h2').fadeOut('slow'); }); // Oculta o título de "carregando..." do gráfico

            while (chart.series.length > 0)
                chart.series[0].remove(true); // Remove as séries atuais do gráfico (caso existam), uma vez que as séries serão criadas/recriadas

            chart.xAxis[0].setCategories(data.map(function (linha) { return linha.Exercicio; }), true, true); // Projeta o resultado em um array de anos (retornados pelo servidor) para setar as categorias (eixo X) do gráfico
            //Adiciona as três séries (cada série = barra) com os dados retornados do servidor
            chart.addSeries({ name: 'Valor Empenhado', data: data.map(function (linha) { return linha.ValorEmpenhada; }) }, false);
            chart.addSeries({ name: 'Valor Liquidado', data: data.map(function (linha) { return linha.ValorLiquidada; }) }, false);
            chart.addSeries({ name: 'Valor Pago', data: data.map(function (linha) { return linha.ValorPaga; }) }, false);
            chart.redraw();
        }

    }).fail(function () {
        $('#tabelaDespesasContainer').html('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Ocorreu um erro ao efetuar a pesquisa. Caso o problema persista, entre em contato com a CGE através do e-mail <a href="mailto:contato@cge.ro.gov.br">contato@cge.ro.gov.br</a>.</div>');
    });
}

// Método usado para o detalhamento das despesas (este método serve para todos os níveis)
function detalharGenerico(pParametros, sender, getAction, pIdTemplateRetorno, pTituloTabela) {        
    var tabela = $(sender).closest('table').DataTable();
    var linhaDataTable = tabela.row($(sender).closest('tr'));
    if (!linhaDataTable.child.isShown()) {
        var guidNovaLinha = guid(); // É gerado um GUID para identificação única da linha, para ser usado o recurso de toggle        
        $.get(getAction, { pEncParametros: pParametros })
        .done(function (data) {
            var template = $('#' + pIdTemplateRetorno).html(); // Recupera o template a ser renderizado com Mustache
            var usarPaginacao = pIdTemplateRetorno != 'templateMes'; // Não irá paginar quando o template for o do mês (sempre haverá, no máximo, 12 linhas)
            var htmlRenderizado = Mustache.render(template, { "Titulo": pTituloTabela, "guid": guidNovaLinha, "dados": data }); // Renderiza o template e guarda o resultado HTML na variável htmlRenderizado  
            linhaDataTable.child(htmlRenderizado).show();
            $('#' + guidNovaLinha).dataTablePadrao({"paging": usarPaginacao});
        })
        .fail(function () {
            linhaDataTable.child('<tr><td colspan="' + $(linha).find('td,th').length + '"><div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Ocorreu um erro ao efetuar a pesquisa. Caso o problema persista, entre em contato com a CGE através do e-mail <a href="mailto:contato@cge.ro.gov.br">contato@cge.ro.gov.br</a>.</div></td></tr>').show();
        });
    }
    else {
        linhaDataTable.child.hide();
        $(".DTFC_ScrollWrapper").css({ "height": "100%" });
    }
}