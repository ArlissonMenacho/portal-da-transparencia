﻿'use strict';

$(document).ready(function () {
    $("#UnidadeGestoraId").chosen();
    $('#files').change(function () {
        enviarArquivos();
    });
    $('#filesPrestacaoConta').change(function () {
        enviarArquivosPrestacaoContas();
    });
    $('#filesMonitoramento').change(function () {
        enviarArquivosMonitoramento();
    });
    $(document).on('click', '.btn-excluir-arquivo', function () {
        excluirArquivo($(this).data('dadosArquivo').guidArquivo, $(this).data('dadosArquivo').NomeArquivo);
    });
});

function enviarArquivos() {
    postArquivos('AdicionarArquivo');
}

function enviarArquivosPrestacaoContas() {
    postArquivoslistaArquivosPrestacaoContas('AdicionarArquivo');
}
function enviarArquivosMonitoramento() {
    postArquivoslistaArquivosMonitoramento('AdicionarArquivo');
}

function excluirArquivo(guidArquivo, nomeArquivo) {
    if (confirm('Deseja realmente excluir o arquivo ' + nomeArquivo + '?')) {
        postArquivos('RemoverArquivo', guidArquivo);
    }
}

function postArquivos(url, guidArquivo) {

    var dados = new FormData($('form')[0]);

    if (guidArquivo != null) {
        dados.append('guidArquivo', guidArquivo);
        dados.delete('files');
    }
    dados.delete('filesPrestacaoConta');
    dados.delete('filesMonitoramento');

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        success: function success(response) {
            $('#listaArquivosContainer').empty();
            $('#listaArquivosContainer').html(response);
        }
    });
}

function postArquivoslistaArquivosPrestacaoContas(url) {

    var dados = new FormData($('form')[0]);

    dados.delete('files');
    dados.delete('filesMonitoramento');

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        success: function success(response) {

            $('#listaArquivosPrestacaoContas').empty();
            //$('#listaArquivosPrestacaoContas').html("");
            $('#listaArquivosPrestacaoContas').html(response);
        }
    });
}

function postArquivoslistaArquivosMonitoramento(url) {

    var dados = new FormData($('form')[0]);

    dados.delete('files');
    dados.delete('filesPrestacaoConta');

    $.ajax({
        type: "POST",
        url: url,
        enctype: 'multipart/form-data',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        success: function success(response) {
            $('#listaArquivosfilesMonitoramento').empty();
            $('#listaArquivosfilesMonitoramento').html(response);
        }
    });
}
