﻿

function showLoading(pSender) {

    console.log(pSender[0].id);

    if (pSender[0].id != 'banner-corona-virus') {
        $(pSender).addClass('pulsar');
    }
}

function requestFolderList(pAction) {
    $.get(pAction,
        function (data) {
            var strBreadCrumb = "<ol class='breadcrumb'>";
            $('#breadCrumb').html('<ol class="breadcrumb fundoEscuro">' + data.breadCrumb.map(function (x) { return '<li><a href="#!" onclick="' + x.OnClick + '">' + x.Descricao + '</a></li>'; }).join('') + '</ol>');
            openMenu(data.dados, data.imagens);

            console.log(data.dados);
            console.log(imagens);

        });
}
var ultimoArrayMenu;
function openMenu(pMenu, imagens) {
    var classTema;
    ultimoArrayMenu = pMenu;
    $('#menu').addClass('menuFadeOut');

    setTimeout(function () {
        var htmlFinal = '';


        $(pMenu).each(function (index, agrupamento) {
            var htmlMenu = '<ul class="ulMenu temaPadrao">';
            var classeMobile = 'col-sm-2';

            $(agrupamento.Pastas).each(function (index, opcao) {

                //Define o icone dos botões
                var htmlIcon = '><span class="icon"><i aria-hidden="true" class="fa fa-' + opcao.faIcon + '"></i></span><span>';

                if (opcao.imgIcon != null) {
                    htmlIcon = '><span class="icon"><img id="imgMenu" src="data:image/' + opcao.tipoImg + ';base64,' + opcao.imgIcon + '" alt="Icone"/></span><span>';
                }

                if (opcao.Id != '1cee309c-d331-42a0-8cae-6c87154870e3') {

                    htmlMenu += '<li><a  class="tooltipMenu" href="' + (opcao.url != null ? opcao.url + '' : '#!') + '" ' + (opcao.onClick != null ? ' onclick="showLoading($(this).parent()); ' + opcao.onClick + '"' : '') + (opcao.target != null ? 'target="' + opcao.target + '"' : '') + htmlIcon + opcao.Descricao + '</span> ' + (opcao.Note != null ? '<span class="tooltiptext" > ' + opcao.Note + '</span >' : '') + '</a></li>';
                }
                //Temas: temaPadrao temaAmarelo temaVermelho temaAzul temaPreto temaVerde
                classTema = opcao.temaCss;
            });
            htmlMenu += '</ul>';
            htmlFinal += htmlMenu;
        });
        console.log(imagens);
        $(imagens).each(function (index, imagem) {
            htmlFinal += '<div class="imagemMenu tamanho-' + imagem.NumeroColunas + '"><div class="imagemTitulo">' + imagem.Titulo + '</div><img src="' + imagem.URLImagem + '">' + (imagem.Descricao != null ? '<div class="descricao">' + imagem.Descricao + '</div>' : '') + '</div>';

        });

        $('#menu').html(htmlFinal);

        $('#menu').removeClass('menuFadeOut');
        $('.ulMenu').addClass(classTema);

    }, 300);
}



$(function () {
    console.log(pastaId);
    if (pastaId != null && pastaId != '') {
        requestFolderList(_constPath + '/api/PastaApi?pEncPastaId=' + pastaId);
        console.log(pastaId);
    }
    else {
        requestFolderList(_constPath + '/api/PastaApi?pEncPastaId=' + pastaId);
    }
});