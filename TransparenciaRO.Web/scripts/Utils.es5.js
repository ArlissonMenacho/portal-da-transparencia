﻿// Método utilizado para formatar um valor numérico para moeda (com separadores de milhar e decimais)
"use strict";

function numeroParaMoeda(value) {
    var c = 2; //Casas decimais
    var d = ","; //Separador decimal
    var t = "."; //Separador milhar

    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = value < 0 ? "-" : "", i = parseInt(value = Math.abs(+value || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(value - i).toFixed(c).slice(2) : "");
}

/*
 * Exemplo de chamada:
 * 
 * var chart;
 * //<div id="grafico" style="min-width: 600px" class="grafico"></div>
 * preparaGrafico('Título do gráfico', 'grafico', null, null, null);
 * chart.xAxis[0].setCategories([2012,2013,2014,2015]); // Para alterar a descrição no eixo X
 * chart.series[0].setData([1000,1001,1010,1012]); // Para alterar os valores da primeira série
 * chart.series[1].setData([1005,998,1010,1014]); // Para alterar os valores de uma segunda série
 */
function inicializaGraficoColunas(pTitulo, pSeletorIdGrafico, pSeries, pNomesValores, pDrilldown) {
    inicializaGrafico(pTitulo, pSeletorIdGrafico, pSeries, pNomesValores, pDrilldown, 'column');
    preparaGraficoParaMoeda(chart);
}

// Injeta funções de tratamento para números para serem exibidos como moeda BRL
function preparaGraficoParaMoeda(pChart) {
    pChart.yAxis[0].update({
        title: { text: 'R$' },
        labels: { formatter: function formatter() {
                return 'R$ ' + numeroParaMoeda(this.value);
            } },
        tooltip: {
            shared: true,
            formatter: function formatter() {
                return 123;
            }
        }
    });
}

function inicializaGrafico(pTitulo, pSeletorIdGrafico, pSeries, pNomesValores, pDrilldown, pTipoGrafico) {
    //Setar variáveis globais (no caso, ponto decimal como vírgula e milhar como ponto, para se adequar ao padrão brasileiro)
    Highcharts.setOptions({
        lang: { decimalPoint: ',', thousandsSep: '.' }
    });
    chart = new Highcharts.chart({
        chart: {
            renderTo: pSeletorIdGrafico,
            type: pTipoGrafico
        },
        title: {
            text: pTitulo
        },
        xAxis: {
            categories: pNomesValores
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: pSeries,
        drilldown: { series: pDrilldown }
    });
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

// Função de extensão para datas (para enviar no formato YYYY-MM-DD como parâmetro string nas chamadas do VS
Date.prototype.toYMD = function () {
    return this.getFullYear().toString() + "-" + (this.getMonth() + 1).toString() + "-" + this.getDate().toString();
};

$(document).ready(function () {

    // aumentando a fonte
    $(".inc-font").click(function () {
        var size = $("body").css('font-size');

        size = size.replace('px', '');
        size = parseInt(size) + 1.4;

        $("body").animate({ 'font-size': size + 'px' });
    });

    //diminuindo a fonte
    $(".dec-font").click(function () {
        var size = $("body").css('font-size');

        size = size.replace('px', '');
        size = parseInt(size) - 1.4;

        $("body").animate({ 'font-size': size + 'px' });
    });

    // resetando a fonte
    $(".res-font").click(function () {
        $("body").animate({ 'font-size': '14px' });
    });
});

//A função debounce foi criada com a finalidade de evitar chamadas repetitivas em um determinado intervalo de tempo, para que assim
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function later() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

