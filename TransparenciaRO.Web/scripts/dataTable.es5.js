﻿"use strict";

$.fn.dataTablePadrao = function (opcoes) {

    var opts = $.extend({}, $.fn.dataTablePadrao.defaults, opcoes);
    console.log(opts);
    this.DataTable({
        "paging": opts.paging,
        "dom": "Bfrtip",
        "buttons": [{ "extend": "copy", "text": "Copiar" },
                    { "extend": "csvHtml5", "title": "Dados", "filename": "Dados" },
                    { "extend": "excelHtml5", "title": "Dados", "filename": "Dados" },                    
                    { "extend": "print", "text": "Imprimir" }],
                    //['copy', 'csv', 'excel', 'pdf', 'print'],
        "fixedColumns": {
            "leftColumns": opts.colunasFixas
        },
        "ordering": opts.ordering,
        "order": [[opts.colunaOrdenacao, opts.tipoOrdenacao]],
        "scrollX": true,
        "language": { 
            "buttons": {
                "copyTitle": 'Copiado com sucesso',
                "copySuccess": {
                    _: '%d Linhas copiadas',
                    1: '1 Linha copiada'
                }
            },
            "decimal": ",",
            "emptyTable": "Sem dados disponíveis",
            "info": "Mostrando de _START_ à _END_ do total de _TOTAL_ registros",
            "infoEmpty": "Mostrando de 0 à 0 de 0 registros",
            "infoFiltered": "(filtrado do total de _MAX_ registros)",
            "infoPostFix": "",
            "thousands": ".",
            "lengthMenu": "Exibir _MENU_ registros/página",
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
            "search": "Procurar:",
            "zeroRecords": "Nenhum registro encontrado",
            "paginate": {
                "first": "Primeira",
                "last": "Última",
                "next": "Próxima",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": ativar para ordenar por ordem crescente",
                "sortDescending": ": ativar para ordenar por ordem decrescente"
            }
        }
    });

    return this;
};

$.fn.dataTablePadrao.defaults = {
    "colunasFixas": 0,
    "ordering": true,
    "paging": true,
    "colunaOrdenacao": 0,
    "tipoOrdenacao": "asc"
};

