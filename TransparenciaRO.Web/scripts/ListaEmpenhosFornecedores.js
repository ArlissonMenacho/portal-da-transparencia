﻿function detalharEmpenho(pLinkDetalhamento, pSender) {
    var tabela = $(pSender).closest('table').DataTable();
    var linhaDataTable = tabela.row($(pSender).closest('tr'));
    if (!linhaDataTable.child.isShown()) {
        $.get('/Fornecedor/DetalhaEmpenhoFornecedor', { pEncLinkDetalhamento: pLinkDetalhamento })
        .done(function (data) {
            var htmlRenderizado = Mustache.render($('#templateDetalhamento').html(), { "dados": data } );
            linhaDataTable.child(htmlRenderizado).show();
            $(".DTFC_ScrollWrapper").css({ "height": "100%" });
        })
        .fail(
        );
    }
    else {
        linhaDataTable.child.hide();
        $(".DTFC_ScrollWrapper").css({ "height": "100%" });
    }
}