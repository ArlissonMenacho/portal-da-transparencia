﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Model.View.OrnanogramaAPISETIC;

namespace TransparenciaRO.Web.Services.Ornanograma
{
    public class OrnanogramaAPI
    {
        const string IdAuthorization = "71fb7f7f-5052-44b5-aa53-817f0b27e5b0";
        const string baseAddress = "https://api.organograma.ro.gov.br";

        public async Task<RetornoViewModel> ObterUnidadesSESAU()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(IdAuthorization);

            client.DefaultRequestHeaders.Add("Origin", VerificaAmbiente());
            client.BaseAddress = new Uri(baseAddress);

            var response = await client.GetAsync(baseAddress + "/unidades/4/organograma");
            var retornoVM = new RetornoViewModel();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                dynamic dados = JToken.Parse(responseContent);

                var listaUnidades = ObterUnidades(dados);
                retornoVM.Sucesso = true;
                retornoVM.Dados = listaUnidades;
            }
            else
            {
                retornoVM.Sucesso = false;
                retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
            }
            return retornoVM;
        }

        private List<Unidade> ObterUnidades(dynamic dados)
        {
            var unidades = new List<Unidade>();

            foreach (var unidade in dados)
            {
                foreach (var unidadeSubordinada in unidade.children)
                {
                    if (unidadeSubordinada.attributes.__tipo == "unidade")
                    {
                        string sigla = "";
                        if (unidadeSubordinada.children != null)
                        {
                            foreach (var item in unidadeSubordinada.children)
                            {
                                sigla = item.name;
                                sigla = sigla.Replace("SESAU", "").Replace("DG", "").Replace("-", "");
                                break;
                            }
                        }
                        var novaUnidade = new Unidade()
                        { name = unidadeSubordinada.attributes.nome + " - " + sigla };
                        unidades.Add(novaUnidade);
                    }
                }
            }
            return unidades;
        }

        private static string VerificaAmbiente()
        {
            //Se For rodar localmente descomentar
            //return "http://localhost";

            return "https://transparencia.ro.gov.br";
        }
        private void ObterUnidadesSubordinadas(dynamic unidadeSubordinada, List<Unidade> unidades)
        {
            foreach (var unidadeMaisSubordinada in unidadeSubordinada.children)
            {
                if (unidadeMaisSubordinada.attributes.__tipo == "unidade")
                {
                    var unidadeNova = new Unidade() { name = unidadeMaisSubordinada.attributes.nome };
                    unidades.Add(unidadeNova);
                }

                if (unidadeMaisSubordinada.children != null)
                {
                    ObterUnidadesSubordinadas(unidadeMaisSubordinada, unidades);
                }
            }
        }
    }
}