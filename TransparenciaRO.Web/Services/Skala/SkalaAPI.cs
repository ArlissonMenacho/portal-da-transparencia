﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Model.View.SkalaAPISETIC;
using TransparenciaRO.Web.Services.Ornanograma;
using static TransparenciaRO.Infra.Model.View.SkalaAPISETIC.SkalaDescerialize;

namespace TransparenciaRO.Web.servi
{
    public class Skala
    {
        const string login = "{\"id\":0,\"nome\":\"string\",\"login\":\"SkalaApi\",\"senha\":\"7127d8a78f68374161af47d896d248c9\"}";

        const string baseAddress = "https://skala-api-publica.sistemas.ro.gov.br";

        public async Task<RetornoViewModel> ObterDados(ESkalaTipoFiltro eSkalaTipoFiltro, string descricao, string data, string Unidade, ETipoPlatao tipoPlantao)
        {
            var retornoVM = new RetornoViewModel();

            var client = new HttpClient();

            var retornoToken = await ObterToken(client);

            if (retornoToken.Sucesso == false)
            {
                retornoVM.Sucesso = retornoToken.Sucesso;
                retornoVM.Mensagem = retornoToken.Mensagem;
                return retornoVM;
            }

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(retornoToken.Dados));
            var servidores = new List<Infra.Model.View.SkalaAPISETIC.Root>();

            switch (eSkalaTipoFiltro)
            {
                case ESkalaTipoFiltro.DataPlantao:
                    {
                        var periodo = ObterPeriodo(data);
                        foreach (var dia in periodo)
                        {
                            var retornoGetServidores = await GetServidores(client, eSkalaTipoFiltro, descricao, dia, Unidade, tipoPlantao);

                            if (retornoGetServidores.Sucesso == false)
                            {
                                retornoVM.Sucesso = false;
                                retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
                                return retornoVM;
                            }

                            servidores.AddRange(retornoGetServidores.Dados);
                        }
                        break;
                    }

                case ESkalaTipoFiltro.UnidadeLocal:
                    {
                        var periodo = ObterPeriodo(data);

                        foreach (var dia in periodo)
                        {
                            var dataFormatada = dia.Substring(0, 10);
                            dataFormatada = dataFormatada.Replace("/", "-");
                            var retornoGetServidores = await GetServidores(client, eSkalaTipoFiltro, descricao, dataFormatada, Unidade, tipoPlantao);

                            if (retornoGetServidores.Sucesso == false)
                            {
                                retornoVM.Sucesso = false;
                                retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
                                return retornoVM;
                            }
                            servidores.AddRange(retornoGetServidores.Dados);
                        }
                        break;
                    }

                case ESkalaTipoFiltro.TipoPlantao:
                    {
                        var dataFormatada = data.Substring(0, 10);
                        dataFormatada = dataFormatada.Replace("/", "-");
                        var retornoGetServidores = await GetServidores(client, eSkalaTipoFiltro, descricao, dataFormatada, Unidade, tipoPlantao);

                        if (retornoGetServidores.Sucesso == false)
                        {
                            retornoVM.Sucesso = false;
                            retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
                            return retornoVM;
                        }

                        servidores.AddRange(retornoGetServidores.Dados);
                        break;
                    }
                case ESkalaTipoFiltro.Cargo:
                    {
                        var retornoGetServidores = await GetServidores(client, eSkalaTipoFiltro, descricao, data, Unidade, tipoPlantao);

                        if (retornoGetServidores.Sucesso == false)
                        {
                            retornoVM.Sucesso = false;
                            retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
                            return retornoVM;
                        }

                        var novoServidor = new List<Infra.Model.View.SkalaAPISETIC.Root>();
                        var plantaoServidores = new List<Planto>();

                        foreach (var servidor in retornoGetServidores.Dados)
                        {

                            foreach (var plantao in servidor.servidor.plantoes)
                            {
                                DateTime datass = DateTime.Parse(plantao.dataInicial);
                                if (DateTime.Now.Month == datass.Month)
                                {
                                    plantaoServidores.Add(plantao);
                                }
                            }
                            servidor.servidor.plantoes = null;
                            servidor.servidor.plantoes = plantaoServidores;
                            novoServidor.Add(servidor);
                        }
                        servidores.AddRange(novoServidor);
                        break;

                    }
                default:
                    {
                        var retornoGetServidores = await GetServidores(client, eSkalaTipoFiltro, descricao, data, Unidade, tipoPlantao);

                        if (retornoGetServidores.Sucesso == false)
                        {
                            retornoVM.Sucesso = false;
                            retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
                            return retornoVM;
                        }

                        var novoServidor = new Infra.Model.View.SkalaAPISETIC.Root();
                        var plantaoServidores = new List<Planto>();

                        if (retornoGetServidores.Dados.Count > 0)
                        {


                            foreach (var servidor in retornoGetServidores.Dados)
                            {
                                novoServidor = servidor;
                                foreach (var plantao in servidor.servidor.plantoes)
                                {
                                    DateTime datass = DateTime.Parse(plantao.dataInicial);
                                    if (DateTime.Now.Month == datass.Month)
                                    {
                                        plantaoServidores.Add(plantao);
                                    }
                                }
                            }
                            novoServidor.servidor.plantoes = null;
                            novoServidor.servidor.plantoes = plantaoServidores;
                            servidores.Add(novoServidor);
                        }
                        break;
                    }
            }

            retornoVM.Sucesso = true;
            retornoVM.Dados = servidores;
            return retornoVM;
        }

        private async Task<RetornoViewModel> GetServidores(HttpClient client, ESkalaTipoFiltro eSkalaTipoFiltro, string descricao, string data, string Unidade, ETipoPlatao tipoPlantao)
        {

            var novaUnidade = Unidade == "" ? "" : Unidade.Split('-')[1].Trim();
            var retornoVM = new RetornoViewModel();

            var urlTipoDeBusca = ObterUrlTipoDeBuscaPorTipoDeFiltro(eSkalaTipoFiltro, descricao, data, novaUnidade, tipoPlantao);

            var response = await client.GetAsync(baseAddress + urlTipoDeBusca);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseContent = await response.Content.ReadAsStringAsync();

                var servidoresd = JsonConvert.DeserializeObject<List<Infra.Model.View.SkalaAPISETIC.Root>>(responseContent);

                retornoVM.Sucesso = true;
                retornoVM.Dados = servidoresd;
            }
            else
            {
                retornoVM.Sucesso = false;
                retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
            }

            return retornoVM;
        }

        private string ObterUrlTipoDeBuscaPorTipoDeFiltro(ESkalaTipoFiltro eSkalaTipoFiltro, string descricao, string data, string Unidade, ETipoPlatao tipoPlantao)
        {

            switch (eSkalaTipoFiltro)
            {
                case ESkalaTipoFiltro.Matricula:
                    return ESkalaRequisicao.Matricula.GetDescricao() + descricao;
                case ESkalaTipoFiltro.UnidadeLocal:
                    return ESkalaRequisicao.UnidadeLocal.GetDescricao() + "/" + Unidade + "/data/" + data;
                case ESkalaTipoFiltro.Nome:
                    return ESkalaRequisicao.Nome.GetDescricao() + descricao;
                case ESkalaTipoFiltro.Cargo:
                    return ESkalaRequisicao.Cargo.GetDescricao() + descricao;
                case ESkalaTipoFiltro.DataPlantao:
                    return ESkalaRequisicao.Plantao.GetDescricao() + "?data=" + data;
                case ESkalaTipoFiltro.TipoPlantao:
                    return ESkalaRequisicao.Plantao.GetDescricao() + "/tipoDePlantao/" + tipoPlantao + "/departamento/" + Unidade + "/data/" + data;
                default:
                    return "";
            }
        }

        private async Task<RetornoViewModel> ObterToken(HttpClient client)
        {
            //endereço base
            client.BaseAddress = new Uri(baseAddress);

            // serialize your json using newtonsoft json serializer then add it to the StringContent
            var content = new StringContent(login, Encoding.UTF8, "application/json");

            // method address would be like api/callUber:SomePort for example
            var response = await client.PostAsync(ESkalaRequisicao.Autenticacao.GetDescricao(), content);

            var retornoVM = new RetornoViewModel();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseContent = await response.Content.ReadAsStringAsync();

                dynamic dados = JToken.Parse(responseContent);

                retornoVM.Sucesso = true;
                retornoVM.Dados = dados.accessToken;
            }
            else
            {
                retornoVM.Sucesso = false;
                retornoVM.Mensagem = "Houve algum problema na conexão com a fonte de dados";
            }

            return retornoVM;
        }

        private List<string> ObterPeriodo(string data)
        {
            var dataInicial = Convert.ToDateTime(data.Substring(0, 10)).Date;

            var dataFinal = Convert.ToDateTime(data.Substring(13, 10)).Date;

            var datas = new List<string>();

            var count = 0;

            for (var i = 0; i <= (int)dataFinal.Subtract(dataInicial).TotalDays; i++)
            {
                datas.Add(dataInicial.AddDays(count).ToShortDateString());

                count++;
            }

            return datas;
        }
    }
}