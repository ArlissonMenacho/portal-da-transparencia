﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using TransparenciaRO.Infra.Factory;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Model.View.DividaAtivaSEFIN;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Web.Services.DividaAtiva
{
    public static class DividaAtivaAPI
    {
        const string baseAddress = "https://consultacge.dividaativa.sefin.ro.gov.br/";

        public static async Task<List<TransparenciaRO.Infra.Model.View.DividaAtivaSEFIN.DividaAtivaViewModel>> ObterDividasAtivas(string cpf, string nomeRazaoSocial)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("access-key", GuidUtils.GuidAcessKeyDividaAtivaAPI());

            client.DefaultRequestHeaders.Add("secret-key", GuidUtils.GuidSecretKeyDividaAtivaAPI());

            var dividaAtivaAPIVMPag1 = await GetDividaAtiva(client, cpf, nomeRazaoSocial, null);

            var dividasAtivasVM = new List<DividaAtivaViewModel>();

            dividasAtivasVM.AddRange(DividaAtivaAPIFactory.ConverterDividasAtivasEmDividasAtivasViewModel(dividaAtivaAPIVMPag1.dados));

            var countPaginas = 2;

            while ((countPaginas <= Convert.ToInt32(dividaAtivaAPIVMPag1.pagina_final)) &&
                  (!string.IsNullOrEmpty(cpf) || !string.IsNullOrEmpty(nomeRazaoSocial)) &&
                  countPaginas <= 40)
            {
                var dividaAtivaAPIVM = await GetDividaAtiva(client, cpf, nomeRazaoSocial, countPaginas);

                var dividaAtivaVM = DividaAtivaAPIFactory.ConverterDividasAtivasEmDividasAtivasViewModel(dividaAtivaAPIVM.dados);

                dividasAtivasVM.AddRange(dividaAtivaVM);

                countPaginas++;
            }

            return dividasAtivasVM;
        }

        public static async Task<DividaAtivaViewModel> ObterDetalheDividaAtiva(int id)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("access-key", GuidUtils.GuidAcessKeyDividaAtivaAPI());

            client.DefaultRequestHeaders.Add("secret-key", GuidUtils.GuidSecretKeyDividaAtivaAPI());

            var dividaAtivaAPIVM = await GetDividaAtiva(client, id);

            var dividaAtiva = dividaAtivaAPIVM.dados.FirstOrDefault();

            var dividaAtivaVM = DividaAtivaAPIFactory.ConverterDividaAtivaEmDividaAtivaViewModelParaDetalhes(dividaAtiva);

            return dividaAtivaVM;
        }

        private static async Task<DividaAtivaAPISEFINViewModel> GetDividaAtiva(HttpClient client, string cpf, string nomeRazaoSocial, int? pagina)
        {
            var parametrosDeConsulta = ObterParametrosDeConsulta(cpf, nomeRazaoSocial, pagina);

            var enderecoCompleto = baseAddress + parametrosDeConsulta;

            var response = await client.GetAsync(enderecoCompleto);

            string responseContent = await response.Content.ReadAsStringAsync();

            var dividaAtivaAPIVM = JsonConvert.DeserializeObject<DividaAtivaAPISEFINViewModel>(responseContent);

            return dividaAtivaAPIVM;
        }

        private static async Task<DividaAtivaAPISEFINViewModel> GetDividaAtiva(HttpClient client, int id)
        {
            var enderecoCompleto = baseAddress + $"?q[tuk_eq]={id}";

            var response = await client.GetAsync(enderecoCompleto);

            string responseContent = await response.Content.ReadAsStringAsync();

            var dividaAtivaAPIVM = JsonConvert.DeserializeObject<DividaAtivaAPISEFINViewModel>(responseContent);

            return dividaAtivaAPIVM;
        }

        private static string ObterParametrosDeConsulta(string cpf, string nomeRazaoSocial, int? pagina)
        {
            var parametroCpf = $"q[it_cnpj_cpf_eq]={cpf}";

            var parametroNome = $"q[it_no_pessoa_eq]={nomeRazaoSocial}";

            var parametroPagina = $"page={pagina}";

            var parametrosDeConsulta = (string.IsNullOrEmpty(cpf) ? "" : parametroCpf + (string.IsNullOrEmpty(nomeRazaoSocial) && pagina == null ? "" : "&")) +
                                       (string.IsNullOrEmpty(nomeRazaoSocial) ? "" : parametroNome + (pagina == null ? "" : "&")) +
                                       (pagina == null ? "" : parametroPagina);

            if (parametrosDeConsulta != "")
            {
                return "?" + parametrosDeConsulta;
            }

            return parametrosDeConsulta;
        }
    }
}