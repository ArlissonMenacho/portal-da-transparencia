﻿namespace TransparenciaRO.ImportadorContratoConvenio
{
    partial class SelecaoOrgao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstOrgaos = new System.Windows.Forms.ListBox();
            this.lblOrgaoEncontrado = new System.Windows.Forms.Label();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstOrgaos
            // 
            this.lstOrgaos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstOrgaos.FormattingEnabled = true;
            this.lstOrgaos.Location = new System.Drawing.Point(0, 37);
            this.lstOrgaos.Name = "lstOrgaos";
            this.lstOrgaos.Size = new System.Drawing.Size(517, 313);
            this.lstOrgaos.TabIndex = 0;
            this.lstOrgaos.Click += new System.EventHandler(this.lstOrgaos_Click);
            this.lstOrgaos.DoubleClick += new System.EventHandler(this.lstOrgaos_DoubleClick);
            // 
            // lblOrgaoEncontrado
            // 
            this.lblOrgaoEncontrado.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOrgaoEncontrado.Location = new System.Drawing.Point(0, 0);
            this.lblOrgaoEncontrado.Name = "lblOrgaoEncontrado";
            this.lblOrgaoEncontrado.Size = new System.Drawing.Size(517, 37);
            this.lblOrgaoEncontrado.TabIndex = 1;
            this.lblOrgaoEncontrado.Text = "O órgão XXX não teve um órgão do sistema correspondente encontrado automaticament" +
    "e. Por gentileza, escolha manualmente um órgão abaixo:";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnConfirmar.Location = new System.Drawing.Point(0, 350);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(517, 39);
            this.btnConfirmar.TabIndex = 2;
            this.btnConfirmar.Text = "OK";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // SelecaoOrgao
            // 
            this.AcceptButton = this.btnConfirmar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 389);
            this.Controls.Add(this.lstOrgaos);
            this.Controls.Add(this.lblOrgaoEncontrado);
            this.Controls.Add(this.btnConfirmar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SelecaoOrgao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Selecione o órgão correto";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstOrgaos;
        private System.Windows.Forms.Label lblOrgaoEncontrado;
        private System.Windows.Forms.Button btnConfirmar;
    }
}