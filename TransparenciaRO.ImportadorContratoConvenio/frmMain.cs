﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Utils;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using static System.Configuration.ConfigurationManager;

namespace TransparenciaRO.ImportadorContratoConvenio
{
    public partial class FrmMain : Form
    {
        private Dictionary<string, Guid> Sinonimos = new Dictionary<string, Guid>();
        private readonly DbTransparencia _dbTransparencia = new DbTransparencia();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnIniciarProcesso_Click(object sender, EventArgs e)
        {
            /*teste*/
            IniciarProcesso(AppSettings["caminho_origem_arquivos"].ToString());
            return;

            //if (fbdPastaArquivos.ShowDialog() == DialogResult.OK)
            //{
            //    IniciarProcesso(fbdPastaArquivos.SelectedPath);
            //}
        }
        int documentosProcessados = 0;
        private void IniciarProcesso(string pPasta)
        {
            try
            {
                var arquivosPDF = Directory.GetFiles(pPasta, "*.pdf").ToList();
                var arquivosDOC = Directory.GetFiles(pPasta, "*.doc").ToList();
                var arquivosDOCX = Directory.GetFiles(pPasta, "*.docx").ToList();
                List<string> arquivosDOCS = new List<string>();
                arquivosDOCS.AddRange(arquivosDOC);
                arquivosDOCS.AddRange(arquivosDOCX);

                var tempoInicial = DateTime.Now;
                documentosProcessados = 0;
                foreach (var arquivoWord in arquivosDOCS)
                {
                    ProcessarArquivo(arquivoWord, arquivosPDF);
                }
                Logar($"Processo encerrado! Documentos processados: {documentosProcessados}. Tempo decorrido: {(DateTime.Now - tempoInicial).TotalSeconds.ToString("0.0")}s", true);
            }
            catch (Exception exc)
            {
                Logar($"Erro geral: {exc}", true);
            }
        }

        private void ProcessarArquivo(string pArquivoWord, List<String> pArquivosPdf)
        {
            var app = new Microsoft.Office.Interop.Word.Application();
            try
            {
                Logar("Iniciando processo...");
                Logar("Buscando o arquivo de índice de contratos/convênios...");

                var documentos = new List<ContratoConvenio>();
                Logar($"Arquivo DOC a processar: {pArquivoWord}");
                Logar($"Abrindo o Microsoft Word...");
                app.Visible = true;
                var doc = app.Documents.Open(pArquivoWord, ReadOnly: true);
                Logar("Recuperando todas as UGs do BD");
                var unidadesGestoras = new UnidadeGestoraDAL(_dbTransparencia).GetUnidadesGestoras().ToList();
                Logar("UGs carregadas");
                using (var db = new DbTransparencia())
                {
                    foreach (Microsoft.Office.Interop.Word.Table tabela in doc.Tables)
                    {
                        var valores = tabela.GetText(9, 2).TryGetValores();
                        var siglaUg = tabela.GetText(1, 4);
                        if (string.IsNullOrWhiteSpace(siglaUg))
                        {
                            Logar($"UG não encontrada para {tabela.GetText(1, 1)}. Registro ignorado");
                            continue;
                        }
                        var ugID = unidadesGestoras.Where(ug => ug.sigla == siglaUg).Select(ug => ug.unidadeGestoraID).FirstOrDefault();
                        if (ugID == null || ugID == new Guid())
                        {
                            Logar($"Unidade gestora não encontrada para a sigla {siglaUg}!");
                            if (Sinonimos.ContainsKey(siglaUg.Trim()))
                            {
                                Logar($"O aplicativo determinou automaticamente que a UG para a sigla {siglaUg} é {Sinonimos[siglaUg.Trim()]:N}");
                                ugID = Sinonimos[siglaUg.Trim()];
                            }
                            else
                            {
                                ugID = new SelecaoOrgao().IniciarSelecao(unidadesGestoras, siglaUg);
                                if (ugID == null || ugID == new Guid())
                                {
                                    Logar($"Nenhum órgão foi selecionado. O documento {tabela.GetText(1, 1).Replace("LIVRO ESPECIAL Nº ", "")} terá sua inserção ignorada", true);
                                    continue;
                                }
                                else
                                {
                                    Sinonimos.Add(siglaUg.Trim(), ugID);
                                    Logar($"Ensinando o importador que a UG para a sigla {siglaUg} é {Sinonimos[siglaUg.Trim()]:N}");
                                }
                            }
                        }
                        Logar($"Processando documento número {tabela.GetText(3, 2)}");
                        ContratoConvenio novoContratoConvenio;
                        if (tabela.GetText(1, 6).Trim() == "CANCELADO")
                        {
                            //Documento cancelado
                            novoContratoConvenio = new ContratoConvenio();
                            novoContratoConvenio.Cancelado = true;
                            novoContratoConvenio.NumeroDocumento = tabela.GetText(1, 2).ToUpper().Contains("CNV") ? tabela.GetText(1, 2).Replace("CNV N°", "").Replace("CNV Nº", "").Trim() : tabela.GetText(1, 2).Replace("CNT N°", "").Replace("CNT Nº", "").Trim();
                            novoContratoConvenio.TipoDocumento = tabela.GetText(1, 2).ToUpper().Contains("CNV")
                                ? Infra.Enum.ETipoDocumento.Convenio
                                : Infra.Enum.ETipoDocumento.Contrato;
                            novoContratoConvenio.Empresa = "DOCUMENTO CANCELADO";
                            novoContratoConvenio.Objeto = tabela.GetText(4, 3).Replace("CANCELAMENTO:", "").Trim();
                            novoContratoConvenio.DataElaboracao = DateTime.Now;
                            novoContratoConvenio.Valor = 0;
                            novoContratoConvenio.UnidadeGestoraId = new Guid("0A00ECFE-B503-4E86-BA0F-D5074AEA30AC");
                        }
                        else
                        {
                            //Documento cadastrado
                            novoContratoConvenio = new ContratoConvenio();
                            novoContratoConvenio.DataAssinatura = tabela.GetText(5, 2).TryToDateTime();
                            novoContratoConvenio.DataElaboracao = tabela.GetText(4, 2).TryToDateTime() ?? DateTime.Now;
                            novoContratoConvenio.DataRetorno = tabela.GetText(8, 2).TryToDateTime();
                            novoContratoConvenio.DataVigencia = tabela.GetText(6, 2)
                                .TryDataVigencia(tabela.GetText(5, 2).TryToDateTime());
                            novoContratoConvenio.TipoDocumento = tabela.GetText(1, 2).ToUpper().Contains("CNV")
                                ? Infra.Enum.ETipoDocumento.Convenio
                                : tabela.GetText(1, 2).ToUpper().Contains("CNT") ? Infra.Enum.ETipoDocumento.Contrato : Infra.Enum.ETipoDocumento.TermoFomento;
                            novoContratoConvenio.Empresa = tabela.GetText(2, 4);
                            novoContratoConvenio.Objeto = tabela.GetText(3, 3).Replace("OBJETO:", "").Trim();
                            novoContratoConvenio.UnidadeGestoraId = ugID;
                            novoContratoConvenio.NumeroDocumento = tabela.GetText(1, 2).ToUpper().Contains("CNV") ? tabela.GetText(1, 2).Replace("CNV N°", "").Replace("CNV Nº", "").Trim() : tabela.GetText(1, 2).ToUpper().Contains("CNT") ? tabela.GetText(1, 2).Replace("CNT N°", "").Replace("CNT Nº", "").Trim() : tabela.GetText(1, 2).Replace("T. FOMENTO N°", "").Replace("T. FOMENTO Nº", "").Trim();
                            novoContratoConvenio.NumeroProcesso = tabela.GetText(3, 2).Trim();
                            novoContratoConvenio.Cancelado = false;
                            novoContratoConvenio.Valor = valores.Item1 ?? 0;
                            novoContratoConvenio.ValorEmpenhado = valores.Item2?.ToString("C");
                            novoContratoConvenio.Cnpj = tabela.GetText(2, 2);
                            novoContratoConvenio.Execucao = tabela.GetText(7, 2).TryToDateTime();

                            if (db.ContratosConvenios.FirstOrDefault(cc => cc.NumeroDocumento == novoContratoConvenio.NumeroDocumento && cc.TipoDocumento == novoContratoConvenio.TipoDocumento) != null)
                            {
                                Logar("Contrato/convênio já cadastrado no BD. Registro não será inserido novamente");
                                continue;
                            }
                        }

                        var numeroDocumento = "";
                        try
                        {
                            numeroDocumento = Convert.ToInt32(novoContratoConvenio.NumeroDocumento.Split('-').First()).ToString("000");
                        }
                        catch (Exception e)
                        {
                            numeroDocumento = Convert.ToInt32(novoContratoConvenio.NumeroDocumento.Split('/').First()).ToString("000");
                        }
                        var arquivosPDF = pArquivosPdf.Where(a => a.Contains($"{(novoContratoConvenio.TipoDocumento == Infra.Enum.ETipoDocumento.Contrato ? "CNT" : novoContratoConvenio.TipoDocumento == Infra.Enum.ETipoDocumento.Convenio ? "CNV" : "FOMENTO")}")
                            && (a.Contains(numeroDocumento + "_") || a.Contains(numeroDocumento + "-"))
                            && a.Contains(novoContratoConvenio.DataElaboracao.Year.ToString())
                            && (novoContratoConvenio.NumeroDocumento.Contains("/A") && a.Contains("A") || !novoContratoConvenio.NumeroDocumento.Contains("/A") && !a.Contains("A")));

                        var arquivos = new List<Arquivo>();
                        foreach (var arquivoPDF in arquivosPDF)
                        {
                            var guidArquivo = Guid.NewGuid();
                            Logar($"Arquivo PDF encontrado: {arquivoPDF}");
                            File.Copy(arquivoPDF, Path.Combine(AppSettings["caminho_destino_arquivos_pdf"], guidArquivo.ToString()));
                            arquivos.Add(new Arquivo
                            {
                                ArquivoId = guidArquivo,
                                DataUpload = DateTime.Now,
                                Descricao = Path.GetFileNameWithoutExtension(arquivoPDF),
                                Extensao = "pdf",
                                Temporario = false,
                                UsuarioIdUpload = new Guid(),
                                PastaId = GuidUtils.GuidPastaContratosEConvenios()
                            });
                        }

                        if (!arquivosPDF.Any())
                        {
                            Logar($"Arquivos PDF não encontrados para o prefixo {(novoContratoConvenio.TipoDocumento == Infra.Enum.ETipoDocumento.Contrato ? "CNT" : "CNV")} Nº { novoContratoConvenio.NumeroDocumento.Split('/').First()}");
                        }


                        try
                        {
                            documentos.Add(novoContratoConvenio);
                        }
                        catch (Exception exc)
                        {
                            Logar($"Erro ao tentar salvar o novo documento.\nErro: {exc}.\nDados do documento: {SerializacaoUtils.Serializar(novoContratoConvenio)}");
                        }


                        var msgErro = "";
                        var guidDocumento = new ContratoConvenioDAL(db).AdicionarEditar(novoContratoConvenio, out msgErro);
                        documentosProcessados++;

                        if (msgErro != "")
                        {
                            Logar($"Erro ao tentar inserir o documento no BD: {msgErro}\nDados do documento: {SerializacaoUtils.Serializar(novoContratoConvenio)}");
                            continue;
                        }

                        foreach (var arquivo in arquivos)
                        {
                            try
                            {
                                arquivo.ContratoConvenioId = guidDocumento;

                                new ArquivoDAL(db).Adicionar(arquivo);
                            }
                            catch (Exception exc)
                            {

                                Logar($"Erro ao tentar salvar o arquivo.\nErro: {exc}.\nDados do documento: {SerializacaoUtils.Serializar(arquivo)}");
                            }
                        }


                    }
                }
                app.Quit();
                Logar("Microsoft Word encerrado...");
            }
            catch (Exception exc)
            {
                Logar($"Erro crítico: {exc}", true);
                app.Quit();
            }

        }


        private void Logar(string pMessage, bool pNotificar = false)
        {
            this.Refresh();
            var mensagemFormatada = $"{DateTime.Now:dd/MM/yyyy - HH:mm} - {pMessage}" + System.Environment.NewLine + txtLog.Text;
            MensagensLog.Enqueue(mensagemFormatada);
            txtLog.Text = mensagemFormatada;
            if (pNotificar)
                MessageBox.Show(pMessage);
        }


        private void Log2TXT()
        {
            var msgLog = "";
            if (MensagensLog.TryDequeue(out msgLog))
            {
                try
                {
                    File.AppendAllText(Path.Combine(AppSettings["caminho_destino_arquivos_pdf"], $"Log{DateTime.Now:yyyyMMddHHmm}.txt"), msgLog);
                }
                catch (Exception)
                {


                }
            }
            else
            {
                System.Threading.Thread.Sleep(2000);
            }
            Log2TXT();
        }

        private readonly ConcurrentQueue<string> MensagensLog = new ConcurrentQueue<string>();

        private void frmMain_Load(object sender, EventArgs e)
        {
            Task.Factory.StartNew(Log2TXT);
            Logar("Logger inicializado...");
        }
    }


    public static class WordUtils
    {
        public static string GetText(this Microsoft.Office.Interop.Word.Document doc, int pTabela, int pLinha, int pColuna)
        {
            return doc.Tables[pTabela].Cell(pLinha, pColuna).Range.Text.Replace("\r\a", "").Replace("\r", "");
        }

        public static string GetText(this Microsoft.Office.Interop.Word.Table table, int pLinha, int pColuna)
        {
            return table.Cell(pLinha, pColuna).Range.Text.Replace("\r\a", "");
        }
    }

    public static class ConvertUtils
    {
        public static int? TryToInt(this string pInfo)
        {
            int retorno;
            if (int.TryParse(pInfo, out retorno))
                return retorno;
            else
                return null;
        }

        public static DateTime? TryToDateTime(this string pInfo)
        {
            try
            {
                return DateTime.Parse(String.Join("-", pInfo.Split('.').Reverse()));
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? TryDataVigencia(this string pInfo, DateTime? pDataElaboracao)
        {
            if (pDataElaboracao == null)
                return null;
            if (pInfo.ToUpper().Contains("DIAS"))
            {
                return pDataElaboracao?.AddDays(pInfo.ToUpper().Replace("DIAS", "").Trim().TryToInt() ?? 0);
            }
            else if (pInfo.ToUpper().Contains("MESES"))
            {
                return pDataElaboracao?.AddMonths(pInfo.ToUpper().Replace("MESES", "").Trim().TryToInt() ?? 0);
            }
            else if (pInfo.ToUpper().Contains("ANOS"))
            {
                return pDataElaboracao?.AddYears(pInfo.ToUpper().Replace("ANOS", "").Trim().TryToInt() ?? 0);
            }
            else if (pInfo.ToUpper().Contains("ANO"))
            {
                return pDataElaboracao?.AddYears(1);
            }
            else
            {
                return pInfo.TryToDateTime();
            }
        }

        public static Tuple<decimal?, decimal?> TryGetValores(this string pInfo)
        {
            //new Regex(@"[0-9.,]+").Matches(tabela.GetText(8, 3))
            var retorno = new Regex(@"[0-9.,]+").Matches(pInfo);
            return new Tuple<decimal?, decimal?>(retorno.Count >= 1 ? retorno[0].ToString().TryToDecimal() : null, retorno.Count >= 2 ? retorno[1].ToString().TryToDecimal() : null);
        }

        public static decimal? TryToDecimal(this string pInfo)
        {
            return Decimal.Parse(pInfo, new CultureInfo("pt-BR"));
        }

    }

}
