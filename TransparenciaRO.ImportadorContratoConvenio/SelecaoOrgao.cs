﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.ImportadorContratoConvenio

{
    public partial class SelecaoOrgao : Form
    {


        public Guid OrgaoIdSelecionado { get; set; }
        public List<UnidadeGestora> UnidadesGestoras { get; set; }

        public SelecaoOrgao()
        {
            InitializeComponent();
        }

        public Guid IniciarSelecao(List<UnidadeGestora> pUnidadesGestoras, string pSiglaNaoEncontrada)
        {
            lblOrgaoEncontrado.Text = $"O órgão {pSiglaNaoEncontrada} não teve um órgão do sistema correspondente encontrado automaticamente. Por gentileza, escolha manualmente um órgão abaixo:";
            UnidadesGestoras = pUnidadesGestoras;
            RenderizarLista();
            this.ShowDialog();
            return OrgaoIdSelecionado;
        }

        private void RenderizarLista()
        {
            lstOrgaos.Items.Clear();
            lstOrgaos.ValueMember = nameof(UnidadeGestora.unidadeGestoraID);
            lstOrgaos.DisplayMember = nameof(UnidadeGestora.nomeUG);
            lstOrgaos.DataSource = UnidadesGestoras.Select(ug => new { unidadeGestoraID = ug.unidadeGestoraID, nomeUG = (ug.sigla != null ? ug.sigla + " - " : "") + ug.nomeUG }).Distinct().OrderBy(ug => ug.nomeUG).ToList();
            this.Refresh();
        }

      

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            Confirmar();
        }

        private void lstOrgaos_Click(object sender, EventArgs e)
        {
            OrgaoIdSelecionado = (Guid)lstOrgaos.SelectedValue;
        }

        private void lstOrgaos_DoubleClick(object sender, EventArgs e)
        {
            Confirmar();
        }

        private void Confirmar()
        {
            if (OrgaoIdSelecionado == null || OrgaoIdSelecionado == new Guid())
            {
                MessageBox.Show("Por favor, selecione um órgão para o registro em questão!");
                return;
            }
            this.Close();
        }
    }
}
