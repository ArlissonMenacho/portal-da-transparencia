﻿namespace TransparenciaRO.ImportadorContratoConvenio
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fbdPastaArquivos = new System.Windows.Forms.FolderBrowserDialog();
            this.btnIniciarProcesso = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnIniciarProcesso
            // 
            this.btnIniciarProcesso.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIniciarProcesso.Location = new System.Drawing.Point(0, 0);
            this.btnIniciarProcesso.Name = "btnIniciarProcesso";
            this.btnIniciarProcesso.Size = new System.Drawing.Size(447, 37);
            this.btnIniciarProcesso.TabIndex = 0;
            this.btnIniciarProcesso.Text = "Iniciar processo de importação";
            this.btnIniciarProcesso.UseVisualStyleBackColor = true;
            this.btnIniciarProcesso.Click += new System.EventHandler(this.btnIniciarProcesso_Click);
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(0, 37);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(447, 423);
            this.txtLog.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnIniciarProcesso;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 460);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.btnIniciarProcesso);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importador de contratos e convênios";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fbdPastaArquivos;
        private System.Windows.Forms.Button btnIniciarProcesso;
        private System.Windows.Forms.TextBox txtLog;
    }
}

