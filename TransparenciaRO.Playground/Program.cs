﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Automation;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.IO;

namespace TransparenciaRO.Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            //ListarDespesas();            
            //ExercorAutomation.ExecutarScript();

            Console.WriteLine("Processo finalizado. Encerrando em 20 segundos...");
            System.Threading.Thread.Sleep(20000);
        }

        static void EmailTeste()
        {
            Console.Write("Informe destinatário: ");
            var destinatario = Console.ReadLine();
            MailUtils.SendMail("Teste de envio");
        }

        static void ChamadaMapaSite()
        {
            using (var db = new DbTransparencia())
            {
                var detalhes = new PastaDAL(db).DetalharPasta(GuidUtils.GuidMenuPrincipal());
            }
        }

        static void ListarDespesas()
        {
            using (var db = new DbTransparencia())
            {
                var despesas = new DespesaDAL(db).GetDespesasPorAno(null, "", "", "").FirstOrDefault(d => d.Exercicio == 2017);

                Console.WriteLine($"Total empenhado: {despesas.StrValorEmpenhada}");
                Console.WriteLine($"Total liquidado: {despesas.StrValorLiquidada}");
                Console.WriteLine($"Total pago:      {despesas.StrValorPaga}");
            }
        }

        static void RotinaInsercaoFornecedores()
        {
            //INSERT INTO Fornecedores(Uf, RazaoSocial, TipoPessoa, CpfCnpj, CodigoDoBanco, Agencia, ContaCorrente, Cep, Endereco, CidadeId, FornecedorId) VALUES
            var linhas = File.ReadAllLines("cmdInsercaoFornecedores.txt").AsParallel();
            var linhasComErros = new List<int>();
            var i = 1;
            using (var db = new DbTransparencia())
            {
                foreach (var linha in linhas)
                {
                    Console.WriteLine($"Executando linha {i} de {linhas.Count()}...");
                    if (!linha.IsNullOrEmpty())
                    {
                        try
                        {
                            db.Database.ExecuteSqlCommand("INSERT INTO Fornecedores(Uf, RazaoSocial, TipoPessoa, CpfCnpj, CodigoDoBanco, Agencia, ContaCorrente, Cep, Endereco, CidadeId, FornecedorId) VALUES " + linha.Replace(")),", "))"));
                        }

                        catch
                        {
                            Console.WriteLine($"Erro na linha {i}");
                            linhasComErros.Add(i);
                        }
                    }
                    i++;
                };
            }
            Console.WriteLine($"Linhas com erros: {String.Join(",", linhasComErros.Select(l => l.ToString()))}");
            Console.ReadLine();
        }

        static void testeBuscaSite()
        {
            Console.Write("Informe os termos de busca (separados por espaço): ");
            var termos = Console.ReadLine();
            using (var db = new DbTransparencia())
            {
                var retorno = new IndiceDAL(db).Buscar(termos);
                foreach (var item in retorno)
                {
                    Console.WriteLine($"Ocorrência: {item.Titulo} // Palavras-chave: {item.PalavrasChave}");
                }
                if (retorno.Count() == 0)
                {
                    Console.WriteLine("Nenhum registro retornado");
                }
            }
        }

        static void testeGetHierarquiaPastas()
        {
            var guidPasta = new Guid("B0D36967-0A08-4AF0-80E7-2A2EEADAC48A");
            using (var db = new DbTransparencia())
            {
                var hierarquia = new PastaDAL(db).GetHierarquiaPastas(guidPasta);
            }
        }

        static void CriacaoUsuario()
        {
            Console.Write("Informe e-mail: ");
            var email = Console.ReadLine();
            Console.Write("Informe a senha: ");
            var senha = Console.ReadLine();
            Console.Write("Informe o nome: ");
            var nome = Console.ReadLine();
            using (var db = new DbTransparencia())
            {
                new UsuarioDAL(db).SalvarDados(new Usuario { Nome = nome, Email = email, Senha = senha.GenerateHash() });
            }
        }

        static void GenerateRandomString()
        {
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine(StringUtils.GenerateRandomString(8));
            }
        }
    }
}

