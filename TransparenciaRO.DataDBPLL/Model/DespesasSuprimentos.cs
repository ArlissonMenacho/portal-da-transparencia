﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.DataDBPLL.Model
{
    public class DespesasSuprimentos
    {
        [DisplayName("Ano de Exercicio")]
        public Int16 Exercicio { get; set; }
        [DisplayName("Detalhe Despesa")]
        public Int64 EspecificacaoDespesa { get; set; }
        [DisplayName("Código da Unidade")]
        public string CodUnidadeGestora { get; set; }
        [DisplayName("Unidade Gestora")]
        public string UnidadeGestora { get; set; }
        [DisplayName("Documento Credor")]
        public string DocCredor { get; set; }
        [DisplayName("Nome Credor")]
        public string Credor { get; set; }
        [Key]
        public string DocumentoNE { get; set; }
        public string DocumentoOB { get; set; }
        public string DataDocumento { get; set; }
        [DisplayName("Valor Pago")]
        public decimal ValorPaga { get; set; }
        public string Descricao { get; set; }
    }
}
