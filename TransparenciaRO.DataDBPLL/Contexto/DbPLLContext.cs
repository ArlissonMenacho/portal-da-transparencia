﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.DataDBPLL.Model;

namespace TransparenciaRO.DataDBPLL.Contexto
{
    public class DbPLLContext : DbContext
    {
        public DbPLLContext() : base("name=DbPLL")
        {

        }

        public virtual DbSet<DespesasSuprimentos> DespesasSuprimentos { get; set; }
    }
}
