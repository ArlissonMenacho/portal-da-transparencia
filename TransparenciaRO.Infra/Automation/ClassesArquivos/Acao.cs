﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.Automation.ClassesArquivos
{
    public class Acao : ClasseArquivo
    {

        public Acao(DbTransparencia db, int pExercicio) : base(db, pExercicio) { }

        public override string NomeArquivo
        {
            get
            {
                return "ditbpasp.txt";
            }
        }

        public override bool InserirLinhaNoBD(string pLinhaDados, out string pMensagemErro)
        {
            pMensagemErro = "";

            var CodProjeto = pLinhaDados.Substring(0, 1).ToInt();
            if (CodProjeto == null)
            {
                pMensagemErro = $"Não foi possível ler o valor int de CodProjeto. Valor encontrado:'{pLinhaDados.Substring(0, 1)}'";
                return false;
            }

            var CodAcao = pLinhaDados.Substring(1, 3).ToInt();
            if (CodAcao == null)
            {
                pMensagemErro = $"Não foi possível ler o valor int de CodAcao. Valor encontrado:'{pLinhaDados.Substring(1, 3)}'";
                return false;
            }

            var NomAcao = pLinhaDados.Substring(9, 60);

            
            if (db.Database.SqlQuery<int>("select count(1) from DBPLL.dbo.TBPLLACAO where Exercicio = {0} and CodProjeto = {1} and CodAcao = {2} and CodAdministracao = 0;", Exercicio, CodProjeto, CodAcao).Single() > 0)
            {
                pMensagemErro = "Não foi possível inserir a ação. O mesmo já existe na base de dados";
                return false;
            }
            var sql = String.Format("insert into DBPLL.dbo.TBPLLACAO (Exercicio, CodProjeto, CodAcao, CodAdministracao, NomAcao) values ({0},{1},{2},0,'{3}')", Exercicio, CodProjeto, CodAcao, NomAcao);
            try
            {
                db.Database.ExecuteSqlCommand(sql);
            }
            catch (Exception e)
            {
                pMensagemErro = $"Erro ao tentar inserir registro: '{e.Message}'\nLinha: '{pLinhaDados}'\nQuery: '{sql}'";
                return false;
            }

            return true;
        }

    }
}
