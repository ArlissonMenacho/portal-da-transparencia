﻿using System;
using TransparenciaRO.Infra.DAL;

namespace TransparenciaRO.Infra.Automation.ClassesArquivos
{
    public abstract class ClasseArquivo
    {

        internal DbTransparencia db;
        internal int Exercicio;

        public abstract string NomeArquivo { get; }

        public ClasseArquivo(DbTransparencia db, int pExercicio) {
            this.db = db;
            this.Exercicio = pExercicio;
        }

        public abstract bool InserirLinhaNoBD(string pLinhaDados, out string pMensagemErro);
    }
}
