﻿using Renci.SshNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using static System.Configuration.ConfigurationManager;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Automation.ClassesArquivos;

namespace TransparenciaRO.Infra.Automation
{



    //public static class ExercorAutomation
    //{
    //    private static string host = AppSettings["exercor_sftp_host"];
    //    private static int port = Convert.ToInt32(AppSettings["exercor_sftp_port"]);
    //    private static string username = AppSettings["exercor_sftp_username"];
    //    private static string password = AppSettings["exercor_sftp_password"];
    //    private static string destinoArquivos = AppSettings["exercor_destination_folder"];
    //    private static string caminho7Z = AppSettings["exercor_7zapp_folder"];
    //    private static string caminhoAppImportador = AppSettings["exercor_importador_path"];
    //    private static string storedProcedureRefresh = AppSettings["exercor_refresh_storedprocedure"];

    //    public static bool ExecutarScript()
    //    {
    //        try
    //        {

    //            var arquivos = new List<String> { "exerant", "exercor"  };
    //            foreach (var arquivo in arquivos)
    //            {
    //                LogUtils.Logar($"Iniciando rotina para {arquivo}...");
    //                var nomeArquivo = BaixarArquivo(arquivo);
    //                if (String.IsNullOrWhiteSpace(nomeArquivo))
    //                {
    //                    LogUtils.Logar("Rotina de importação de script não foi executada com sucesso. Por favor, verifique possíveis erros e faça a importação manual");
    //                    return false;
    //                }
    //                DescompactarArquivo(nomeArquivo);
    //                ProcessarArquivosNaoSuportadosPeloImportador(arquivo == "exercor" ? DateTime.Now.Year : DateTime.Now.Year - 1);
    //                ExecutarImportador();
    //            }

    //            ExecutarProcedureProcessamentoDados();

    //            return true;
    //        }
    //        catch (Exception e)
    //        {
    //            LogUtils.Logar($"Rotina de importação de script não foi executada com sucesso. Erro: '{e}'");
    //            return false;
    //        }
    //    }

    //    private static string BaixarArquivo(string pNomeArquivo7Z)
    //    {
    //        var tentativas = 5;
    //        while (tentativas > 0)
    //        {
    //            using (var sftp = new SftpClient(host, username, password))
    //            {
    //                try
    //                {
    //                    LogUtils.Logar("Conectando ao servidor SFTP...");


    //                    sftp.Connect();
    //                    var nomeArquivoDestino = $"{destinoArquivos}{pNomeArquivo7Z}-{DateTime.Now.ToString("yyyy-MM-dd")}.7z";
    //                    LogUtils.Logar($"Conexão efetuada com sucesso! Nome do arquivo destino: '{nomeArquivoDestino}'");
    //                    if (File.Exists(nomeArquivoDestino))
    //                    {
    //                        LogUtils.Logar($"Já existe um arquivo no caminho '{nomeArquivoDestino}'. Tentando excluir...");
    //                        try
    //                        {
    //                            File.Delete(nomeArquivoDestino);
    //                            LogUtils.Logar($"Arquivo excluído com sucesso!");
    //                        }
    //                        catch (Exception e)
    //                        {
    //                            LogUtils.Logar($"Não foi possível excluir o arquivo '{nomeArquivoDestino}'. Erro: '{e}'");
    //                            return null;
    //                        }
    //                    }

    //                    using (var arquivoDestino = File.OpenWrite(nomeArquivoDestino))
    //                    {
    //                        LogUtils.Logar($"Baixando o arquivo {pNomeArquivo7Z}.7z...");
    //                        sftp.DownloadFile($"{pNomeArquivo7Z}.7z", arquivoDestino);
    //                        LogUtils.Logar($"Arquivo {pNomeArquivo7Z}.7z baixado");
    //                    }
    //                    LogUtils.Logar("Download efetuado com sucesso!");
    //                    sftp.Disconnect();
    //                    return nomeArquivoDestino;
    //                }
    //                catch (Exception e)
    //                {
    //                    LogUtils.Logar($"Erro ao tentar baixar o arquivo exercor do SFTP. Erro: '{e.ToString()}'. Número de tentativas restantes: {tentativas}");
    //                    tentativas--;
    //                }
    //            }
    //        }
    //        return null;
    //    }

    //    private static void DescompactarArquivo(string pNomeArquivo)
    //    {
    //        var proc = new Process();
    //        proc.StartInfo.FileName = $"\"{caminho7Z}7z.exe\"";
    //        proc.StartInfo.Arguments = $"x \"{pNomeArquivo}\" -o\"{destinoArquivos}\" * -r -aoa";
    //        Console.WriteLine("Executando:");
    //        Console.WriteLine($"{proc.StartInfo.FileName} {proc.StartInfo.Arguments}");
    //        proc.Start();
    //        proc.WaitForExit();
    //        proc.Close();
    //    }

    //    public static void ProcessarArquivosNaoSuportadosPeloImportador(int pExercicio)
    //    {
    //        Console.WriteLine("Processando arquivos não suportados pelo importador antigo...");
    //        using (var db = new DbTransparencia())
    //        {
    //            //Ação
    //            Console.WriteLine("Processando arquivo de ação...");
    //            ProcessarArquivoNaoSuportadoPeloImportador(new Acao(db, pExercicio));
    //        }

    //    }

    //    private static void ProcessarArquivoNaoSuportadoPeloImportador(ClasseArquivo pClasseArquivo)
    //    {
    //        var linhas = File.ReadAllLines($"{destinoArquivos}\\{pClasseArquivo.NomeArquivo}").Skip(1); //Pula a primeira linha (apenas texto do header)
    //        foreach (var linha in linhas)
    //        {
    //            var msgErro = "";
    //            pClasseArquivo.InserirLinhaNoBD(linha, out msgErro);
    //            if (msgErro == "")
    //                Console.WriteLine($"Importação de ação OK. Linha: '{linha}'");
    //            else
    //            {
    //                Console.ForegroundColor = ConsoleColor.Red;
    //                Console.WriteLine($"Importação de ação NOK.\nErro: '{msgErro}'.\nLinha: '{linha}'");
    //                Console.ForegroundColor = ConsoleColor.White;
    //            }
    //        }

    //    }

    //    private static void ExecutarImportador()
    //    {
    //        Console.WriteLine("Executando importador");
    //        using (var proc = new Process())
    //        {
    //            proc.StartInfo.FileName = caminhoAppImportador;
    //            proc.Start();
    //            proc.WaitForExit();
    //        }
    //        Console.WriteLine("Importação concluída");
    //    }

    //    private static void ExecutarProcedureProcessamentoDados()
    //    {
    //        Console.WriteLine("Executando procedure de atualização");
    //        try
    //        {
    //            using (var db = new DbTransparencia())
    //            {
    //                db.Database.CommandTimeout = 480;
    //                db.Database.ExecuteSqlCommand($"[dbo].[{storedProcedureRefresh}]");
    //            }
    //            Console.WriteLine("Procedure de atualização realizada com sucesso");
    //        }
    //        catch(Exception e)
    //        {
    //            Console.WriteLine($"Erro ao executar a procedure de atualização: '{e.Message}'");
    //        }
            
    //    }
    //}
}
