﻿using System;

using System.Linq;
using TransparenciaRO.Infra.Constantes;
using System.Security.Claims;

namespace TransparenciaRO.Infra.Utils
{
    public static class ClaimsUtils
    {
        public static Guid IdUsuario(this ClaimsPrincipal usuario)
        {
            try
            {
                return new Guid(usuario.Claims.FirstOrDefault(c => c.Type == ClaimsCustomizadas.IdUsuario).Value);
            }
            catch
            {
                return new Guid();
            }
        }

        public static Guid UgID(this ClaimsPrincipal usuario)
        {
            try
            {
                return new Guid(usuario.Claims.FirstOrDefault(c => c.Type == ClaimsCustomizadas.UgID).Value);
            }
            catch
            {
                return new Guid();
            }
        }
    }
}
