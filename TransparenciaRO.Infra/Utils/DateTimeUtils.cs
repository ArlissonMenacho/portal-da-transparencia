﻿using System;

namespace TransparenciaRO.Infra.Utils
{
    public static class DateTimeUtils
    {
        public static DateTime LastSecondInMonth(this DateTime pData)
        {
            return new DateTime(pData.Year, pData.Month, DateTime.DaysInMonth(pData.Year, pData.Month), 23, 59, 59);
        }

        public static DateTime FirstDayInMonth(this DateTime pData)
        {
            return new DateTime(pData.Year, pData.Month, 1);
        }

        public static DateTime EndOfTheDay(this DateTime pData)
        {
            return new DateTime(pData.Year, pData.Month, pData.Day, 23, 59, 59);
        }

        public static string NomeMes(this int pMes)
        {
            switch (pMes)
            {
                case 1: return "Janeiro";
                case 2: return "Fevereiro";
                case 3: return "Março";
                case 4: return "Abril";
                case 5: return "Maio";
                case 6: return "Junho";
                case 7: return "Julho";
                case 8: return "Agosto";
                case 9: return "Setembro";
                case 10: return "Outubro";
                case 11: return "Novembro";
                case 12: return "Dezembro";
                case 13: return "Décimo Terceiro";
                default: return "";
            }
        }
    }

    public class MonthYear
    {
        public int Month { get; set; }
        public int Year { get; set; }

        public DateTime FirstDay()
        {
            return new DateTime(this.Year, this.Month, 1);
        }
        public DateTime LastSecond()
        {
            return new DateTime(this.Year, this.Month, 1).LastSecondInMonth();
        }

        public MonthYear()
        {

        }

        public MonthYear(DateTime pData)
        {
            Month = pData.Month;
            Year = pData.Year;
        }

        public MonthYear(int pMes, int pAno)
        {
            Month = pMes;
            Year = pAno;
        }
    }
}
