﻿using System.Web.Script.Serialization;

namespace TransparenciaRO.Infra.Utils
{
    public static class SerializacaoUtils
    {

        public static string Serializar(object pObj)
        {
            return new JavaScriptSerializer().Serialize(pObj);
        }

        public static T Desserializar<T>(string pObj)
        {
            return new JavaScriptSerializer().Deserialize<T>(pObj);
        }

        public static T TentarDesserializar<T>(string pObj)
        {
            try
            {
                return Desserializar<T>(pObj);
            }
            catch
            {
                return default(T);
            }
        }
    }
}
