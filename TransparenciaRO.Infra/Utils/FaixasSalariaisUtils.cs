﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Utils
{
    public class FaixasSalariaisUtils
    {
        public string FaixaSalarialDescricao(decimal salario)
        {
            const decimal salarioMinimo = 937;

            return salario <= salarioMinimo
                ? "Até 1 Salário Mínimo Federal"
                : salario > salarioMinimo && salario <= salarioMinimo * 2
                    ? "De 1 até 2 Salários Mínimos Federais"
                    : salario > salarioMinimo * 2 && salario <= salarioMinimo * 4
                        ? "De 2 até 4 Salários Mínimos Federais"
                        : salario > salarioMinimo * 4 && salario <= salarioMinimo * 8
                            ? "De 4 até 8 Salários Mínimos Federais"
                            : salario > salarioMinimo * 8 && salario <= salarioMinimo * 16
                                ? "De 8 até 16 Salários Mínimos Federais"
                                : "Acima de 16 Salários Mínimos Federais";
        }

        public Tuple<decimal, decimal> FaixaSalarialValor(string faixaSalarial)
        {
            var salarioMinimo = 937;
            var valorInicial = 0m;
            var valorFinal = 0m;

            if (faixaSalarial == "Até 1 Salário Mínimo Federal")
            {
                valorInicial = -1; valorFinal = salarioMinimo;
            }

            if (faixaSalarial == "De 1 até 2 Salários Mínimos Federais")
            {
                valorInicial = salarioMinimo; valorFinal = salarioMinimo * 2;
            }

            if (faixaSalarial == "De 2 até 4 Salários Mínimos Federais")
            {
                valorInicial = salarioMinimo * 2; valorFinal = salarioMinimo * 4;
            }

            if (faixaSalarial == "De 4 até 8 Salários Mínimos Federais")
            {
                valorInicial = salarioMinimo * 4; valorFinal = salarioMinimo * 8;
            }

            if (faixaSalarial == "De 8 até 16 Salários Mínimos Federais")
            {
                valorInicial = salarioMinimo * 8; valorFinal = salarioMinimo * 16;
            }

            if (faixaSalarial == "Acima de 16 Salários Mínimos Federais")
            {
                valorInicial = salarioMinimo * 16; valorFinal = salarioMinimo * 9999999;
            }

            return Tuple.Create(valorInicial, valorFinal);
        }
    }
}
