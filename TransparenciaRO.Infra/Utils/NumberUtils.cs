﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Utils
{
    public static class NumberUtils
    {
        /// <summary>
        /// Tenta converter o valor string para int. Caso consiga já retorna o seu valor em int no mesmo método
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int? ToInt(this string pValor)
        {
            var numero = 0;
            if (int.TryParse(pValor, out numero))
                return numero;
            return null;
        }
    }
}
