﻿using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Configuration;
using System.Net.Mime;
using System.Security;
using System.Web.Configuration;
using static System.Configuration.ConfigurationManager;
using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TransparenciaRO.Infra.Utils
{
    public static class MailUtils
    {
        public static string SendMail(string pConteudo, string pAssunto = "Contato pelo portal", bool pHTML = false, string pDestinatario = null)
        {
            try
            {
                bool bValidaEmail = ValidaEnderecoEmail(pDestinatario);

                // Se o email não é validao retorna uma mensagem
                if (bValidaEmail == false)
                    return "Email do destinatário inválido: " + pDestinatario;

                var smtp = new SmtpClient();
                var smtpSection = (SmtpSection)GetSection("system.net/mailSettings/smtp");
                using (var message = new MailMessage(smtpSection.From, pDestinatario ?? AppSettings["mailContatoTo"])
                {
                    IsBodyHtml = pHTML,
                    Subject = pAssunto,
                    Body = pConteudo
                })
                {
                    smtp.Send(message);
                }
                //LogUtils.Logar($"E-mail enviado com sucesso! Conteúdo: '{pConteudo}' | Assunto: '{pAssunto}'");
                return "";
            }
            catch (Exception e)
            {
                var msgErro = $"Erro ao tentar enviar e-mail! Conteúdo: '{pConteudo}' | Assunto: '{pAssunto}' | MensagemErro: '{e.ToString()}'";
                //LogUtils.Logar(msgErro);
                return msgErro;
            }
        }

        public static bool ValidaEnderecoEmail(string enderecoEmail)
        {
            try
            {
                //define a expressão regulara para validar o email
                string texto_Validar = enderecoEmail;
                Regex expressaoRegex = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");

                // testa o email com a expressão
                if (expressaoRegex.IsMatch(texto_Validar))
                {
                    // o email é valido
                    return true;
                }
                else
                {
                    // o email é inválido
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}