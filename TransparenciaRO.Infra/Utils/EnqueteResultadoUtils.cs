﻿using System;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Utils
{
    public class EnqueteResultadoUtils
    {
        public string ResultadoDescricao(EAvaliacao avaliacao)
        {
            return avaliacao == EAvaliacao.Otimo
                ? "Ótimo"
                : avaliacao == EAvaliacao.Bom
                    ? "Bom"
                    : avaliacao == EAvaliacao.Indiferente
                        ? "Indiferente"
                        : avaliacao == EAvaliacao.Ruim
                            ? "Ruim"
                            : avaliacao == EAvaliacao.Pessima
                                ? "Péssima"
                                : "Outros";
        }
    }
}