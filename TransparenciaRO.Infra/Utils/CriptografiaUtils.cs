﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using static System.Text.Encoding;
using static System.Convert;

namespace TransparenciaRO.Infra.Utils
{
    public static class CriptografiaUtils
    {

        private static string reservedKey = "CGERONDONIA20167";

        private static string equalPlaceHolder = "3QU4L";

        private static Rijndael GetEncryptionAlgorithm(string pSalt)
        {
            var returnValue = Rijndael.Create();
            returnValue.Key = ASCII.GetBytes(reservedKey);
            returnValue.IV = ASCII.GetBytes(pSalt);
            return returnValue;
        }

        public static string ToEncrypted(this string pText) => Encrypt(pText);
        public static string Encrypt(string pText, string pSalt = "C123454781503126")
        {
            try
            {
                using (var algorithm = GetEncryptionAlgorithm(pSalt))
                {
                    var encrypter = algorithm.CreateEncryptor();
                    using (var ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encrypter, CryptoStreamMode.Write))
                        {

                            var plainBytes = new UnicodeEncoding().GetBytes(pText);
                            cs.Write(plainBytes, 0, plainBytes.Length);
                            cs.FlushFinalBlock();
                            var returnValue = ToBase64String(ms.ToArray());
                            return returnValue.Replace("=", equalPlaceHolder).Replace("/", "_").Replace("+", "-");
                        }
                    }
                }
            }
            catch
            {
                return "";
            }
        }
        public static string ToDecrypted(this string pText) => Decrypt(pText);
        public static string Decrypt(string pText, string pSalt = "C123454781503126")
        {
            if (pText == null || pText.Trim() == "")
                return "";
            using (var algorithm = GetEncryptionAlgorithm(pSalt))
            {
                var decrypter = algorithm.CreateDecryptor();
                using (var ms = new MemoryStream())
                {
                    using (CryptoStream csStream = new CryptoStream(ms, decrypter, CryptoStreamMode.Write))
                    {
                        pText = pText.Replace(equalPlaceHolder, "=").Replace("_", "/").Replace("-", "+");
                        var cryptedBytes = FromBase64String(pText);
                        csStream.Write(cryptedBytes, 0, cryptedBytes.Length);
                        csStream.FlushFinalBlock();
                        var returnValue = new UnicodeEncoding().GetString(ms.ToArray());
                        return returnValue;
                    }
                }
            }
        }


        public static string GenerateHashToBase64(string pText, HashAlgorithm pHashAlgorithm = HashAlgorithm.Bits256) => ToBase64String(GenerateHash(pText, pHashAlgorithm)).TrimEnd(new char[] { '=' }).Replace('+', '-').Replace('/', '_');
        public static byte[] GenerateHash(this string pText, HashAlgorithm pHashAlgorithm = HashAlgorithm.Bits256)
        {
            var textBytes = Encoding.UTF8.GetBytes(pText);
            switch (pHashAlgorithm)
            {
                case HashAlgorithm.Bits384:
                    var alg384 = new SHA384Managed();
                    return alg384.ComputeHash(textBytes);
                case HashAlgorithm.Bits512:
                    var alg512 = new SHA512Managed();
                    return alg512.ComputeHash(textBytes);
                default:
                    var alg = new SHA256Managed();
                    return alg.ComputeHash(textBytes);
            }
        }
    }

    public enum HashAlgorithm
    {
        Bits256,
        Bits384,
        Bits512
    }
}
