﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TransparenciaRO.Infra.Utils
{
    public static class ComparerUtils
    {
        public static IEnumerable<T> Except<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;

        }

        public static IEnumerable<T> NotContains<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                       equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;

        }

        public static IEnumerable<T> Contains<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from item in items
                    join otherItem in other on getKey(item)
                        equals getKey(otherItem) into tempItems
                    from temp in tempItems.DefaultIfEmpty()
                    where !ReferenceEquals(null, temp) || !temp.Equals(default(T))
                    select item;
            
        }
    }
}
