﻿using TransparenciaRO.Infra.Enum;
using System.Linq;
using System;

namespace TransparenciaRO.Infra.Utils
{
    public static class StringUtils
    {
        public static byte[] ToBytes(this string str) => GetBytes(str);
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string BytesToString(this byte[] bytes) => GetString(bytes);
        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }        

        public static bool IsNullOrEmpty(this string pString) => (pString ?? "") == "";

        static Random random = new Random();
        public static string GenerateRandomString(int pLength)
        {
            byte[] buffer = new byte[pLength / 2];
            random.NextBytes(buffer);
            string result = String.Concat(buffer.Select(x => x.ToString("X2")).ToArray());
            if (pLength % 2 == 0)
                return result;
            return result + random.Next(16).ToString("X");
        }

        public static string RemoveEntries(this string pText, string[] pWords) => RemoveEntriesFromText(pText, pWords);
        public static string RemoveEntriesFromText(string pText, string[] pWords)
        {            
            var retorno = String.Join(" ", pText.Split(' ').Where(w => !pWords.Select(wo => wo.ToUpper()).Contains(w.ToUpper())).Select(w => w));
            
            return retorno;
        }
    }
}
