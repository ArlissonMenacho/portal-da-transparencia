﻿using System;

namespace TransparenciaRO.Infra.Utils
{
    public static class GuidUtils
    {

        public static Guid GuidMenuPrincipal()
        {
            return new Guid("05f5821e-ee84-43cf-abe5-d62b2094f330");
        }

        public static Guid GuidAreaAdministrativa()
        {
            return new Guid("d908eb92-9b33-4258-a811-b631f3cc94a1");
        }

        public static Guid GuidPastaContratosEConvenios()
        {
            return new Guid("AFBB2F05-BB14-46AB-B942-4E830A758580");
        }

        public static Guid ToGuid(this string pGuidString)
        {
            return new Guid(pGuidString);
        }

        public static Guid GuidPastaComiteTransparencia()
        {
            ////produção
            return new Guid("0C20AD16-D9ED-482E-93CC-36264F40E150");

            //teste
            //return new Guid("D7162851-4940-4157-90E2-FC7AC216D543");
        }

        public static Guid GuidPastaBoletins()
        {
            return new Guid("cae49c47-4f80-4296-9943-264c273cd42b");
        }

        //api divida ativa sefin

        public static string GuidAcessKeyDividaAtivaAPI()
        {
            return "b259d91e452ce8684fb951a999f602c9";
        }

        public static string GuidSecretKeyDividaAtivaAPI()
        {
            return "28d666c702783f4c0703d59ce75eae29";
        }
    }
}
