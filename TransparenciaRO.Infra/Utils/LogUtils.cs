﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.DAL;
using System.IO;
using static System.Configuration.ConfigurationManager;

namespace TransparenciaRO.Infra.Utils
{
    public static class LogUtils
    {
        private static readonly object _lock = new object();

        public static Guid Logar(string pMensagem, Guid? pUsuarioLogado = null)
        {
            lock (_lock)
            {
                using (var db = new DbTransparencia())
                {
                    var guid = new Guid();
                    try
                    {
                        guid = new LogDAL(db).SalvarDados(new Model.DB.dbTransparencia.Log { Descricao = pMensagem, UsuarioId = pUsuarioLogado });
                     #if DEBUG
                        Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} - Mensagem do log ({DateTime.Now.ToString("HH:mm:ss")}): '{pMensagem}'");
                    #endif
                        db.SaveChanges();
                    }
                    catch { }
                 
                    return guid;
                }
            }
        }
    }
}
