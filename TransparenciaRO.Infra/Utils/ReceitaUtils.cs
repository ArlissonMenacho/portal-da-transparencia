﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Utils
{
    public class ReceitaUtils
    {
        public string QuadrimestreDescricao(int ano, int mes)
        {
            string quardrimestre = "";

            switch(mes)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                    quardrimestre = $"1º Quard/{ano}";
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                    quardrimestre = $"2º Quard/{ano}";
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                    quardrimestre = $"3º Quard/{ano}";
                    break;
            }

            return quardrimestre;
        }

        public Tuple<int,int> QuadrimestreDetalhes(string quadrimestre)
        {

            return quadrimestre.Contains("1º")
                ? Tuple.Create(1, 4)
                : quadrimestre.Contains("2º")
                ? Tuple.Create(5, 8)
                : Tuple.Create(9, 12);
        }
    }
}
