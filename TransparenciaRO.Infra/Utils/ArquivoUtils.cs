﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static System.Configuration.ConfigurationManager;

namespace TransparenciaRO.Infra.Utils
{
    public static class ArquivoUtils
    {
        public static string CaminhoArquivo(Guid pArquivoId)
        {
            var resultado = Path.Combine(AppSettings["caminho_arquivos"], pArquivoId.ToString());
            return resultado;
        }

        /// <summary>
        /// Valida se o arquivo atende os requisitos do portal de transparência (se é PDF, Excel ou Word e possui um tamanho menor do que 10485760 bytes)
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool ArquivoValido(HttpPostedFileBase file)
        {
            var tiposArquivosPermitidos = new List<string>()
            {
                ".pdf", ".xlsx", ".xls", ".doc", ".docx", ".png", ".jpeg", ".jpg"
            };

            var tipoArquivo = Path.GetExtension(file.FileName)?.ToLower();

            return (file.ContentLength <= 2147483647 && tiposArquivosPermitidos.Contains(tipoArquivo));
        }

        /// <summary>
        /// Salva o arquivo no HD (fisicamente). Este método NÃO insere a entidade Arquivo no BD
        /// </summary>
        /// <param name="file"></param>
        /// <param name="pArquivoId"></param>
        /// <returns></returns>
        public static bool SalvarArquivo(HttpPostedFileBase file, Guid pArquivoId)
        {
         
                if (File.Exists(CaminhoArquivo(pArquivoId)))
                    return false;
                file.SaveAs(CaminhoArquivo(pArquivoId));
                return true;     
         
        }

        public static bool ArquivoExiste(Guid pArquivoId)
        {
            return File.Exists(CaminhoArquivo(pArquivoId));
        }

        public static bool ExcluirArquivo(Guid pArquivoId)
        {
                if (File.Exists(CaminhoArquivo(pArquivoId)))
                {
                    File.Delete(CaminhoArquivo(pArquivoId));
                    return true;
                }
                return false;          
        }
    }
}
