﻿USE [DBPLL]
GO
/****** Object:  StoredProcedure [dbo].[paIncluirAlterarReceitaArrecadaAcumuladaSIAFEM]    Script Date: 22/11/2016 10:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[paIncluirAlterarReceitaArrecadaAcumuladaSIAFEM]
@Exercicio					smallint
,@CodAdministracao			int
,@CodEspecificacaoReceita	bigint
,@Descricao					varchar(255)
,@ValorMensal				money
,@ValorAcumulado			money
,@ValorSaldoFinal			money
,@Mes						Tinyint
As


UPDATE DBPLL.dbo.TbPLLReceitaAcumuladaSiafem
   SET	ValorMensal = @ValorMensal
		,ValorAcumulado = @ValorAcumulado
		,ValorSaldoFinal = @ValorSaldoFinal
		,Descricao = @Descricao
 WHERE	Exercicio = @Exercicio
   and	CodAdministracao = @CodAdministracao
   and	CodEspecificacaoReceita = @CodEspecificacaoReceita
   and	Mes = @Mes


if @@ROWCOUNT = 0

		Begin
		
				INSERT INTO DBPLL.dbo.TbPLLReceitaAcumuladaSiafem
						   (Exercicio
						   ,CodAdministracao
						   ,CodEspecificacaoReceita
						   ,Descricao
						   ,ValorMensal
						   ,ValorAcumulado
						   ,ValorSaldoFinal
						   ,Mes)
					 VALUES
						   (@Exercicio
						   ,@CodAdministracao
						   ,@CodEspecificacaoReceita
						   ,@Descricao
						   ,@ValorMensal
						   ,@ValorAcumulado
						   ,@ValorSaldoFinal
						   ,@Mes)

		
		
		End