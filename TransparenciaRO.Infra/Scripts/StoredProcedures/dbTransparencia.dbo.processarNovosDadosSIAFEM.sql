﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Felipe Campos
-- Create date: 2016.11.30
-- Description:	Rotina de atualização de tabelas de despesas (que são espelhadas em dados de DBPLL) e que será disparada pelo ExercorAutomation.cs. Fará: 1) Atualização das tabelas despesasportal e despesasportaldetalhadas. 2) Atualização das datas de atualização de dados na tabela de parâmetros
-- =============================================
CREATE PROCEDURE ProcessarNovosDadosSIAFEM
	
AS
BEGIN
	
	
DECLARE @dataminima AS DATE = Dateadd(month, -6, Getdate()); 

SELECT @dataminima; 

DELETE FROM despesasportal 
WHERE  datadocumento >= @dataminima; 

DELETE FROM despesasportaldetalhadas 
WHERE  datadocumento >= @dataminima; 

INSERT INTO despesasportal 
			(registrodespesa, 
			 codprograma, 
			 codacao, 
			 exercicio, 
			 programa, 
			 valorempenhada, 
			 valorliquidada, 
			 valorpaga, 
			 acao, 
			 valordespesa, 
			 codprojeto, 
			 evento, 
			 codfuncao, 
			 nomfuncao, 
			 codsubfuncao, 
			 fonte, 
			 desfonterecurso, 
			 nomdespesa, 
			 especificacaodespesa, 
			 nomorgao, 
			 numempenho, 
			 documento, 
			 datadocumento, 
			 credor, 
			 doccredor, 
			 ug, 
			 descricaosubfuncao) 
SELECT Newid(), 
	   codprograma, 
	   codacao, 
	   exercicio, 
	   programa, 
	   valorempenhada, 
	   valorliquidada, 
	   valorpaga, 
	   acao, 
	   valordespesa, 
	   codprojeto, 
	   evento, 
	   codfuncao, 
	   nomfuncao, 
	   codsubfuncao, 
	   fonte, 
	   desfonterecurso, 
	   nomdespesa, 
	   especificacaodespesa, 
	   nomorgao, 
	   numempenho, 
	   documento, 
	   datadocumento, 
	   credor, 
	   doccredor, 
	   ug, 
	   descricaosubfuncao 
FROM   despesas 
WHERE  datadocumento >= @dataminima; 

DELETE FROM despesasportaldetalhadas 
WHERE  datadocumento >= @dataminima; 

SELECT DISTINCT datadocumento, 
				dataempenho 
FROM   despesas 
WHERE  datadocumento >= @dataminima; 

INSERT INTO despesasportaldetalhadas 
			([codprograma], 
			 [programa], 
			 [codacao], 
			 [acao], 
			 [processo], 
			 [exercicio], 
			 [valorempenhada], 
			 [valorliquidada], 
			 [valorpaga], 
			 [valordespesa], 
			 [codprojeto], 
			 [evento], 
			 [codfuncao], 
			 [nomfuncao], 
			 [codsubfuncao], 
			 [descricaosubfuncao], 
			 [fonte], 
			 [desfonterecurso], 
			 [codnomdespesa], 
			 [nomdespesa], 
			 [especificacaodespesa], 
			 [naturezadespesa], 
			 [nomorgao], 
			 [numempenho], 
			 [documento], 
			 [datadocumento], 
			 [credor], 
			 [doccredor], 
			 [ug], 
			 [codcategoriadespesa], 
			 [categoriadespesa], 
			 [codgrupodespesa], 
			 [grupodespesa], 
			 [codmodaplicacao], 
			 [modaplicacao], 
			 [documentone], 
			 [tipoempenho], 
			 [modalidadelicitacao], 
			 [dataempenho]) 
SELECT distinct [codprograma], 
	   [programa], 
	   [codacao], 
	   [acao], 
	   [processo], 
	   [exercicio], 
	   [valorempenhada], 
	   [valorliquidada], 
	   [valorpaga], 
	   [valordespesa], 
	   [codprojeto], 
	   [evento], 
	   [codfuncao], 
	   [nomfuncao], 
	   [codsubfuncao], 
	   [descricaosubfuncao], 
	   [fonte], 
	   [desfonterecurso], 
	   [codnomdespesa], 
	   [nomdespesa], 
	   [especificacaodespesa], 
	   [naturezadespesa], 
	   [nomorgao], 
	   [numempenho], 
	   [documento], 
	   [datadocumento], 
	   [credor], 
	   [doccredor], 
	   [ug], 
	   [codcategoriadespesa], 
	   [categoriadespesa], 
	   [codgrupodespesa], 
	   [grupodespesa], 
	   [codmodaplicacao], 
	   [modaplicacao], 
	   [documentone], 
	   [tipoempenho], 
	   [modalidadelicitacao], 
	   [dataempenho] 
FROM   despesas 
WHERE  datadocumento >= @dataminima
and ValorPaga <> 0; 

-- Data de atualização de despesas/receitas
DELETE FROM Parametros where TipoParametro = 1000 or TipoParametro = 1001;
INSERT INTO Parametros (TipoParametro, Valor) values (1000,LEFT(CONVERT(VARCHAR, (select max(datadocumento) from despesas), 120), 10)),(1001,LEFT(CONVERT(VARCHAR, (select max(datadocumento) from despesas), 120), 10));

-- Data de atualização de dotação inicial
DELETE FROM Parametros where TipoParametro = 1002;
INSERT INTO Parametros (TipoParametro, Valor) values (1002,LEFT(CONVERT(VARCHAR, (select max([dt-emirz]) from [DBPLL].[dbo].TbPLLRazaoContabilSiafem_DotacaoInicial), 120), 10));

-- Compras contratações / Empenhos
DELETE FROM Parametros where TipoParametro = 1003;
INSERT INTO Parametros (TipoParametro, Valor) values (1003,LEFT(CONVERT(VARCHAR, (select max(DataEmpenho) from Empenhos), 120), 10));



END
GO
