﻿-- Arquivo unificado com script de criação de todas as views

USE [dbTransparencia]
GO

IF OBJECT_ID('Despesas', 'V') IS NOT NULL
	DROP VIEW Despesas;

GO

CREATE VIEW [dbo].[Despesas]
AS
SELECT        a.CodPrograma, a.CodAcao, a.Exercicio,  right('0000' + a.CodPrograma,4) + ' - ' + b.NomPrograma AS Programa, CASE WHEN (d .[ct.contab] IN (522910100, 522910200, 522910900) AND 
						 d .[DT-EMIRZ] >= '20130101') OR
						 (d .[ct.contab] IN (192410101, 192410102, 192410109) AND d .[DT-EMIRZ] < '20130101') THEN d .[VALOR DEBITO] - d .[VALOR CREDITO] ELSE 0 END AS ValorEmpenhada, 
						 CASE WHEN (d .[ct.contab] = 292420101 AND d .[DT-EMIRZ] < '20130101') OR
						 (d .[ct.contab] = 622910200 AND d .[DT-EMIRZ] >= '20130101') THEN d .[VALOR CREDITO] - d .[VALOR DEBITO] ELSE 0 END AS ValorLiquidada, CASE WHEN (d .[ct.contab] = 192510100 AND 
						 d .[DT-EMIRZ] < '20130101') OR
						 (d .[ct.contab] = 622910500 AND d .[DT-EMIRZ] >= '20130101') THEN d .[VALOR CREDITO] - d .[VALOR DEBITO] ELSE 0 END AS ValorPaga, right(c.CodProjeto + '0',1)
						 + right('000' + a.CodAcao,3) + ' - ' + c.NomAcao AS Acao, d.[VALOR CREDITO] - d.[VALOR DEBITO] AS valorDespesa, c.CodProjeto, a.Evento, a.CodFuncao, fnc.NomFuncao, a.CodSubFuncao, 
						 a.Fonte, fr.DesFonteRecurso, ed.NomDespesa, a.EspecificacaoDespesa, org.NomOrgao, a.NumEmpenho, d.[DOCUMENT-DL] AS Documento, d.[DT-EMIRZ] AS DataDocumento, a.Credor,  a.DocCredor,  a.UG, 
						 sfnc.Descricao AS DescricaoSubFuncao
FROM            [DBPLL].dbo.TbPLLRazaoContabilSiafem AS d INNER JOIN
						 [DBPLL].dbo.TbPLLAcompanhamentoMetaFisicaSIAFEM AS a ON a.Exercicio = d.EXER AND d.[UN.RZ.] COLLATE database_default = a.UG AND d.[GE.RZ] COLLATE database_default = a.Orgao AND 
						 a.NumEmpenho COLLATE database_default = d.[CONTA CORRENTE] INNER JOIN
						 [DBPLL].dbo.TbPLLPrograma AS b ON a.Exercicio = b.Exercicio AND a.CodPrograma = b.CodPrograma INNER JOIN
						 [DBPLL].dbo.TbPLLAcao AS c ON a.Exercicio = c.Exercicio AND a.CodAcao = c.CodAcao AND a.CodProjeto = c.CodProjeto INNER JOIN
						 [DBPLL].dbo.TbPLLFuncao AS fnc ON a.CodFuncao = fnc.CodFuncao AND a.Exercicio = fnc.Exercicio INNER JOIN
						 [DBPLL].dbo.TbPLLSubFuncaoSiafem AS sfnc ON a.CodSubFuncao = sfnc.CodSubFuncao INNER JOIN
						 [DBPLL].dbo.TbPLLFonteRecurso AS fr ON a.Fonte = fr.CodFonteRecurso AND a.Exercicio = fr.Exercicio INNER JOIN
						 [DBPLL].dbo.TbPLLOrgao AS org ON a.Exercicio = org.Exercicio AND a.CodOrgao = org.CodOrgao LEFT OUTER JOIN
						 [DBPLL].dbo.TbPLLEspecificacaoDespesa AS ed ON a.EspecificacaoDespesa = ed.CodEspecificacaoDespesa AND a.Exercicio = ed.Exercicio
WHERE        ((d.[CT.CONTAB] IN (192410101, 192410102, 192410109, 522910100, 522910200, 522910900, 192510100, 622910500)) AND (a.Evento IN ('400091', '400891')) OR
						 (d.[CT.CONTAB] IN (292420101, 622910200)) AND (a.Evento IN ('400091', '400891')) /*AND (d.EVENTO BETWEEN 510000 AND 519999)*/)
						 and
						 d .[DT-EMIRZ] > '20130101';

GO





GO



IF OBJECT_ID('DotacaoInicial', 'V') IS NOT NULL
	DROP VIEW DotacaoInicial;
go
create view DotacaoInicial as
Select
	A.[UN.RZ.] as CodigoOrgao,
	case when org.NOMEORGAO is null then A.[UN.RZ.] else A.[UN.RZ.] + ' - ' + org.NOMEORGAO end as Orgao
	,E.ProgramaTrabalho as ProgramadeTrabalho
	,substring(A.[CONTA CORRENTE], 18, 1) + '.' + substring(A.[CONTA CORRENTE], 19, 1) + '.' + substring(A.[CONTA CORRENTE], 20, 2) + '.' + substring(A.[CONTA CORRENTE], 22, 2) + '.00' as  NaturezadeDespesa
	,B.NomDespesa
	,YEAR(A.[DT-EMIRZ]) Exercicio
	,CASE WHEN YEAR(A.[DT-EMIRZ]) <= 2012 THEN sum(A.[VALOR CREDITO]) ELSE   sum(A.[VALOR DEBITO] - A.[VALOR CREDITO]) END as SaldoInicial
from
	[DBPLL].[dbo].TbPLLRazaoContabilSiafem_DotacaoInicial A
	inner join [DBPLL].[dbo].TbPLLEspecificacaoDespesa B ON  Year(A.[DT-EMIRZ]) = B.Exercicio /* 01 */  and (substring(A.[CONTA CORRENTE], 18, 6) + '00') = B.CodEspecificacaoDespesa /* 02 */
	inner join [DBPLL].[dbo].TbPLLPTRESSiafem E ON  Year(A.[DT-EMIRZ]) = E.Exercicio /* 03 */ and substring(A.[CONTA CORRENTE], 2, 6) = E.[PTRES ] /* 04 */ and	SUBSTRING(A.[UN.RZ.], 1, 3) = Substring(E.UnOrc, 1, 3) /* 05 */ and SUBSTRING(A.[UN.RZ.], 5, 2) = Substring(E.UnOrc, 4, 2) /* 06 */
	left join [DBPLL].[dbo].TbPllOrgaoSiafem org ON a.[UN.RZ.] = org.ORGAO
	group by
	a.[UN.RZ.]
	,org.NOMEORGAO
	,b.NomDespesa
	,e.ProgramaTrabalho
	,substring(A.[CONTA CORRENTE], 18, 1) + '.' + substring(A.[CONTA CORRENTE], 19, 1) + '.' + substring(A.[CONTA CORRENTE], 20, 2) + '.' + substring(A.[CONTA CORRENTE], 22, 2)
	,YEAR(A.[DT-EMIRZ]);

	GO



	
GO

IF OBJECT_ID('Empenhos', 'V') IS NOT NULL
	DROP VIEW [Empenhos];

GO
CREATE VIEW [dbo].[Empenhos]
AS
select distinct 
  a.NumEmpenho as NumeroEmpenho,
  a.Processo as NumeroProcesso,
  case when left(a.docCredor, 2) = 'PF' then org.NomOrgao else isnull(a.credor,org.nomorgao) end as Credor,
  a.Processo,
  isnull(tpemp.Descricao,'N/D') as TipoEmpenho,
  isnull(mdlc.Descricao,'N/D') as ModalidadeCompra,
  e.Data_NE as DataEmpenho,
  org2.NomOrgao as UnidadeGestora,
  de.ItemEmpenho as Item,
  de.UnidadeMedida as UnidadeMedida,
  de.Qtde as Quantidade,
  case de.VlrUnitario when 0 then de.VlrEmpenho else de.VlrUnitario end as ValorItem,
  de.Descricao as Descricao,
  a.DataArquivo,
  a.CodOrgao as CodUnidadeGestora,
  a.Exercicio,
  mdlc.Codigo as CodModalidadeCompra
from
[DBPLL].[dbo].TbPLLAcompanhamentoMetaFisicaSIAFEM AS A
left join [DBPLL].[dbo].TbPLLEmpenhoSiafem e on a.NumEmpenho = e.DOC_ORIG_NE collate latin1_general_ci_ai and a.UG = e.UN_GES
left join [DBPLL].[dbo].TbPLLDescricaoEmpenho de on a.NumEmpenho = de.DOCUMENT_NE collate latin1_general_ci_ai and a.CodOrgao = de.UG
left join [DBPLL].[dbo].tbpllOrgao org on a.ug = org.CodOrgao
left join [DBPLL].[dbo].TbPLLTipoEmpenho tpemp on e.M_TipoEmpenho = tpemp.codigo
left join [DBPLL].[dbo].TbPLLModalidadeLicitacao mdlc on e.T_TipoLicitacao = mdlc.Codigo
left join [DBPLL].[dbo].tbpllorgao org2 on org2.CodOrgao = e.UN_GES and year(a.DataArquivo) = org2.Exercicio



GO






IF OBJECT_ID('EmpenhosMunicipios', 'V') IS NOT NULL
	DROP VIEW EmpenhosMunicipios;

	GO

CREATE VIEW EmpenhosMunicipios
AS
	SELECT   A.EXERCICIO,
			 A.Credor,
			 A.Credor + ' - ' + DBTCE.dbo.FT_RemontaDoc(A.DocCredor) AS CredorCNPJ,
			 a.doccredor,
			 b.CodigoMunicipio,
			 b.CodigoMunicipioDV,
			 B.[NomeMunicipio],
			 B.[ANOINST],
			 B.[LATITUDE],
			 B.[LONGITUDE],
			 B.[ALTITUDE],
			 B.[AREA],
			 A.CodFuncao,
			 C.NomFuncao,
			 Sum(A.VlrEmpenhado) AS ValorEmpenhado,
			 Sum(A.VlrEmpenhado) - Sum(A.VlrLiquidado) AS ValorEmpenhadoALiquidar,
			 Sum(A.VlrEmpenhado) - Sum(A.VlrPago) AS ValorEmpenhadoAPagar,
			 Sum(A.VlrLiquidado) AS ValorLiquidado,
			 Sum(A.VlrLiquidado) - Sum(A.VlrPago) AS ValorLiquidadoAPagar,
			 Sum(A.VlrPago) AS ValorPago
	FROM     DbPLL.dbo.TbPLLAcompanhamentoMetaFisicaSIAFEM AS A, DBTCE.dbo.TbGeoMunicipio AS B, DBPLL.dbo.TbPLLFuncao AS C
	WHERE    A.DocCredor = B.CNPJ COLLATE Latin1_General_CI_AI
			 AND A.Exercicio = C.Exercicio
			 AND A.CodFuncao = C.CodFuncao
	GROUP BY A.DocCredor, A.EXERCICIO, A.DocCredor, A.Credor, b.CodigoMunicipio, b.CodigoMunicipioDV, B.[NomeMunicipio], B.[ANOINST], B.[LATITUDE], B.[LONGITUDE], B.[ALTITUDE], B.[AREA], A.CodFuncao, C.NomFuncao;

	
GO

IF OBJECT_ID('FolhaPagamento', 'V') IS NOT NULL
	DROP VIEW FolhaPagamento;

	GO

CREATE VIEW [dbo].[FolhaPagamento]
AS
select * from [DBTCE].[dbo].FolhaPagamento;



GO


GO

IF OBJECT_ID('ModalidadeLicitacao', 'V') IS NOT NULL
	DROP VIEW ModalidadeLicitacao;

	
GO


CREATE VIEW [dbo].[ModalidadeLicitacao]
AS
select * from [DBPLL].[dbo].TbPLLModalidadeLicitacao;


GO


IF OBJECT_ID('Municipios', 'V') IS NOT NULL
	DROP VIEW Municipios;

	
GO

create view Municipios
AS
select
*
from
[DBTCE].[dbo].tbgeomunicipio;
go

IF OBJECT_ID('Orgaos', 'V') IS NOT NULL
	DROP VIEW Orgaos;	
GO

CREATE VIEW [dbo].[Orgaos]
AS
select * from [DBPLL].[dbo].TbPLLOrgao;

GO

IF OBJECT_ID('OrgaosSiafem', 'V') IS NOT NULL
	DROP VIEW OrgaosSiafem;	
GO

CREATE VIEW [dbo].[OrgaosSiafem]
AS
select * from [DBPLL].[dbo].TbPllOrgaoSiafem;


GO

IF OBJECT_ID('ReceitaAcumuladaSiafem', 'V') IS NOT NULL
	DROP VIEW ReceitaAcumuladaSiafem;	
GO

CREATE VIEW [dbo].[ReceitaAcumuladaSiafem]
AS
select * from [DBPLL].[dbo].TbPLLReceitaAcumuladaSiafem;


GO

IF OBJECT_ID('Diarias', 'V') IS NOT NULL
	DROP VIEW Diarias;	
GO
create view Diarias
as
select
	A.[DOCUMENT-NE] as Documento,
	A.[DT-EMIRZ] DataDocumento, -- Parâmetro período
	DBTCE.dbo.FT_RemontaDoc(isnull(A.[CONTA CORRENTE], 0)) as CPF,
	Isnull(C.NOME, A.[CONTA CORRENTE]) as Nome,
	A.[UN.RZ.] as UG, -- Parâmetro cod órgão (view Orgaos)
	Isnull(D.NomeOrgao, 'Erro ao Localizar Nome') as NomeOrgao,
	A.[Valor Credito] - A.[Valor Debito] as ValorDocumento
from
	[DBPLL].[dbo].TbPLLRazaoContabilSiafem_Diarias A
	left outer join [DBPLL].[dbo].TbPLLCredorSiafem C On A.[CONTA CORRENTE] = C.Credor collate Latin1_General_CI_AI
	Left outer join [DBPLL].[dbo].TbPllOrgaoSiafem D On cast(A.[UN.RZ.] as int)= cast(D.Orgao as int)
where
	A.[CT.CONTAB] IN (296310300, 858910300, 868910200)
	and
	substring(A.[DOCUMENT-NE], 5, 2) in('OB', 'GR', 'NL', 'NS');

go

IF OBJECT_ID('FornecedoresImpedidosLicitar', 'V') IS NOT NULL
	DROP VIEW Diarias;	
GO
CREATE VIEW [dbo].[FornecedoresImpedidosLicitar] AS
SELECT I.Codigo, U.sigla, U.nomeUG, F.CpfCnpj, F.RazaoSocial, I.InformacaoPublica, I.DataInicio, I.DataFinal, I.NumeroProcesso, I.DataPublicacao, I.PenasEventuais, 
CASE I.Sancao 
	WHEN 1 THEN 'Multa' 
	WHEN 2 THEN 'Suspensão'
	WHEN 3 THEN 'Declaração de Inidoneidade'
	WHEN 4 THEN 'Advertência' END AS Sancao
FROM Fornecedores F JOIN FornecedoresImpedidos I ON(F.FornecedorId = I.FornecedorId)
	JOIN UnidadesGestoras U ON(U.unidadeGestoraID = I.UnidadeGestoraId)
GO
