﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoParametro
    {
        DataAtualizacaoDespesas = 1000,
        DataAtualizacaoReceita = 1001,
        DataAtualizacaoDotacaoInicial = 1002,
        DataAtualizacaoComprasContratacoes = 1003
    }
}
