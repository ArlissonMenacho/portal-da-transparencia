﻿namespace TransparenciaRO.Infra.Enum
{
    public enum EEspecificacaoReceita : long
    {
        ReceitaCorrente = 10000000,
        ReceitaCapital = 20000000,
        ReceitaIntraOrcamentaria = 70000000,
        Tributos = 90000000,
        Total = 0
    }
}
