﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoPessoa
    {
        [Display(Name = "Física")]
        Fisica = 1,

        [Display(Name = "Jurídica")]
        Juridica = 2
    }
}
