﻿using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum EEmpresa
    {
        [Display(Name = "SOPH")]
        Soph,

        [Display(Name = "CMR")]
        Cmr,

        [Display(Name = "CAERD")]
        Caerd,

        [Display(Name = "EMATER")]
        Emater
    }
}
