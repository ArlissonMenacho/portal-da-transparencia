﻿using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum ESancaoImpedirFornecedor
    {
        Multa = 1,

        [Display(Name = "Suspensão")]
        Suspensao = 2,

        [Display(Name = "Declaração de Inidoneidade")]
        DeclaracaoDeInidoneidade = 3,

        [Display(Name = "Advertência")]
        Advertencia = 4
    }
}
