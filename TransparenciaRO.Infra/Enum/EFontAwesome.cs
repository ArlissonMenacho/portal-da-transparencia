﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum EFontAwesome
    {
        [Display(Name = "500px")]
        fa_500px,
        [Display(Name = "amazon")]
        fa_amazon,
        [Display(Name = "android")]
        fa_android,
        [Display(Name = "angellist")]
        fa_angellist,
        [Display(Name = "apple")]
        fa_apple,
        [Display(Name = "behance")]
        fa_behance,
        [Display(Name = "behance-square")]
        fa_behance_square,
        [Display(Name = "bitbucket")]
        fa_bitbucket,
        [Display(Name = "bitbucket-square")]
        fa_bitbucket_square,
        [Display(Name = "bitcoin")]
        fa_bitcoin,
        [Display(Name = "black-tie")]
        fa_black_tie,
        [Display(Name = "btc")]
        fa_btc,
        [Display(Name = "buysellads")]
        fa_buysellads,
        [Display(Name = "cc-amex")]
        fa_cc_amex,
        [Display(Name = "cc-diners-club")]
        fa_cc_diners_club,
        [Display(Name = "cc-mastercard")]
        fa_cc_mastercard,
        [Display(Name = "cc-paypal")]
        fa_cc_paypal,
        [Display(Name = "cc-stripe")]
        fa_cc_stripe,
        [Display(Name = "cc-visa")]
        fa_cc_visa,
        [Display(Name = "chrome")]
        fa_chrome,
        [Display(Name = "codepen")]
        fa_codepen,
        [Display(Name = "connectdevelop")]
        fa_connectdevelop,
        [Display(Name = "contao")]
        fa_contao,
        [Display(Name = "css3")]
        fa_css3,
        [Display(Name = "dashcube")]
        fa_dashcube,
        [Display(Name = "delicious")]
        fa_delicious,
        [Display(Name = "deviantart")]
        fa_deviantart,
        [Display(Name = "digg")]
        fa_digg,
        [Display(Name = "dribbble")]
        fa_dribbble,
        [Display(Name = "dropbox")]
        fa_dropbox,
        [Display(Name = "drupal")]
        fa_drupal,
        [Display(Name = "empire")]
        fa_empire,
        [Display(Name = "expeditedssl")]
        fa_expeditedssl,
        [Display(Name = "facebook")]
        fa_facebook,
        [Display(Name = "facebook-f")]
        fa_facebook_f,
        [Display(Name = "facebook-official")]
        fa_facebook_official,
        [Display(Name = "facebook-square")]
        fa_facebook_square,
        [Display(Name = "firefox")]
        fa_firefox,
        [Display(Name = "flickr")]
        fa_flickr,
        [Display(Name = "fonticons")]
        fa_fonticons,
        [Display(Name = "forumbee")]
        fa_forumbee,
        [Display(Name = "foursquare")]
        fa_foursquare,
        [Display(Name = "ge")]
        fa_ge,
        [Display(Name = "get-pocket")]
        fa_get_pocket,
        [Display(Name = "gg")]
        fa_gg,
        [Display(Name = "gg-circle")]
        fa_gg_circle,
        [Display(Name = "git")]
        fa_git,
        [Display(Name = "git-square")]
        fa_git_square,
        [Display(Name = "github")]
        fa_github,
        [Display(Name = "github-alt")]
        fa_github_alt,
        [Display(Name = "github-square")]
        fa_github_square,
        [Display(Name = "gittip")]
        fa_gittip,
        [Display(Name = "google")]
        fa_google,
        [Display(Name = "google-plus")]
        fa_google_plus,
        [Display(Name = "google-plus-square")]
        fa_google_plus_square,
        [Display(Name = "gratipay")]
        fa_gratipay,
        [Display(Name = "hacker-news")]
        fa_hacker_news,
        [Display(Name = "houzz")]
        fa_houzz,
        [Display(Name = "html5")]
        fa_html5,
        [Display(Name = "instagram")]
        fa_instagram,
        [Display(Name = "internet-explorer")]
        fa_internet_explorer,
        [Display(Name = "ioxhost")]
        fa_ioxhost,
        [Display(Name = "joomla")]
        fa_joomla,
        [Display(Name = "jsfiddle")]
        fa_jsfiddle,
        [Display(Name = "lastfm")]
        fa_lastfm,
        [Display(Name = "lastfm-square")]
        fa_lastfm_square,
        [Display(Name = "leanpub")]
        fa_leanpub,
        [Display(Name = "linkedin")]
        fa_linkedin,
        [Display(Name = "linkedin-square")]
        fa_linkedin_square,
        [Display(Name = "linux")]
        fa_linux,
        [Display(Name = "maxcdn")]
        fa_maxcdn,
        [Display(Name = "meanpath")]
        fa_meanpath,
        [Display(Name = "medium")]
        fa_medium,
        [Display(Name = "odnoklassniki")]
        fa_odnoklassniki,
        [Display(Name = "odnoklassniki-square")]
        fa_odnoklassniki_square,
        [Display(Name = "opencart")]
        fa_opencart,
        [Display(Name = "openid")]
        fa_openid,
        [Display(Name = "opera")]
        fa_opera,
        [Display(Name = "optin-monster")]
        fa_optin_monster,
        [Display(Name = "pagelines")]
        fa_pagelines,
        [Display(Name = "paypal")]
        fa_paypal,
        [Display(Name = "pied-piper")]
        fa_pied_piper,
        [Display(Name = "pied-piper-alt")]
        fa_pied_piper_alt,
        [Display(Name = "pinterest")]
        fa_pinterest,
        [Display(Name = "pinterest-p")]
        fa_pinterest_p,
        [Display(Name = "pinterest-square")]
        fa_pinterest_square,
        [Display(Name = "qq")]
        fa_qq,
        [Display(Name = "ra")]
        fa_ra,
        [Display(Name = "rebel")]
        fa_rebel,
        [Display(Name = "reddit")]
        fa_reddit,
        [Display(Name = "reddit-square")]
        fa_reddit_square,
        [Display(Name = "renren")]
        fa_renren,
        [Display(Name = "safari")]
        fa_safari,
        [Display(Name = "sellsy")]
        fa_sellsy,
        [Display(Name = "share-alt")]
        fa_share_alt,
        [Display(Name = "share-alt-square")]
        fa_share_alt_square,
        [Display(Name = "shirtsinbulk")]
        fa_shirtsinbulk,
        [Display(Name = "simplybuilt")]
        fa_simplybuilt,
        [Display(Name = "skyatlas")]
        fa_skyatlas,
        [Display(Name = "skype")]
        fa_skype,
        [Display(Name = "slack")]
        fa_slack,
        [Display(Name = "slideshare")]
        fa_slideshare,
        [Display(Name = "soundcloud")]
        fa_soundcloud,
        [Display(Name = "spotify")]
        fa_spotify,
        [Display(Name = "stack-exchange")]
        fa_stack_exchange,
        [Display(Name = "stack-overflow")]
        fa_stack_overflow,
        [Display(Name = "steam")]
        fa_steam,
        [Display(Name = "steam-square")]
        fa_steam_square,
        [Display(Name = "stumbleupon")]
        fa_stumbleupon,
        [Display(Name = "stumbleupon-circle")]
        fa_stumbleupon_circle,
        [Display(Name = "tencent-weibo")]
        fa_tencent_weibo,
        [Display(Name = "trello")]
        fa_trello,
        [Display(Name = "tripadvisor")]
        fa_tripadvisor,
        [Display(Name = "tumblr")]
        fa_tumblr,
        [Display(Name = "tumblr-square")]
        fa_tumblr_square,
        [Display(Name = "twitch")]
        fa_twitch,
        [Display(Name = "twitter")]
        fa_twitter,
        [Display(Name = "twitter-square")]
        fa_twitter_square,
        [Display(Name = "viacoin")]
        fa_viacoin,
        [Display(Name = "vimeo")]
        fa_vimeo,
        [Display(Name = "vimeo-square")]
        fa_vimeo_square,
        [Display(Name = "vine")]
        fa_vine,
        [Display(Name = "vk")]
        fa_vk,
        [Display(Name = "wechat")]
        fa_wechat,
        [Display(Name = "weibo")]
        fa_weibo,
        [Display(Name = "weixin")]
        fa_weixin,
        [Display(Name = "whatsapp")]
        fa_whatsapp,
        [Display(Name = "wikipedia-w")]
        fa_wikipedia_w,
        [Display(Name = "windows")]
        fa_windows,
        [Display(Name = "wordpress")]
        fa_wordpress,
        [Display(Name = "xing")]
        fa_xing,
        [Display(Name = "xing-square")]
        fa_xing_square,
        [Display(Name = "y-combinator")]
        fa_y_combinator,
        [Display(Name = "yahoo")]
        fa_yahoo,
        [Display(Name = "yelp")]
        fa_yelp,
        [Display(Name = "yc")]
        fa_yc,
        [Display(Name = "youtube")]
        fa_youtube,
        [Display(Name = "youtube-play")]
        fa_youtube_play,
        [Display(Name = "youtube-square")]
        fa_youtube_square,
        [Display(Name = "area-chart")]
        fa_area_chart,
        [Display(Name = "bar-chart")]
        fa_bar_chart,
        [Display(Name = "line-chart")]
        fa_line_chart,
        [Display(Name = "pie-chart")]
        fa_pie_chart,
        [Display(Name = "cny")]
        fa_cny,
        [Display(Name = "dollar")]
        fa_dollar,
        [Display(Name = "eur")]
        fa_eur,
        [Display(Name = "euro")]
        fa_euro,
        [Display(Name = "gbp")]
        fa_gbp,
        [Display(Name = "ils")]
        fa_ils,
        [Display(Name = "inr")]
        fa_inr,
        [Display(Name = "jpy")]
        fa_jpy,
        [Display(Name = "krw")]
        fa_krw,
        [Display(Name = "money")]
        fa_money,
        [Display(Name = "rmb")]
        fa_rmb,
        [Display(Name = "rouble")]
        fa_rouble,
        [Display(Name = "rub")]
        fa_rub,
        [Display(Name = "rupee")]
        fa_rupee,
        [Display(Name = "shekel")]
        fa_shekel,
        [Display(Name = "sheqel")]
        fa_sheqel,
        [Display(Name = "try")]
        fa_try,
        [Display(Name = "turkish-lira")]
        fa_turkish_lira,
        [Display(Name = "usd")]
        fa_usd,
        [Display(Name = "won")]
        fa_won,
        [Display(Name = "yen")]
        fa_yen,
        [Display(Name = "angle-double-down")]
        fa_angle_double_down,
        [Display(Name = "angle-double-left")]
        fa_angle_double_left,
        [Display(Name = "angle-double-right")]
        fa_angle_double_right,
        [Display(Name = "angle-double-up")]
        fa_angle_double_up,
        [Display(Name = "angle-down")]
        fa_angle_down,
        [Display(Name = "angle-left")]
        fa_angle_left,
        [Display(Name = "angle-right")]
        fa_angle_right,
        [Display(Name = "angle-up")]
        fa_angle_up,
        [Display(Name = "arrow-circle-down")]
        fa_arrow_circle_down,
        [Display(Name = "arrow-circle-left")]
        fa_arrow_circle_left,
        [Display(Name = "arrow-circle-right")]
        fa_arrow_circle_right,
        [Display(Name = "arrow-circle-up")]
        fa_arrow_circle_up,
        [Display(Name = "arrow-circle-o-down")]
        fa_arrow_circle_o_down,
        [Display(Name = "arrow-circle-o-left")]
        fa_arrow_circle_o_left,
        [Display(Name = "arrow-circle-o-right")]
        fa_arrow_circle_o_right,
        [Display(Name = "arrow-circle-o-up")]
        fa_arrow_circle_o_up,
        [Display(Name = "arrow-down")]
        fa_arrow_down,
        [Display(Name = "arrow-left")]
        fa_arrow_left,
        [Display(Name = "arrow-right")]
        fa_arrow_right,
        [Display(Name = "arrow-up")]
        fa_arrow_up,
        [Display(Name = "arrows")]
        fa_arrows,
        [Display(Name = "arrows-alt")]
        fa_arrows_alt,
        [Display(Name = "arrows-h")]
        fa_arrows_h,
        [Display(Name = "arrows-v")]
        fa_arrows_v,
        [Display(Name = "caret-down")]
        fa_caret_down,
        [Display(Name = "caret-left")]
        fa_caret_left,
        [Display(Name = "caret-right")]
        fa_caret_right,
        [Display(Name = "caret-up")]
        fa_caret_up,
        [Display(Name = "caret-square-o-down")]
        fa_caret_square_o_down,
        [Display(Name = "caret-square-o-left")]
        fa_caret_square_o_left,
        [Display(Name = "caret-square-o-right")]
        fa_caret_square_o_right,
        [Display(Name = "caret-square-o-up")]
        fa_caret_square_o_up,
        [Display(Name = "chevron-circle-down")]
        fa_chevron_circle_down,
        [Display(Name = "chevron-circle-left")]
        fa_chevron_circle_left,
        [Display(Name = "chevron-circle-right")]
        fa_chevron_circle_right,
        [Display(Name = "chevron-circle-up")]
        fa_chevron_circle_up,
        [Display(Name = "chevron-down")]
        fa_chevron_down,
        [Display(Name = "chevron-left")]
        fa_chevron_left,
        [Display(Name = "chevron-right")]
        fa_chevron_right,
        [Display(Name = "chevron-up")]
        fa_chevron_up,
        [Display(Name = "long-arrow-down")]
        fa_long_arrow_down,
        [Display(Name = "long-arrow-left")]
        fa_long_arrow_left,
        [Display(Name = "long-arrow-right")]
        fa_long_arrow_right,
        [Display(Name = "long-arrow-up")]
        fa_long_arrow_up,
        [Display(Name = "toggle-down")]
        fa_toggle_down,
        [Display(Name = "toggle-left")]
        fa_toggle_left,
        [Display(Name = "toggle-right")]
        fa_toggle_right,
        [Display(Name = "toggle-up")]
        fa_toggle_up,
        [Display(Name = "file")]
        fa_file,
        [Display(Name = "file-archive-o")]
        fa_file_archive_o,
        [Display(Name = "file-audio-o")]
        fa_file_audio_o,
        [Display(Name = "file-code-o")]
        fa_file_code_o,
        [Display(Name = "file-excel-o")]
        fa_file_excel_o,
        [Display(Name = "file-image-o")]
        fa_file_image_o,
        [Display(Name = "file-movie-o")]
        fa_file_movie_o,
        [Display(Name = "file-o")]
        fa_file_o,
        [Display(Name = "file-pdf-o")]
        fa_file_pdf_o,
        [Display(Name = "file-photo-o")]
        fa_file_photo_o,
        [Display(Name = "file-picture-o")]
        fa_file_picture_o,
        [Display(Name = "file-powerpoint-o")]
        fa_file_powerpoint_o,
        [Display(Name = "file-text")]
        fa_file_text,
        [Display(Name = "file-text-o")]
        fa_file_text_o,
        [Display(Name = "file-video-o")]
        fa_file_video_o,
        [Display(Name = "file-word-o")]
        fa_file_word_o,
        [Display(Name = "file-zip-o")]
        fa_file_zip_o,
        [Display(Name = "check-square")]
        fa_check_square,
        [Display(Name = "check-square-o")]
        fa_check_square_o,
        [Display(Name = "circle")]
        fa_circle,
        [Display(Name = "circle-o")]
        fa_circle_o,
        [Display(Name = "dot-circle-o")]
        fa_dot_circle_o,
        [Display(Name = "minus-square")]
        fa_minus_square,
        [Display(Name = "minus-square-o")]
        fa_minus_square_o,
        [Display(Name = "plus-square")]
        fa_plus_square,
        [Display(Name = "plus-square-o")]
        fa_plus_square_o,
        [Display(Name = "square")]
        fa_square,
        [Display(Name = "square-o")]
        fa_square_o,
        [Display(Name = "genderless")]
        fa_genderless,
        [Display(Name = "mars")]
        fa_mars,
        [Display(Name = "mars-double")]
        fa_mars_double,
        [Display(Name = "mars-stroke")]
        fa_mars_stroke,
        [Display(Name = "mars-stroke-h")]
        fa_mars_stroke_h,
        [Display(Name = "mars-stroke-v")]
        fa_mars_stroke_v,
        [Display(Name = "mercury")]
        fa_mercury,
        [Display(Name = "neuter")]
        fa_neuter,
        [Display(Name = "transgender")]
        fa_transgender,
        [Display(Name = "transgender-alt")]
        fa_transgender_alt,
        [Display(Name = "venus")]
        fa_venus,
        [Display(Name = "venus-double")]
        fa_venus_double,
        [Display(Name = "venus-mars")]
        fa_venus_mars,
        [Display(Name = "hand-grab-o")]
        fa_hand_grab_o,
        [Display(Name = "hand-lizard-o")]
        fa_hand_lizard_o,
        [Display(Name = "hand-o-down")]
        fa_hand_o_down,
        [Display(Name = "hand-o-left")]
        fa_hand_o_left,
        [Display(Name = "hand-o-right")]
        fa_hand_o_right,
        [Display(Name = "hand-o-up")]
        fa_hand_o_up,
        [Display(Name = "hand-paper-o")]
        fa_hand_paper_o,
        [Display(Name = "hand-peace-o")]
        fa_hand_peace_o,
        [Display(Name = "hand-pointer-o")]
        fa_hand_pointer_o,
        [Display(Name = "rocket")]
        fa_rocket,
        [Display(Name = "hand-scissors-o")]
        fa_hand_scissors_o,
        [Display(Name = "hand-spock-o")]
        fa_hand_spock_o,
        [Display(Name = "hand-stop-o")]
        fa_hand_stop_o,
        [Display(Name = "thumbs-down")]
        fa_thumbs_down,
        [Display(Name = "thumbs-o-down")]
        fa_thumbs_o_down,
        [Display(Name = "thumbs-o-up")]
        fa_thumbs_o_up,
        [Display(Name = "thumbs-up")]
        fa_thumbs_up,
        [Display(Name = "ambulance")]
        fa_ambulance,
        [Display(Name = "h-square")]
        fa_h_square,
        [Display(Name = "heart")]
        fa_heart,
        [Display(Name = "heart-o")]
        fa_heart_o,
        [Display(Name = "heartbeat")]
        fa_heartbeat,
        [Display(Name = "hospital-o")]
        fa_hospital_o,
        [Display(Name = "medkit")]
        fa_medkit,
        [Display(Name = "stethoscope")]
        fa_stethoscope,
        [Display(Name = "user-md")]
        fa_user_md,
        [Display(Name = "cc-discover")]
        fa_cc_discover,
        [Display(Name = "cc-jcb")]
        fa_cc_jcb,
        [Display(Name = "google-wallet")]
        fa_google_wallet,
        [Display(Name = "align-center")]
        fa_align_center,
        [Display(Name = "align-justify")]
        fa_align_justify,
        [Display(Name = "align-left")]
        fa_align_left,
        [Display(Name = "align-right")]
        fa_align_right,
        [Display(Name = "bold")]
        fa_bold,
        [Display(Name = "chain")]
        fa_chain,
        [Display(Name = "chain-broken")]
        fa_chain_broken,
        [Display(Name = "clipboard")]
        fa_clipboard,
        [Display(Name = "columns")]
        fa_columns,
        [Display(Name = "copy")]
        fa_copy,
        [Display(Name = "cut")]
        fa_cut,
        [Display(Name = "dedent")]
        fa_dedent,
        [Display(Name = "files-o")]
        fa_files_o,
        [Display(Name = "floppy-o")]
        fa_floppy_o,
        [Display(Name = "font")]
        fa_font,
        [Display(Name = "header")]
        fa_header,
        [Display(Name = "indent")]
        fa_indent,
        [Display(Name = "italic")]
        fa_italic,
        [Display(Name = "link")]
        fa_link,
        [Display(Name = "list")]
        fa_list,
        [Display(Name = "list-alt")]
        fa_list_alt,
        [Display(Name = "list-ol")]
        fa_list_ol,
        [Display(Name = "list-ul")]
        fa_list_ul,
        [Display(Name = "outdent")]
        fa_outdent,
        [Display(Name = "paperclip")]
        fa_paperclip,
        [Display(Name = "paragraph")]
        fa_paragraph,
        [Display(Name = "paste")]
        fa_paste,
        [Display(Name = "repeat")]
        fa_repeat,
        [Display(Name = "rotate-left")]
        fa_rotate_left,
        [Display(Name = "rotate-right")]
        fa_rotate_right,
        [Display(Name = "save")]
        fa_save,
        [Display(Name = "scissors")]
        fa_scissors,
        [Display(Name = "strikethrough")]
        fa_strikethrough,
        [Display(Name = "subscript")]
        fa_subscript,
        [Display(Name = "superscript")]
        fa_superscript,
        [Display(Name = "table")]
        fa_table,
        [Display(Name = "text-height")]
        fa_text_height,
        [Display(Name = "text-width")]
        fa_text_width,
        [Display(Name = "th")]
        fa_th,
        [Display(Name = "th-large")]
        fa_th_large,
        [Display(Name = "th-list")]
        fa_th_list,
        [Display(Name = "underline")]
        fa_underline,
        [Display(Name = "undo")]
        fa_undo,
        [Display(Name = "unlink")]
        fa_unlink,
        [Display(Name = "subway")]
        fa_subway,
        [Display(Name = "train")]
        fa_train,
        [Display(Name = "backward")]
        fa_backward,
        [Display(Name = "compress")]
        fa_compress,
        [Display(Name = "eject")]
        fa_eject,
        [Display(Name = "expand")]
        fa_expand,
        [Display(Name = "fast-backward")]
        fa_fast_backward,
        [Display(Name = "fast-forward")]
        fa_fast_forward,
        [Display(Name = "forward")]
        fa_forward,
        [Display(Name = "pause")]
        fa_pause,
        [Display(Name = "play")]
        fa_play,
        [Display(Name = "play-circle")]
        fa_play_circle,
        [Display(Name = "play-circle-o")]
        fa_play_circle_o,
        [Display(Name = "step-backward")]
        fa_step_backward,
        [Display(Name = "step-forward")]
        fa_step_forward,
        [Display(Name = "stop")]
        fa_stop,
        [Display(Name = "adjust")]
        fa_adjust,
        [Display(Name = "anchor")]
        fa_anchor,
        [Display(Name = "archive")]
        fa_archive,
        [Display(Name = "asterisk")]
        fa_asterisk,
        [Display(Name = "at")]
        fa_at,
        [Display(Name = "automobile")]
        fa_automobile,
        [Display(Name = "balance-scale")]
        fa_balance_scale,
        [Display(Name = "ban")]
        fa_ban,
        [Display(Name = "bank")]
        fa_bank,
        [Display(Name = "bar-chart-o")]
        fa_bar_chart_o,
        [Display(Name = "barcode")]
        fa_barcode,
        [Display(Name = "bars")]
        fa_bars,
        [Display(Name = "battery-0")]
        fa_battery_0,
        [Display(Name = "battery-1")]
        fa_battery_1,
        [Display(Name = "battery-2")]
        fa_battery_2,
        [Display(Name = "battery-3")]
        fa_battery_3,
        [Display(Name = "battery-4")]
        fa_battery_4,
        [Display(Name = "battery-empty")]
        fa_battery_empty,
        [Display(Name = "battery-full")]
        fa_battery_full,
        [Display(Name = "battery-half")]
        fa_battery_half,
        [Display(Name = "battery-quarter")]
        fa_battery_quarter,
        [Display(Name = "battery-three-quarters")]
        fa_battery_three_quarters,
        [Display(Name = "bed")]
        fa_bed,
        [Display(Name = "beer")]
        fa_beer,
        [Display(Name = "bell")]
        fa_bell,
        [Display(Name = "bell-o")]
        fa_bell_o,
        [Display(Name = "bell-slash")]
        fa_bell_slash,
        [Display(Name = "bell-slash-o")]
        fa_bell_slash_o,
        [Display(Name = "bicycle")]
        fa_bicycle,
        [Display(Name = "binoculars")]
        fa_binoculars,
        [Display(Name = "birthday-cake")]
        fa_birthday_cake,
        [Display(Name = "bolt")]
        fa_bolt,
        [Display(Name = "bomb")]
        fa_bomb,
        [Display(Name = "book")]
        fa_book,
        [Display(Name = "bookmark")]
        fa_bookmark,
        [Display(Name = "bookmark-o")]
        fa_bookmark_o,
        [Display(Name = "briefcase")]
        fa_briefcase,
        [Display(Name = "bug")]
        fa_bug,
        [Display(Name = "building")]
        fa_building,
        [Display(Name = "building-o")]
        fa_building_o,
        [Display(Name = "bullhorn")]
        fa_bullhorn,
        [Display(Name = "bullseye")]
        fa_bullseye,
        [Display(Name = "bus")]
        fa_bus,
        [Display(Name = "cab")]
        fa_cab,
        [Display(Name = "calculator")]
        fa_calculator,
        [Display(Name = "calendar")]
        fa_calendar,
        [Display(Name = "calendar-o")]
        fa_calendar_o,
        [Display(Name = "calendar-check-o")]
        fa_calendar_check_o,
        [Display(Name = "calendar-minus-o")]
        fa_calendar_minus_o,
        [Display(Name = "calendar-plus-o")]
        fa_calendar_plus_o,
        [Display(Name = "calendar-times-o")]
        fa_calendar_times_o,
        [Display(Name = "camera")]
        fa_camera,
        [Display(Name = "camera-retro")]
        fa_camera_retro,
        [Display(Name = "car")]
        fa_car,
        [Display(Name = "cart-arrow-down")]
        fa_cart_arrow_down,
        [Display(Name = "cart-plus")]
        fa_cart_plus,
        [Display(Name = "cc")]
        fa_cc,
        [Display(Name = "certificate")]
        fa_certificate,
        [Display(Name = "check")]
        fa_check,
        [Display(Name = "check-circle")]
        fa_check_circle,
        [Display(Name = "check-circle-o")]
        fa_check_circle_o,
        [Display(Name = "child")]
        fa_child,
        [Display(Name = "circle-o-notch")]
        fa_circle_o_notch,
        [Display(Name = "circle-thin")]
        fa_circle_thin,
        [Display(Name = "clock-o")]
        fa_clock_o,
        [Display(Name = "clone")]
        fa_clone,
        [Display(Name = "close")]
        fa_close,
        [Display(Name = "cloud")]
        fa_cloud,
        [Display(Name = "cloud-download")]
        fa_cloud_download,
        [Display(Name = "cloud-upload")]
        fa_cloud_upload,
        [Display(Name = "code")]
        fa_code,
        [Display(Name = "code-fork")]
        fa_code_fork,
        [Display(Name = "coffee")]
        fa_coffee,
        [Display(Name = "cog")]
        fa_cog,
        [Display(Name = "cogs")]
        fa_cogs,
        [Display(Name = "comment")]
        fa_comment,
        [Display(Name = "comment-o")]
        fa_comment_o,
        [Display(Name = "comments")]
        fa_comments,
        [Display(Name = "comments-o")]
        fa_comments_o,
        [Display(Name = "commenting")]
        fa_commenting,
        [Display(Name = "commenting-o")]
        fa_commenting_o,
        [Display(Name = "compass")]
        fa_compass,
        [Display(Name = "copyright")]
        fa_copyright,
        [Display(Name = "credit-card")]
        fa_credit_card,
        [Display(Name = "creative-commons")]
        fa_creative_commons,
        [Display(Name = "crop")]
        fa_crop,
        [Display(Name = "crosshairs")]
        fa_crosshairs,
        [Display(Name = "cube")]
        fa_cube,
        [Display(Name = "cubes")]
        fa_cubes,
        [Display(Name = "cutlery")]
        fa_cutlery,
        [Display(Name = "dashboard")]
        fa_dashboard,
        [Display(Name = "database")]
        fa_database,
        [Display(Name = "desktop")]
        fa_desktop,
        [Display(Name = "diamond")]
        fa_diamond,
        [Display(Name = "download")]
        fa_download,
        [Display(Name = "edit")]
        fa_edit,
        [Display(Name = "ellipsis-h")]
        fa_ellipsis_h,
        [Display(Name = "ellipsis-v")]
        fa_ellipsis_v,
        [Display(Name = "envelope")]
        fa_envelope,
        [Display(Name = "envelope-o")]
        fa_envelope_o,
        [Display(Name = "envelope-square")]
        fa_envelope_square,
        [Display(Name = "eraser")]
        fa_eraser,
        [Display(Name = "exchange")]
        fa_exchange,
        [Display(Name = "exclamation")]
        fa_exclamation,
        [Display(Name = "exclamation-circle")]
        fa_exclamation_circle,
        [Display(Name = "exclamation-triangle")]
        fa_exclamation_triangle,
        [Display(Name = "external-link")]
        fa_external_link,
        [Display(Name = "external-link-square")]
        fa_external_link_square,
        [Display(Name = "eye")]
        fa_eye,
        [Display(Name = "eye-slash")]
        fa_eye_slash,
        [Display(Name = "eyedropper")]
        fa_eyedropper,
        [Display(Name = "fax")]
        fa_fax,
        [Display(Name = "female")]
        fa_female,
        [Display(Name = "fighter-jet")]
        fa_fighter_jet,
        [Display(Name = "file-sound-o")]
        fa_file_sound_o,
        [Display(Name = "film")]
        fa_film,
        [Display(Name = "filter")]
        fa_filter,
        [Display(Name = "fire")]
        fa_fire,
        [Display(Name = "fire-extinguisher")]
        fa_fire_extinguisher,
        [Display(Name = "flag")]
        fa_flag,
        [Display(Name = "flag-checkered")]
        fa_flag_checkered,
        [Display(Name = "flag-o")]
        fa_flag_o,
        [Display(Name = "flash")]
        fa_flash,
        [Display(Name = "flask")]
        fa_flask,
        [Display(Name = "folder")]
        fa_folder,
        [Display(Name = "folder-o")]
        fa_folder_o,
        [Display(Name = "folder-open")]
        fa_folder_open,
        [Display(Name = "folder-open-o")]
        fa_folder_open_o,
        [Display(Name = "frown-o")]
        fa_frown_o,
        [Display(Name = "futbol-o")]
        fa_futbol_o,
        [Display(Name = "gamepad")]
        fa_gamepad,
        [Display(Name = "gavel")]
        fa_gavel,
        [Display(Name = "gear")]
        fa_gear,
        [Display(Name = "gears")]
        fa_gears,
        [Display(Name = "gift")]
        fa_gift,
        [Display(Name = "glass")]
        fa_glass,
        [Display(Name = "globe")]
        fa_globe,
        [Display(Name = "graduation-cap")]
        fa_graduation_cap,
        [Display(Name = "group")]
        fa_group,
        [Display(Name = "hdd-o")]
        fa_hdd_o,
        [Display(Name = "headphones")]
        fa_headphones,
        [Display(Name = "history")]
        fa_history,
        [Display(Name = "home")]
        fa_home,
        [Display(Name = "hotel")]
        fa_hotel,
        [Display(Name = "hourglass")]
        fa_hourglass,
        [Display(Name = "hourglass-1")]
        fa_hourglass_1,
        [Display(Name = "hourglass-2")]
        fa_hourglass_2,
        [Display(Name = "hourglass-3")]
        fa_hourglass_3,
        [Display(Name = "hourglass-end")]
        fa_hourglass_end,
        [Display(Name = "hourglass-half")]
        fa_hourglass_half,
        [Display(Name = "hourglass-o")]
        fa_hourglass_o,
        [Display(Name = "hourglass-start")]
        fa_hourglass_start,
        [Display(Name = "i-cursor")]
        fa_i_cursor,
        [Display(Name = "image")]
        fa_image,
        [Display(Name = "inbox")]
        fa_inbox,
        [Display(Name = "industry")]
        fa_industry,
        [Display(Name = "info")]
        fa_info,
        [Display(Name = "info-circle")]
        fa_info_circle,
        [Display(Name = "institution")]
        fa_institution,
        [Display(Name = "key")]
        fa_key,
        [Display(Name = "keyboard-o")]
        fa_keyboard_o,
        [Display(Name = "language")]
        fa_language,
        [Display(Name = "laptop")]
        fa_laptop,
        [Display(Name = "leaf")]
        fa_leaf,
        [Display(Name = "legal")]
        fa_legal,
        [Display(Name = "lemon-o")]
        fa_lemon_o,
        [Display(Name = "level-down")]
        fa_level_down,
        [Display(Name = "level-up")]
        fa_level_up,
        [Display(Name = "life-bouy")]
        fa_life_bouy,
        [Display(Name = "life-buoy")]
        fa_life_buoy,
        [Display(Name = "life-ring")]
        fa_life_ring,
        [Display(Name = "life-saver")]
        fa_life_saver,
        [Display(Name = "lightbulb-o")]
        fa_lightbulb_o,
        [Display(Name = "location-arrow")]
        fa_location_arrow,
        [Display(Name = "lock")]
        fa_lock,
        [Display(Name = "magic")]
        fa_magic,
        [Display(Name = "magnet")]
        fa_magnet,
        [Display(Name = "mail-forward")]
        fa_mail_forward,
        [Display(Name = "mail-reply")]
        fa_mail_reply,
        [Display(Name = "mail-reply-all")]
        fa_mail_reply_all,
        [Display(Name = "male")]
        fa_male,
        [Display(Name = "map")]
        fa_map,
        [Display(Name = "map-o")]
        fa_map_o,
        [Display(Name = "map-pin")]
        fa_map_pin,
        [Display(Name = "map-signs")]
        fa_map_signs,
        [Display(Name = "map-marker")]
        fa_map_marker,
        [Display(Name = "meh-o")]
        fa_meh_o,
        [Display(Name = "microphone")]
        fa_microphone,
        [Display(Name = "microphone-slash")]
        fa_microphone_slash,
        [Display(Name = "minus")]
        fa_minus,
        [Display(Name = "minus-circle")]
        fa_minus_circle,
        [Display(Name = "mobile")]
        fa_mobile,
        [Display(Name = "mobile-phone")]
        fa_mobile_phone,
        [Display(Name = "moon-o")]
        fa_moon_o,
        [Display(Name = "mortar-board")]
        fa_mortar_board,
        [Display(Name = "motorcycle")]
        fa_motorcycle,
        [Display(Name = "mouse-pointer")]
        fa_mouse_pointer,
        [Display(Name = "music")]
        fa_music,
        [Display(Name = "navicon")]
        fa_navicon,
        [Display(Name = "newspaper-o")]
        fa_newspaper_o,
        [Display(Name = "object-group")]
        fa_object_group,
        [Display(Name = "object-ungroup")]
        fa_object_ungroup,
        [Display(Name = "paint-brush")]
        fa_paint_brush,
        [Display(Name = "paper-plane")]
        fa_paper_plane,
        [Display(Name = "paper-plane-o")]
        fa_paper_plane_o,
        [Display(Name = "paw")]
        fa_paw,
        [Display(Name = "pencil")]
        fa_pencil,
        [Display(Name = "pencil-square")]
        fa_pencil_square,
        [Display(Name = "pencil-square-o")]
        fa_pencil_square_o,
        [Display(Name = "phone")]
        fa_phone,
        [Display(Name = "phone-square")]
        fa_phone_square,
        [Display(Name = "photo")]
        fa_photo,
        [Display(Name = "picture-o")]
        fa_picture_o,
        [Display(Name = "plane")]
        fa_plane,
        [Display(Name = "plug")]
        fa_plug,
        [Display(Name = "plus")]
        fa_plus,
        [Display(Name = "plus-circle")]
        fa_plus_circle,
        [Display(Name = "power-off")]
        fa_power_off,
        [Display(Name = "print")]
        fa_print,
        [Display(Name = "puzzle-piece")]
        fa_puzzle_piece,
        [Display(Name = "qrcode")]
        fa_qrcode,
        [Display(Name = "question")]
        fa_question,
        [Display(Name = "question-circle")]
        fa_question_circle,
        [Display(Name = "quote-left")]
        fa_quote_left,
        [Display(Name = "quote-right")]
        fa_quote_right,
        [Display(Name = "random")]
        fa_random,
        [Display(Name = "recycle")]
        fa_recycle,
        [Display(Name = "refresh")]
        fa_refresh,
        [Display(Name = "registered")]
        fa_registered,
        [Display(Name = "remove")]
        fa_remove,
        [Display(Name = "reorder")]
        fa_reorder,
        [Display(Name = "reply")]
        fa_reply,
        [Display(Name = "reply-all")]
        fa_reply_all,
        [Display(Name = "retweet")]
        fa_retweet,
        [Display(Name = "road")]
        fa_road,
        [Display(Name = "rss")]
        fa_rss,
        [Display(Name = "rss-square")]
        fa_rss_square,
        [Display(Name = "search")]
        fa_search,
        [Display(Name = "search-minus")]
        fa_search_minus,
        [Display(Name = "search-plus")]
        fa_search_plus,
        [Display(Name = "send")]
        fa_send,
        [Display(Name = "send-o")]
        fa_send_o,
        [Display(Name = "server")]
        fa_server,
        [Display(Name = "share")]
        fa_share,
        [Display(Name = "share-square")]
        fa_share_square,
        [Display(Name = "share-square-o")]
        fa_share_square_o,
        [Display(Name = "shield")]
        fa_shield,
        [Display(Name = "ship")]
        fa_ship,
        [Display(Name = "shopping-cart")]
        fa_shopping_cart,
        [Display(Name = "sign-in")]
        fa_sign_in,
        [Display(Name = "sign-out")]
        fa_sign_out,
        [Display(Name = "signal")]
        fa_signal,
        [Display(Name = "sitemap")]
        fa_sitemap,
        [Display(Name = "sliders")]
        fa_sliders,
        [Display(Name = "smile-o")]
        fa_smile_o,
        [Display(Name = "soccer-ball-o")]
        fa_soccer_ball_o,
        [Display(Name = "sort")]
        fa_sort,
        [Display(Name = "sort-alpha-asc")]
        fa_sort_alpha_asc,
        [Display(Name = "sort-alpha-desc")]
        fa_sort_alpha_desc,
        [Display(Name = "sort-amount-asc")]
        fa_sort_amount_asc,
        [Display(Name = "sort-amount-desc")]
        fa_sort_amount_desc,
        [Display(Name = "sort-asc")]
        fa_sort_asc,
        [Display(Name = "sort-desc")]
        fa_sort_desc,
        [Display(Name = "sort-down")]
        fa_sort_down,
        [Display(Name = "sort-numeric-asc")]
        fa_sort_numeric_asc,
        [Display(Name = "sort-numeric-desc")]
        fa_sort_numeric_desc,
        [Display(Name = "sort-up")]
        fa_sort_up,
        [Display(Name = "space-shuttle")]
        fa_space_shuttle,
        [Display(Name = "spinner")]
        fa_spinner,
        [Display(Name = "spoon")]
        fa_spoon,
        [Display(Name = "star")]
        fa_star,
        [Display(Name = "star-half")]
        fa_star_half,
        [Display(Name = "star-half-empty")]
        fa_star_half_empty,
        [Display(Name = "star-half-full")]
        fa_star_half_full,
        [Display(Name = "star-half-o")]
        fa_star_half_o,
        [Display(Name = "star-o")]
        fa_star_o,
        [Display(Name = "sticky-note")]
        fa_sticky_note,
        [Display(Name = "sticky-note-o")]
        fa_sticky_note_o,
        [Display(Name = "street-view")]
        fa_street_view,
        [Display(Name = "suitcase")]
        fa_suitcase,
        [Display(Name = "sun-o")]
        fa_sun_o,
        [Display(Name = "support")]
        fa_support,
        [Display(Name = "tablet")]
        fa_tablet,
        [Display(Name = "tachometer")]
        fa_tachometer,
        [Display(Name = "tag")]
        fa_tag,
        [Display(Name = "tags")]
        fa_tags,
        [Display(Name = "tasks")]
        fa_tasks,
        [Display(Name = "taxi")]
        fa_taxi,
        [Display(Name = "television")]
        fa_television,
        [Display(Name = "terminal")]
        fa_terminal,
        [Display(Name = "thumb-tack")]
        fa_thumb_tack,
        [Display(Name = "ticket")]
        fa_ticket,
        [Display(Name = "times")]
        fa_times,
        [Display(Name = "times-circle")]
        fa_times_circle,
        [Display(Name = "times-circle-o")]
        fa_times_circle_o,
        [Display(Name = "tint")]
        fa_tint,
        [Display(Name = "toggle-off")]
        fa_toggle_off,
        [Display(Name = "toggle-on")]
        fa_toggle_on,
        [Display(Name = "trademark")]
        fa_trademark,
        [Display(Name = "trash")]
        fa_trash,
        [Display(Name = "trash-o")]
        fa_trash_o,
        [Display(Name = "tree")]
        fa_tree,
        [Display(Name = "trophy")]
        fa_trophy,
        [Display(Name = "truck")]
        fa_truck,
        [Display(Name = "tty")]
        fa_tty,
        [Display(Name = "tv")]
        fa_tv,
        [Display(Name = "umbrella")]
        fa_umbrella,
        [Display(Name = "university")]
        fa_university,
        [Display(Name = "unlock")]
        fa_unlock,
        [Display(Name = "unlock-alt")]
        fa_unlock_alt,
        [Display(Name = "unsorted")]
        fa_unsorted,
        [Display(Name = "upload")]
        fa_upload,
        [Display(Name = "user")]
        fa_user,
        [Display(Name = "user-plus")]
        fa_user_plus,
        [Display(Name = "user-secret")]
        fa_user_secret,
        [Display(Name = "user-times")]
        fa_user_times,
        [Display(Name = "users")]
        fa_users,
        [Display(Name = "video-camera")]
        fa_video_camera,
        [Display(Name = "volume-down")]
        fa_volume_down,
        [Display(Name = "volume-off")]
        fa_volume_off,
        [Display(Name = "volume-up")]
        fa_volume_up,
        [Display(Name = "warning")]
        fa_warning,
        [Display(Name = "wheelchair")]
        fa_wheelchair,
        [Display(Name = "wifi")]
        fa_wifi,
        [Display(Name = "wrench")]
        fa_wrench,
        [Display(Name = "handshake-o")]
        fa_handshake_o,
        [Display(Name = "blind")]
        fa_blind
    }
}
