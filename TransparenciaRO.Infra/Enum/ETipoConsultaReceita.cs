﻿namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoConsultaReceita
    {
        Total = 0,
        Liquida = 1,
        Corrente = 2,
        Capital = 3,
        IntraOrcamentaria = 4
    }
}
