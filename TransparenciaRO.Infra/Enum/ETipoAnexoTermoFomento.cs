﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoAnexoTermoFomento
    {
       
        [Description("Relatorio Técnico de Monitoramento"), Display(Name = "Relatorio Técnico de Monitoramento")]
        Monitoramento = 1,
        [Description("Relatório Integra dos Termos"), Display(Name = "Relatório Integra dos Termos")]
        Termos = 2,
        [Description("Relatório Relatório Prestação de Contas"), Display(Name = "Relatório Relatório Prestação de Contas")]
        Prestacao_Contas = 3

    }
}
