﻿
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    //Aki tu adiciona a role
    public enum EPerfilUsuario
    {
        [Display(Name = "Administrador")]
        AdministradorUsuarios,

        [Display(Name = "Administrar de Notícias")]
        AdministradorNoticias,

        [Display(Name = "Administrador Somente Leitura")]
        AdministradorReadOnly,

        [Display(Name = "Precisa Trocar a Senha")]
        PrecisaTrocarSenha,

        [Display(Name = "Usuário Autenticado")]
        AreaAdministrativa,

        [Display(Name = "Administrador de Contratos e Convênios")]
        AdministradorContratosConvenios,

        [Display(Name = "Administrador de Fornecedores e Impedimentos")]
        AdministradorFornecedoresImpedimentos,

        [Display(Name = "Menu Topo")]
        MenuTopo,

        [Display(Name = "Administrador de Pastas")]
        AdministradorPastas,

        [Display(Name = "Administrador de Relação Servidor")]
        AdministradorRelacaoServidor,

        [Display(Name = "Administrador de Renumeração Servidor")]
        AdministradorRenumeracaoServidor,

        [Display(Name = "Visualizador de Log")]
        VisualizadorLog,

        [Display(Name = "Administrador de Licitações")]
        AdministradorLicitacoes,

        [Display(Name = "Visualização Completa/Debug de Despesas")]
        VisualizacaoCompletaDespesas,

        [Display(Name = "Administrador de Despesas e Receitas - Soph")]
        AdministradorDespesasReceitasSoph,

        [Display(Name = "Administrador de Despesas e Receitas - CAERD")]
        AdministradorDespesasReceitasCaerd,

        [Display(Name = "Administrador de Despesas e Receitas - CRM")]
        AdministradorDespesasReceitasCrm,

        [Display(Name = "Administrador de Remuneração de Servidores Empresa - SOPH ")]
        AdministradorRemuneracaoServidoresEmpresaSoph,

        [Display(Name = "Administrador de Remuneração de Servidores Empresa - CMR ")]
        AdministradorRemuneracaoServidoresEmpresaCmr,

        [Display(Name = "Administrador de Remuneração de Servidores Empresa - CAERD ")]
        AdministradorRemuneracaoServidoresEmpresaCaerd,

        [Display(Name = "Administrador de Portaria e Resolução Normativa")]
        AdministradorPortariaResolucaoNormativa,

        [Display(Name = "Administrador de Controle Cidadão")]
        AdministradorFiscalCidadao,

        [Display(Name = "Administrador de Documentos Externo")]
        AdministradorControleDocumentoExterno,

        [Display(Name = "Administrador de Remuneração de Servidores Empresa - EMATER")]
        AdministradorRemuneracaoServidoresEmpresaEmater,

        [Display(Name = "Administrador de Pagamento de Fornecedores RPV")]
        AdministradorPagamentoFornecedoresRpv,

        [Display(Name = "Administrador de Fundo Fixo - SOPH ")]
        AdministradorFundoFixoEmpresaSoph,

        [Display(Name = "Administrador de Fundo Fixo Empresa - CMR ")]
        AdministradorFundoFixoEmpresaCmr,

        [Display(Name = "Administrador de Fundo Fixo Empresa - CAERD ")]
        AdministradorFundoFixoEmpresaCaerd,
        [Display(Name = "Administrador do Dialog do Transparência ")]
        AdministradorDialog,

        [Display(Name = "Administrador do Boletim de Controle Interno ")]
        AdministradorBoletimControleInterno
    }
}