﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum EFusoHorario
    {
        [Display(Name = "UTC −2 (Horário de Fernando de Noronha)")]
        UTC2,

        [Display(Name = "UTC −3 (Horário de Brasília)")]
        UTC3,

        [Display(Name = "UTC −4 (Horário da Amazônia)")]
        UTC4,

        [Display(Name = "UTC −5 (Horário do Acre)")]
        UTC5
    }
}
