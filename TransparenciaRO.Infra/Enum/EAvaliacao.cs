﻿namespace TransparenciaRO.Infra.Enum
{
    public enum EAvaliacao
    {
        Otimo,
        Bom,
        Indiferente,
        Ruim,
        Pessima
    }
}