﻿namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoConsolidacao
    {
        PorAno = 0,                
        PorMes = 1,
        PorQuadrimestre = 2
    }
}
