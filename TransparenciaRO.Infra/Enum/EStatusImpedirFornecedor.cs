﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum EStatusImpedirFornecedor
    {
        Impedido = 1,
        Apto = 2
    }
}