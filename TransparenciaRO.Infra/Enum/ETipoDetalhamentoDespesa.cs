﻿namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoDetalhamentoDespesa
    {
        Despesas,
        DespesasMeses,
        DespesasProgramaAcao,
        DespesasFuncoes,
        DespesasNaturezas,
        DespesasEmpenhos
    }
}
