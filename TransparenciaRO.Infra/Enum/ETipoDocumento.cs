﻿namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoDocumento
    {
        Contrato = 1,
        Convenio = 2,
        ConvenioFederal = 3,
        Portaria = 4,
        Resolucao = 5,
        Decreto = 6,
        LeiComplementar = 7,
        InstrucaoNormativa = 8,
        CriacaoOrgao = 9,
        LeiOrdinaria = 10,
        TermoFomento = 11,
        ContratoEmergencialCOVID19 = 12,
        TermoDeCooperacao=13
    }

    public static class ETipoDocumentoHelper
    {
        public static string GetDescricao(this ETipoDocumento pTipoDocumento)
        {
            switch (pTipoDocumento)
            {
                case ETipoDocumento.Contrato:
                    return "Contrato";
                case ETipoDocumento.Convenio:
                    return "Convênio";
                case ETipoDocumento.ConvenioFederal:
                    return "Convênio Federal";
                case ETipoDocumento.Portaria:
                    return "Portaria";
                case ETipoDocumento.Resolucao:
                    return "Resolução";
                case ETipoDocumento.Decreto:
                    return "Decreto";
                case ETipoDocumento.LeiComplementar:
                    return "Lei Complementar";
                case ETipoDocumento.InstrucaoNormativa:
                    return "Instrução Normativa";
                case ETipoDocumento.CriacaoOrgao:
                    return "Criação do Órgão";
                case ETipoDocumento.LeiOrdinaria:
                    return "Lei Ordinária";
                case ETipoDocumento.TermoFomento:
                    return "Termos de Fomento";
                case ETipoDocumento.ContratoEmergencialCOVID19:
                    return "Contrato Emergencial COVID-19";
                case ETipoDocumento.TermoDeCooperacao:
                    return "Termos de Cooperação/Colaboração";
                default:
                    return "";
            }
        }
    }
}