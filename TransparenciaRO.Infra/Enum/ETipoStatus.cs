﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoStatus
    {
        EmAtraso,
        Expirando,
        Prorrogado,
        EmPrazo,
        EmAndamento,
        EmAberto,
        Finalizado
    }
    public static class ETipoStatusHelper
    {
        public static string GetDescricao(this ETipoStatus pTipoStatus)
        {
            switch (pTipoStatus)
            {
                case ETipoStatus.EmAberto:
                    return "Em Aberto";
                case ETipoStatus.EmAndamento:
                    return "Em Andamento";
                case ETipoStatus.EmAtraso:
                    return "Em Atraso";
                case ETipoStatus.Finalizado:
                    return "Finalizado";
                case ETipoStatus.Expirando:
                    return "Expirando";
                case ETipoStatus.EmPrazo:
                    return "Em Prazo";
                case ETipoStatus.Prorrogado:
                    return "Prorrogado";
                default:
                    return "";
            }
        }
    }
}
