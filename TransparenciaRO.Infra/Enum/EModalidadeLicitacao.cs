﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum EModalidadeLicitacao
    {

        //[Description("Concorrência Pública")]
        //[Display(Name = "Concorrência Pública")]
        //ConcorrenciaPublica = 1,

        //[Description("Tomada de Preços")]
        //[Display(Name = "Tomada de Preços")]
        //TomadaDePrecos = 2,

        //[Description("Convite")]
        //[Display(Name = "Convite")]
        //Convite = 3,

        //[Description("Concurso")]
        //[Display(Name = "Concurso")]
        //Concurso = 4,

        //[Description("Leilão")]
        //[Display(Name = "Leilão")]
        //Leilao = 5,

        [Description("Pregão Eletrônico")]
        [Display(Name = "Pregão Eletrônico")]
        PregaoEletronico = 6,

        //[Description("Pregão Presencial")]
        //[Display(Name = "Pregão Presencial")]
        //PregaoPresencial = 7,

        //[Description("Dispensa de Licitação")]
        //[Display(Name = "Dispensa de Licitação")]
        //DispensaDeLicitacao = 8,

        [Description("Chamamento Público")]
        [Display(Name = "Chamamento Público")]
        ChamamentoPublico = 9,

        [Description("Inexigibilidade")]
        [Display(Name = "Inexigibilidade")]
        ContratacaoDireta = 10,

        //[Description("RDC")]
        //[Display(Name = "RDC")]
        //RDC = 11,

        [Description("Pregão Eletrônico para Registro de Preços")]
        [Display(Name = "Pregão Eletrônico para Registro de Preços")]
        PregaoEletronicoParaRegistroDePrecos = 12,

        [Description("Dispensa / Contratação direta")]
        [Display(Name = "Dispensa / Contratação direta")]
        DispensaContratacaoDireta = 13
    }
}
