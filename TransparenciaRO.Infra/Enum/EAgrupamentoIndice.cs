﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Enum
{
    public enum EAgrupamentoIndice
    {
        Tela,
        Noticia,
        Empenho
    }

    public static class EAgrupamentoIndiceExtensions
    {
        public static string Descricao(this EAgrupamentoIndice pAgrupamento)
        {
            switch (pAgrupamento)
            {
                case EAgrupamentoIndice.Tela:
                    return "Telas";
                case EAgrupamentoIndice.Noticia:
                    return "Noticias";
                case EAgrupamentoIndice.Empenho:
                    return "Empenho";
                default:
                    return pAgrupamento.ToString() + "s";
            }
        }
    }
}
