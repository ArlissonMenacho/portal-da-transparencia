﻿using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum EStatusItemLicitacao
    {
        Entregue = 1,

        [Display(Name = "Em dia")]
        EmDia,

        [Display(Name = "Entrega parcial")]
        EntregaParcial,

        [Display(Name = "Atraso notificado")]
        AtrasoNotificado,

        [Display(Name = "Em atraso")]
        EmAtraso
    }

    public static class EStatusItemLicitacaoHelper
    {
        public static string GetDescription(this EStatusItemLicitacao statusItemLicitacao)
        {
            switch (statusItemLicitacao)
            {
                case EStatusItemLicitacao.Entregue: return "Entregue";
                case EStatusItemLicitacao.EmDia: return "Em dia";
                case EStatusItemLicitacao.EntregaParcial: return "Entrega parcial";
                case EStatusItemLicitacao.AtrasoNotificado: return "Atraso notificado";
                case EStatusItemLicitacao.EmAtraso: return "Em atraso";
                default: return "---";
            }
        }
    }
}
