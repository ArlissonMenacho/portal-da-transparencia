﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Enum
{
    public enum ETipoAnexo
    {
        #region Referente aos tipos de anexo do Comitê de Tranparência
        //[Description("Formulário de inscrição"), Display(Name = "Formulário de inscrição")]
        //FormularioInscricao = 1,

        //[Description("Formulário do projeto"), Display(Name = "Formulário do projeto")]
        //FormularioProjeto,

        //[Description("Cópia do CNPJ"), Display(Name = "Cópia do CNPJ")]
        //CopiaCnpj,

        //[Description("Documento constitucional da empresa"), Display(Name = "Documento constitucional da empresa")]
        //DocumentoConstituicionalEmpresa,
        #endregion

        #region Referente aos tipos de anexo de Dispensa de Licitações
        [Description("Edital")]
        Edital = 5,

        [Description("Extrato da publicação"), Display(Name = "Extrato da publicação")]
        ExtratoPublicacao,

        [Description("Homologação"), Display(Name = "Homologação")]
        Homologacao,

        //[Description("Nota de empenho"), Display(Name = "Nota de empenho")]
        //NotaEmpenho,

        [Description("Ordem de pagamento"), Display(Name = "Ordem de pagamento")]
        OrdemPagamento = 9,

        [Description("Termo de referência"), Display(Name = "Termo de referência")]
        TermoReferencia,

        [Description("Documento de liquidação"), Display(Name = "Documento de liquidação")]
        DocumentoLiquidacao,

        [Description("Aviso")]
        Aviso,

        [Description("Contrato")]
        Contrato,

        [Description("Recurso")]
        Recurso,

        [Description("Outros")]
        Outros,

        [Description("Íntegra do processo"), Display(Name = "Íntegra do processo")]
        IntegraProcesso,

        [Description("Termo aditivo"), Display(Name = "Termo aditivo")]
        TermoAditivo = 18,
        #endregion

        #region Referente aos tipos de anexo de FornecedorDispensa

        [Description("Contrato / Nota de empenho"), Display(Name = "Contrato / Nota de empenho")]
        InstrumentoContratual = 17

        #endregion

    }
}
