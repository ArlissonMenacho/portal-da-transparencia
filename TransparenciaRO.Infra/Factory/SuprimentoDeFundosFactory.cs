﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.DataDBPLL.Model;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Infra.Factory
{
    public static class SuprimentoDeFundosFactory
   {
        public static List<SuprimentoDeFundosViewModel> ConverterSuprimentosDeFundosParaSuprimentosDeFundosViewModel(List<DespesasSuprimentos> suprimentoDeFundos)
        {
            return suprimentoDeFundos.Select(suprimento => ConverterSuprimentoDeFundoParaSuprimentoDeFundoViewModel(suprimento)).ToList();
        }

        public static SuprimentoDeFundosViewModel ConverterSuprimentoDeFundoParaSuprimentoDeFundoViewModel(TransparenciaRO.DataDBPLL.Model.DespesasSuprimentos suprimento)
        {
            var suprimentoVM = new SuprimentoDeFundosViewModel()
            {
                Credor               = suprimento.Credor,
                NomDespesa           = suprimento.Descricao,
                NumEmpenho           = suprimento.DocumentoNE,
                ValorDespesa         = suprimento.ValorPaga,
                EspecificacaoDespesa = suprimento.EspecificacaoDespesa,
                Exercicio            = suprimento.Exercicio,
                NomOrgao             = suprimento.UnidadeGestora
            };
            
            return suprimentoVM;
        }
   }
}
