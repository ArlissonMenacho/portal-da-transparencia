﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.ComiteTransparencia;

namespace TransparenciaRO.Infra.Factory
{
    public static class ComiteTransparenciaFactory
    {
        public static InscricaoComiteTransparencia CriarInscricao(Guid inscricaoId, string razaoSocial, string cnpj, string representanteLegal, string telefone)
        {
            var inscricao = new InscricaoComiteTransparencia(inscricaoId, razaoSocial, cnpj, representanteLegal, telefone);

            return inscricao;
        }

        public static List<InscricaoComiteTransparenciaViewModel> ConverterInscricoesComiteTransparenciaParaInscricoesComiteTransparenciaViewModel(List<InscricaoComiteTransparencia> inscricoesComiteTransparencia)
        {
            var inscricoes = inscricoesComiteTransparencia.Select(inscricao => ConverterInscricaoComiteTransparenciaParaInscricaoComiteTransparenciaViewModel(inscricao)).ToList();

            return inscricoes;
        }

        public static InscricaoComiteTransparenciaViewModel ConverterInscricaoComiteTransparenciaParaInscricaoComiteTransparenciaViewModel(InscricaoComiteTransparencia inscricaoComiteTransparencia)
        {
            var inscricao = new InscricaoComiteTransparenciaViewModel()
            {
                InscricaoId = inscricaoComiteTransparencia.InscricaoId,
                Cnpj = inscricaoComiteTransparencia.Cnpj,
                RazaoSocial = inscricaoComiteTransparencia.RazaoSocial,
                RepresentanteLegal = inscricaoComiteTransparencia.RepresentanteLegal,
                Telefone = inscricaoComiteTransparencia.Telefone,
                Arquivos = inscricaoComiteTransparencia.Arquivos
            };

            return inscricao;
        }
    }
}
