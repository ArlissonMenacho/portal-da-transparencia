﻿using System.Collections.Generic;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.DadosLicitacao;

namespace TransparenciaRO.Infra.Factory
{
    public static class DadosLicitacaoFactory
    {
        public static IndexDadosLicitacaoViewModel CreateIndexDadosLicitacaoViewModel(List<DadosLicitacao> licitacoes)
        {
            var filtro = new FiltroDadosLicitacaoViewModel()
            {
                QuantidadeResultados = 50
            };

            var indexDadosLicitacaoViewModel = new IndexDadosLicitacaoViewModel()
            {
                Filtro = filtro,
                Licitacoes = licitacoes
            };

            return indexDadosLicitacaoViewModel;
        }
    }
}
