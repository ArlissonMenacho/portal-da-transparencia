﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.View.DividaAtivaSEFIN;

namespace TransparenciaRO.Infra.Factory
{
    public static class DividaAtivaAPIFactory
    {
        public static DividaAtivaViewModel ConverterDividaAtivaEmDividaAtivaViewModel(DividaAtivaAPISEFINViewModel.DividaAtiva dividaAtiva)
        {
            var dividaAtivaVM = new DividaAtivaViewModel
            {
                Id = dividaAtiva.tuk,
                CnpjCpf = TratarCpfCnpjParaExibição(dividaAtiva.it_cnpj_cpf),
                NomePessoa = dividaAtiva.it_no_pessoa,
                ValorTotalLancamento = decimal.Parse(dividaAtiva.it_va_total_lancamento, CultureInfo.InvariantCulture).ToString("N"),
                ValorTotalPagamentoEfetuado = decimal.Parse(dividaAtiva.it_va_total_pgto_efetuado, CultureInfo.InvariantCulture).ToString("N"),
                ValorAPagar = decimal.Parse(dividaAtiva.it_va_apagar, CultureInfo.InvariantCulture).ToString("N"),
            };

            return dividaAtivaVM;
        }

        public static List<DividaAtivaViewModel> ConverterDividasAtivasEmDividasAtivasViewModel(List<DividaAtivaAPISEFINViewModel.DividaAtiva> dividasAtivas)
        {
            var dividasAtivasVM = dividasAtivas.Select(da => ConverterDividaAtivaEmDividaAtivaViewModel(da))
                                               .ToList();

            return dividasAtivasVM;
        }

        public static DividaAtivaViewModel ConverterDividaAtivaEmDividaAtivaViewModelParaDetalhes(DividaAtivaAPISEFINViewModel.DividaAtiva dividaAtiva)
        {
            decimal value = decimal.Parse(dividaAtiva.it_va_total_lancamento, CultureInfo.InvariantCulture);

            string teste = value.ToString("N");

            var dividaAtivaVM = new DividaAtivaViewModel
            {
                Id = dividaAtiva.tuk,
                CnpjCpf = TratarCpfCnpjParaExibição(dividaAtiva.it_cnpj_cpf),
                NomePessoa = dividaAtiva.it_no_pessoa,
                ValorTotalLancamento = decimal.Parse(dividaAtiva.it_va_total_lancamento, CultureInfo.InvariantCulture).ToString("N"),
                ValorTotalPagamentoEfetuado = decimal.Parse(dividaAtiva.it_va_total_pgto_efetuado, CultureInfo.InvariantCulture).ToString("N"),
                ValorAPagar = decimal.Parse(dividaAtiva.it_va_apagar, CultureInfo.InvariantCulture).ToString("N"),

                InscricaoEstadual = dividaAtiva.it_nu_inscricao_estadual,
                InscricaoCDA = dividaAtiva.it_nu_cda,
                Cartorio = dividaAtiva.it_no_cartorio,
                DocumentoCartorio = dividaAtiva.it_nu_documento,
                DataConfirmacaoCartorio = dividaAtiva.it_da_confirmacao,
                Acao = dividaAtiva.it_no_ocorrencia,
                Situacao = dividaAtiva.it_co_situacao_lancamento,
                CodigoReceita = dividaAtiva.it_co_receita,
                GuiaLancamento = dividaAtiva.it_nu_guia_lancamento
            };

            return dividaAtivaVM;
        }

        private static string TratarCpfCnpjParaExibição(string cpfCnpj)
        {
            if (cpfCnpj.Length < 14)
            {
                return "***" + cpfCnpj.Substring(3, 6) + "***";
            }

            return cpfCnpj;
        }
    }
}