﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Infra.Factory
{
    public class RegistroBoletimFactory
    {
        public static RegistroBoletim RegistrarBoletim(Guid boletimId, DateTime data, string descricao)
        {
            var registro = new RegistroBoletim(boletimId, data, descricao);

            return registro;
        }

        public static List<RegistroBoletimViewModel> ConverterRegistrosBoletimsParaRegistrosBoletimsViewModel(List<RegistroBoletim> registroBoletims)
        {
            var registros = registroBoletims.Select(registro => ConverterRegistroBoletimParaRegistroBoletimViewModel(registro)).ToList();

            return registros;
        }

        public static RegistroBoletimViewModel ConverterRegistroBoletimParaRegistroBoletimViewModel(RegistroBoletim registroBoletim)
        {
            var registro = new RegistroBoletimViewModel()
            {
                BoletimId = registroBoletim.BoletimId,
                Data = registroBoletim.Data,
                Descricao = registroBoletim.Descricao,
                Arquivo = registroBoletim.Arquivo
            };

            return registro;
        }
    }
}
