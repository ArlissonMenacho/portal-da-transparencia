﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    class ContratoConvenioConfig : EntityTypeConfiguration<ContratoConvenio>
    {
        public ContratoConvenioConfig()
        {
            //HasRequired(contratoConvenio => contratoConvenio.TermoFomento)
            //     .WithMany()
            //        .HasForeignKey(contratoConvenio => contratoConvenio.TermoFomentoId);

            HasRequired(cc => cc.UnidadeGestora)
                .WithMany(ug => ug.ContratoConvenios)
                   .HasForeignKey(cc => cc.UnidadeGestoraId);
          


        }
    }
}
