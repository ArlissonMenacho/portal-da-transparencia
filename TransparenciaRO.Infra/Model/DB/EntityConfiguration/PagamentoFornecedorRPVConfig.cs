﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class PagamentoFornecedorRPVConfig : EntityTypeConfiguration<PagamentoFornecedorRPV>
    {
        public PagamentoFornecedorRPVConfig()
        {
            HasKey(pfRPV => pfRPV.PagamentoFornecedorRPVId);

            Property(pfRPV => pfRPV.PagamentoFornecedorRPVId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(pfRPV => pfRPV.CpfCnpjCessionario).HasMaxLength(18);

            Property(pfRPV => pfRPV.Cessionario).HasMaxLength(500);
        }
    }
}