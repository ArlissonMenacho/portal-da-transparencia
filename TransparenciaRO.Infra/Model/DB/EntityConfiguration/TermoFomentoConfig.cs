﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class TermoFomentoConfig : EntityTypeConfiguration<TermoFomento>
    {

        public TermoFomentoConfig()
        {
            HasKey(termoFomento => termoFomento.Id);

            HasRequired(termoFomento => termoFomento.PrestacaoDeContas)
                .WithMany()
                   .HasForeignKey(termoFomento => termoFomento.PrestacaoDeContasId);

            //HasRequired(termoFomento => termoFomento.RemuneracaoEquipe)
            //    .WithMany()
            //       .HasForeignKey(termoFomento => termoFomento.RemuneracaoEquipeId);
        }

    }
}