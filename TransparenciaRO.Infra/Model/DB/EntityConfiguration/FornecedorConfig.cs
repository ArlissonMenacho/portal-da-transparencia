﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class FornecedorConfig : EntityTypeConfiguration<Fornecedor>
    {
        public FornecedorConfig()
        {
            //HasOptional(p => p.PastaOrigem).WithMany(p => p.SubPastas).HasForeignKey(p => p.PastaOrigemId);
            HasRequired(f => f.Cidade).WithMany(c => c.Fornecedores).HasForeignKey(f => f.CidadeId);
        }
    }
}
