﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class InscricaoComiteTransparenciaConfig : EntityTypeConfiguration<InscricaoComiteTransparencia>
    {
        public InscricaoComiteTransparenciaConfig()
        {
            HasKey(i => i.InscricaoId);

            Property(i => i.RazaoSocial).IsRequired().HasMaxLength(100);

            Property(i => i.Cnpj).IsRequired().HasMaxLength(18);

            Property(i => i.RepresentanteLegal).IsRequired().HasMaxLength(100);

            Property(i => i.Telefone).IsRequired().HasMaxLength(15);

            HasMany(i => i.Arquivos).WithOptional(a => a.InscricaoComiteTransparencia).HasForeignKey(a => a.InscricaoComiteTransparenciaId);
        }
    }
}