﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class PastaConfig : EntityTypeConfiguration<Pasta>
    {        
        public PastaConfig()
        {
            Property(p => p.Nome).IsRequired();
            HasKey(p => p.PastaId);
            Property(p => p.Nome).HasMaxLength(100);
            HasOptional(p => p.PastaOrigem).WithMany(p => p.SubPastas).HasForeignKey(p => p.PastaOrigemId);
            Property(p => p.Href).HasMaxLength(200);
            HasMany(p => p.Imagens).WithRequired(p => p.Pasta);
        }       
    }
}
