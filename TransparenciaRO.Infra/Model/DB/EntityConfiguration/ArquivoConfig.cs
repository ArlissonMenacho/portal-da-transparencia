﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    class ArquivoConfig : EntityTypeConfiguration<Arquivo>
    {
        public ArquivoConfig()
        {
            HasRequired(a => a.Pasta).WithMany(p => p.Arquivos).HasForeignKey(p => p.PastaId); // Relacionamento com pastas

            HasOptional(a => a.ContratoConvenio).WithMany(cc => cc.Arquivos);
            HasOptional(a => a.Monitoramento).WithMany(cc => cc.ArquivoRelatorioMonitoramento);
            HasOptional(a => a.PrestacaoDeConta).WithMany(cc => cc.ArquivoResultadoConclusivo);

            HasOptional(a => a.Licitacao).WithMany(l => l.Arquivos);

            HasOptional(a => a.Boletim);
        }
    }
}
