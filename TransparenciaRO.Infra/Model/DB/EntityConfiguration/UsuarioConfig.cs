﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class UsuarioConfig : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            ToTable("Usuarios");
            Property(u => u.Email).HasMaxLength(100).IsRequired();
            Property(u => u.Nome).HasMaxLength(150).IsRequired();
            HasKey(u => u.UsuarioId);
            HasRequired(u => u.UnidadeGestora).WithMany(s => s.Usuarios).HasForeignKey(u => u.UnidadeGestoraID);
            HasMany(u => u.Pastas).WithMany(p => p.Usuarios);
        }
    }
}