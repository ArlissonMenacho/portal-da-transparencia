﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class FiscalCidadaoConfig : EntityTypeConfiguration<FiscalCidadao>
    {
        public FiscalCidadaoConfig()
        {
            Property(f => f.Imagem).HasColumnType("nvarchar(MAX)");
            //HasOptional(x => x.RespostaFiscalCidadao)
            //    .WithMany()
            //    .HasForeignKey(x => x.RespostaFiscalCidadaoId);
        }
    }
}
