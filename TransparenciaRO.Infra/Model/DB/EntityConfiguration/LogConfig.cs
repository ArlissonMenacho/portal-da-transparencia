﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class LogConfig : EntityTypeConfiguration<Log>
    {
        public LogConfig()
        {
            HasKey(l => l.LogId);
            HasOptional(l => l.Usuario).WithMany(u => u.Log).HasForeignKey(l => l.UsuarioId);

        }
    }
}
