﻿using System.Data.Entity.ModelConfiguration;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.DB.EntityConfiguration
{
    public class ConteudoDinamicoConfig : EntityTypeConfiguration<ConteudoDinamico>
    {
        public ConteudoDinamicoConfig()
        {
            Property(c => c.ConteudoHTML).HasColumnType("nvarchar(MAX)");
        }
    }
}
