﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RespostaFiscalCidadao
    {

        [Key]
        public int RespostaId { get; set; }

        public long ProtocoloId { get; set; }

        [Required]
        public string Resposta { get; set; }

        public DateTime DataResposta { get; set; }



        public Guid OrgaoVinculado { get; set; }

        [ForeignKey("OrgaoVinculado")]
        public UnidadeGestora UnidadeGestora { get; set; }

        
        public virtual FiscalCidadao FiscalCidadao { get; set; }



        public Guid UsuarioId { get; set; }

        public Usuario Usuario { get; set; }
    }
}
