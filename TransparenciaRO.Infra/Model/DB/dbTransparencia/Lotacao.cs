﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Lotacao
    {
        public Lotacao()
        {
            LotacaoId = Guid.NewGuid();
        }

        [Key]
        public Guid LotacaoId { get; set; }

        [Index, Required]
        public int Lot { get; set; }

        [Required]
        public string Descricao { get; set; }

        public virtual ICollection<RelacaoServidor> Servidores { get; set; }
    }
}
