using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ReceitaAcumuladaSiafem")]
    public partial class ReceitaAcumuladaSiafem
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Exercicio { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte Mes { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodAdministracao { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CodEspecificacaoReceita { get; set; }

        [StringLength(255)]
        public string Descricao { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorMensal { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorAcumulado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorSaldoFinal { get; set; }
    }
}
