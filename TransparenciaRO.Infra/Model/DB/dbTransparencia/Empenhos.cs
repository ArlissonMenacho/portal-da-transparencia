using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Empenhos")]
    public partial class Empenho
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string NumeroEmpenho { get; set; }

        [StringLength(15)]
        public string NumeroProcesso { get; set; }

        [StringLength(14)]
        public string DocCredor { get; set; }

        [StringLength(80)]
        public string Credor { get; set; }

        [StringLength(15)]
        public string Processo { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string TipoEmpenho { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string ModalidadeCompra { get; set; }

        public DateTime? DataEmpenho { get; set; }

        [StringLength(80)]
        public string UnidadeGestora { get; set; }

        public int? Item { get; set; }

        [StringLength(20)]
        public string UnidadeMedida { get; set; }

        public int? Quantidade { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorItem { get; set; }

        [StringLength(2000)]
        public string Descricao { get; set; }

        public DateTime? DataArquivo { get; set; }

        public int? CodUnidadeGestora { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Exercicio { get; set; }

        public int? CodModalidadeCompra { get; set; }
    }
}
