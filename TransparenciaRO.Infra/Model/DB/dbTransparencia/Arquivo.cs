﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Arquivos")]
    public class Arquivo
    {
        public Arquivo()
        {
            ArquivoId = Guid.NewGuid();
            DataUpload = DateTime.Now;
        }

        /// <summary>
        /// Além de chave primária, o nome físico do arquivo no servidor será este próprio valor
        /// </summary>
        public Guid ArquivoId { get; set; }

        [Required]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        public string Extensao { get; set; }

        public DateTime DataUpload { get; set; }

        public DateTime? DataRemocao { get; set; }

        public Guid UsuarioIdUpload { get; set; }

        public Guid? UsuarioIdRemocao { get; set; }

        public Pasta Pasta { get; set; }

        public Guid PastaId { get; set; }

        public int? Ordem { get; set; }

        public Guid? ContratoConvenioId { get; set; }

        public virtual ContratoConvenio ContratoConvenio { get; set; }

        public Guid? LicitacaoId { get; set; }

        public virtual Licitacao Licitacao { get; set; }

        public Guid? PortariaResolucaoId { get; set; }

        public virtual PortariaResolucao PortariaResolucao { get; set; }

        public Guid? ControleDoumentoExternoId { get; set; }

        public virtual ControleDoumentoExterno ControleDoumentoExterno { get; set; }

        public Guid? InscricaoComiteTransparenciaId { get; set; }

        public virtual InscricaoComiteTransparencia InscricaoComiteTransparencia { get; set; }

        public Guid? FornecedorDispensaId { get; set; }
        public Guid? PrestacaoDeContasId { get; set; }
        public PrestacaoDeContas PrestacaoDeConta { get; set; }
        public Guid? MonitoramentoId { get; set; }
        public Monitoramento Monitoramento { get; set; }

        //public virtual FornecedorDispensa FornecedorDispensa { get; set; }

        public ETipoAnexo? TipoAnexo { get; set; }
        public ETipoAnexoTermoFomento? TipoAnexoTermoFomento { get; set; }
        public bool Temporario { get; set; }

        [NotMapped]
        public string RazaoSocialFornecedor { get; set; }

        public Guid? BoletimId { get; set; }

        public virtual RegistroBoletim Boletim { get; set; }
    }
}
