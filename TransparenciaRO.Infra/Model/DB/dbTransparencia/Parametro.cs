﻿using TransparenciaRO.Infra.Enum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Parametros")]
    public class Parametro
    {
        [Key]
        public ETipoParametro TipoParametro { get; set; }
        public string Valor { get; set; }
    }
}
