﻿using System;
using System.CodeDom;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;


namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ReceitasEmpresas")]
    public class ReceitaEmpresa
    {
        public ReceitaEmpresa()
        {
            ReceitaEmpresaId = Guid.NewGuid();
        }

        [Key]
        public Guid ReceitaEmpresaId { get; set; }

        [Required]
        public DateTime Data { get; set; }

        [DisplayName("Nota Fiscal")]
        public string NotaFiscal { get; set; }

        [Required]
        [DisplayName("CPF/CNPJ")]
        public string CpfCnpj { get; set; }

        [Required]
        public string Cliente { get; set; }

        [Required]
        public decimal Receita { get; set; }
        
        public string Chave { get; set; }

        public EEmpresa Empresa { get; set; }
    }
}