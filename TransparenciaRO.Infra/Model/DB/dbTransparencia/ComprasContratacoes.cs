﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ComprasContratacoes")]
    public class ComprasContratacoes
    {
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Exercicio { get; set; }

        public string UG { get; set; }
        public string Status { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string NumeroEmpenho { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string TipoEmpenho { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string ModalidadeCompra { get; set; }

        public int CodModalidadeCompra { get; set; }

        [StringLength(15)]
        public string NumeroProcesso { get; set; }

        public DateTime? DataDocumento { get; set; }

        [StringLength(80)]
        public string Credor { get; set; }

        [Column(TypeName = "money")]
        public decimal ValorEmpenhada { get; set; }

    }
}
