﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public partial class Licitacaoo
    {
        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string Processo { get; set; }
        [Key]
        [Column(Order = 2)]
        [StringLength(30)]
        public string Pregao { get; set; }
        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Modalidade { get; set; }
        [Key]
        [Column(Order = 4)]
        [StringLength(200)]
        public string Unidade_Gestora { get; set; }
        [Key]
        [Column(Order = 5)]
        [StringLength(600)]
        public string Objeto { get; set; }
        [Key]
        [Column(Order = 6)]
        [StringLength(200)]
        public string Item { get; set; }
        [Key]
        [Column(Order = 7)]
        [StringLength(20)]
        public string Valor_Est { get; set; }
        [Key]
        [Column(Order = 8)]
        [StringLength(20)]
        public string Valor_Adj { get; set; }
        [Key]
        [Column(Order = 9)]
        [StringLength(24)]
        public string CNPJ { get; set; }
        [Key]
        [Column(Order = 10)]
        [StringLength(150)]
        public string Empresa_Vencedora { get; set; }
        [Key]
        [Column(Order = 11)]
        [StringLength(50)]
        public string Situacao { get; set; }
        [NotMapped]
        public string Resultado { get; set; }
        [NotMapped]
        public virtual IEnumerable<CaminhoArquivo> CaminhoArquivo { get; set; }
    }

    public partial class CaminhoArquivo
    {
        public string Arquivo { get; set; }
        public string DescricaoArquivo { get; set; }
    }
}
