﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RelacaoPensionista 
    {
        
        [Key]
        public int RelacacaoPensionistaId { get; set; }

        [Required]
        public int Ano { get; set; }
        
        [Required]
        public int Mes { get; set; }

        [Required]
        public int Matricula { get; set; }

       [Required]
        public long Cpf { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Cargo { get; set; }

        public DateTime? DataNascimento { get; set; }

        [Required]
        public DateTime Admissao { get; set; }

        [Required]
        public int CargaHoraria { get; set; }

        [Required]
        public string Classificacao { get; set; }

        [Required]
        public string LocalTrabalho { get; set; }

        [Required]
        public string Vinculo { get; set; }

        public Guid LotacaoId { get; set; }

        public virtual Lotacao Lotacao { get; set; }
    }
}
