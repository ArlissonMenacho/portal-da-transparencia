﻿using System;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class ItemLicitacao
    {
        [Key]
        public Guid ItemLicitacaoId { get; set; }

        public Guid? FornecedorDispensaId { get; set; }

        public Guid? LicitacaoId { get; set; }

        public int Item { get; set; }

        public int Quantidade { get; set; }

        [StringLength(500)]
        public string Descricao { get; set; }

        [StringLength(100)]
        public string NumeroEmpenho { get; set; }

        [StringLength(100)]
        public string Unidade { get; set; }

        public decimal ValorUnitario { get; set; }

        public EStatusItemLicitacao Status { get; set; }
    }
}