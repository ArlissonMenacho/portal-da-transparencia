﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Usuarios")]
    public class Usuario
    {
        [Key]
        public Guid UsuarioId { get; set; }
        [Index]
        public string Email { get; set; }
        public string Nome { get; set; }
        public byte[] Senha { get; set; }

        [Index]
        public long CPF { get; set; }

        public Guid UnidadeGestoraID { get; set; }
        public virtual UnidadeGestora UnidadeGestora { get; set; }
        
        public DateTime DataCriacao { get; set; }
        public DateTime? DataUltimoLogin { get; set; }
        [Index]
        public bool Ativo { get; set; }
        public bool AlterarSenhaProximoLogin { get; set; }

        public virtual ICollection<Noticia> Noticias { get; set; }
        public virtual ICollection<Permissao> Permissoes { get; set; }
        public virtual ICollection<Pasta> Pastas { get; set; }
        public virtual ICollection<Log> Log { get; set; }
        public virtual ICollection<FluxoFiscalCidadao> FluxoFiscalCidadaos { get; set; }

        public Usuario()
        {
            Permissoes = new List<Permissao>();
            Pastas = new List<Pasta>();
        }
    }
}