﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Log
    {
        public Guid LogId { get; set; }
        public Guid? UsuarioId { get; set; }        
        [Index]
        public DateTime DataHoraEvento { get; set; }
        public string Descricao { get; set; }

        public virtual Usuario Usuario { get; set; }

        public Log()
        {
            DataHoraEvento = DateTime.Now;
        }
    }
}
