﻿using System;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Indice
    {
        [Key]
        public Guid IndiceId { get; set; }
        public string Titulo { get; set; }
        public EAgrupamentoIndice Agrupamento { get; set; }
        //public string PreviaResultado { get; set; } //Exemplo em notícias quando se buscar por palavra-chave RECEBIMENTO: "... fulano encontrou o RECEBIMENTO xys..."
        public string PalavrasChave { get; set; }
        public string Href { get; set; }
    }
}
