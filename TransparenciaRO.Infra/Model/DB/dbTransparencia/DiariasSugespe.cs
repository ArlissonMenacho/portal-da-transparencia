﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class DiariasSugespe
    {
        public string Motorista { get; set; }

        [Key]
        [Column(Order = 0)]
        public int? Solicitacao { get; set; }

        //[Key]
        [Column(Order = 2)]
        [StringLength(14)]
        public string MatriculaMotorista { get; set; }

        [StringLength(60)]
        public string Cargo { get; set; }

        [StringLength(20)]
        public string UGViagem { get; set; }

        public DateTime? DataIda { get; set; }

        public DateTime? DataVolta { get; set; }

        public decimal? QuantidadeDiarias { get; set; }

        public decimal? ValorDiarias { get; set; }

        public decimal? ValorDiariaMotorista { get; set; }

        [StringLength(20)]
        public string UGMotorista { get; set; }

        public string Objetivo { get; set; }

        [StringLength(100)]
        public string TipoViagem { get; set; }

        [StringLength(120)]
        public string Passageiro { get; set; }

        [StringLength(60)]
        public string CargoPassageiro { get; set; }

        //[Key]
        [Column(Order = 1)]
        [StringLength(14)]
        public string MatriculaPassageiro { get; set; }

        [StringLength(20)]
        public string UGViagemPassageiro { get; set; }

        public string ObjetivoPassageiro { get; set; }

        public decimal? QuantidadediariasPassageiro { get; set; }

        public decimal? ValorDiariasPassageiro { get; set; }

        public decimal? TransladoAereo { get; set; }

        public decimal? TotalDiaria { get; set; }

        [StringLength(20)]
        public string UGPassageiro { get; set; }

        [StringLength(14)]
        public string CpfPassageiro { get; set; }

        [StringLength(14)]
        public string CpfMotorista { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        public DateTime? DataDocumento { get; set; }
    }
}
