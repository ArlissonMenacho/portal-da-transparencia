namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Municipios
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoMunicipio { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoMunicipioDV { get; set; }

        [StringLength(255)]
        public string NomeMunicipio { get; set; }

        [StringLength(255)]
        public string NomeMunicipioEX { get; set; }

        public short? ANOINST { get; set; }

        public decimal? LATITUDE { get; set; }

        public decimal? LONGITUDE { get; set; }

        public decimal? ALTITUDE { get; set; }

        public decimal? AREA { get; set; }

        [StringLength(20)]
        public string CNPJ { get; set; }

        [StringLength(255)]
        public string NomePrefeitura { get; set; }
    }
}
