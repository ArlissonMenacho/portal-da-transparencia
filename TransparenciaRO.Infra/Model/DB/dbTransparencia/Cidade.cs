﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Cidades")]
    public class Cidade
    {
        public Guid CidadeId { get; set; }

        public string Nome { get; set; }

        public EEstado EstadoId { get; set; }

        public ICollection<Fornecedor> Fornecedores { get; set; }
    }
}