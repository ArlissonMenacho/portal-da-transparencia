﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;
using System;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ContratosConvenios")]
    public class ContratoConvenio
    {
        #region Construtor
        public ContratoConvenio()
        {
            UnidadeGestoraId = new Guid();
            //ContratoConvenioId = Guid.NewGuid();
            Arquivos = new List<Arquivo>();
        }
        #endregion
       
        #region Propriedades da Classe

        [Key]
        public Guid ContratoConvenioId { get; set; }
        [NotMapped]
        public virtual Arquivo ArquivoTemporario { get; set; }

        public bool Cancelado { get; set; }

        [Required, DisplayName("Número do Documento")]
        public string NumeroDocumento { get; set; }

        public string NumeroProcesso { get; set; }

        [DisplayName("Data de Elaboração")]
        public DateTime DataElaboracao { get; set; }

        [DisplayName("Data de Vigência")]
        public DateTime? DataVigencia { get; set; }

        public string DataVigenciaDescritiva { get; set; }

        public DateTime? DataRetorno { get; set; }

        public DateTime? DataAssinatura { get; set; }

        public string NumeroDoe { get; set; }

        public DateTime? Execucao { get; set; }

        public int? NumeroLivroEspecial { get; set; }

        public int? NumeroFolha { get; set; }

        [Required]
        public string Empresa { get; set; }

        public string Cnpj { get; set; }

        [Required]
        public string Objeto { get; set; }

        public decimal Valor { get; set; }

        public string ValorEmpenhado { get; set; }

        public string ValorAContraPartida { get; set; }

        public string LicitacaoOrigem { get; set; }

        public string OrigemRecursos { get; set; }

        public string Obsevacao { get; set; }
        /// <summary>
        /// Não é mais utilizado, mas será preservado para fazer a migração da estrutura antiga: O nome do arquivo físico salvo será o guid do contrato ou convênio
        /// </summary>
        public string NomeArquivoDocumento { get; set; }

        public ICollection<Arquivo> Arquivos { get; set; }

        public ETipoDocumento TipoDocumento { get; set; }

        #endregion
        [DisplayName("Unidade Gestora")]
        public Guid UnidadeGestoraId { get; set; }

        [Column("unidadeGestoraID")]
        public virtual UnidadeGestora UnidadeGestora { get; set; }

        public Guid? TermoFomentoId { get; set; }
        public TermoFomento TermoFomento { get; set; }
    }
}