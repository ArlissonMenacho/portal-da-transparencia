﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    /// <summary>
    /// Classe usada para mostrar imagens estáticas para uma pasta no MENU PRINCIPAL
    /// </summary>
    [Table("ImagensPasta")]
    public class ImagemPasta
    {
        [Key]
        public Guid ImagemPastaId { get; set; }
        public Guid PastaId { get; set; }
        /// <summary>
        /// Tamanho horizontal da imagem em colunas (mínimo: 1, máximo: 12)
        /// </summary>
        public int NumeroColunas { get; set; }
        public string Titulo { get; set; }
        public string URLImagem { get; set; }
        public string Descricao { get; set; }
        public int Ordem { get; set; }

        [ForeignKey(nameof(PastaId))]
        public Pasta Pasta { get; set; }
    }
}
