﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RemuneracaoEmpresa
    {
        public RemuneracaoEmpresa()
        {
            RemuneracaoEmpresaId = Guid.NewGuid();
        }

        [Key]
        public Guid RemuneracaoEmpresaId { get; set; }

        public int CodigoEmpresa { get; set; }

        public string NomeEmpresa { get; set; }

        public int CodigoEmpregado { get; set; }

        public string NomeEmpregado { get; set; }

        public int Ano { get; set; }

        public int Mes { get; set; }

        public string Cargo { get; set; }

        public string Departamento { get; set; }

        public string TipoCargo { get; set; }

        public DateTime Admissao { get; set; }

        public string Cpf { get; set; }

        public string Pis { get; set; }

        public DateTime? Demissao { get; set; }

        public decimal TotalProvento { get; set; }

        public decimal TotalDesconto { get; set; }

        public decimal TotalLiquido { get; set; }
        public EEmpresa Empresa { get; set; }
    }
}
