﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Noticias")]
    public class Noticia
    {
        [Key]
        public Guid NoticiaId { get; set; }
        public string Titulo { get; set; }
        public DateTime DataCriacao { get; set; }
        [Index]
        public DateTime DataUltimaAtualizacao { get; set; }
        public bool Publicado { get; set; }
        [Index]
        public DateTime? DataExclusao { get; set; }
        public byte[] HTMLNoticia { get; set; }

        public Guid UsuarioCriadorId { get; set; }

        [ForeignKey("UsuarioCriadorId")]
        public Usuario UsuarioCriador { get; set; }

        public bool Popup { get; set; }
    }
}
