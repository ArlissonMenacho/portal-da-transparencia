﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Fornecedores")]
    public class Fornecedor
    {
        public Fornecedor()
        {
            FornecedorId = Guid.NewGuid();
        }

        public Guid FornecedorId { get; set; }

        [Required]
        [Display(Name = "Razão Social")]
        public string RazaoSocial { get; set; }

        [Required]
        public ETipoPessoa TipoPessoa { get; set; }

        [Required]
        [Display(Name = "CPF / CNPJ")]
        public string CpfCnpj { get; set; }

        public EEstado? Uf { get; set; }

        public Guid? CidadeId { get; set; }

        public virtual Cidade Cidade { get; set; }

        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        public ICollection<FornecedorImpedido> Impedimentos { get; set; }
    }
}