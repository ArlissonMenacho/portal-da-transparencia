﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("FornecedoresImpedidos")]
    public class FornecedorImpedido
    {
        public FornecedorImpedido()
        {
            FornecedorImpedidoId = Guid.NewGuid();
        }

        public Guid FornecedorImpedidoId { get; set; }

        [Key]
        public int Codigo { get; set; }

        [Required]
        public Guid FornecedorId { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }
         
        [Required]
        public ESancaoImpedirFornecedor Sancao { get; set; }

        [Index("PeriodoDePunicao", 1), Required]
        public DateTime DataInicio { get; set; }

        [Index("PeriodoDePunicao", 2), Required]
        public DateTime DataFinal { get; set; }

        public string NumeroProcesso { get; set; }

        public string NumeroProcessoSemFormatacao { get; set; }

        public DateTime? DataPublicacao { get; set; }

        public string PenasEventuais { get; set; }

        [Required]
        [Display(Name = "Unidade Gestora")]
        public Guid UnidadeGestoraId { get; set; }

        public virtual UnidadeGestora UnidadeGestora { get; set; }

        public string InformacaoPublica { get; set; }

        public string ObservacaoInterna { get; set; }

        public DateTime DataInclusao { get; set; }

        public DateTime? DataUltimaAlteracao { get; set; }

        public Guid? UsuarioId { get; set; }

        public virtual Usuario Usuario { get; set; }

        public bool Removido { get; set; }
    }
}