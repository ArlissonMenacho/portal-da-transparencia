﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class PagamentoFornecedorRPV
    {

        public Guid PagamentoFornecedorRPVId { get; set; }

        public string DocumentoOB { get; set; }

        public string Credor { get; set; }

        public string DocCredor { get; set; }

        public string NumeroProcesso { get; set; }

        public string Finalidade { get; set; }

        public DateTime DataOb { get; set; }

        public string StatusOb { get; set; }

        public decimal ValorPaga { get; set; }

        public decimal Movimentacao { get; set; }

        public DateTime? DataAtualizacao { get; set; }

        public string CpfCnpjCessionario { get; set; }

        public string Cessionario { get; set; }
    }
}