﻿using System;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class EmpresaRemuneracao
    {
        public EmpresaRemuneracao()
        {
            RemuneracaoId = Guid.NewGuid();
        }
        [Key]
        public Guid RemuneracaoId { get; set; }
        [Required]
        public int Mes { get; set; }
        [Required]
        public int Ano { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cargo { get; set; }
        [Required]
        public string RequisitoCargo { get; set; }
        [Required]
        public string Departamento { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Matricula { get; set; }
        [Required]
        public string PisPasep { get; set; }
        public DateTime DataNascimento { get; set; }
        [Required]
        public string Sexo { get; set; }
        [Required]
        public string EstadoCivil { get; set; }
        public DateTime Admissao { get; set; }
        public DateTime? Demissao { get; set; }
        [Required]
        public string TipoJornada { get; set; }
        [Required]
        public int CargaHoraria { get; set; }
        [Required]
        public string CategoriaSituacao { get; set; }
        [Required]
        public string FormaComissao { get; set; }
        [Required]
        public decimal Salario { get; set; }
        [Required]
        public decimal OutrosProventos { get; set; }
        [Required]
        public decimal TotalProventos { get; set; }
        [Required]
        public decimal Inss { get; set; }
        [Required]
        public decimal Irrf { get; set; }
        [Required]
        public decimal OutrosDescontos { get; set; }
        [Required]
        public decimal TotalDescontos { get; set; }
        [Required]
        public decimal TotalLiquido { get; set; }
        [Required]
        public decimal Fgts { get; set; }

        public EEmpresa EEmpresa { get; set; }
        public string TipoCargo { get; set; }
    }
}