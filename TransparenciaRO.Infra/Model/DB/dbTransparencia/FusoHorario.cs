﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("FusoHorario")]
    public class FusoHorario
    {
        [Key]
        public Guid FusoHorarioId { get; set; }
        public string Descricao { get; set; } //Exemplo: Fuso-horário de Rondônia
        public int GMT { get; set; } //Exemplo: -4 (Horário de Rondônia)
    }
}
