using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Orgaos")]
    public partial class Orgao
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Exercicio { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodOrgao { get; set; }

        [StringLength(80)]
        public string NomOrgao { get; set; }

        [StringLength(10)]
        public string NomSigla { get; set; }

        [StringLength(8000)]
        public string DesFuncao { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodAdministracao { get; set; }

        public int? CodOrgaoTce { get; set; }
    }
}
