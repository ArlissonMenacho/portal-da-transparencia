﻿using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class EnqueteResultado
    {
        [Key]
        public int EnqueteResultadoId { get; set; }
        public EAvaliacao Avaliacao { get; set; }
        public int EnqueteId { get; set; }
        public virtual Enquete Enquete { get; set; }
    }
}