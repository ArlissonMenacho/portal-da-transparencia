﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Monitoramento
    {
        public Monitoramento()
        {
            Id = Guid.NewGuid();
            ArquivoRelatorioMonitoramento = new List<Arquivo>();
        }
        public Guid Id { get; set; }
        public string Gestor { get; set; }
        public string PortariaGestor { get; set; }
        public string Comissao { get; set; }
        public string PortariaComissao { get; set; }
        public ICollection<Arquivo> ArquivoRelatorioMonitoramento { get; set; }


    }
}
