﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class SocioFornecedor
    {
        public SocioFornecedor()
        {
            SocioFornecedorId = Guid.NewGuid();
        }

        public Guid SocioFornecedorId { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [DisplayName("CPF")]
        public string Cpf { get; set; }

        [Required]
        public Guid ImpedirFornecedorId { get; set; }

        [Required]
        [DisplayName("Código Impedimento do Fornecedor")]
        public int ImpedirFornecedorCodigo { get; set; }

        public virtual FornecedorImpedido ImpedirFornecedor { get; set; }
    }
}
