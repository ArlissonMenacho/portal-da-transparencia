﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("DetalheEmpenhos")]
    public class DetalheEmpenho
    {
        [Key]
        [Column(Order = 0)]
        public int UG { get; set; }
        [Key]
        [Column(Order = 1)]
        public string DocumentoNE { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataNE { get; set; }

        public int? ItemEmpenho { get; set; }

        public string UnidadeMedida { get; set; }
        [DisplayFormat(DataFormatString = "{0:c2}")]
        public decimal? ValorUnitario { get; set; }

        public int? Quantidade { get; set; }

        public string Descricao { get; set; }
       

      
    }
}
