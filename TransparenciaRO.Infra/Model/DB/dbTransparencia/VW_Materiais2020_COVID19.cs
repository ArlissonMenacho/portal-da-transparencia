﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public partial class VW_Materiais2020_COVID19
    {
        [Key]
        [Column(Order = 0, TypeName = "smallint")]
        [Required]
        public short Exercicio { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string UG { get; set; }
        [NotMapped]
        public string NomeUG{ get; set; }
        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        [Required]
        public string NumEmpenho { get; set; }
        [Key]
        [Column(Order = 3)]
        public DateTime? DataDocumento { get; set; }
        [Key]
        [Column(Order = 4, TypeName = "bigint")]
        public long? CodEspecificacaoDespesa { get; set; }
        [Key]
        [Column(Order = 5, TypeName = "bigint")]
        public long? EspecificacaoDespesa { get; set; }
        [Key]
        [Column(Order = 6)]
        public string NomDespesa { get; set; }
        [Key]
        [Column(Order = 7)]
        [StringLength(255)]
        public string Credor { get; set; }
        [Key]
        [Column(Order = 8, TypeName = "money")]
        public decimal? ValorEmpenhada { get; set; }
        [Key]
        [Column(Order = 9, TypeName = "money")]
        public decimal? ValorLiquidada { get; set; }
        [Key]
        [Column(Order = 10, TypeName = "money")]
        public decimal? ValorPaga { get; set; }
        [Key]
        [Column(Order = 11, TypeName = "money")]
        public decimal? valorDespesa { get; set; }
        [Key]
        [Column(Order = 12)]
        public string Descricao { get; set; }
        [Key]
        [Column(Order = 13)]
        public int Qtde { get; set; }
        [Key]
        [Column(Order = 14)]
        [StringLength(20)]
        public string UnidadeMedida { get; set; }
        [Key]
        [Column(Order = 15, TypeName = "money")]
        public decimal VlrUnitario { get; set; }
        [Key]
        [Required]
        [StringLength(40)]
        [Column(Order = 16)]
        public string Documento { get; set; }
        [Key]
        [Required]
        [StringLength(41)]
        [Column(Order = 17)]
        public string Status { get; set; }
    }
}