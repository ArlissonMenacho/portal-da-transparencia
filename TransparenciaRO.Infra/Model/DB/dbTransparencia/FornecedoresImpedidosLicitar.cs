﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("FornecedoresImpedidosLicitar")]
    public partial class FornecedoresImpedidosLicitar
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo { get; set; }
        public string CpfCnpj { get; set; }
        public string NumeroProcesso { get; set; }
        public string Sigla { get; set; }
        public string NomeUG { get; set; }
        public string RazaoSocial { get; set; }
        public string InformacaoPublica { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFinal { get; set; }
        public DateTime? DataPublicacao { get; set; }
        public string PenasEventuais { get; set; }
        public string Sancao { get; set; }
    }
}