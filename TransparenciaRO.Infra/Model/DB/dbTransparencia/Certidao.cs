﻿using System;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Certidao
    {
        public Certidao()
        {
            CertidaoId = Guid.NewGuid();
            DataDeEmissao = Convert.ToDateTime(DateTime.Now.ToString("G"));
        }

        public Guid CertidaoId { get; set; }

        public DateTime DataDeEmissao { get; set; }

        public bool Impedido { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }
    }
}