﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Dialog
    {
        [Key]
        public int DialogId { get; set; }

        [Required, DisplayName("Descrição"), MaxLength(160, ErrorMessage = "Limite de 160 caracteres")]
        public string Descricao { get; set; }
        [DisplayName("Url")]
        public string url { get; set; }

    }
}