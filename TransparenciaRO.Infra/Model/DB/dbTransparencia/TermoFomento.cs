﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{

    public class TermoFomento
    {

        public TermoFomento()
        {
            Id = Guid.NewGuid();
            RemuneracaoEquipe = new List<RemuneracaoEquipe>();
        }

        //Tem Contrato convernio

        public Guid Id { get; set; }

        public Guid PrestacaoDeContasId { get; set; }

        public PrestacaoDeContas PrestacaoDeContas { get; set; }

        public Guid MonitoramentoId { get; set; }

        public Monitoramento Monitoramento { get; set; }

        public List<RemuneracaoEquipe> RemuneracaoEquipe { get; set; }

    }
}
