using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{

    [Table("DespesasPortalDetalhadas")]
    public partial class DespesaPortalDetalhada
    {
        [Key, Column(Order = 0)]
        [StringLength(14)]
        public string DocCredor { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(20)]
        public string NumEmpenho { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(20)]
        public string DocumentoNE { get; set; }

        [Key, Column(Order = 3)]
        public long? EspecificacaoDespesa { get; set; }

        [Key, Column(Order = 4)]
        public string Processo { get; set; }

        [Key, Column(TypeName = "money", Order = 5)]
        public decimal? valorDespesa { get; set; }

        [Key, Column(Order = 6)]
        public DateTime DataDocumento { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CodProjeto { get; set; }

        [StringLength(255)]
        public string Acao { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Exercicio { get; set; }

        public int? CodPrograma { get; set; }

        [StringLength(80)]
        public string Programa { get; set; }

        [StringLength(4)]
        public string CodAcao { get; set; }
        
        [Column(TypeName = "money")]
        public decimal? ValorEmpenhada { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorLiquidada { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorPaga { get; set; }

        [StringLength(6)]
        public string Evento { get; set; }

        public int? CodFuncao { get; set; }

        [StringLength(80)]
        public string NomFuncao { get; set; }

        public int? CodSubFuncao { get; set; }

        [StringLength(100)]
        public string DescricaoSubFuncao { get; set; }

        [StringLength(4)]
        public string Fonte { get; set; }

        [StringLength(100)]
        public string DesFonteRecurso { get; set; }

        [StringLength(2)]
        public string CodNomDespesa { get; set; }

        [StringLength(255)]
        public string NomDespesa { get; set; }

        [StringLength(12)]
        public string NaturezaDespesa { get; set; }

        [StringLength(80)]
        public string NomOrgao { get; set; }
        
        [StringLength(20)]
        public string Documento { get; set; }
        
        [StringLength(45)]
        public string Credor { get; set; }
        
        
        [StringLength(6)]
        public string UG { get; set; }

        [StringLength(1)]
        public string CodCategoriaDespesa { get; set; }

        [StringLength(255)]
        public string CategoriaDespesa { get; set; }

        [StringLength(1)]
        public string CodGrupoDespesa { get; set; }

        [StringLength(255)]
        public string GrupoDespesa { get; set; }

        [StringLength(2)]
        public string CodModAplicacao { get; set; }

        [StringLength(255)]
        public string ModAplicacao { get; set; }

        [StringLength(20)]
        public string TipoEmpenho { get; set; }

        [StringLength(50)]
        public string ModalidadeLicitacao { get; set; }

        public DateTime? DataEmpenho { get; set; }
    }
}
