﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("DespesasEmpresas")]
    public class DespesaEmpresa
    {
        public DespesaEmpresa()
        {
            DespesaEmpresaId = Guid.NewGuid();
        }

        [Key]
        public Guid DespesaEmpresaId { get; set; }

        [Required]
        [DisplayName("Fonte de Recurso")]
        public int FonteRecurso { get; set; }

        [Required]
        [DisplayName("Pagamento ou Recebimento")]
        public DateTime PagamentoRecebimento { get; set; }

        [Required]
        [DisplayName("Credor ou Devedor")]
        public string CredorDevedor { get; set; }

        public string Outros { get; set; }

        [DisplayName("Nota Fiscal")]
        public string NotaFiscal { get; set; }

        [DisplayName("CPF/CNPJ")]
        public string CpfCnpj { get; set; }

        [Required]
        public decimal Valor { get; set; }

        public EEmpresa Empresa { get; set; }
    }
}
