﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class FornecedorDispensa
    {
        [Key]
        public Guid FornecedorId { get; set; }

        [Required, MaxLength(18)]
        public string CNPJ { get; set; }

        [Required, MaxLength(250)]
        [Display(Name = "Razão social")]
        public string RazaoSocial { get; set; }

        [Display(Name = "Nota de empenho"), MaxLength(20)]
        public string NumeroEmpenho { get; set; }

        [Required]
        public decimal Valor { get; set; }

        [StringLength(50)]
        public string Vigencia { get; set; }

        public bool Cancelado { get; set; }

        [StringLength(500)]
        public string LocalExecucao { get; set; }

        public DateTime? DataAssinatura { get; set; }

        public Guid? LicitacaoId { get; set; }

        [NotMapped]
        public ICollection<Arquivo> Arquivos { get; set; }

        [NotMapped]
        public virtual Arquivo ArquivoTemporario { get; set; }
    }
}
