﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography.X509Certificates;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("UnidadesGestoras")]
    public class UnidadeGestora
    {
        [Key]        
        public Guid unidadeGestoraID { get; set; }
        [StringLength(200)]
        public string nomeUG { get; set; }
        [StringLength(10)]
        public string sigla { get; set; }
        [StringLength(200)]
        public string endereco { get; set; }
        [StringLength(50)]
        public string telefone { get; set; }

        [Index]
        [MaxLength(6)]
        public string unidadeGestoraIDSIAFEM { get; set; }
    
        public virtual ICollection<ContratoConvenio> ContratoConvenios { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
        public virtual ICollection<Licitacao> Licitacoes { get; set; }
        public virtual ICollection<FiscalCidadao> FiscalCidadaos { get; set; }

    }
}
