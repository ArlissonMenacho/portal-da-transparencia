﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Enquete
    {
        [Key]
        public int EnqueteId { get; set; }
        [Required, DisplayName("Descrição"), MaxLength(50, ErrorMessage = "Limite de 50 caracteres")]
        public string Descricao { get; set; }

        public virtual ICollection<EnqueteResultado> EnqueteResultados { get; set; }
    }
}