﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public partial class VW_DespesasDiretas
    {
        [Key]
        [Column(Order = 1, TypeName = "smallint")]
        public short Exercicio { get; set; }
        [Key]
        [Column(Order = 2, TypeName = "smallint")]
        public short CodProjeto { get; set; }
        [Key]
        [Column(Order = 3)]
        public string EVENTO { get; set; }
        [Key]
        [Column(Order = 4)]
        public string NumEmpenho { get; set; }
        [Key]
        [Column(Order = 5)]
        public string DataDocumento { get; set; }
        [Key]
        [Column(Order = 6)]
        public string UG { get; set; }
        [Key]
        [Column(Order = 7)]
        public string N_PROCESSO_NE { get; set; }
        [Key]
        [Column(Order = 8)]
        public int CodPrograma { get; set; }
        [Key]
        [Column(Order = 9)]
        public string Programa { get; set; }
        [Key]
        [Column(Order = 10)]
        public int CodAco { get; set; }
        [Key]
        [Column(Order = 11)]
        public string Acao { get; set; }
        [Key]
        [Column(Order = 12)]
        public int CodFuncao { get; set; }
        [Key]
        [Column(Order = 13)]
        public string NomFuncao { get; set; }
        [Key]
        [Column(Order = 14)]
        public int CodSubFuncao { get; set; }
        [Key]
        [Column(Order = 15, TypeName = "bigint")]
        public long CodEspecificacaoDespesa { get; set; }
        [Key]
        [Column(Order = 16)]
        public string NomDespesa { get; set; }
        [Key]
        [Column(Order = 17, TypeName = "smallint")]
        public short Fonte { get; set; }
        [Key]
        [Column(Order = 18)]
        public string DesFonteRecurso { get; set; }
        [Key]
        [Column(Order = 19)]
        public int CodOrgao { get; set; }
        [Key]
        [Column(Order = 20)]
        public string NomOrgao { get; set; }
        [Key]
        [Column(Order = 21)]
        public string NomSigla { get; set; }
        [Key]
        [Column(Order = 22)]
        public string documento { get; set; }
        [Key]
        [Column(Order = 23)]
        public string DOCUMENT_NE { get; set; }
        [Key]
        [Column(Order = 24, TypeName = "money")]
        public decimal VLR_EMPENHO { get; set; }
        [Key]
        [Column(Order = 25)]
        public string DocCredor { get; set; }
        [Key]
        [Column(Order = 26)]
        public string Credor { get; set; }
        [Key]
        [Column(Order = 27)]
        public string DescricaoSubfuncao { get; set; }
        [Key]
        [Column(Order = 28, TypeName = "money")]
        public decimal ValorEmpenhada { get; set; }
        [Key]
        [Column(Order = 29, TypeName = "money")]
        public decimal ValorLiquidada { get; set; }
        [Key]
        [Column(Order = 30, TypeName = "money")]
        public decimal ValorPaga { get; set; }
        [Key]
        [Column(Order = 31, TypeName = "money")]
        public decimal valorDespesa { get; set; }
        [Key]
        [Column(Order = 32)]
        public string Status { get; set; }
    }
}