﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("TermosGlossario")]
    public class TermoGlossario
    {
        [Key]
        [MaxLength(100)]
        public string Termo { get; set; }
        public string Definicao { get; set; }
    }
}
