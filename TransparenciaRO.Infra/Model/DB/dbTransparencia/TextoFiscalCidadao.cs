﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class TextoFiscalCidadao
    {
        [Key]
        public int TextoID { get; set; }

        [Required]
        public string Texto { get; set; }

        public UnidadeGestora UnidadeGestora { get; set; }

        public Guid UnidadeGestoraId { get; set; }
    }
}