﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("DespesasPortal")]
    public class DespesaPortal
    {
        
        [Key]
        public Guid RegistroDespesa { get; set; }

        [Index("IX_Detalhamento", Order = 2)]
        public int? CodPrograma { get; set; }
        
        [Index("IX_Detalhamento", Order = 3)]
        public int? CodAcao { get; set; }

        
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        
        [Index]
        public short Exercicio { get; set; }

        [StringLength(87)]
        
        public string Programa { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEmpenhada { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorLiquidada { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorPaga { get; set; }

        [StringLength(262)]
        
        public string Acao { get; set; }

        [Column(TypeName = "money")]
        public decimal? valorDespesa { get; set; }

        
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Index("IX_Detalhamento", Order = 4)]
        public short CodProjeto { get; set; }

        
        [Column(Order = 2)]
        [StringLength(6)]
        public string Evento { get; set; }

        [Index]        
        public int? CodFuncao { get; set; }

        [StringLength(80)]
        public string NomFuncao { get; set; }

        public int? CodSubFuncao { get; set; }

        
        [Index("IX_Detalhamento", Order = 5)]
        public int? Fonte { get; set; }

        [StringLength(100)]
        public string DesFonteRecurso { get; set; }

        [StringLength(255)]
        public string NomDespesa { get; set; }

        [Index("IX_Detalhamento", Order = 6)]
        public long? EspecificacaoDespesa { get; set; }

        [StringLength(80)]
        public string NomOrgao { get; set; }

        
        [Column(Order = 3)]
        [StringLength(20)]
        public string NumEmpenho { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        
        [Column(Order = 4)]
        [Index]
        [Index("IX_Detalhamento", Order = 1)]
        public DateTime DataDocumento { get; set; }

        [StringLength(45)]
        public string Credor { get; set; }

        [StringLength(14)]
        public string DocCredor { get; set; }


        [Column(Order = 5)]
        [StringLength(6)]
        public string UG { get; set; }

        [StringLength(100)]
        public string DescricaoSubFuncao { get; set; }
    }
}
