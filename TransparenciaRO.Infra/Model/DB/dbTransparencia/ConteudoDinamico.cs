﻿using System;
using System.Collections.Generic;
using TransparenciaRO.Infra.Enum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ConteudosDinamicos")]
    public class ConteudoDinamico
    {
        [Key]
        public Guid ConteudoDinamicoId { get; set; }

        [MaxLength(50)]
        public string Titulo { get; set; }

        [MaxLength(1000)]
        public string Descricao { get; set; }

        public DateTime DataCriacao { get; set; }
        
        public string ConteudoHTML { get; set; }

        /// <summary>
        /// Se não possuir itens na lista, significa que o conteúdo não possui restrição de perfis
        /// </summary>
        public List<EPerfilUsuario> PerfisAutorizados { get; set; }
        
    }
}
