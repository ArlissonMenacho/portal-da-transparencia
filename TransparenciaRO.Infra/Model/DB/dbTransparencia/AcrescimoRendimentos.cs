﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class AcrescimoRendimentos
    {
        [Key]
        public long? codvba { get; set; }
        public string Nome { get; set; }
        [Column(TypeName = "bigint")]
        public long? CPF { get; set; }
        public string NomVba { get; set; }
        public decimal? valorverba { get; set; }
        public string Ano { get; set; }
        public string Mes { get; set; }
    }
}
