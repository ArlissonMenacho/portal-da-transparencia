﻿using System;
using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class InscricaoComiteTransparencia
    {
        public InscricaoComiteTransparencia()
        {
            //
        }

        public InscricaoComiteTransparencia(Guid inscricaoId, string razaoSocial, string cnpj, string representanteLegal, string telefone)
        {
            InscricaoId = inscricaoId;
            RazaoSocial = razaoSocial;
            Cnpj = cnpj;
            RepresentanteLegal = representanteLegal;
            Telefone = telefone;
            Protocolo = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
        }

        public Guid InscricaoId { get; private set; }

        public long Protocolo { get; private set; }

        public string RazaoSocial { get; private set; }

        public string Cnpj { get; private set; }

        public string RepresentanteLegal { get; private set; }

        public string Telefone { get; private set; }

        public virtual ICollection<Arquivo> Arquivos { get; private set; }
    }
}
