﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class ServidoresDesligadosExonerados
    {
        [Key]
        public int ServidoresDesligadosExoneradosId { get; set; }
        public string Matricula { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public DateTime Admissao { get; set; }

        [Required]
        public DateTime Desligamento { get; set; }

        [Required]
        public string Cpf { get; set; }
        
        [Required]    
        public string CausaDesligamento { get; set; }

        [Index("IDX_ANOMES",1), Required]
        public int Ano { get; set; }

        [Index("IDX_ANOMES",2), Required]
        public int Mes { get; set; }

        public Guid LotacaoId { get; set; }

        public virtual Lotacao Lotacao { get; set; }

    }
}
