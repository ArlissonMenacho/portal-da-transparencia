﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public interface IClasseComArquivos
    {
        ICollection<Arquivo> Arquivos { get; set; }

        Guid GetId();
        void SetId(Guid pId);        
    }
}
