﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class ContatoInstitucional
    {
        public Guid ContatoInstitucionalId { get; set; }

        public string Sigla { get; set; }

        public string Descricao { get; set; }

        public string Competencias { get; set; }

        public string LegistacaoAplicavel { get; set; }

        public string AtendimentoAoPublico { get; set; }

        public string ExpedienteInterno { get; set; }

        public string RegulamentoHorario { get; set; }

        public string EstruturaOrganizacional { get; set; }

        public string Endereco { get; set; }

        public string Telefone { get; set; }

        public virtual Collection<CargosInstituicao> CargosInstituicoes { get; set; }
    }
}
