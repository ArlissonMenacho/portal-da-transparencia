﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class PortariaResolucao
    {
        public PortariaResolucao()
        {
            UnidadeGestoraId = new Guid();
            Arquivos = new List<Arquivo>();
        }

        public Guid PortariaResolucaoId { get; set; }
        public ICollection<Arquivo> Arquivos { get; set; }

        public ETipoDocumento ETipoDocumento { get; set; }

        [Required]
        public DateTime Data { get; set; }

        public DateTime DataPublicacao { get; set; }
        
        [Required]
        public int Numero { get; set; }

        public string Assunto { get; set; }

        [DisplayName("Unidade Gestora")]
        public Guid UnidadeGestoraId { get; set; }

        [Column("unidadeGestoraID")]
        public virtual UnidadeGestora UnidadeGestora { get; set; }

    }
}
