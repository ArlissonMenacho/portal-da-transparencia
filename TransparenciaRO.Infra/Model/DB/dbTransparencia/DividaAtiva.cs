﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class DividaAtiva
    {
        [Key]
        public int Id { get; set; }

        [StringLength(14)]
        public string CpfCnpj { get; set; }

        // Nome ou Razão social
        public string Nome { get; set; }
        
        public decimal ValorTotal { get; set; }

        public decimal ValorPago { get; set; }

        public decimal ValorAPagar { get; set; }

        public string InscricaoEstadual { get; set; }

        public string InscricaoCDA { get; set; }
        
        public string Cartorio { get; set; }
        
        public string DocumentoCartorio { get; set; }
        
        public DateTime DataConfirmacaoCartorio { get; set; }
        
        public string Acao { get; set; }

        public string Situacao { get; set; }

        public string CodigoReceita { get; set; }

        public string GuiaLancamento { get; set; }
    }
}
