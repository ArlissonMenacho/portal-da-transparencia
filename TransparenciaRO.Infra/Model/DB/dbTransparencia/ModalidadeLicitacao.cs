using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("ModalidadeLicitacao")]
    public partial class ModalidadeLicitacao
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo { get; set; }

        [StringLength(50)]
        public string Descricao { get; set; }
    }
}
