﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class ControleDoumentoExterno
    {
        public ControleDoumentoExterno()
        {
            Arquivos = new List<Arquivo>();
        }
        [Key]
        public Guid ControleDoumentoExternoId { get; set; }

        [Display(Name = "Nº Processo / Documento")]
        public string NumeroProcessoDocumento { get; set; }
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        public int Prazo { get; set; }
        public int? ProrrogacaoDePrazo { get; set; }

        [Display(Name = "Data de Abertura")]
        public DateTime DataOrigem { get; set; }
        public ETipoStatus ETipoStatus { get; set; }
        public DateTime? DataFinalizacao { get; set; }

        public Guid UnidadeGestoraId { get; set; }
        public UnidadeGestora UnidadeGestora { get; set; }
        public ICollection<Arquivo> Arquivos { get; set; }
    }
}
