﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class FiscalCidadao
    {

        public FiscalCidadao()
        {
            ProtocoloId = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            UnidadeGestoraId = new Guid("0A00ECFE-B503-4E86-BA0F-D5074AEA30AC");
            DataFiscalizacao = DateTime.Now;
            EStatus = ETipoStatus.EmAberto;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Protocolo")]
        public long ProtocoloId { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Categoria { get; set; }
        
        public string Telefone { get; set; }

        [Required]
        public string Mensagem { get; set; }
        
        [Display(Name = "Manter Sigilo")]
        public string SigiloDaInformacoes { get; set; }
        
        public string Imagem { get; set; }
        
        public string ExtensaoImagem { get; set; }

        [Required]
        [Display(Name= "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Data da Fiscalização")]
        public DateTime DataFiscalizacao { get; set; }

        public Guid UnidadeGestoraId { get; set; }

        [Display(Name = "Órgão Vinculado")]
        public virtual UnidadeGestora UnidadeGestora { get; set; }

        public ETipoStatus EStatus { get; set; }

        public DateTime? DataDaFinaliacao { get; set; }

        public Guid? UsuarioId { get; set; }

        public Usuario Usuario { get; set; }

        public ICollection<FluxoFiscalCidadao> FluxoFiscalCidadaos { get; set; }


        public virtual ICollection<RespostaFiscalCidadao> RespostaFiscalCidadao { get; set; }

    }
}
