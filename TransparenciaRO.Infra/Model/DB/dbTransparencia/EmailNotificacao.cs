﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class EmailNotificacao
    {
        public EmailNotificacao()
        {
            //
        }

        [Key]
        public Guid Id { get; set; }

        [Required, MaxLength(160)]
        public string Nome { get; set; }

        [Required, DisplayName("E-mail"), MaxLength(160)]
        public string Email { get; set; }

        [DisplayName("Nova Dispensa de Licitação COVID")]
        public bool NotificarDispensaLicitacao { get; set; }

        [DisplayName("Nova Íntegra de Processo COVID")]
        public bool NotificarIntegraProcesso { get; set; }

        [DisplayName("Novo Boletim de Controle COVID")]
        public bool NotificarBoletim { get; set; }

        public void AlterarDados(string nome, bool notificarDispensaLicitacao, bool notificarIntegraProcesso, bool notificarBoletim)
        {
            Nome = nome;
            NotificarDispensaLicitacao = notificarDispensaLicitacao;
            NotificarIntegraProcesso = notificarIntegraProcesso;
            NotificarBoletim = notificarBoletim;
        }

        public void DesabilitarNotificacoes()
        {
            NotificarDispensaLicitacao = false;
            NotificarIntegraProcesso = false;
            NotificarBoletim = false;
        }
    }
}
