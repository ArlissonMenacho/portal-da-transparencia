﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
   public class FluxoFiscalCidadao
    {
        
        [Key]
        public int Id { get; set; }
        public DateTime DataMovimentacao { get; set; }
        public string Observacao { get; set; }
        
        public long ProtocoloId { get; set; }
        public virtual FiscalCidadao FiscalCidadao { get; set; }
        
        public Guid OrgaoOrigemId { get; set; }
        [ForeignKey("OrgaoOrigemId")]
        public virtual UnidadeGestora OrgaoOrigem { get; set; }

        public Guid OrgaoDestinoId { get; set; }
        [ForeignKey("OrgaoDestinoId")]
        public virtual UnidadeGestora OrgaoDestino { get; set; }


        public Guid UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
