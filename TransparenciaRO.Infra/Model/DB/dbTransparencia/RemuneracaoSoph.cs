﻿using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RemuneracaoSoph
    {
        [Key]
        public long RemuneracaoId { get; set; }
        [Required]
        public int Mes { get; set; }
        [Required]
        public int Ano { get; set; }
        [Required]
        public int CodEmpregado { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cargo { get; set; }
        [Required]
        public string Departamento { get; set; }
        [Required]
        public string Cpf { get; set; } 
        [Required]
        public decimal Salario { get; set; }
        [Required]
        public decimal OutrosProventos { get; set; }
        [Required]
        public decimal TotalProventos { get; set; }
        [Required]
        public decimal Inss { get; set; }
        [Required]
        public decimal Irrf { get; set; }
        [Required]
        public decimal OutrosDescontos { get; set; }
        [Required]
        public decimal TotalDescontos { get; set; }
        [Required]
        public decimal TotalLiquido { get; set; }
        [Required]
        public decimal Fgts { get; set; }

        public EEmpresa EEmpresa { get; set; }
        
    }
}