﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Utils;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Permissoes")]
    public class Permissao
    {
        [Key]
        public EPerfilUsuario PermissaoId { get; set; }        
        public string Descricao { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
