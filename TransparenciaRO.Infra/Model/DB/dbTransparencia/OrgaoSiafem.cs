using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("OrgaosSiafem")]
    public partial class OrgaoSiafem
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(6)]
        public string ORGAO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(150)]
        public string NOMEORGAO { get; set; }

        public int? Ad { get; set; }

        public byte? Poder { get; set; }

        [StringLength(14)]
        public string CGC { get; set; }
    }
}
