﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class PrestacaoDeContas
    {
        public PrestacaoDeContas()
        {
            Id = Guid.NewGuid();
            ArquivoResultadoConclusivo = new List<Arquivo>();
        }
        public Guid Id { get; set; }
        public DateTime? DataPrevista { get; set; }
        public DateTime? DataApresentacao { get; set; }
        public DateTime? PrazoAnalise { get; set; }
        public ICollection<Arquivo> ArquivoResultadoConclusivo { get; set; }
    }
}
