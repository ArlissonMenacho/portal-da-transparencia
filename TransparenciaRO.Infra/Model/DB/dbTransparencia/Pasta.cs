﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    /// <summary>
    /// Classe de abstração do conceito de pastas. Os uploads do portal da transparência serão guardados hierarquicamente em pastas, porém será um conceito lógico, uma vez que as pastas não serão efetivamente criadas na estrutura de arquivos do servidor, e a apenas categorizadas no BD
    /// </summary>
    [Table("Pastas")]
    public class Pasta
    {
        public Pasta()
        {
            PastaId = Guid.NewGuid();
        }

        public Guid PastaId { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        public byte[] IconeImagem { get; set; }

        public string TipoImagem { get; set; }

        /// <summary>
        /// Caso seja informado, agrupa as pastas em menus distintos na exibição para o usuário final
        /// </summary>
        public int? Agrupamento { get; set; }

        public EFontAwesome? IconeFa { get; set; }

        /// <summary>
        /// Indica se a "pasta" se comportará como uma opção do menu que abrirá um link externo (e não uma sub-pasta ou arquivo). Isto será usado apenas pelo menu e não deverá constar no CRUD de pastas.
        /// </summary>
        [Index]
        public string Href { get; set; }

        /// <summary>
        /// Indica se é uma "pasta-raíz", ou seja, o usuário administrador não poderá editar seus dados ou dados de seus ancestrais
        /// </summary>
        public bool Contexto { get; set; }

        [Index]
        public DateTime? DataRemocao { get; set; }

        [Index]
        public Guid? PastaOrigemId { get; set; }

        /// <summary>
        /// Uma pasta administrável é utilizada como tipo de serviço, onde usuários (com suas devidas permissões poderão inserir, remover ou editar arquivos e pastas).
        /// Uma pasta não-administrável (false) são pastas vitalícias do sistema, que não poderão ser alteradas por ninguém, a não ser a própria equipe de desenvolvimento
        /// </summary>
        public bool Administravel { get; set; }

        /// <summary>
        /// Uma pasta pública pode ser visualizada no CRUD de pastas (tanto para administradores como visão pública e aberta)
        /// </summary>
        public bool Publica { get; set; }

        /// <summary>
        /// Ordem de exibição do item no menu
        /// </summary>
        public int Ordem { get; set; }

        /// <summary>
        /// Define a cor que as SubPastas devem adotar
        /// </summary>
        public string TemaCss { get; set; }

        public virtual Pasta PastaOrigem { get; set; }

        public EPerfilUsuario? PerfilPasta { get; set; }

        public virtual ICollection<Pasta> SubPastas { get; set; }

        public virtual ICollection<Arquivo> Arquivos { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }

        public virtual ICollection<ImagemPasta> Imagens { get; set; }
    }
}