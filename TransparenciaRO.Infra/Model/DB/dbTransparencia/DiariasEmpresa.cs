﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class DiariasEmpresa
    {
        public DiariasEmpresa()
        {
            DiariasEmpresaId = Guid.NewGuid();

        }

        public Guid DiariasEmpresaId { get; set; }

        [Required]
        public string FonteRecurso { get; set; }

        [Required]
        public string NumeroProcesso { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Cargo { get; set; }

        [Required]
        public string Destino { get; set; }

        [Required]
        public DateTime Ida { get; set; }

        [Required]
        public DateTime Volta { get; set; }

        [Required]
        public string Motivo { get; set; }

        [Required]
        public string Transporte { get; set; }

        public string DadosVeiculo { get; set; }

        public string ValorPassagem { get; set; }

        [Required]
        public decimal ValorDiaria { get; set; }

        [Required]
        public string TotalDiarias { get; set; }

        [Required]
        public decimal ValorTotal { get; set; }

        [Required]
        public int Ano { get; set; }

        [Required]
        public int Mes { get; set; }

        public EEmpresa Empresa { get; set; }

        public DateTime? DataPublicacao { get; set; }
        public string OrdemBancaria { get; set; }
        public DateTime? DataPagamento { get; set; }
    }
}
