namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Diarias")]
    public partial class Diaria
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string Documento { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DataDocumento { get; set; }

        [StringLength(20)]
        public string CPF { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string Nome { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(6)]
        public string UG { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(150)]
        public string NomeOrgao { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorDocumento { get; set; }
    }
}
