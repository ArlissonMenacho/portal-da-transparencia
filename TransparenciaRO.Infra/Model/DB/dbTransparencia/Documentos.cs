﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class Documentos
    {
        [Key]
        public int DocumentoID { get; set; }
        public ETipoDocumento TipoDocumento { get; set; }
        public DateTime Data { get; set; }

        [StringLength(20)]
        public string Numero { get; set; }
        public string Assunto { get; set; }
        public string Anexo { get; set; }
    }
}
