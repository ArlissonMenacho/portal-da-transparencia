namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DotacaoInicial")]
    public partial class DotacaoInicial
    {
        [Key]
        [Column(Order = 0)]
        public string CodigoOrgao { get; set; }

        [Key]
        [Column(Order = 1)]
        public string Orgao { get; set; }

        [Key]
        [Column(Order = 2)]
        public string ProgramaTrabalho { get; set; }

        [Key]
        [Column(Order = 3)]
        public string NaturezaDespesa { get; set; }

        [Key]
        [Column(Order = 4)]
        public string DescricaoDespesa { get; set; }

        [Key]
        [Column(Order = 5)]
        public int Exercicio { get; set; }

        [Key]
        [Column(Order = 6)]
        public decimal SaldoInicial { get; set; }
    }
}
