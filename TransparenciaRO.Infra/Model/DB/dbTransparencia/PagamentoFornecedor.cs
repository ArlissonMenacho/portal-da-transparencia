﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class PagamentoFornecedor
    {
        public PagamentoFornecedor()
        {
            PagamentoFornecedorId = Guid.NewGuid();
        }

        public Guid PagamentoFornecedorId { get; set; }
        public int Exercicio { get; set; }
        public string Processo { get; set; }
        public string Evento { get; set; }
        public string NaturezaDespesa { get; set; }
        public long EspecificacaoDespesa { get; set; }
        public long CodNomeDespesa { get; set; }
        public int CodCategoriaDespesa { get; set; }
        public int CodGrupoDespesa { get; set; }
        public int CodModalidadeAplicacaoDespesa { get; set; }
        public int CodProjeto { get; set; }
        public string Projeto { get; set; }
        public int CodPrograma { get; set; }
        public string Programa { get; set; }
        public int CodAcao { get; set; }
        public string Acao { get; set; }
        public int CodFuncao { get; set; }
        public string Funcao { get; set; }
        public int CodSubFuncao { get; set; }
        public string SubFuncao { get; set; }
        public int CodFonte { get; set; }
        public string Fonte { get; set; }
        public string CodUnidadeGestora { get; set; }
        public string UnidadeGestora { get; set; }
        public string DocCredor { get; set; }
        public string Credor { get; set; }
        public int CodModlidadeLicitacao { get; set; }
        public string ModalidadeLicitacao { get; set; }
        public string Documento { get; set; }
        public DateTime DataDocumento { get; set; }
        public string DocumentoNE { get; set; }
        public string DocumentoOB { get; set; }
        public int CodTipoEmpennho { get; set; }
        public string TipoEmpenho { get; set; }
        public DateTime DataEmpenho { get; set; }
        public string NumEmpenho { get; set; }
        //public decimal? ValorEmpenhada { get; set; }
        //public decimal? ValorLiquidada { get; set; }
        public decimal? ValorPaga { get; set; }
        //public decimal? ValorDespesa { get; set; }
        [StringLength(11)]
        public string NumeroDocumentoLiquidacao { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? DataDocumentoLiquidacao { get; set; }
        public string ObjetivoDocumentoLiquidacao { get; set; }
    }
}