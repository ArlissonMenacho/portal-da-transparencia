﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RemuneracaoServidor
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Ano { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte Mes { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(10)]
        public string Matricula { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Sequencia { get; set; }

        [StringLength(255)]
        public string Nome { get; set; }

        [StringLength(11)]
        public string Cpf { get; set; }

        [StringLength(255)]
        public string Cargo { get; set; }

        [StringLength(255)]
        public string Classificacao_Func { get; set; }

        [StringLength(255)]
        public string Lotacao { get; set; }
        [StringLength(255)]
        public string CDSFG { get; set; }
        [StringLength(255)]
        public string SubLotacao { get; set; }

        [Column(TypeName = "money")]
        public decimal? Vencimentos { get; set; }

        [Column(TypeName = "money")]
        public decimal? Auxilios { get; set; }

        [Column(TypeName = "money")]
        public decimal? Vantagens { get; set; }

        [Column(TypeName = "money")]
        public decimal? VbaTemporarias { get; set; }

        [Column(TypeName = "money")]
        public decimal? VbaProdutividade { get; set; }

        [Column(TypeName = "money")]
        public decimal? VbaPrevidencias { get; set; }

        [Column(TypeName = "money")]
        public decimal? VbaImpostorenda { get; set; }

        [Column(TypeName = "money")]
        public decimal? VbaDescontosDiversos { get; set; }

        [Column(TypeName = "money")]
        public decimal? RendimentosTributaveis { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalDescontosFinanceiros { get; set; }

        [Column(TypeName = "money")]
        public decimal? Liquido { get; set; }

        public int? CargaHoraria { get; set; }
    }
}