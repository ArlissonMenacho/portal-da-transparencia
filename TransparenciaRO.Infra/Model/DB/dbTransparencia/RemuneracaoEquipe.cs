﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RemuneracaoEquipe
    {
        //public RemuneracaoEquipe(Guid termoFomentoId, ContratoConvenio contratoConvenios)
        //{
        //    Id = Guid.NewGuid();
        //    TermoFomentoId = termoFomentoId;
        //    ContratoConvenio = contratoConvenios;
        //}
        public RemuneracaoEquipe()
        {
            Id = Guid.NewGuid();
            //
        }


        public Guid Id { get; set; }

        public string Nome { get; set; }

        public Guid TermoFomentoId { get; set; }

        public TermoFomento TermoFomento { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }

        public string Cargo { get; set; }

        public double ValorTotal { get; set; }

        public decimal ValorIndividual { get; set; }
    }
}
