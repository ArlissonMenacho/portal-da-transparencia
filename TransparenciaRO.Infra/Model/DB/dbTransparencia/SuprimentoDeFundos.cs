﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class SuprimentoDeFundos
    {
        public SuprimentoDeFundos()
        {
            //
        }
       
        public SuprimentoDeFundos(short exercicio, int TipoLicitacao, short codProjeto, string evento, string numEmpenho, DateTime dataDocumento, string uG, int codPrograma, int codAco, string programa, string acao, int codFuncao, string nomeFuncao, int codSubFuncao, long codEspecificaoDespesa, string despFonteRecurso, short fonte, string nomeDespesa, long especificacaoDespesa, string nomeOrgao, string doc, string docCredor, string credor, string descricaoSubFuncao, decimal valorEmpenhada, decimal valorLiquida, decimal valorPaga, decimal valorDespesa)
        {

            Exercicio             = exercicio;
            tipoLicitacao         = TipoLicitacao;
            CodProjeto            = codProjeto;
            EVENTO                = evento;
            NumEmpenho            = numEmpenho;
            DataDocumento         = dataDocumento;
            UG                    = uG;
            CodPrograma           = codPrograma;
            CodAco                = codAco;
            Programa              = programa;
            Acao                  = acao;
            CodFuncao             = codFuncao;
            NomFuncao             = nomeFuncao;
            CodSubFuncao          = codSubFuncao;
            CodEspecificaoDespesa = codEspecificaoDespesa;
            DesFonteRecurso       = despFonteRecurso;
            Fonte                 = fonte;
            NomDespesa            = nomeDespesa;
            EspecificacaoDespesa  = especificacaoDespesa;
            NomOrgao              = nomeOrgao;
            documento             = doc;
            DocCredor             = docCredor;
            Credor                = credor;
            DescricaoSubFuncao    = descricaoSubFuncao;
            ValorEmpenhada        = valorEmpenhada;
            ValorLiquidada        = valorLiquida;
            ValorPaga             = valorPaga;
            ValorDespesa          = valorDespesa;
        }

        
        [Column(Order = 0, TypeName = "smallint")]     
        public short Exercicio { get; set; }

        public int tipoLicitacao { get; set; }

        [Column(Order = 2, TypeName = "smallint")]
        public short CodProjeto { get; set; }

        [StringLength(6)]
        public string EVENTO { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(20)]
        public string NumEmpenho { get; set; }

        public DateTime DataDocumento { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(6)]
        public string UG { get; set; }

        public int CodPrograma { get; set; }

        public int CodAco { get; set; }

        [StringLength(80)]
        public string Programa { get; set; }

        [StringLength(255)]
        public string Acao { get; set; }

        public int CodFuncao { get; set; }

        [StringLength(80)]
        public string NomFuncao { get; set; }

        public int CodSubFuncao { get; set; }

        [Column(Order = 14, TypeName = "bigint")]
        public long CodEspecificaoDespesa { get; set; }

        [StringLength(100)]
        public string DesFonteRecurso { get; set; }

        [Column(Order = 16, TypeName = "smallint")]
        public short Fonte { get; set; }

        public string NomDespesa { get; set; }

        [Column(Order = 18, TypeName = "bigint")]
        public long EspecificacaoDespesa { get; set; }

        [StringLength(80)]
        public string NomOrgao { get; set; }

        [StringLength(40)]
        public string documento { get; set; }

        [Key]
        [Column(Order = 21)]
        [StringLength(255)]
        public string DocCredor { get; set; }
        
        [StringLength(255)]
        public string Credor { get; set; }

        [StringLength(100)]
        public string DescricaoSubFuncao { get; set; }

        [Column(Order = 24, TypeName = "money")]
        public decimal ValorEmpenhada { get; set; }

        [Column(Order = 25, TypeName = "money")]
        public decimal ValorLiquidada { get; set; }

        [Column(Order = 26, TypeName = "money")]
        public decimal ValorPaga { get; set; }

        [Column(Order = 27, TypeName = "money")]
        public decimal ValorDespesa { get; set; }

    }
}
