﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class RegistroBoletim
    {
        public RegistroBoletim()
        {
            //
        }

        public RegistroBoletim(Guid boletimId, DateTime data, string descricao)
        {
            BoletimId = boletimId;
            Data = data;
            Descricao = descricao;
        }

        [Key]
        public Guid BoletimId { get; set; }

        public DateTime Data { get; set; }

        public string Descricao { get; set; }       
        
        [NotMapped]
        public virtual Arquivo Arquivo { get; set; }
    }
}
