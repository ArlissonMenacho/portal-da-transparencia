﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class FundoEmpresa
    {
        public FundoEmpresa()
        {
            FundoId = Guid.NewGuid();
        }
        [Key]
        public Guid FundoId { get; set; }

        [DisplayName("Data entrada")]
        public DateTime DataEntrada { get; set; }
        [Required]
        public string Processo { get; set; }
        [Required]
        public string Objeto { get; set; }
        [Required]
        [DisplayName("Valor estimado")]
        public decimal ValorEstimado { get; set; }
        public string Modalidade { get; set; }
        public string Vigencia { get; set; }
        [DisplayName("Data decisão")]
        public DateTime DataDecisao { get; set; }
        [DisplayName("Data publicação")]
        public DateTime DataPublicacao { get; set; }
        [DisplayName("Fundamento geral")]
        public string FundamentoGeral { get; set; }
        [Required]
        public string Favorecido { get; set; }
        [Required]
        [DisplayName("CPF/CNPJ")]
        public string CnpjCpf { get; set; }
        [DisplayName("Data contabilizada")]
        public DateTime DataContabilizada { get; set; }
        [DisplayName("Data pagamento")]
        public DateTime DataPagamento { get; set; }
        [DisplayName("Data arquivamento")]
        public DateTime DataArquivamento { get; set; }
        [DisplayName("Situação")]
        public string Situacao { get; set; }

        public EEmpresa EEmpresa { get; set; }

    }
}