﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("VW_DadosLicitacao2020")]
    public class DadosLicitacao
    {
        public DadosLicitacao()
        {
            //
        }

        [Key]
        public Guid ItemLicitacaoId { get; set; }

        public string UnidadeGestoraNome { get; set; }

        public string UnidadeGestoraSigla { get; set; }

        [DisplayName("Unidade Gestora")]
        public string UnidadeGestora { get => UnidadeGestoraSigla + " - " + UnidadeGestoraNome; }

        public Guid LicitacaoId { get; set; }

        [DisplayName("Processo administrativo")]
        public string NumeroProcessoAdministrativo { get; set; }

        [DisplayName("Instrumento contratual")]
        public string Contrato { get; set; }

        public string CNPJ { get; set; }

        [DisplayName("Razão social")]
        public string RazaoSocial { get; set; }

        [DisplayName("Data de assinatura")]
        public DateTime DataAssinatura { get; set; }

        [DisplayName("Vigência / Prazo de entrega (em dias)")]
        public string Vigencia { get; set; }

        [DisplayName("Local de execução")]
        public string LocalExecucao { get; set; }

        public string Objeto { get; set; }

        public int Quantidade { get; set; }

        public string Unidade { get; set; }

        [DisplayName("Valor unitário (em R$)")]
        public decimal? ValorUnitario { get; set; }

        [DisplayName("Valor global (quantidade x valor unitário | em R$)")]
        public decimal? ValorGlobal { get; set; }

        public Guid FornecedorArquivoId { get; set; }

        public ETipoAnexo FornecedorTipoAnexo { get; set; }

        public Guid LicitacaoArquivoId { get; set; }

        public ETipoAnexo LicitacaoTipoAnexo { get; set; }

        [DisplayName("Modadlidade")]
        public EModalidadeLicitacao ModalidadeLicitacao { get; set; }

        [DisplayName("Status da entrega")]
        public EStatusItemLicitacao Status { get; set; }

        public int? Ano { get; set; }

        [DisplayName("Número do certame / Ano")]
        public int? NumeroLicitacao { get; set; }

        public Guid? TermoAditivoArquivoId { get; set; }     

        public ETipoAnexo? TermoAditivoTipoAnexo { get; set; }


        [NotMapped]
        public List<ContratacaoAditivadas> ContratacaoAditivadas { get; set; }

        [NotMapped]
        public ICollection<Arquivo> ArquivosTermosAditivos { get; set; }
    }
}
