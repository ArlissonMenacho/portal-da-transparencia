﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("Licitacoes")]
    public class Licitacao : IClasseComArquivos
    {
        public Licitacao()
        {
            Arquivos = new List<Arquivo>();
            LicitacaoId = new Guid();
        }

        [Key]
        public Guid LicitacaoId { get; set; }
        public int? NumeroLicitacao { get; set; }
        public int? Ano { get; set; }
        public string Objeto { get; set; }
        public Guid UnidadeGestoraId { get; set; }
        public string NumeroProcessoAdministrativo { get; set; }
        public string FonteDeRecurso { get; set; } // Exemplo: 0100
        public string ProjetoAtividade { get; set; } // Exemplo: 2087
        public string ElementoDespesa { get; set; } // Exemplo: 33.90.30
        public decimal ValorEstimado { get; set; }
        public decimal Valor { get; set; }
        public DateTime? DataHoraAbertura { get; set; }


        [Column("ModalidadeLicitacao")]
        public string ModalidadeLicitacaoString
        {
            get { return ModalidadeLicitacao.ToString(); }
            private set { ModalidadeLicitacao = value.ParseEnum<EModalidadeLicitacao>(); }
        }

        [NotMapped]
        public EModalidadeLicitacao ModalidadeLicitacao { get; set; }

        public EFusoHorario? EFusoHorario { get; set; }
        public string EnderecoEletronico { get; set; }
        public string Local { get; set; }
        public string MaisInformacoes { get; set; }
        public string Pregoeiro { get; set; }
        public string Empresa { get; set; }
        public string Cnpj { get; set; }
        public DateTime DataDaPublicacao { get; set; }

        [NotMapped]
        public virtual Arquivo ArquivoTemporario { get; set; }

        public ICollection<Arquivo> Arquivos { get; set; }
        public virtual UnidadeGestora UnidadeGestora { get; set; }

        public List<FornecedorDispensa> Fornecedores { get; set; }

        public List<ItemLicitacao> ItensLicitacao { get; set; }

        [StringLength(50)]
        public string Sigla { get; set; }

        public Guid GetId()
        {
            return this.LicitacaoId;
        }

        public void SetId(Guid pId)
        {
            this.LicitacaoId = pId;
        }

        public decimal CalcularValorTotalDispensaLicitacao()
        {
            if (Fornecedores.Any())
                return Fornecedores.Sum(f => f.Valor);

            return Valor;
        }
    }
}
