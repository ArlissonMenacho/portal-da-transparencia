﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    [Table("TBContratacaoAditivadas")]
    public class ContratacaoAditivadas
    {
        [Key]
        [Column(Order = 0)]
        public string NumeroProcesso { get; set; }
        [Key]
        [Column(Order = 1)]
        public string NumeroContrato { get; set; }
        public string TipoFormalizacao { get; set; }
        public string TipoAlteracao { get; set; }
        public string DataAlteracao { get; set; }
        public string DataVigencia { get; set; }
        public string Quantidade { get; set; }
        public decimal ValorAcrescido { get; set; }
    }
}