namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EmpenhosMunicipios
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short EXERCICIO { get; set; }

        [StringLength(45)]
        public string Credor { get; set; }

        [StringLength(68)]
        public string CredorCNPJ { get; set; }

        [StringLength(14)]
        public string doccredor { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoMunicipio { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodigoMunicipioDV { get; set; }

        [StringLength(255)]
        public string NomeMunicipio { get; set; }

        public short? ANOINST { get; set; }

        public decimal? LATITUDE { get; set; }

        public decimal? LONGITUDE { get; set; }

        public decimal? ALTITUDE { get; set; }

        public decimal? AREA { get; set; }

        public int? CodFuncao { get; set; }

        [StringLength(80)]
        public string NomFuncao { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEmpenhado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEmpenhadoALiquidar { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorEmpenhadoAPagar { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorLiquidado { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorLiquidadoAPagar { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorPago { get; set; }
    }
}
