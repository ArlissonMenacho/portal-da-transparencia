﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.DB.dbTransparencia
{
    public class CargosInstituicao
    {
        public Guid CargosInstituicaoId { get; set; }

        public string Cargo { get; set; }

        public string Ocupante { get; set; }

        public Guid InstituicaoId { get; set; }

        public virtual ContatoInstitucional Instituicao { get; set; }
    }
}
