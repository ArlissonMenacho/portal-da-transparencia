﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View
{
    public class ArquivoUploadViewModel
    {
        public HttpPostedFileBase File { get; set; }

        public string Descricao { get; set; }

        public ETipoAnexo TipoAnexo { get; set; }

        public Guid FornecedorDispensaId { get; set; }
    }
}
