﻿using System;
using TransparenciaRO.Infra.Enum;

namespace TransparenciaRO.Infra.Model.View
{
    public class ReceitaModel
    {
        public ReceitaModel()
        {
            //
        }
        public String Referencia { get; set; }
        public decimal ValorArrecadado { get; set; }
        public decimal ValorPrevisto { get; set; }        
        public decimal ValorAcumulado { get; set; }        
        public int? Mes { get; set; }
        public int? Ano { get; set; }

        public string StrValorArrecadado => ValorArrecadado.ToString("C2");
        public string StrValorPrevisto => ValorPrevisto.ToString("C2");
        public string StrValorAcumulado => ValorAcumulado.ToString("C2");

        public static string GetDescricaoEspecificacaoReceita(EEspecificacaoReceita pElemento)
        {
            switch (pElemento)
            {
                case EEspecificacaoReceita.ReceitaCorrente:
                    return "Receita corrente";
                case EEspecificacaoReceita.ReceitaCapital:
                    return "Receita capital";
                case EEspecificacaoReceita.ReceitaIntraOrcamentaria:
                    return "Receita Intra-Orçamentária";
                case EEspecificacaoReceita.Tributos:
                    return "Tributos";
                case EEspecificacaoReceita.Total:
                    return "Total";
                default:
                    return "";
            }
        }
    }


}


