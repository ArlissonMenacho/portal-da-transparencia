﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View
{
    public class FornecedorViewModel
    {
        public FornecedorViewModel()
        {
            FornecedorId = Guid.NewGuid();
        }

        public Guid FornecedorId { get; set; }

        [Required(ErrorMessage = "Informe seu Nome / Razão Social")]
        [Display(Name = "Razão Social")]
        public string RazaoSocial { get; set; }

        [Required(ErrorMessage = "Selecione o tipo de pessoa")]
        public ETipoPessoa TipoPessoa { get; set; }

        [Required]
        [Display(Name = "CPF / CNPJ")]
        public string CpfCnpj { get; set; }

        [Required(ErrorMessage = "Selecione um estado")]
        public EEstado Uf { get; set; }

        [Required(ErrorMessage = "Selecione uma cidade")]
        public Guid CidadeId { get; set; }

        public virtual Cidade Cidade { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        public ICollection<FornecedorImpedido> Impedimentos { get; set; }

        //Converte para o model Fornecedor
        public Fornecedor ModelFornecedor()
        {
            return new Fornecedor
            {
                CpfCnpj = CpfCnpj,
                RazaoSocial = RazaoSocial,
                CidadeId = CidadeId,
                Endereco = Endereco,
                FornecedorId = FornecedorId,
                TipoPessoa = TipoPessoa,
                Uf = Uf
            };
        }
    }
}