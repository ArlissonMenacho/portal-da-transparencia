﻿using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.View
{
    public class ResultadoPregaoViewModel
    {
        public string NomeFornecedor { get; set; }
        public List<Item> Itens { get; set; }
    }

    public partial class Item
    {
        public string UnidadeFornecimento { get; set; }
        public string Descricao { get; set; }
        public string Quantidade { get; set; }
        public double? Valor { get; set; }
        public double? ValorTotal { get; set; }
        public string Marca { get; set; }
        public string DescricaoDetalhada { get; set; }
    }
}
