﻿using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System;

namespace TransparenciaRO.Infra.Model.View
{
    public class AdicionarEditarNoticiaViewModel
    {
        public string EncIdNoticia { get; set; }
        public string TituloNoticia { get; set; }
        public string HTMLNoticia { get; set; }
        public string DataUltimaAtualizacao { get; set; }
        public bool Publicado { get; set; }
        public bool Popup { get; set; }
        public Guid UsuarioCriadorId { get; set; }
        public AdicionarEditarNoticiaViewModel()
        {
            EncIdNoticia = new Guid().ToString("N").ToEncrypted();
        }
    }

    public static class AdicionarEditarNoticiaViewModelExtensions
    {
        public static AdicionarEditarNoticiaViewModel ToAdicionarEditarNoticiaViewModel(this Noticia pDbModel)
        {
            return new AdicionarEditarNoticiaViewModel
            {
                EncIdNoticia = pDbModel.NoticiaId.ToString("N").ToEncrypted(),
                HTMLNoticia = pDbModel.HTMLNoticia.BytesToString(),
                Publicado = pDbModel.Publicado,
                TituloNoticia = pDbModel.Titulo,
                DataUltimaAtualizacao = pDbModel.DataUltimaAtualizacao.ToString("dd/MM/yyyy HH:mm"),
                UsuarioCriadorId = pDbModel.UsuarioCriadorId,
                Popup = pDbModel.Popup
            };
        }

        public static Noticia ToDBModel(this AdicionarEditarNoticiaViewModel pViewModel)
        {
            return new Noticia
            {
                NoticiaId = new Guid(pViewModel.EncIdNoticia.ToDecrypted()),
                HTMLNoticia = pViewModel.HTMLNoticia.ToBytes(),
                Titulo = pViewModel.TituloNoticia,
                Publicado = pViewModel.Publicado,
                UsuarioCriadorId = pViewModel.UsuarioCriadorId,
                Popup =  pViewModel.Popup

            };
        }
    }
}
