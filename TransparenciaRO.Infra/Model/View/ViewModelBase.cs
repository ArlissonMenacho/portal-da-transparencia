﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View
{
    public abstract class ViewModelBase<DBModel>
    {
        public abstract DBModel ToDBModel();        
        public ViewModelBase() { }
        
        internal abstract void loadDBModel(DBModel pDBModel);
        public ViewModelBase(DBModel pDBModel)
        {
            loadDBModel(pDBModel);
        }        
    }
}
