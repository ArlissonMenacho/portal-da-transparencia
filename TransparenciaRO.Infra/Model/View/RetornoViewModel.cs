﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.View.SkalaAPISETIC;

namespace TransparenciaRO.Infra.Model.View
{
    public class RetornoViewModel
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public dynamic Dados { get; set; }
        //public List<Servidor> Servidores { get; set; }
    }
}
