﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View
{
    public class ListarDiariasViewModel
    {

        public string Documento { get; set; }
        public string DataDocumento { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string UG { get; set; }
        public string NomeOrgao { get; set; }
        public string ValorDocumento { get; set; }
    }

    public static class ListarDiariasViewModelExtensions
    {
        public static ListarDiariasViewModel ToListarDiariasViewModel(this Diaria pDiaria)
        {
            return new ListarDiariasViewModel
            {
                CPF = pDiaria.CPF,
                DataDocumento = pDiaria.DataDocumento.ToString("dd/MM/yyyy"),
                Documento = pDiaria.Documento,
                Nome = pDiaria.Nome,
                NomeOrgao = pDiaria.NomeOrgao,
                UG = pDiaria.UG,
                ValorDocumento = pDiaria.ValorDocumento?.ToString("C2")

            };
        }
    }
}
