﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View
{
   
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Attributes
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public int gestora { get; set; }
            public int orcamentaria { get; set; }
            public int administrativa { get; set; }
            public object colegiada { get; set; }
            public object staff { get; set; }
            public string __tipo { get; set; }
        }

        public class TipoNormatizacao
        {
            public int id { get; set; }
            public string nome { get; set; }
        }

        public class Attributes2
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public TipoNormatizacao tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
            public int? gestora { get; set; }
            public int? orcamentaria { get; set; }
            public int? administrativa { get; set; }
            public int? colegiada { get; set; }
            public int? staff { get; set; }
        }

        public class TipoNormatizacao2
        {
            public int id { get; set; }
            public string nome { get; set; }
        }

        public class Attributes3
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public TipoNormatizacao2 tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
            public object gestora { get; set; }
            public object orcamentaria { get; set; }
            public int? administrativa { get; set; }
            public object colegiada { get; set; }
            public object staff { get; set; }
        }

        public class TipoNormatizacao3
        {
            public int id { get; set; }
            public string nome { get; set; }
        }

        public class Attributes4
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public TipoNormatizacao3 tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
        }

        public class Attributes5
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public object tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
        }

        public class Attributes6
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public object tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
        }

        public class Attributes7
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public object tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
        }

        public class Attributes8
        {
            public string nome { get; set; }
            public string sigla { get; set; }
            public object tipo_normatizacao { get; set; }
            public string __tipo { get; set; }
        }

        public class Child7
        {
            public string name { get; set; }
            public Attributes8 attributes { get; set; }
        }

        public class Child6
        {
            public string name { get; set; }
            public Attributes7 attributes { get; set; }
            public List<Child7> children { get; set; }
        }

        public class Child5
        {
            public string name { get; set; }
            public Attributes6 attributes { get; set; }
            public List<Child6> children { get; set; }
        }

        public class Child4
        {
            public string name { get; set; }
            public Attributes5 attributes { get; set; }
            public List<Child5> children { get; set; }
        }

        public class Child3
        {
            public string name { get; set; }
            public Attributes4 attributes { get; set; }
            public List<Child4> children { get; set; }
        }

        public class Child2
        {
            public string name { get; set; }
            public Attributes3 attributes { get; set; }
            public List<Child3> children { get; set; }
        }

        public class Child
        {
            public string name { get; set; }
            public Attributes2 attributes { get; set; }
            public List<Child2> children { get; set; }
        }

        public class MyArrays
        {
            public string name { get; set; }
            public Attributes attributes { get; set; }
            public List<Child> children { get; set; }
        }

        public class Root
        {
            public List<MyArrays> MyArray { get; set; }
        }


    }

