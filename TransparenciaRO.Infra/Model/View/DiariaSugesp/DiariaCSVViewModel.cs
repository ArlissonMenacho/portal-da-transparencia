﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.DiariaSugesp
{
    public class DiariaCSVViewModel
    {
        public int? Solicitacao { get; set; }
        public string Matricula { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public string UnidadeGestora { get; set; }
        public DateTime? DataIda { get; set; }
        public DateTime? DataVolta { get; set; }
        public decimal? QuantidadeDiarias { get; set; }
        public decimal? ValorDiarias { get; set; }
        public decimal? TotalDiarias { get; set; }
        public string TipoViagem { get; set; }
        public string Objetivo { get; set; }
    }
}