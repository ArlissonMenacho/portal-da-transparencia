﻿using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.View
{
    public class UsuarioTrocarSenhaViewModel
    {

        [DataType(DataType.Password)]
        [Display(Description = "Senha")]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Compare(nameof(Senha))]
        [Display(Description = "Confirmação de senha")]
        public string ConfirmarSenha { get; set; }

    }
}
