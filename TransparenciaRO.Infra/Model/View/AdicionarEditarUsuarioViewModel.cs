﻿using System;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.View
{
    public class AdicionarEditarUsuarioViewModel : ViewModelBase<Usuario>
    {
        public AdicionarEditarUsuarioViewModel()
        {
            this.Permissoes = new List<Permissao>();
            this.PastasGuids = new List<Guid>();
            this.Pastas = new List<Pasta>();
            this.EncIdUsuario = new Guid().ToString("N").ToEncrypted();
        }

        public string EncIdUsuario { get; set; }
        [Required]
        public string Nome { get; set; }

        [EmailAddress]
        [Display(Description = "E-mail")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Description = "Senha")]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Compare(nameof(Senha))]
        [Display(Description = "Confirmação de senha")]
        public string ConfirmarSenha { get; set; }

        public ICollection<Permissao> Permissoes { get; set; }
        public ICollection<Pasta> Pastas { get; set; }
        public ICollection<Guid> PastasGuids { get; set; }

        public Guid UnidadeGestoraId { get; set; }

        public bool AlterarSenhaProximoLogin { get; set; }
        public bool Ativo { get; set; }
        public DateTime? DataUltimoLogin { get; set; }
        public string CPF { get; set; }

        #region ViewModelBase implementation

        public AdicionarEditarUsuarioViewModel(Usuario pDBModel) : base(pDBModel) { }

        public override Usuario ToDBModel()
        {
            return new Usuario
            {
                Email = this.Email,
                Nome = this.Nome,
                Permissoes = this.Permissoes,
                Pastas = this.Pastas,
                Senha = this.Senha != null ? this.Senha.GenerateHash() : null,
                UsuarioId = new Guid(EncIdUsuario.ToDecrypted()),
                AlterarSenhaProximoLogin = this.AlterarSenhaProximoLogin,
                Ativo = this.Ativo,                
                DataUltimoLogin = this.DataUltimoLogin,
                CPF = Convert.ToInt64(this.CPF.Replace("-", "").Replace(".", "")),
                UnidadeGestoraID = this.UnidadeGestoraId
            };
        }

        internal override void loadDBModel(Usuario pDBModel)
        {
            this.Email = pDBModel.Email;
            this.EncIdUsuario = pDBModel.UsuarioId.ToString("N").ToEncrypted();
            this.Nome = pDBModel.Nome;
            this.Permissoes = pDBModel.Permissoes;
            this.AlterarSenhaProximoLogin = pDBModel.AlterarSenhaProximoLogin;
            this.Ativo = pDBModel.Ativo;
            this.DataUltimoLogin = pDBModel.DataUltimoLogin;
            this.Pastas = pDBModel.Pastas;
            this.CPF = pDBModel.CPF.ToString("00000000000");
            this.UnidadeGestoraId = pDBModel.UnidadeGestoraID;
            //Senhas não são carregadas do banco, pois o mesmo armazena apenas o hash
        }
        #endregion
    }
}