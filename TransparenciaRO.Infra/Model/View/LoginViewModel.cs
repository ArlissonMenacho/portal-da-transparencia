﻿using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.View
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(14)]
        public string CPF { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Senha { get; set; }
    }
}
