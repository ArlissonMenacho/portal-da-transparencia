﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransparenciaRO.Infra.Model.View
{
    public class SuprimentoDeFundosViewModel
    {
        public SuprimentoDeFundosViewModel()
        {
            //
        }

        [StringLength(80)]
        public string NomOrgao { get; set; }

        [Column(Order = 0, TypeName = "smallint")]
        public short? Exercicio { get; set; }

        [StringLength(255)]
        public string Credor { get; set; }

        [Column(Order = 18, TypeName = "bigint")]
        public long? EspecificacaoDespesa { get; set; }

        public string NomDespesa { get; set; }

        [Key]
        [StringLength(20)]
        public string NumEmpenho { get; set; }

        [Column(Order = 27, TypeName = "money")]
        public decimal? ValorDespesa { get; set; }

    }
}
