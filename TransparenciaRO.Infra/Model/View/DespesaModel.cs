﻿using System;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;
using System.Runtime.Serialization;

namespace TransparenciaRO.Infra.Model.View
{

    public class ParametrosDetalharDespesaModel
    {
        public ETipoDetalhamentoDespesa TipoDetalhamentoDespesa { get; set; }
        public int Exercicio { get; set; }
        public MonthYear MesAno { get; set; }
        public int? CodUnidadeGestora { get; set; }
        public int CodPrograma { get; set; }
        public int CodAcao { get; set; }
        public int CodProjeto { get; set; }
        public int CodFonteRecursos { get; set; }
        public long CodEspecificacaoDespesa { get; set; }
        public string NumeroEmpenho { get; set; }
        public string NomeCredor { get; set; }
        public string DocCredor { get; set; }

    }

    public static class ParametrosDetalharDespesaModelExtensions
    {
        public static string ToEncString(this ParametrosDetalharDespesaModel pParametros)
        {
            return CriptografiaUtils.ToEncrypted(SerializacaoUtils.Serializar(pParametros));
        }

        public static ParametrosDetalharDespesaModel ToParametrosDetalharDespesaModel(this string pParametros)
        {
            try
            {
                return SerializacaoUtils.Desserializar<ParametrosDetalharDespesaModel>(CriptografiaUtils.ToDecrypted(pParametros));
            }
            catch
            {
                return null;
            }
        }
    }

    public abstract class DespesaBaseModel
    {
        public decimal ValorEmpenhada { get; set; }
        public decimal ValorLiquidada { get; set; }
        public decimal ValorPaga { get; set; }

        public string StrValorEmpenhada { get { return ValorEmpenhada.ToString("c2"); } }
        public string StrValorLiquidada { get { return ValorLiquidada.ToString("c2"); } }
        public string StrValorPaga { get { return ValorPaga.ToString("c2"); } }
        public abstract string LinkDetalhamento { get; }
    }

    public class DespesaPorAnoModel : DespesaBaseModel
    {
        public int Exercicio { get; set; }
        public int? CodUnidadeGestora { get; set; }
        public string NumeroEmpenho { get; set; }
        public string NomeCredor { get; set; }
        public string DocCredor { get; set; }

        public override string LinkDetalhamento
        {
            get
            {
                return new ParametrosDetalharDespesaModel
                {
                    TipoDetalhamentoDespesa = ETipoDetalhamentoDespesa.DespesasMeses,
                    Exercicio = this.Exercicio,
                    NumeroEmpenho = this.NumeroEmpenho,
                    NomeCredor = this.NomeCredor,
                    CodUnidadeGestora = this.CodUnidadeGestora,
                    DocCredor = this.DocCredor
                }.ToEncString();
            }
        }
    }

    public class DespesaPorMesModel : DespesaBaseModel
    {
        [IgnoreDataMember]
        public DespesaPorAnoModel Ano { get; set; } // Parâmetros da entidade superior
        public int Mes { get; set; }
        public string NomeMes { get { return $"{this.Mes.NomeMes()}/{Ano.Exercicio.ToString()}"; } }
        public override string LinkDetalhamento
        {
            get
            {
                return new ParametrosDetalharDespesaModel
                {
                    TipoDetalhamentoDespesa = ETipoDetalhamentoDespesa.DespesasProgramaAcao,
                    MesAno = new MonthYear(this.Mes, this.Ano.Exercicio),
                    NumeroEmpenho = this.Ano.NumeroEmpenho,
                    CodUnidadeGestora = this.Ano.CodUnidadeGestora,
                    NomeCredor = this.Ano.NomeCredor,
                    DocCredor = this.Ano.DocCredor
                }.ToEncString();
            }
        }
    }

    public class DespesaProgramaAcaoModel : DespesaBaseModel
    {
        [IgnoreDataMember]
        public DespesaPorMesModel Mes { get; set; } // Parâmetros da entidade superior
        public int CodPrograma { get; set; } // Parâmetro para listar funções
        public int CodAcao { get; set; } // Parâmetro para listar funções        
        public int CodProjeto { get; set; } // Parâmetro para listar funções
        public string Programa { get; set; }
        public string Acao { get; set; }
        public DateTime MesDocumento { get; set; }
        public override string LinkDetalhamento
        {
            get
            {
                return new ParametrosDetalharDespesaModel
                {
                    TipoDetalhamentoDespesa = ETipoDetalhamentoDespesa.DespesasFuncoes,
                    CodPrograma = this.CodPrograma,
                    CodAcao = this.CodAcao,
                    CodProjeto = this.CodProjeto,
                    MesAno = new MonthYear(Mes.Mes, Mes.Ano.Exercicio),
                    NumeroEmpenho = this.Mes.Ano.NumeroEmpenho,
                    CodUnidadeGestora = this.Mes.Ano.CodUnidadeGestora,
                    NomeCredor = this.Mes.Ano.NomeCredor,
                    DocCredor = this.Mes.Ano.DocCredor
                }.ToEncString();
            }
        }
    }

    public class DespesaFuncaoModel : DespesaBaseModel // Listafonte
    {
        [IgnoreDataMember]
        public DespesaProgramaAcaoModel ProgramaAcao { get; set; } // Parâmetros da entidade superior
        public int CodFonte { get; set; } // Parâmetro para listar naturezas
        public int CodFuncao { get; set; }
        public int CodSubFuncao { get; set; }

        public string DescricaoFuncao { get; set; }
        public string DescricaoSubFuncao { get; set; }
        public string DescFonteRecursos { get; set; }
        public override string LinkDetalhamento
        {
            get
            {
                return new ParametrosDetalharDespesaModel
                {
                    TipoDetalhamentoDespesa = ETipoDetalhamentoDespesa.DespesasNaturezas,
                    CodPrograma = this.ProgramaAcao.CodPrograma,
                    CodAcao = this.ProgramaAcao.CodAcao,
                    CodProjeto = this.ProgramaAcao.CodProjeto,
                    CodFonteRecursos = CodFonte,
                    MesAno = new MonthYear(this.ProgramaAcao.Mes.Mes, this.ProgramaAcao.Mes.Ano.Exercicio),
                    NumeroEmpenho = this.ProgramaAcao.Mes.Ano.NumeroEmpenho,
                    CodUnidadeGestora = this.ProgramaAcao.Mes.Ano.CodUnidadeGestora,
                    NomeCredor = this.ProgramaAcao.Mes.Ano.NomeCredor,
                    DocCredor = this.ProgramaAcao.Mes.Ano.DocCredor
                }.ToEncString();
            }
        }
    }

    public class DespesaNaturezaModel : DespesaBaseModel // group by A.EspecificacaoDespesa, O.NomDespesa
    {
        [IgnoreDataMember]
        public DespesaFuncaoModel Funcao { get; set; } // Parâmetros da entidade superior
        public long CodNaturezaDespesa { get; set; } // Parâmetro para listar empenhos
        public string NaturezaDaDespesa { get; set; }
        public override string LinkDetalhamento
        {
            get
            {
                return new ParametrosDetalharDespesaModel
                {
                    TipoDetalhamentoDespesa = ETipoDetalhamentoDespesa.DespesasEmpenhos,
                    CodPrograma = this.Funcao.ProgramaAcao.CodPrograma,
                    CodAcao = this.Funcao.ProgramaAcao.CodAcao,
                    CodProjeto = this.Funcao.ProgramaAcao.CodProjeto,
                    CodFonteRecursos = this.Funcao.CodFonte,
                    CodEspecificacaoDespesa = CodNaturezaDespesa,
                    MesAno = new MonthYear(this.Funcao.ProgramaAcao.Mes.Mes, this.Funcao.ProgramaAcao.Mes.Ano.Exercicio),
                    NumeroEmpenho = this.Funcao.ProgramaAcao.Mes.Ano.NumeroEmpenho,
                    CodUnidadeGestora = this.Funcao.ProgramaAcao.Mes.Ano.CodUnidadeGestora,
                    NomeCredor = this.Funcao.ProgramaAcao.Mes.Ano.NomeCredor,
                    DocCredor = this.Funcao.ProgramaAcao.Mes.Ano.DocCredor
                }.ToEncString();
            }
        }
    }

    public class DespesaEmpenhoModel : DespesaBaseModel // A.NumEmpenho, A.CodOrgao,  A.DataArquivo
    {
        [IgnoreDataMember]
        public DespesaNaturezaModel Natureza { get; set; } // Parâmetros da entidade superior
        public string NaturezaDaDespesa { get; set; }
        public DateTime DataDocumento { get; set; }
        public string DataDocumentoFormatada { get { return this.DataDocumento.ToString("dd/MM/yyyy"); } }
        public string NumeroEmpenho { get; set; }
        public string NumeroDocumento { get; set; }
        public string Credor { get; set; }
        public string UnidadeGestora { get; set; }
        public string IsLinkEmpenho { get; set; }
        public string IsLinkItem { get; set; }

        public string DataEmpenho => ValorEmpenhada > 0 ? DataDocumentoFormatada : "";
        public string DataLiquidacao => ValorLiquidada > 0 ? DataDocumentoFormatada : "";
        public string DataPagamento => ValorPaga > 0 ? DataDocumentoFormatada : "";

        //public string DataEmpenho
        //{
        //    get { return DataEmpenho; }
        //    private set { DataEmpenho = ValorEmpenhada > 0 ? DataDocumentoFormatada : ""; }
        //}

        //public string DataLiquidacao
        //{
        //    get { return DataLiquidacao; }
        //    set { DataLiquidacao = ValorLiquidada > 0 ? DataDocumentoFormatada : ""; }
        //}
        //public string DataPagamento
        //{
        //    get { return DataPagamento; }
        //    set { DataPagamento = ValorPaga > 0 ? DataDocumentoFormatada : ""; }
        //}

        public override string LinkDetalhamento
        {
            get
            {
                return "";
            }
        }
    }


}
