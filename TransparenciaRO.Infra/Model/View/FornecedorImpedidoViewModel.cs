﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View
{
    public class FornecedorImpedidoViewModel
    {
        public FornecedorImpedidoViewModel()
        {
            FornecedorImpedidoId = Guid.NewGuid();
        }

        //Propriedades
        public Guid FornecedorImpedidoId { get; set; }

        [Key]
        public int Codigo { get; set; }

        [Required]
        public Guid FornecedorId { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }

        [Required(ErrorMessage = "Selecione uma sanção")]
        [Display(Name = "Sanção")]
        public ESancaoImpedirFornecedor Sancao { get; set; }

        [Index("PeriodoDePunicao", 1), Required]
        public DateTime DataInicio { get; set; }

        [Index("PeriodoDePunicao", 2), Required]
        public DateTime DataFinal { get; set; }

        [Required(ErrorMessage = "Informe o número do processo")]
        public string NumeroProcesso { get; set; }

        public string NumeroProcessoSemFormatacao { get; set; }

        [Required(ErrorMessage = "Informe a data de publicação")]
        [Display(Name = "Data de publicação")]
        public DateTime DataPublicacao { get; set; }

        [Display(Name = "Penas eventuais")]
        public string PenasEventuais { get; set; }

        [Display(Name = "Informação pública")]
        public string InformacaoPublica { get; set; }

        [Display(Name = "Observação interna")]
        public string ObservacaoInterna { get; set; }

        [Display(Name = "Data de inclusão")]
        public DateTime DataInclusao { get; set; }

        public DateTime? DataUltimaAlteracao { get; set; }

        //Relacionamentos
        [Required(ErrorMessage = "Selecione uma unidade gestora")]
        [Display(Name = "Unidade Gestora")]
        public Guid UnidadeGestoraId { get; set; }

        public virtual UnidadeGestora UnidadeGestora { get; set; }

        [Required]
        public Guid UsuarioId { get; set; }

        public virtual Usuario Usuario { get; set; }

        //Métodos
        public FornecedorImpedido ConverteModel()
        {
            return new FornecedorImpedido
            {
                Codigo = Codigo,
                DataInicio = DataInicio,
                DataFinal = DataFinal,
                DataPublicacao = DataPublicacao,
                DataInclusao = DataInclusao,
                FornecedorId = FornecedorId,
                DataUltimaAlteracao = DataUltimaAlteracao,
                NumeroProcesso = NumeroProcesso,
                NumeroProcessoSemFormatacao = NumeroProcesso.Replace(".","").Replace("/", "").Replace("-", ""),
                UsuarioId = UsuarioId,
                Sancao = Sancao,
                ObservacaoInterna = ObservacaoInterna,
                FornecedorImpedidoId = FornecedorImpedidoId,
                InformacaoPublica = InformacaoPublica,
                PenasEventuais = PenasEventuais,
                UnidadeGestoraId = UnidadeGestoraId
            };
        }

        public FornecedorImpedidoViewModel ConverteViewModel(FornecedorImpedido fornecedorImpedido)
        {
            return new FornecedorImpedidoViewModel
            {
                Codigo = fornecedorImpedido.Codigo,
                DataInicio = fornecedorImpedido.DataInicio,
                DataFinal = fornecedorImpedido.DataFinal,
                DataPublicacao = fornecedorImpedido.DataPublicacao ?? new DateTime(),
                DataInclusao = fornecedorImpedido.DataInclusao,
                FornecedorId = fornecedorImpedido.FornecedorId,
                DataUltimaAlteracao = fornecedorImpedido.DataUltimaAlteracao,
                NumeroProcesso = fornecedorImpedido.NumeroProcesso,
                NumeroProcessoSemFormatacao = NumeroProcessoSemFormatacao,
                Sancao = fornecedorImpedido.Sancao,
                ObservacaoInterna = fornecedorImpedido.ObservacaoInterna,
                FornecedorImpedidoId = fornecedorImpedido.FornecedorImpedidoId,
                InformacaoPublica = fornecedorImpedido.InformacaoPublica,
                PenasEventuais = fornecedorImpedido.PenasEventuais,
                UnidadeGestoraId = fornecedorImpedido.UnidadeGestoraId,
                Fornecedor = fornecedorImpedido.Fornecedor
            };
        }
    }
}