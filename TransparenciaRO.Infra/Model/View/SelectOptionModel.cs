﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.Model.View
{
    public class SelectOptionModel
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public SelectOptionModel(string pCodigo, string pDescricao)
        {
            Codigo = pCodigo.ToEncrypted();
            Descricao = pDescricao;
        }
    }

    public static class SelectOptionModelExtensions
    {
        /// <summary>
        /// Adiciona um item "Todos" com código 0 à lista, e depois ordena o resultado por código
        /// </summary>
        /// <param name="pList"></param>
        /// <param name="pLabelTodos">Texto opcional para ser exibido no controle select. Por padrão será "Todos"</param>
        /// <returns></returns>
        public static IEnumerable<SelectOptionModel> Combify(this IEnumerable<SelectOptionModel> pList, string pLabelTodos = "TODOS")
        {
            return new List<SelectOptionModel> { new SelectOptionModel("0", pLabelTodos) }.Union(pList.OrderBy(lst => lst.Descricao));
        }
    }
}
