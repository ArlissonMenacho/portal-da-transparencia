﻿using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.View
{
    public class IndexSuprimentoDeFundosViewModel
    {
        public IndexSuprimentoDeFundosViewModel()
        {
            //
        }

        public List<string> UG { get; set; }

        public List<short?> Ano { get; set; }

        public List<SuprimentoDeFundosViewModel> SuprimentoDeFundos { get; set; }
    }
}
