﻿using System;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View
{
   public class RegistroBoletimViewModel
    {
        public Guid BoletimId { get; set; }

        [Required]
        [Display(Name = "Data")]
        public DateTime Data { get; set; }
                
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
                
        [Display(Name = "Arquivo")]
        public Arquivo Arquivo { get; set; }
    }
}
