﻿using System.Collections.Generic;

namespace TransparenciaRO.Infra.Model.View.DadosLicitacao
{
    public class IndexDadosLicitacaoViewModel
    {
        public IndexDadosLicitacaoViewModel()
        {
            //
        }

        public FiltroDadosLicitacaoViewModel Filtro { get; set; }

        public List<DB.dbTransparencia.DadosLicitacao> Licitacoes { get; set; }
    }
}
