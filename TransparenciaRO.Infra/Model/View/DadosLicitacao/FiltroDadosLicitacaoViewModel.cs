﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TransparenciaRO.Infra.Model.View.DadosLicitacao
{
    public class FiltroDadosLicitacaoViewModel
    {
        public FiltroDadosLicitacaoViewModel()
        {
            //
        }

        [DisplayName("Processo administrativo")]
        public string NumeroProcessoAdministrativo { get; set; }

        public string CNPJ { get; set; }

        [DisplayName("Razão social")]
        public string RazaoSocial { get; set; }

        public string Objeto { get; set; }

        [Required, DisplayName("Quantidade máxima de resultados")]
        public int QuantidadeResultados { get; set; }
    }
}
