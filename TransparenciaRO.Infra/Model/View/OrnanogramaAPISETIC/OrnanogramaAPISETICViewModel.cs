﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.OrnanogramaAPISETIC
{
    public class OrnanogramaAPISETICViewModel
    {
        public List<Unidade> Unidades { get; set; }
    }

    public partial class Unidade
    {
        public string name { get; set; }
        public Attributes attributes { get; set; }
        public List<Child> children { get; set; }
    }

    public class Attributes
    {
        public string nome { get; set; }
        public string sigla { get; set; }
        public int? gestora { get; set; }
        public int? orcamentaria { get; set; }
        public int? administrativa { get; set; }
        public object colegiada { get; set; }
        public object staff { get; set; }
        public TipoNormatizacao tipo_normatizacao { get; set; }
        public string __tipo { get; set; }
    }
    public class Child
    {
        public string name { get; set; }
        public Attributes attributes { get; set; }
        public List<Child> children { get; set; }
    }

    public class TipoNormatizacao
    {
        public int id { get; set; }
        public string nome { get; set; }
    }
}