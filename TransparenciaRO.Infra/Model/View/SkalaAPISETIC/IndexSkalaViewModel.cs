﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    public class IndexSkalaViewModel
    {
        public FiltroSkalaViewModel Filtro { get; set; }
        public TabelaSkalaViewModel TabelaSkalaViewModel { get; set; }
    }
}
