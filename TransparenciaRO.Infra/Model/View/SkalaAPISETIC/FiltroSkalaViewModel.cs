﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.View.OrnanogramaAPISETIC;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    public class FiltroSkalaViewModel
    {
        public ESkalaTipoFiltro ESkalaTipoFiltro { get; set; }
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public List<Unidade> Unidades { get; set; }
        public ETipoPlatao tipoPlantao { get; set; }
    }
}
