﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    public enum ESkalaRequisicao
    {
        Autenticacao,
        Matricula,
        UnidadeLocal,
        Nome,
        Cargo,
        Plantao,
    }
    public static class ESkalaRequisicaoHelper
    {
        public static string GetDescricao(this ESkalaRequisicao eSkalaRequisicao)
        {
            switch (eSkalaRequisicao)
            {
                case ESkalaRequisicao.Autenticacao:
                    return "/api/Autenticacao";
                case ESkalaRequisicao.Matricula:
                    return "/api/Escalas/matricula/";
                case ESkalaRequisicao.UnidadeLocal:
                    return "/api/Escalas/plantao/localdoplantao";
                case ESkalaRequisicao.Nome:
                    return "/api/Escalas/nome/";
                case ESkalaRequisicao.Cargo:
                    return "/api/Escalas/cargo/";
                case ESkalaRequisicao.Plantao:
                    return "/api/Escalas/plantao";
                default:
                    return "";
            }
        }
    }
}
