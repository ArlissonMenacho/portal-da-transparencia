﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class TotalDeHorasDoPlantao
    {
        public object ticks { get; set; }
        public int days { get; set; }
        public int hours { get; set; }
        public int milliseconds { get; set; }
        public int minutes { get; set; }
        public int seconds { get; set; }
        public double totalDays { get; set; }
        public double totalHours { get; set; }
        public double totalMilliseconds { get; set; }
        public double totalMinutes { get; set; }
        public double totalSeconds { get; set; }
    }

    public class Planto
    {
        public string dataInicial { get; set; }
        public string dataFinal { get; set; }
        public DateTime dataDeRegistro { get; set; }
        public string horarioDeEntrada { get; set; }
        public string horarioDeSaida { get; set; }
        public string tipoDePlantao { get; set; }
        public string local { get; set; }
        public TotalDeHorasDoPlantao totalDeHorasDoPlantao { get; set; }
        public string cpfDoResponsavel { get; set; }
        public string nomeDoResponsavel { get; set; }
    }

    public class Servidor
    {
        public string nome { get; set; }
        public string matricula { get; set; }
        public string cpf { get; set; }
        public string departamento { get; set; }
        public string cargo { get; set; }
        public string lotacao { get; set; }
        public string horasSemanais { get; set; }
        public string horasMensais { get; set; }
        public List<object> trocasDePlantoes { get; set; }
        public List<Planto> plantoes { get; set; }
    }

    public class Root
    {
        public Servidor servidor { get; set; }
    }



}
