﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    public class TabelaSkalaViewModel
    {        
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public List<Root> Servidores { get; set; }
    }
}
