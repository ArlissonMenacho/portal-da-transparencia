﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.SkalaAPISETIC
{
    public enum ESkalaTipoFiltro
    {
        [Display(Name = "Matricula")]
        Matricula,
        [Display(Name = "Unidades Hospitalar")]
         UnidadeLocal,
        [Display(Name = "Nome")]
        Nome,
        [Display(Name = "Cargo")]
        Cargo,
        [Display(Name = "Data do plantão")]
        DataPlantao,
        [Display(Name = "Tipo de Plantão")]
        TipoPlantao
    }



    public static class ESkalaTipoFiltroHelper
    {
        public static string GetTipoDeFiltro(this ESkalaTipoFiltro eSkalaTipoFiltro)
        {
            switch (eSkalaTipoFiltro)
            {
                case ESkalaTipoFiltro.Matricula:
                    return "Matrícula";
                case ESkalaTipoFiltro.UnidadeLocal:
                    return "Unidades";
                case ESkalaTipoFiltro.Nome:
                    return "Nome";
                case ESkalaTipoFiltro.Cargo:
                    return "Cargo";
                case ESkalaTipoFiltro.DataPlantao:
                    return "Data do Plantão";
                default:
                    return "";
            }
        }
    }

    public enum ETipoPlatao 
    {
        [Display(Name = "Normal")]
        Normal,
        [Display(Name = "Sobreaviso")]
        Sobreaviso,
        [Display(Name = "Especial")]
        Especial
    }
}
