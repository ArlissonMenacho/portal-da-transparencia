﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.DividaAtivaSEFIN
{
    public class DividaAtivaViewModel
    {
        public int Id { get; set; }
        public string NomePessoa { get; set; }

        public string CnpjCpf { get; set; }

        public string ValorTotalLancamento { get; set; }

        public string ValorTotalPagamentoEfetuado { get; set; }

        public string ValorAPagar { get; set; }

        //
        public string InscricaoEstadual { get; set; }

        public string InscricaoCDA { get; set; }

        public string Cartorio { get; set; }

        public string DocumentoCartorio { get; set; }

        public string DataConfirmacaoCartorio { get; set; }

        public string Acao { get; set; }

        public string Situacao { get; set; }

        public string CodigoReceita { get; set; }

        public string GuiaLancamento { get; set; }
    }
}