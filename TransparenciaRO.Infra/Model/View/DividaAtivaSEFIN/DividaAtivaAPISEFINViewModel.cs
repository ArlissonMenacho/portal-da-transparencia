﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.DividaAtivaSEFIN
{
    public class DividaAtivaAPISEFINViewModel
    {
        public List<DividaAtiva> dados { get; set; }
        public string pagina_atual { get; set; }
        public string pagina_final { get; set; }

        public class DividaAtiva
        {
            public int tuk { get; set; }
            public string it_nu_cda { get; set; }
            public string it_in_situacao { get; set; }
            public string it_co_receita { get; set; }
            public string it_cnpj_cpf { get; set; }
            public string it_nu_inscricao_estadual { get; set; }
            public string it_no_pessoa { get; set; }
            public string it_nu_guia_lancamento { get; set; }
            public string it_co_receita_l { get; set; }
            public string it_co_situacao_lancamento { get; set; }
            public string it_va_total_lancamento { get; set; }
            public string it_va_total_pgto_efetuado { get; set; }
            public string it_va_apagar { get; set; }
            public string it_no_cartorio { get; set; }
            public string it_nu_documento { get; set; }
            public string it_no_ocorrencia { get; set; }
            public string it_da_confirmacao { get; set; }
        }
    }
}