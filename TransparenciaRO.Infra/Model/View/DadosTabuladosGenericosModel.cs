﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View
{
    public class DadosTabuladosGenericosModel
    {
        public IEnumerable<object> dados { get; set; }
        public string msgErro { get; set; }
        public string msgInformacao { get; set; }
    }
}
