﻿
using System.Text.RegularExpressions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.Model.View
{
    public class ListarNoticiasViewModel
    {
        public string EncNoticiaId { get; set; }
        public string Titulo { get; set; }
        public string DataCriacao { get; set; }
        public string PrimeiroParagrafo { get; set; }
        public string DataUltimaAtualizacao { get; set; }
        public bool Publicado { get; set; }
        /// <summary>
        /// Primeira imagem encontrada na notícia (se existir)
        /// </summary>
        public string HTMLImagem { get; set; }
        public string UsuarioEditor { get; set; }
        public bool Popup { get; set; }

    }

    public static class ListarNoticiasViewModelExtensions
    {
        public static ListarNoticiasViewModel ToListarNoticiasViewModel(this Noticia pNoticia)
        {

            return new ListarNoticiasViewModel
            {
                DataCriacao = pNoticia.DataCriacao.ToString("dd/MM/yyyy"),
                DataUltimaAtualizacao = pNoticia.DataUltimaAtualizacao.ToString("dd/MM/yyyy HH:mm"),
                PrimeiroParagrafo = Regex.Replace(Regex.Match("<body>" + pNoticia.HTMLNoticia.BytesToString() + "</body>", @"[>]([^<]+)<").Groups[0].Value, @"[>]|[<]", ""),
                HTMLImagem = Regex.Replace(Regex.Match(pNoticia.HTMLNoticia.BytesToString(), @"<img[^>]*>").Groups[0].Value, "style=\"[^\"]*\"", ""),
                EncNoticiaId = pNoticia.NoticiaId.ToString("N").ToEncrypted(),
                Publicado = pNoticia.Publicado,
                Titulo = pNoticia.Titulo,
                UsuarioEditor = pNoticia.UsuarioCriador != null ? pNoticia.UsuarioCriador.Nome : "Não disponível",
                Popup = pNoticia.Popup
            };
        }
    }
}
