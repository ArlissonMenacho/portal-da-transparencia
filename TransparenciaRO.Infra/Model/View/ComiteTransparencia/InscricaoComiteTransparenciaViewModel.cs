﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.Model.View.ComiteTransparencia
{
    public class InscricaoComiteTransparenciaViewModel
    {
        public InscricaoComiteTransparenciaViewModel()
        {
            InscricaoId = Guid.NewGuid();
        }

        [Key]
        public Guid InscricaoId { get; set; }

        [Required, Display(Name = "Razão social")]
        public string RazaoSocial { get; set; }

        [Required, Display(Name = "CNPJ")]
        public string Cnpj { get; set; }

        [Required, Display(Name = "Representante legal")]
        public string RepresentanteLegal { get; set; }

        [Required]
        public string Telefone { get; set; }

        public string Mensagem { get; set; }

        public virtual ICollection<Arquivo> Arquivos { get; set; }
    }
}
