﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.ProcessosAPISUPEL
{
    public class ProcessosAPISUPELViewModel
    {
        public bool erros { get; set; }
        public string mensagem { get; set; }
        [Display(Name = "Processos")]
        public IList<Processo> processos { get; set; }
    }

    public partial class Processo
    {
        [Display(Name = "Número do processo")]
        public string NumeroProcesso { get; set; }
        [Display(Name = "Situação")]
        public string Situacao { get; set; }
        [Display(Name = "Órgão")]
        public Orgao Orgao { get; set; }
        [Display(Name = "Modalidade de licitação")]
        public string ModalidadeLicitacao { get; set; }
        [Display(Name = "Certames")]
        public IList<Certame> Certames { get; set; }
    }

    public partial class Orgao
    {
        public int OrgaoId { get; set; }
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
    }

    public partial class Certame
    {
        [Display(Name = "Modalidade do certame")]
        public string CodigoModalidadeCertame { get; set; }
        [Display(Name = "Número da licitacao")]
        public string NumeroLicitacao { get; set; }
        [Display(Name = "Ano")]
        public string Ano { get; set; }
        [Display(Name = "Data da abertura")]
        public DateTime DataAbertura { get; set; }
        [Display(Name = "Itens")]
        public IList<Iten> Itens { get; set; }
    }

    public partial class Iten
    {
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        [Display(Name = "Descrição complementar")]
        public string DescricaoComplementar { get; set; }
        [Display(Name = "Situação")]
        public string Situacao { get; set; }
        [Display(Name = "Unidade de fornecimento")]
        public string UnidadeFornecimento { get; set; }
        [Display(Name = "Número")]
        public int Numero { get; set; }
        [Display(Name = "Quantidade")]
        public double Quantidade { get; set; }
        [Display(Name = "Valor unitário")]
        public double ValorUnitario { get; set; }
        [Display(Name = "Valor global")]
        public double ValorGlobal { get; set; }
        [Display(Name = "Fornecedor")]
        public Fornecedor Fornecedor { get; set; }
    }
    
    public partial class Fornecedor
    {
        [Display(Name = "CNPJ")]
        public string CNPJ { get; set; }
        [Display(Name = "Razão social")]
        public string RazaoSocial { get; set; }
    }
}