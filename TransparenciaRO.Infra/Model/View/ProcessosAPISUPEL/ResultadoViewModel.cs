﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.ProcessosAPISUPEL
{
    public class ResultadoViewModel
    {
        //propriedade criada para tratamento de erros, não pertence a API
        public bool Sucesso { get; set; }        


        public ProcessosAPISUPEL.Processo Processo { get; set; }
    }
}
