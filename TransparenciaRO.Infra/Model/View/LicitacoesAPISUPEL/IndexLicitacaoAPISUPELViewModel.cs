﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL
{
    public class IndexLicitacaoAPISUPELViewModel
    {        
        public FiltroLicitacaoAPISUPELViewModel Filtro { get; set; }        
        public LicitacaoAPISUPELViewModel LicitacaoAPISUPELViewModel { get; set; }
    }
}
