﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL
{
    public class FiltroLicitacaoAPISUPELViewModel
    {
        public string Modalidade { get; set; }
        [Display(Name = "Situação")]
        public string Situacao { get; set; }
        [Display(Name = "Unidade administrativa")]
        public string UnidadeAdministrativa { get; set; }
        public int? Ano { get; set; }
        [Display(Name = "Quantidade de resultados")]
        public int QuantidadeResultados { get; set; }
        [Display(Name = "Enfrentamento ao COVID-19")]
        public bool EnfrentamentoAoCovid { get; set; }
    }
}