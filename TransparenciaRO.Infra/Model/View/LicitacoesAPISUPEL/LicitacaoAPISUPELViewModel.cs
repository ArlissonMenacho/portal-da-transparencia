﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL
{
    public class LicitacaoAPISUPELViewModel
    {
        //propriedades criadas para tratamento de erros, não pertence a API
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        //-----------------------------------------------------------------


        public Pagina pagina { get; set; }
        public List<ItemLicitacaoAPISUPEL> itens { get; set; }
    }

    public partial class Pagina
    {
        public int atual { get; set; }
        public int total { get; set; }
    }

    public partial class Meta
    {
        [Display(Name = "Enfrentamento ao COVID-19")]
        public string enfrentamentoCovid { get; set; }
        [Display(Name = "Nº da licitacao")]
        public string numeroLicitacao { get; set; }
        [Display(Name = "Ano")]
        public string ano { get; set; }
        [Display(Name = "Modalidade")]
        public string modalidade { get; set; }
        [Display(Name = "Unidade administrativa")]
        public string unidadeAdministrativa { get; set; }
        [Display(Name = "Número do processo")]
        public string numeroProcesso { get; set; }
        [Display(Name = "Fonte do recurso")]
        public string fonteRecurso { get; set; }
        [Display(Name = "Projeto/Atividade")]
        public string projetoAtividade { get; set; }
        [Display(Name = "Elemento despesa")]
        public string despesa { get; set; }
        [Display(Name = "Valor estimado (R$)")]
        public string valor { get; set; }
        [Display(Name = "Situação")]
        public string situacao { get; set; }
        [Display(Name = "Data da abertura")]
        public string dataAbertura { get; set; }
        [Display(Name = "Hora da abertura")]
        public string horaAbertura { get; set; }
        [Display(Name = "Fuso horário")]
        public string fusoHorario { get; set; }
        [Display(Name = "Endereço eletrônico (URL)")]
        public string url { get; set; }
        [Display(Name = "Local")]
        public string local { get; set; }
        [Display(Name = "Mais informações")]
        public string maisInformacoes { get; set; }
        [Display(Name = "Pregoeiro")]
        public string pregoeiro { get; set; }
    }

    public partial class Anexo
    {
        [Display(Name = "URL")]
        public string url { get; set; }
        [Display(Name = "Título")]
        public string titulo { get; set; }
    }

    public partial class Aviso
    {
        [Display(Name = "Tipo")]
        public string tipo { get; set; }
        [Display(Name = "Data")]
        public string data { get; set; }
        [Display(Name = "Conteúdo")]
        public string conteudo { get; set; }
        [Display(Name = "Anexo")]
        public Anexo anexo { get; set; }
    }

    public partial class ItemLicitacaoAPISUPEL
    {
        [Display(Name = "Título")]
        public string titulo { get; set; }
        [Display(Name = "Objeto")]
        public string conteudo { get; set; }
        [Display(Name = "Data")]
        public string data { get; set; }
        [Display(Name = "Meta")]
        public Meta meta { get; set; }
        [Display(Name = "Anexo")]
        public Anexo anexo { get; set; }
        [Display(Name = "Avisos")]
        public IList<Aviso> avisos { get; set; }
    }
}
