﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View
{

    public abstract class PresencaMunicipiosValores
    {
        public decimal Empenhado { get; set; }
        public decimal EmpenhadoALiquidar { get; set; }
        public decimal EmpenhadoAPagar { get; set; }
        public decimal Liquidado { get; set; }
        public decimal LiquidadoAPagar { get; set; }
        public decimal Pago { get; set; }

        public string StrEmpenhado { get { return Empenhado.ToString("c2"); } }
        public string StrEmpenhadoALiquidar { get { return EmpenhadoALiquidar.ToString("c2"); } }
        public string StrEmpenhadoAPagar { get { return EmpenhadoAPagar.ToString("c2"); } }
        public string StrLiquidado { get { return Liquidado.ToString("c2"); } }
        public string StrLiquidadoAPagar { get { return LiquidadoAPagar.ToString("c2"); } }
        public string StrPago { get { return Pago.ToString("c2"); } }
    }

    public class PresencaMunicipiosModel : PresencaMunicipiosValores
    {
        public int Exercicio { get; set; }
        public string pEncCodMunicipio { get; set; }
        public string Municipio { get; set; }
    }

    public class PresencaMunicipiosPorFuncao : PresencaMunicipiosValores
    {
        public string Investimento { get; set; }
    }
}
