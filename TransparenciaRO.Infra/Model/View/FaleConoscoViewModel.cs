﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Model.View
{
    public class FaleConoscoViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "O nome deve conter menos de 100 caracteres")]
        public string Nome { get; set; }

        public string Telefone { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail em formato inválido.")]        
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O assunto deve conter menos de 100 caracteres")]
        public string Assunto { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "A mensagem deve conter menos de 1000 caracteres")]
        public string Mensagem { get; set; }
    }
}