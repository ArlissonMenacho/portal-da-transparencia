﻿

using System;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.Data.Entity;
using System.Linq;

namespace TransparenciaRO.Infra.DAL
{
    public class ParametrosDAL : ReadOnlyDALBase<Parametro>
    {
        public ParametrosDAL(DbTransparencia db) : base(db) { }

        public string GetParametro(ETipoParametro pTipoParametro)
        {
            return _db.Parametros.FirstOrDefault(p => p.TipoParametro == pTipoParametro)?.Valor;
        }
    }
}
