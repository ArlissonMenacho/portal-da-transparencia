﻿using System;
using System.Collections.Generic;
using System.Linq;

using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.Data.Entity;
using static System.Data.Entity.EntityState;
using System.Web;
using Microsoft.Owin.Security;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class LogDAL : DALBase<Log>
    {

        public LogDAL(DbTransparencia db) : base(db) { }

        public override Log ObterPorId(Guid pId)
        {
            return _db.Log.FirstOrDefault(l => l.LogId == pId);
        }

        public override void Remover(Guid pId)
        {
            throw new Exception("Logs não podem ser removidos do sistema");
        }

        public override Guid SalvarDados(Log pDados)
        {
            var novoGuid = Guid.NewGuid();

            _db.Log.Add(new Log { LogId = novoGuid, Descricao = pDados.Descricao, UsuarioId = pDados.UsuarioId, DataHoraEvento = DateTime.Now }) ;

            return novoGuid;
        }

        public List<Log> ObterPorPeriodo(DateTime pDataInicial, DateTime pDataFinal)
        {

            var obterPorPerido = _db.Log.Include(l => l.Usuario).Where(l => l.DataHoraEvento >= pDataInicial && l.DataHoraEvento <= pDataFinal).ToList();

            return obterPorPerido;
        }
    }
}
