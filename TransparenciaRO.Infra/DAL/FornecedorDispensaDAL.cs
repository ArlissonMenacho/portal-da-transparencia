﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class FornecedorDispensaDAL
    {
        public readonly DbTransparencia dbTransparencia;

        public FornecedorDispensaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(FornecedorDispensa fornecedor)
        {
            dbTransparencia.FornecedorDispensa.Add(fornecedor);
            dbTransparencia.SaveChanges();
        }

        public void Editar(FornecedorDispensa fornecedor)
        {
            dbTransparencia.Entry(fornecedor).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public void Remover(Guid fornecedorId)
        {
            var fornecedor = dbTransparencia.FornecedorDispensa.FirstOrDefault(fd => fd.FornecedorId == fornecedorId);
            dbTransparencia.FornecedorDispensa.Remove(fornecedor);
            dbTransparencia.SaveChanges();
        }

        public FornecedorDispensa Buscar(Guid fornecedorId)
        {
            return dbTransparencia.FornecedorDispensa.FirstOrDefault(fd => fd.FornecedorId == fornecedorId);
        }

        public List<FornecedorDispensa> BuscarPorLicitacao(Guid licitacaoId)
        {
            return dbTransparencia.FornecedorDispensa.Where(fd => fd.LicitacaoId == licitacaoId).ToList();
        }
    }
}
