﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class ImpedirFornecedorDAL
    {
        private readonly DbTransparencia dbTransparencia;

        public ImpedirFornecedorDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(FornecedorImpedido fornecedorImpedido)
        {
            dbTransparencia.FornecedoresImpedidos.Add(fornecedorImpedido);
            dbTransparencia.SaveChanges();
        }

        public void Editar(FornecedorImpedido fornecedorImpedido)
        {
            dbTransparencia.Entry(fornecedorImpedido).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public FornecedorImpedido BuscarFornecedor(Guid fornecedorImpedidoId)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Include(f => f.Fornecedor)
                .FirstOrDefault(f => f.FornecedorImpedidoId == fornecedorImpedidoId);
        }

        public FornecedorImpedido BuscarImpedimento(Guid fornecedorImpedidoId)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Include(f => f.Fornecedor)
                .Include(u => u.UnidadeGestora)
                .FirstOrDefault(f => f.FornecedorImpedidoId == fornecedorImpedidoId);
        }

        public ICollection<FornecedorImpedido> BuscarFornecedores()
        {
            return dbTransparencia.FornecedoresImpedidos
                .Include(f => f.Fornecedor)
                .ToList();
        }

        public ICollection<FornecedorImpedido> BuscarFornecedoresImpedidos()
        {
            var data = DateTime.Now.Date;

            return dbTransparencia.FornecedoresImpedidos
                .Where(i => i.DataInicio <= data && i.DataFinal >= data)
                .Include(f => f.Fornecedor)
                .ToList();
        }

        public ICollection<FornecedorImpedido> BuscarHistoricoDeEncerramentoFornecedoresImpedidos()
        {
            var data = DateTime.Now.Date;

            return dbTransparencia.FornecedoresImpedidos
                .Where(i => i.DataInicio <= data && i.DataFinal < data)
                .Include(f => f.Fornecedor)
                .ToList();
        }

        public FornecedorImpedido BuscarFornecedorPorCodigo(int codigo)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Include(f => f.Fornecedor)
                .FirstOrDefault(x => x.Codigo == codigo);
        }

        public FornecedorImpedido BuscarFornecedorPorCpfCnpj(string cpfCnpj)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Include(f => f.Fornecedor)
                .FirstOrDefault(f => f.Fornecedor.CpfCnpj == cpfCnpj);
        }

        public bool VerificarSeFornecedorEstaImpedido(Guid fornecedorId)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Any(f => f.Fornecedor.FornecedorId == fornecedorId && f.DataInicio < DateTime.Now && f.DataFinal >= DateTime.Now);
        }

        public bool VerificarSeContemImpedimento(string numeroProcessoSemFormatacao, string cpfCnpj, Guid fornecedorImpedidoId)
        {
            return dbTransparencia.FornecedoresImpedidos
                .Any(f => f.NumeroProcessoSemFormatacao == numeroProcessoSemFormatacao
                    && f.Fornecedor.CpfCnpj == cpfCnpj.Replace(".", "").Replace("-", "").Replace("/","") 
                    && f.FornecedorImpedidoId != fornecedorImpedidoId);
        }
    }
}