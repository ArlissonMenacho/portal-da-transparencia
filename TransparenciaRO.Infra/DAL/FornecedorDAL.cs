﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class FornecedorDAL
    {
        public readonly DbTransparencia dbTransparencia;

        public FornecedorDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(Fornecedor fornecedor)
        {
            fornecedor.CpfCnpj = fornecedor.CpfCnpj
                .Replace(".", "")
                .Replace("/", "")
                .Replace("-", "");

            dbTransparencia.Fornecedores.Add(fornecedor);
            dbTransparencia.SaveChanges();
        }

        public void Editar(Fornecedor fornecedor)
        {
            fornecedor.CpfCnpj = fornecedor.CpfCnpj
                .Replace(".", "")
                .Replace("/", "")
                .Replace("-", "");

            dbTransparencia.Entry(fornecedor).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public void Remover(Guid fornecedorId)
        {
            var fornecedor = BuscarFornecedor(fornecedorId);
            dbTransparencia.Fornecedores.Remove(fornecedor);
            dbTransparencia.SaveChanges();
        }

        public ICollection<Fornecedor> BuscarFornecedores()
        {
            return dbTransparencia.Fornecedores
                .Include(x => x.Cidade)
                .Take(1000)
                .ToList();
        }

        public Fornecedor BuscarFornecedor(Guid fornecedorId)
        {
            return dbTransparencia.Fornecedores
                .Find(fornecedorId);
        }

        public Fornecedor BuscarFornecedorPorCpfCnpj(string cpfCnpj)
        {
            var cpfCnpjSemMascara = cpfCnpj.Replace("-", "").Replace("/", "").Replace(".", "");

            return dbTransparencia.Fornecedores
                .Include(f => f.Cidade)
                .FirstOrDefault(f => f.CpfCnpj == cpfCnpj || f.CpfCnpj == cpfCnpjSemMascara);
        }

        public Fornecedor BuscarFornecedorPorCpfCnpjJson(string cpfCnpj)
        {
            var cpfCnpjSemMascara = cpfCnpj.Replace(".", "").Replace("/", "").Replace("-", "");

            var fornecedor = dbTransparencia.Fornecedores
                .Include(f => f.Cidade)
                .FirstOrDefault(f => f.CpfCnpj == cpfCnpjSemMascara);

            if (fornecedor == null) return null;

            if (fornecedor.CidadeId != null)
                fornecedor.Cidade.Fornecedores = null;

            return fornecedor;
        }

        public ICollection<Fornecedor> BuscarFornecedoresPorRazaoSocialCpfCnpj(string razaoSocial, string cpfcnpj)
        {
            return dbTransparencia.Fornecedores
                .Include(f => f.Cidade)
                .Where(f =>
                    (razaoSocial == "" || f.RazaoSocial.Contains(razaoSocial)) &&
                    (cpfcnpj == "" || f.CpfCnpj.Contains(cpfcnpj.Replace(".","").Replace("-","").Replace("/",""))))
                .Take(100)
                .ToList();
        }
    }
}