﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using EntityFramework.Extensions;

namespace TransparenciaRO.Infra.DAL
{
    public class RemuneracaoEmpresaDAL
    {
        public DbTransparencia dbTransparencia;

        public RemuneracaoEmpresaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public int AnoMax(EEmpresa empresa)
        {
            return dbTransparencia.RemuneracoesEmpresas
                .Where(r => r.Empresa == empresa)
                .Max(r => (int?)r.Ano) ?? 0;
        }

        public int AnoMin(EEmpresa empresa)
        {
            return dbTransparencia.RemuneracoesEmpresas
                .Where(r => r.Empresa == empresa)
                .Min(r => (int?)r.Ano) ?? 0;
        }

        public List<int> Meses(int ano, EEmpresa empresa)
        {
            var meses = dbTransparencia.RemuneracoesEmpresas
                .Where(r => r.Ano == ano && r.Empresa == empresa)
                .GroupBy(r => r.Mes)
                .Select(r => new { MesInt = r.Key })
                .ToList();

            return meses
                .Select(r => r.MesInt)
                .ToList();
        }

        public Dictionary<string, int> RelacaoCargo(int ano, int mes, EEmpresa empresa, string tipoCargo)
        {
            var relacaoServidorEmpresa = dbTransparencia.RemuneracoesEmpresas
                .Where(re => re.Ano == ano && re.Mes == mes && re.Empresa == empresa && (tipoCargo == "" || re.TipoCargo == tipoCargo))
                .GroupBy(re => re.Cargo)
                .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> RelacaoDepartamento(int ano, int mes, EEmpresa empresa)
        {
            var relacaoServidorEmpresa = dbTransparencia.RemuneracoesEmpresas
                 .Where(re => re.Ano == ano && re.Mes == mes && re.Empresa == empresa)
                 .GroupBy(re => re.Departamento)
                 .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> FaixasSalariais(int ano, int mes, EEmpresa empresa)
        {
            return dbTransparencia.RemuneracoesEmpresas
                .Where(re => re.Ano == ano && re.Mes == mes && re.Empresa == empresa)
                .Select(re => re.TotalProvento)
                .ToList()
                .GroupBy(new FaixasSalariaisUtils().FaixaSalarialDescricao)
                .ToDictionary(k => k.Key, v => v.Count());
        }

        public Dictionary<string, int> RelacaoTipoCargo(int ano, int mes, EEmpresa empresa) {
            var relacaoTipoCargo = dbTransparencia.RemuneracoesEmpresas
                .Where(re => re.Ano == ano && re.Mes == mes && re.Empresa == empresa)
                .ToList()
                .GroupBy(re => re.TipoCargo);

            return relacaoTipoCargo
                .ToDictionary(k => k.Key, v => v.Count());
        }

        public IList<RemuneracaoEmpresa> Funcionarios(int ano, int mes, EEmpresa empresa, string cargo, string tipoCargo, string departamento, string faixaSalarial)
        {
            var valoresSalarias = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);
            var funcionario = dbTransparencia.RemuneracoesEmpresas
                .Where(re => re.Ano == ano && 
                    re.Mes == mes && 
                    re.Empresa == empresa && 
                    (cargo == "" || re.Cargo == cargo) && 
                    (tipoCargo == "" || re.TipoCargo == tipoCargo) &&
                    (departamento == "" || re.Departamento == departamento) && 
                    (faixaSalarial == "" || (re.TotalProvento > valoresSalarias.Item1 && re.TotalProvento <= valoresSalarias.Item2))
                    )
                .ToList();

            return funcionario;
        }
    }
}
