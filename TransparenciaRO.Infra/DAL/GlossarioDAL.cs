﻿using TransparenciaRO.Infra.Model.DB.dbTransparencia;
namespace TransparenciaRO.Infra.DAL
{
    public class GlossarioDAL : ReadOnlyDALBase<TermoGlossario>
    {
        public GlossarioDAL(DbTransparencia db) : base(db)
        {
        }
    }
}
