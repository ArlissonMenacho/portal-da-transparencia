﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class TextoFiscalCidadaoDAL
    {
        public DbTransparencia dbTransparencia;
        public TextoFiscalCidadaoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Save(TextoFiscalCidadao modeloResposta)
        {
            dbTransparencia.TextoFiscalCidadao.Add(modeloResposta);
            dbTransparencia.SaveChanges();
        }

        public IList<TextoFiscalCidadao> ListaDeModeloResposta(Guid unidadeGestoraId)
        {
           return dbTransparencia.TextoFiscalCidadao.Where(x => x.UnidadeGestoraId == unidadeGestoraId).ToList();
        }
    }
}
