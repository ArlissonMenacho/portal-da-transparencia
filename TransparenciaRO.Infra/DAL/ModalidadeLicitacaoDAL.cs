﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class ModalidadeLicitacaoDAL
    {

        public DbTransparencia dbTransparencia;

        public ModalidadeLicitacaoDAL(DbTransparencia _dbTransparencia)
        {
            this.dbTransparencia = _dbTransparencia;
        }

        public IEnumerable<ModalidadeLicitacao> GetModalidades()
        {
            return dbTransparencia.ModalidadeLicitacao.ToList();
        }
    }
}
