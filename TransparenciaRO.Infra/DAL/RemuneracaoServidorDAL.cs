﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class RemuneracaoServidorDAL
    {
        public DbTransparencia dbTransparencia;

        public RemuneracaoServidorDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void AdicionarDados(List<RemuneracaoServidor> remuneracaoServidores)
        {
            dbTransparencia.BulkInsert(remuneracaoServidores);
            dbTransparencia.SaveChanges();
        }

        public void VerificarRemoverDados(int ano, int mes)
        {
            if (dbTransparencia.RemuneracaoServidores.Any(x => x.Ano == ano && x.Mes == mes))
            {
                dbTransparencia.RemuneracaoServidores.Where(x => x.Ano == ano && x.Mes == mes).Delete();
            }
        }

        public IList<RemuneracaoServidor> FolhasPagamentos(int ano, int mes, string nome, string cpf, string lotacao)
        {
            return dbTransparencia.RemuneracaoServidores
                .Where(rm => rm.Ano == ano &&
                    rm.Mes == mes &&
                    (nome == "" || rm.Nome.Contains(nome)) &&
                    (cpf == "" || rm.Cpf.Contains(cpf)) &&
                    (lotacao == "" || rm.Lotacao == lotacao))
                .OrderBy(rm => rm.Nome)
                .ToList();
        }
        public IList<RemuneracaoServidor> RelacaoCdsFg(int ano, int mes, string lotacao)
        {
            return dbTransparencia.RemuneracaoServidores
                .Where(rm => rm.Ano == ano &&
                    rm.Mes == mes &&
                    (rm.CDSFG != "" && rm.CDSFG != null) &&
                    (lotacao == "" || rm.Lotacao == lotacao))
                .OrderBy(rm => rm.Nome)
                .ToList();
        }

        public List<string> Lotacoes(int ano, int mes)
        {
            return dbTransparencia.RemuneracaoServidores
                .Where(rs => rs.Ano == ano && rs.Mes == mes)
                .Select(rs => rs.Lotacao)
                .Distinct()
                .OrderBy(x => x)
                .ToList();
        }



        public RemuneracaoServidor RemuneracaoServidor(int ano, int mes, string matricula)
        {
            return dbTransparencia.RemuneracaoServidores.FirstOrDefault(fp => fp.Ano == ano && fp.Mes == mes && fp.Matricula == matricula);
        }

        public int Ano()
        {
            return dbTransparencia.RemuneracaoServidores.FirstOrDefault() != null
                ? dbTransparencia.RemuneracaoServidores.Max(x => x.Ano)
                : 0;
        }

        public int Mes(int ano)
        {
            return dbTransparencia.RemuneracaoServidores.FirstOrDefault() != null
                ? dbTransparencia.RemuneracaoServidores.Where(x => x.Ano == ano).Max(x => x.Mes)
                : 0;
        }

        public List<int> MesesAno(int ano)
        {
            var mesMaximo = dbTransparencia.RemuneracaoServidores.Where(rs => rs.Ano == ano).Max(rs => rs.Mes);
            return Enumerable.Range(1, mesMaximo).ToList();
        }

        public Dictionary<string, int> FaixasSalariais(int ano, int mes)
        {
            var faixaSalarial = dbTransparencia.RemuneracaoServidores
                .Where(rs => rs.Ano == ano && rs.Mes == mes && rs.RendimentosTributaveis != null)
                .Select(rs => rs.RendimentosTributaveis ?? 0)
                .ToList()
                .GroupBy(new FaixasSalariaisUtils().FaixaSalarialDescricao)
                .ToDictionary(k => k.Key, v => v.Count());

            return faixaSalarial;
        }

        public Dictionary<string, int> Classificacoes(int ano, int mes, string faixaSalarial)
        {
            var valores = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);
            return dbTransparencia.RemuneracaoServidores
                .Where(c => c.RendimentosTributaveis > valores.Item1
                       && c.RendimentosTributaveis <= valores.Item2
                       && c.Ano == ano
                       && c.Mes == mes)
                .GroupBy(c => c.Classificacao_Func)
                .OrderByDescending(c => c.Count())
                .ToDictionary(k => k.Key, v => v.Count());
        }

        public Dictionary<string, int> FaixasSalariaisLotacoes(int ano, int mes, string faixaSalarial)
        {
            var valores = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);

            var faixaSalarialLotacoes = dbTransparencia.RemuneracaoServidores
                .Where(rs => rs.Ano == ano
                    && rs.Mes == mes
                    && rs.RendimentosTributaveis > valores.Item1
                    && rs.RendimentosTributaveis <= valores.Item2)
                .GroupBy(rs => rs.Lotacao)
                .OrderBy(rs => rs.Key)
                .ToDictionary(k => k.Key, v => v.Count());

            return faixaSalarialLotacoes;
        }

        public IQueryable<RemuneracaoServidor> FaixasSalariaisFuncionarios(int ano, int mes, string faixaSalarial, string classificacao, string lotacao)
        {
            var valores = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);

            var faixaSalarialFuncionarios = dbTransparencia.RemuneracaoServidores
                .Where(rs => rs.Ano == ano
                        && rs.Mes == mes
                        && (classificacao == "" || rs.Classificacao_Func.Contains(classificacao))
                        && (lotacao == "" || rs.Lotacao == lotacao)
                        && rs.RendimentosTributaveis > valores.Item1
                        && rs.RendimentosTributaveis <= valores.Item2)
                .OrderBy(rs => rs.Nome)
                .ThenBy(rs => rs.Cargo);

            return faixaSalarialFuncionarios;
        }

        public Dictionary<string, int> LotacaoFaixasSalariais(int ano, int mes, string lotacao)
        {
            var lotacaoFaixasSalariais = dbTransparencia.RemuneracaoServidores
                .Where(rs => rs.Ano == ano && rs.Mes == mes && rs.Lotacao == lotacao)
                .Select(rs => rs.RendimentosTributaveis ?? 0)
                .ToList()
                .GroupBy(new FaixasSalariaisUtils().FaixaSalarialDescricao)
                .ToDictionary(k => k.Key, v => v.Count());

            return lotacaoFaixasSalariais;
        }

        public IEnumerable<RemuneracaoServidor> ObterTodosOsServidoresPorAno(int ano)
        {
            var servidores = dbTransparencia.RemuneracaoServidores.Where(servidor => servidor.Ano == ano)
                                                                  .OrderBy(servidor => servidor.Nome.Trim())
                                                                  .ThenBy(servidor => servidor.Mes)
                                                                  .ToList();

            return servidores;
        }

    }
}