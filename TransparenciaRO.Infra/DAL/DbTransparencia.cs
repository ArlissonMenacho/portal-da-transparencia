using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.DB.EntityConfiguration;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using TransparenciaRO.Infra.Model.View.LicitacoesAPISUPEL;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace TransparenciaRO.Infra.DAL
{
    public class DbTransparencia : DbContext
    {
        public DbTransparencia() : base("name=DbTransparencia")
        {
            this.Database.Log = s => { if (s.ToUpper().Contains("INSERT") || s.ToUpper().Contains("UPDATE") || s.ToUpper().Contains("SELECT") || s.ToUpper().Contains("DELETE") || s.ToUpper().Contains("COMPLETED")) Debug.WriteLine(s); };
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<DbTransparencia>(null);
        }

        public string TrackedEntities()
        {
            return ChangeTracker.Entries().Aggregate("", (current, entry) => current + ($"Entity name: {entry.Entity.GetType().FullName}. Status: {entry.State}" + Environment.NewLine));
        }

        public virtual DbSet<ServidoresDesligadosExonerados> ServidoresDesligadosExonerados { get; set; }
        public virtual DbSet<ContratoConvenio> ContratosConvenios { get; set; }
        public virtual DbSet<UnidadeGestora> UnidadesGestoras { get; set; }
        public virtual DbSet<Arquivo> Arquivos { get; set; }
        public virtual DbSet<Pasta> Pastas { get; set; }
        public virtual DbSet<Cidade> Cidades { get; set; }
        public virtual DbSet<Fornecedor> Fornecedores { get; set; }
        public virtual DbSet<FornecedorImpedido> FornecedoresImpedidos { get; set; }
        public virtual DbSet<SocioFornecedor> SocioFornecedores { get; set; }
        public virtual DbSet<Noticia> Noticias { get; set; }
        public virtual DbSet<ContatoInstitucional> Instituicoes { get; set; }
        public virtual DbSet<CargosInstituicao> CargosInstituicoes { get; set; }
        public virtual DbSet<RelacaoServidor> RelacaoServidores { get; set; }
        public virtual DbSet<Lotacao> Lotacoes { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<TermoGlossario> TermosGlossario { get; set; }
        public virtual DbSet<DespesaPortal> DespesasPortal { get; set; }
        public virtual DbSet<Indice> Indice { get; set; }
        public virtual DbSet<Certidao> Certidoes { get; set; }
        public virtual DbSet<ImagemPasta> ImagensPasta { get; set; }
        public virtual DbSet<ConteudoDinamico> ConteudosDinamicos { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<Parametro> Parametros { get; set; }
        public virtual DbSet<RemuneracaoServidor> RemuneracaoServidores { get; set; }
        public virtual DbSet<Licitacao> Licitacoes { get; set; }
        public virtual DbSet<DespesaEmpresa> DespesasEmpresas { get; set; }
        public virtual DbSet<ReceitaEmpresa> ReceitasEmpresas { get; set; }
        public virtual DbSet<RemuneracaoEmpresa> RemuneracoesEmpresas { get; set; }
        public virtual DbSet<DividaAtiva> DividaAtivas { get; set; }
        public virtual DbSet<DiariasEmpresa> DiariasEmpresas { get; set; }
        public virtual DbSet<Documentos> Documentos { get; set; }
        public virtual DbSet<RelacaoPensionista> RelacaoPensionista { get; set; }
        public virtual DbSet<PortariaResolucao> PortariaResolucao { get; set; }
        public virtual DbSet<FiscalCidadao> FiscalCidadao { get; set; }
        public virtual DbSet<FluxoFiscalCidadao> FluxoFiscalCidadao { get; set; }
        public virtual DbSet<RespostaFiscalCidadao> RespostaFiscalCidadao { get; set; }
        public virtual DbSet<TextoFiscalCidadao> TextoFiscalCidadao { get; set; }
        public virtual DbSet<RemuneracaoSoph> RemuneracaoSoph { get; set; }
        public virtual DbSet<ControleDoumentoExterno> ControleDoumentoExterno { get; set; }
        public virtual DbSet<PagamentoFornecedorRPV> PagamentoFornecedorRpvs { get; set; }
        public virtual DbSet<FundoEmpresa> FundoEmpresa { get; set; }
        public virtual DbSet<EmpresaRemuneracao> EmpresaRemuneracao { get; set; }
        public virtual DbSet<Enquete> Enquetes { get; set; }
        public virtual DbSet<EnqueteResultado> EnqueteResultados { get; set; }
        public virtual DbSet<Dialog> Dialog { get; set; }
        public virtual DbSet<Temporarias> Temporarias { get; set; }
        public virtual DbSet<Vantagens> Vantagens { get; set; }
        public virtual DbSet<Licitacaoo> Licitacaoo { get; set; }
        public virtual DbSet<DiariasSugespe> DiariasSugespe { get; set; }
        public virtual DbSet<FornecedorDispensa> FornecedorDispensa { get; set; }
        public virtual DbSet<ItemLicitacao> ItemLicitacao { get; set; }
        public virtual DbSet<EmailNotificacao> EmailNotificacao { get; set; }
        public virtual DbSet<RegistroBoletim> RegistroBoletim { get; set; }
        public virtual DbSet<ContratacaoAditivadas> ContratacaoAditivadas { get; set; }
        //public virtual DbSet<TermoFomento> TermoFomentos { get; set; }

        //Views        
        public virtual DbSet<Empenho> Empenhos { get; set; }
        public virtual DbSet<DetalheEmpenho> DetalheEmpenhos { get; set; }
        public virtual DbSet<ComprasContratacoes> ComprasContratacoess { get; set; }
        public virtual DbSet<ComprasContratacoesEmergenciaisCovid19> ComprasContratacoesEmergenciaisCovid19 { get; set; }
        public virtual DbSet<FolhaPagamento> FolhaPagamento { get; set; }
        public virtual DbSet<ModalidadeLicitacao> ModalidadeLicitacao { get; set; }
        public virtual DbSet<Orgao> Orgaos { get; set; }
        public virtual DbSet<OrgaoSiafem> OrgaosSiafem { get; set; }
        public virtual DbSet<ReceitaAcumuladaSiafem> ReceitaAcumuladaSiafem { get; set; }
        public virtual DbSet<DotacaoInicial> DotacaoInicial { get; set; }
        public virtual DbSet<EmpenhosMunicipios> EmpenhosMunicipios { get; set; }
        public virtual DbSet<Municipios> Municipios { get; set; }
        public virtual DbSet<Diaria> Diarias { get; set; }
        
        public virtual DbSet<Permissao> Permissoes { get; set; }
        public virtual DbSet<FusoHorario> FusoHorarios { get; set; }
        public virtual DbSet<PagamentoFornecedor> PagamentosFornecedores { get; set; }
        public virtual DbSet<Materiais2017> Materiais2017 { get; set; }
        public virtual DbSet<Materiais2018> Materiais2018 { get; set; }
        public virtual DbSet<Materiais2019> Materiais2019 { get; set; }
        public virtual DbSet<Materiais2020> Materiais2020 { get; set; }
        public virtual DbSet<VW_Materiais2020_COVID19> VW_Materiais2020_COVID19 { get; set; }
        public virtual DbSet<InscricaoComiteTransparencia> InscricaoComiteTransparencia { get; set; }
        public virtual DbSet<DespesasSuprimento2019> DespesasSuprimento2019 { get; set; }
        public virtual DbSet<FornecedoresImpedidosLicitar> FornecedoresImpedidosLicitar { get; set; }
        public virtual DbSet<VW_DespesasDiretas> VW_DespesasDiretas { get; set; }
        public virtual DbSet<DadosLicitacao> DadosLicitacao { get; set; }
        public virtual DbSet<Empenhos_API> Empenhos_API { get; set; }
        public virtual DbSet<TermoFomento> TermoFomento { get; set; }
        public virtual DbSet<PrestacaoDeContas> PrestacaoDeContas { get; set; }
        public virtual DbSet<Monitoramento> Monitoramento { get; set; }
        public virtual DbSet<RemuneracaoEquipe> RemuneracaoEquipe { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>().Configure(c => c.HasColumnType("varchar"));

            modelBuilder.Configurations.Add(new ArquivoConfig());
            modelBuilder.Configurations.Add(new PastaConfig());
            modelBuilder.Configurations.Add(new ContratoConvenioConfig());
            modelBuilder.Configurations.Add(new UsuarioConfig());
            modelBuilder.Configurations.Add(new LogConfig());
            modelBuilder.Configurations.Add(new ConteudoDinamicoConfig());
            modelBuilder.Configurations.Add(new LicitacaoConfig());
            modelBuilder.Configurations.Add(new FiscalCidadaoConfig());
            modelBuilder.Configurations.Add(new InscricaoComiteTransparenciaConfig());
            modelBuilder.Configurations.Add(new FornecedorDispensaConfig());
            modelBuilder.Configurations.Add(new TermoFomentoConfig());
            modelBuilder.Configurations.Add(new PagamentoFornecedorRPVConfig());
        }

        //object placeHolderVariable;

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors) // <-- Coloque um Breakpoint aqui para conferir os erros de valida��o.
                {
                  Console.WriteLine("Entidade do tipo \"{0}\" no estado \"{1}\" tem os seguintes erros de valida��o:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Erro: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (DbUpdateException e)
            {
         
                foreach (var eve in e.Entries)
                {
                    var mensagem = $"Entidade do tipo: {eve.Entity.GetType().Name} no estado: {eve.State} tem os seguintes erros de valida��o:";
                }
                throw;
            }
            catch (SqlException s)
            {
                Console.WriteLine("- Message: \"{0}\", Data: \"{1}\"",
                            s.Message, s.Data);
                throw;
            }
        }
    }
}