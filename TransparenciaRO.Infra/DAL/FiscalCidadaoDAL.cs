﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class FiscalCidadaoDAL
    {
        public DbTransparencia dbTransparencia;

        public FiscalCidadaoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public FiscalCidadao Salvar(FiscalCidadao fiscalCidadao)
        {
            dbTransparencia.FiscalCidadao.Add(fiscalCidadao);
            dbTransparencia.SaveChanges();
            fiscalCidadao.Imagem = null;
            return fiscalCidadao;
        }

        public void Update(FiscalCidadao fiscalCidadao)
        {
            FiscalCidadao cidadao = new FiscalCidadao();
            cidadao = BuscarProtocolo(fiscalCidadao.ProtocoloId);
            cidadao.EStatus = fiscalCidadao.EStatus;
            cidadao.DataDaFinaliacao = fiscalCidadao.DataDaFinaliacao;
            cidadao.UnidadeGestoraId = fiscalCidadao.UnidadeGestoraId;
            cidadao.UsuarioId = fiscalCidadao.UsuarioId;
            dbTransparencia.Entry(cidadao).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public IList<FiscalCidadao> BuscarDemandaCidadao(string categoria, DateTime dateInicial, DateTime dateFinal, string unidadeGestora)
        {
            var ugGuid = new Guid();
            if (unidadeGestora != "")
            {
                ugGuid = new Guid(unidadeGestora);
            }

            return dbTransparencia.FiscalCidadao
                .Include(u => u.UnidadeGestora)
                .Where(x =>
                    (categoria == "" || x.Categoria.Contains(categoria))
                    && (dateInicial == null || x.DataFiscalizacao >= dateInicial)
                    && (dateFinal == null || x.DataFiscalizacao <= dateFinal)
                    && (unidadeGestora == "" || x.UnidadeGestoraId == ugGuid)
                )
                .OrderBy(x => x.ProtocoloId)
                .ToList();
        }

        public FiscalCidadao ConsultarFluxo(long id)
        {
            FiscalCidadao fiscalCidadao = dbTransparencia.FiscalCidadao
                .Include(r => r.RespostaFiscalCidadao)
                .Include(x => x.UnidadeGestora)
                .FirstOrDefault(x => x.ProtocoloId == id);
            if (fiscalCidadao != null)
            {
                fiscalCidadao.FluxoFiscalCidadaos = new FluxoFiscalCidadaoDAL(dbTransparencia).BuscarFluxos(id);
                
            }
            return fiscalCidadao;
        }

        public Boolean VerificarSePossuiResposta(long id)
        {
            FiscalCidadao fiscalCidadao = dbTransparencia.FiscalCidadao
                .Include(r => r.RespostaFiscalCidadao)
                .Include(x => x.UnidadeGestora)
                .FirstOrDefault(x => x.ProtocoloId == id);

            if (fiscalCidadao != null && fiscalCidadao.RespostaFiscalCidadao.Count > 0)
            {
                return true;
            }
            return false;
        }

        public Dictionary<string, int> CarregarGrafico(string categoria, DateTime dateInicial, DateTime dateFinal, string unidadeGestora)
        {
            var ugGuid = new Guid();
            if (unidadeGestora != "")
            {
                ugGuid = new Guid(unidadeGestora);
            }
            var fiscalizacao = dbTransparencia.FiscalCidadao
                .Include(u => u.UnidadeGestora)
                .Where(x =>
                    (categoria == "" || x.Categoria.Contains(categoria))
                    && (dateInicial == null || x.DataFiscalizacao >= dateInicial)
                    && (dateFinal == null || x.DataFiscalizacao <= dateFinal)
                    && (unidadeGestora == "" || x.UnidadeGestoraId == ugGuid)
                )
                .GroupBy(x => x.Categoria)
                .Select(rs => new { rs.Key, Count = rs.Count() })
                .ToDictionary(k => k.Key, v => v.Count);
            return fiscalizacao;
        }

        public FiscalCidadao BuscarProtocolo(long id)
        {
            return dbTransparencia
                .FiscalCidadao
                .Include(u => u.UnidadeGestora)
                .Include(r => r.RespostaFiscalCidadao)
                .FirstOrDefault(x => x.ProtocoloId == id);

        }

        public IEnumerable<FiscalCidadao> BuscarProtocoloPorEmail(string email)
        {
            return dbTransparencia
                .FiscalCidadao
                .Where(x => x.Email == email).ToList();
        }
    }
}
