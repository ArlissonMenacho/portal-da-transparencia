﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.BulkInsert.Extensions;
using System.Data.Entity;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class ServidoresDesligadosExoneradosDAL
    {
        public DbTransparencia dbTransparencia;

        public ServidoresDesligadosExoneradosDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }
        public void Adicionar(List<ServidoresDesligadosExonerados> servidoresDesligadosExonerados)
        {
            dbTransparencia.BulkInsert(servidoresDesligadosExonerados);
            dbTransparencia.SaveChanges();
        }

        public int Ano()
        {
            return dbTransparencia.RemuneracaoServidores.FirstOrDefault() != null
                ? dbTransparencia.RemuneracaoServidores.Max(x => x.Ano)
                : 0;
        }

        public int Mes(int ano)
        {
            return dbTransparencia.RemuneracaoServidores.FirstOrDefault() != null
                ? dbTransparencia.RemuneracaoServidores.Where(x => x.Ano == ano).Max(x => x.Mes)
                : 0;
        }
        public List<int> Meses(int ano)
        {
            var mesMaximo = ano == DateTime.Now.Year
                ? dbTransparencia.ServidoresDesligadosExonerados.Where(s => s.Ano == ano).Max(s => s.Mes)
                : 12;

            return Enumerable.Range(1, mesMaximo).ToList();
        }

        public void VerficarRemoverDados(int ano, int mes)
        {
            if (dbTransparencia.ServidoresDesligadosExonerados.Any(x => x.Ano == ano && x.Mes == mes))
            {
                dbTransparencia.ServidoresDesligadosExonerados.Where(x => x.Ano == ano && x.Mes == mes).Delete();
            }
        }

        public IList<ServidoresDesligadosExonerados> BuscarServidoresDesligadosExonerados(int ano, int mes, string nome, string cpf)
        {

            return dbTransparencia.ServidoresDesligadosExonerados.Include(sde => sde.Lotacao)
                .Where(sde => sde.Ano == ano &&
                    sde.Mes == mes &&
                    (nome == "" || sde.Nome.Contains(nome)) &&
                    (cpf == "" || sde.Cpf.Contains(cpf)))
                   .OrderBy(sde => sde.Nome)
                    .ToList();
        }
    }
}
