﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class PastaDAL
    {
        public DbTransparencia dbTransparencia;

        public IEnumerable<Pasta> GetPastasEArquivos(Guid? pPastaOrigemId, bool pRecursivo = false)
        {
            var guidMenuPrincipal = GuidUtils.GuidMenuPrincipal();
            var retorno = dbTransparencia.Pastas.Where(p => p.PastaOrigemId == (pPastaOrigemId ?? guidMenuPrincipal)).Include(p => p.Arquivos).ToList();
            if (pRecursivo)
                retorno.ForEach(p => p.SubPastas = GetPastasEArquivos(p.PastaId, true).ToList());
            return retorno;
        }

        public void SalvarPastaEArquivos(List<Pasta> pPastas)
        {
            SalvarPastaEArquivos(pPastas, true);
        }

        private void SalvarPastaEArquivos(List<Pasta> pPastas, bool pSaveChanges)
        {
            foreach (var pasta in pPastas)
            {
                dbTransparencia.Pastas.Add(pasta);
                foreach (var arquivo in pasta.Arquivos)
                {
                    dbTransparencia.Arquivos.Add(arquivo);
                }
                SalvarPastaEArquivos(pasta.SubPastas.ToList(), false);
            }
            if (pSaveChanges)
                dbTransparencia.SaveChanges();
        }

        public PastaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public List<Pasta> ObterPastasAdministraveis(Guid? pPastaOrigemId = null)
        {            
            return dbTransparencia.Pastas.Where(p => p.Administravel && (pPastaOrigemId == null || p.PastaId == pPastaOrigemId.Value)).ToList().Select(p => ObterPastaEAncestrais(p.PastaId)).ToList();
        }

        public Pasta ObterPastaEAncestrais(Guid pPastaId)
        {
            return dbTransparencia.Pastas.Where(p => p.PastaId == pPastaId).ToList().Select(p => new Pasta
            {
                PastaId = pPastaId,
                Descricao = p.Descricao,
                Nome = p.Nome,
                IconeFa = p.IconeFa,
                Arquivos = p.Arquivos,
                PastaOrigem = p.PastaOrigemId != null ? ObterPastaEAncestrais(p.PastaOrigemId.Value) : null,
                PastaOrigemId = p.PastaOrigemId
            }).FirstOrDefault();
        }

        public IEnumerable<Pasta> BuscarPastasPorPastaPai(Guid pPastaOrigemId)
        {
            return dbTransparencia.Pastas.Include(p => p.PastaOrigem).Where(p => p.PastaOrigemId == pPastaOrigemId && p.DataRemocao == null).ToList();
        }

        public IQueryable<Pasta> BuscarPastasAcessoRestrito(List<string> pPermissoes)
        {
            return dbTransparencia.Pastas.Where(p => pPermissoes.Contains(p.PerfilPasta.ToString()));
        }

        public IQueryable<Pasta> BuscarPastasDoUsuario(List<string> pGuids)
        {
            return dbTransparencia.Pastas.Where(p => pGuids.Contains(p.PastaId.ToString().Replace("-", "")));
        }

        public Pasta GetPasta(Guid pPastaId)
        {
            return dbTransparencia.Pastas
                .Include(p => p.Imagens)
                .Include(p => p.SubPastas)
                .Include(p => p.Arquivos)
                .Where(p => p.PastaId == pPastaId && p.DataRemocao == null)
                .ToList()
                .Select(p => new Pasta
                {
                    SubPastas = p.SubPastas.Where(s => s.DataRemocao == null).ToList(),
                    Arquivos = p.Arquivos.Where(a => a.DataRemocao == null).ToList(),
                    PastaOrigemId = p.PastaOrigemId,
                    PastaId = p.PastaId,
                    DataRemocao = p.DataRemocao,
                    Administravel = p.Administravel,
                    Agrupamento = p.Agrupamento,
                    Contexto = p.Contexto,
                    Descricao = p.Descricao,
                    Href = p.Href,
                    IconeFa = p.IconeFa,
                    IconeImagem = p.IconeImagem,
                    Imagens = p.Imagens,
                    Nome = p.Nome,
                    Ordem = p.Ordem,
                    PerfilPasta = p.PerfilPasta,
                    Publica = p.Publica,
                    TemaCss = p.TemaCss,
                    TipoImagem = p.TipoImagem
                }).FirstOrDefault();
        }

        public IEnumerable<Pasta> GetHierarquiaPastasPorHref(string pHref)
        {
            return GetHierarquiaPastas((dbTransparencia.Pastas.FirstOrDefault(p => p.Href == pHref) ?? new Pasta()).PastaId);
        }

        public IEnumerable<Pasta> GetHierarquiaPastas(Guid pPasta)
        {
            var retorno = new List<Pasta>();
            Pasta pastaAtual = GetPasta(pPasta);
            do
            {
                if (pastaAtual != null)
                {
                    retorno.Add(pastaAtual);
                    pastaAtual = dbTransparencia.Pastas.FirstOrDefault(p => p.PastaId == pastaAtual.PastaOrigemId && p.DataRemocao == null);
                }

            } while (pastaAtual != null);
            retorno.Reverse();
            return retorno;
        }

        public void Adicionar(Pasta pasta)
        {
            dbTransparencia.Pastas.Add(pasta);
            dbTransparencia.SaveChanges();
        }

        public ICollection<Pasta> BuscarPastas()
        {
            ICollection<Pasta> pastas = new List<Pasta>();
            foreach (var pasta in dbTransparencia.Pastas.Where(p => p.PastaOrigemId == null && p.DataRemocao == null))
            {
                pastas.Add(pasta);
            }

            return pastas;
        }

        public Pasta BuscarPasta(Guid pastaGuid)
        {
            return dbTransparencia.Pastas.Include(p => p.Imagens).FirstOrDefault(p => p.PastaId == pastaGuid);
        }

        public ICollection<Pasta> BuscarSubPastas(Guid pastaGuid)
        {
            ICollection<Pasta> pastas = new List<Pasta>();
            foreach (var pasta in dbTransparencia.Pastas.Include(p => p.SubPastas).Include(p => p.Arquivos).Where(p => p.PastaOrigemId == pastaGuid && p.DataRemocao == null))
            {
                pastas.Add(pasta);
            }

            return pastas;
        }

        public Pasta DetalharPasta(Guid pPasta)
        {
            var pasta = dbTransparencia
                .Pastas.Include(p => p.Arquivos).Include(p => p.SubPastas).Include(p => p.Imagens)
                .Where(p => p.PastaId == pPasta)
                .ToList()
                .Select(p => new Pasta
                {
                    PastaId = p.PastaId,
                    Administravel = p.Administravel,
                    Agrupamento = p.Agrupamento,
                    Imagens = p.Imagens,
                    Arquivos = p.Arquivos,
                    Contexto = p.Contexto,
                    DataRemocao = p.DataRemocao,
                    Descricao = p.Nome,
                    Href = p.Href,
                    IconeFa = p.IconeFa,
                    IconeImagem = p.IconeImagem,
                    PerfilPasta = p.PerfilPasta,
                    Ordem = p.Ordem,
                    Publica = p.Publica,
                    SubPastas = p.SubPastas.Select(sb => DetalharPasta(sb.PastaId)).OrderBy(sbp => sbp.Ordem).ToList()
                }).FirstOrDefault();

            return pasta;
        }

        public ICollection<Arquivo> BuscarArquivosPorPasta(Guid pastaGuid)
        {
            ICollection<Arquivo> arquivos = new List<Arquivo>();
            foreach (var arquivo in dbTransparencia.Arquivos.Where(arquivo => arquivo.PastaId == pastaGuid && arquivo.DataRemocao == null))
            {
                arquivos.Add(arquivo);
            }

            return arquivos;
        }

        public Pasta BuscarPastaComSubPastasEArquivos(Guid pastaGuid)
        {
            return dbTransparencia.Pastas
                .Include(p => p.Imagens)
                .Include(p => p.SubPastas)
                .Include(p => p.Arquivos)
                .Where(p => p.PastaId == pastaGuid && p.DataRemocao == null)
                .ToList()
                .Select(p => new Pasta
                {
                    SubPastas = p.SubPastas.Where(s => s.DataRemocao == null).ToList(),
                    Arquivos = p.Arquivos.Where(a => a.DataRemocao == null).ToList(),
                    PastaOrigemId = p.PastaOrigemId,
                    PastaId = p.PastaId,
                    DataRemocao = p.DataRemocao,
                    Administravel = p.Administravel,
                    Agrupamento = p.Agrupamento,
                    Contexto = p.Contexto,
                    Descricao = p.Descricao,
                    Href = p.Href,
                    IconeFa = p.IconeFa,
                    IconeImagem = p.IconeImagem,
                    Imagens = p.Imagens,
                    Nome = p.Nome,
                    Ordem = p.Ordem,
                    PerfilPasta = p.PerfilPasta,
                    Publica = p.Publica,
                    TemaCss = p.TemaCss,
                    TipoImagem = p.TipoImagem
                }).FirstOrDefault();
        }

        public void Editar(Pasta pasta)
        {
            dbTransparencia.Entry(pasta).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public void Salvar(Pasta pasta)
        {
            dbTransparencia.Pastas.Add(pasta);
            dbTransparencia.SaveChanges();
        }

        public bool Remover(Guid pastaGuid)
        {
            Pasta pasta = BuscarPasta(pastaGuid);
            pasta.Arquivos = BuscarArquivosPorPasta(pastaGuid);
            var pastasASeremExcluidas = new List<Guid>() { pastaGuid };

            if (pasta.Arquivos.Count > 0)
            {
                return false;
            }

            foreach (var subPasta in BuscarSubPastas(pastaGuid))
            {
                if (subPasta.Arquivos.Count > 0)
                    return false;

                pastasASeremExcluidas.Add(subPasta.PastaId);
                subPasta.SubPastas = BuscarSubPastas(subPasta.PastaId);

                if (!Remover(subPasta.PastaId)) return false;
            }

            dbTransparencia.Pastas.Where(p => pastasASeremExcluidas.Contains(p.PastaId)).ToList()
                .ForEach(p => p.DataRemocao = DateTime.Now);

            dbTransparencia.SaveChanges();

            return true;
        }
    }
}