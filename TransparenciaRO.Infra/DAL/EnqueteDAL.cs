﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class EnqueteDAL
    {
        public readonly DbTransparencia dbTransparencia;

        public EnqueteDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Add(Enquete enquete)
        {
            dbTransparencia.Enquetes.Add(enquete);
            dbTransparencia.SaveChanges();
        }

        public void Update(Enquete enquete)
        {
            dbTransparencia.Entry(enquete).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public Enquete GetById(int? id)
        {
            return dbTransparencia.Enquetes.Include(x => x.EnqueteResultados).FirstOrDefault(x => x.EnqueteId == id);
        }
        public void AddResult(EnqueteResultado enqueteResultado)
        {
            dbTransparencia.EnqueteResultados.Add(enqueteResultado);
            dbTransparencia.SaveChanges();
        }

        public IList<Enquete> Enquetes()
        {
            return dbTransparencia.Enquetes.Include(x => x.EnqueteResultados).ToList();
        }

        public Dictionary<string, int> EnqueteResultados(int enqueteId)
        {
            return dbTransparencia.EnqueteResultados
                .Where(x => x.EnqueteId == enqueteId)
                .Select(r => r.Avaliacao)
                .ToList()
                .GroupBy(new EnqueteResultadoUtils().ResultadoDescricao)
                .ToDictionary(k => k.Key, v => v.Count());
        }
    }
}