﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TransparenciaRO.DataDBPLL.Contexto;
using TransparenciaRO.Infra.Factory;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View;


namespace TransparenciaRO.Infra.DAL
{
    public class SuprimentoDeFundosDAL : ReadOnlyDALBase<SuprimentoDeFundos>
    {
        protected List<string> unidadesGestorasQueNaoDevemAparecerEmSuprimentoDeFundos = new List<string>()
        {
            "MINISTERIO PUBLICO DO ESTADO DE RONDONIA",
            "Ministerio Público do Estado de Rondônia",
            "FUNDO DE DESENV. INSTITUCIONAL DO MP/RO",
            "FUNDO DE INF.EDIF.E APERF.DOS S.JUDICIARIOS",
            "TRIBUNAL DE CONTAS DO ESTADO DE RONDONIA"
        };

        public SuprimentoDeFundosDAL(DbTransparencia db, TransparenciaRO.DataDBPLL.Contexto.DbPLLContext dbPLL) : base(db, dbPLL)
        {
            //
        }

        public async Task<IndexSuprimentoDeFundosViewModel> BuscarTodos()
        {
            var indexSuprimentoDeFundosViewModel = new IndexSuprimentoDeFundosViewModel();

            var despesaSuprimentos = await _dbPLL.DespesasSuprimentos.AsNoTracking()
                                                               .OrderByDescending(x => x.DataDocumento)
                                                               .Take(10).ToListAsync();

            indexSuprimentoDeFundosViewModel.SuprimentoDeFundos = SuprimentoDeFundosFactory.ConverterSuprimentosDeFundosParaSuprimentosDeFundosViewModel(despesaSuprimentos);

            indexSuprimentoDeFundosViewModel.UG = new List<string>();
            var listaDeUnidades = await _dbPLL.DespesasSuprimentos.Where(x => !unidadesGestorasQueNaoDevemAparecerEmSuprimentoDeFundos.Contains(x.UnidadeGestora)).OrderBy(x => x.UnidadeGestora).Select(x => x.UnidadeGestora).ToListAsync();
            indexSuprimentoDeFundosViewModel.UG.Add("TODAS AS UNIDADES GESTORAS");
            foreach (var unidade in listaDeUnidades)
            {
                var JaExiste = indexSuprimentoDeFundosViewModel.UG.Contains(unidade);
                if (!JaExiste)
                {
                    indexSuprimentoDeFundosViewModel.UG.Add(unidade);
                }
            }

            indexSuprimentoDeFundosViewModel.Ano = ObterListaDeAnosAPartirDe2018AteOAnoAtual();

            return indexSuprimentoDeFundosViewModel;
        }

        public async Task<IndexSuprimentoDeFundosViewModel> BuscarDadosParaCarregamentoDaTelaDeConsulta()
        {
            var indexSuprimentoDeFundosViewModel = new IndexSuprimentoDeFundosViewModel();

            indexSuprimentoDeFundosViewModel.SuprimentoDeFundos = new List<SuprimentoDeFundosViewModel>();
            indexSuprimentoDeFundosViewModel.UG = new List<string>();
            var listaDeUnidades = await _dbPLL.DespesasSuprimentos.Where(x => !unidadesGestorasQueNaoDevemAparecerEmSuprimentoDeFundos.Contains(x.UnidadeGestora))
                .OrderBy(x => x.UnidadeGestora)
                .Select(x => x.UnidadeGestora)
                .ToListAsync();
            indexSuprimentoDeFundosViewModel.UG.Add("TODAS AS UNIDADES GESTORAS");
            foreach (var unidade in listaDeUnidades)
            {
                var JaExiste = indexSuprimentoDeFundosViewModel.UG.Contains(unidade);
                if (!JaExiste)
                {
                    indexSuprimentoDeFundosViewModel.UG.Add(unidade);
                }
            }

            indexSuprimentoDeFundosViewModel.Ano = ObterListaDeAnosAPartirDe2018AteOAnoAtual();

            return indexSuprimentoDeFundosViewModel;
        }

        public List<SuprimentoDeFundosViewModel> FiltrarSuprimentos(string unidadeGestora, short? exercicio)
        {
            var suprimentoDeFundos = new List<DataDBPLL.Model.DespesasSuprimentos>();
            if (unidadeGestora == "TODAS AS UNIDADES GESTORAS")
            {
                suprimentoDeFundos = _dbPLL.DespesasSuprimentos.AsNoTracking()
                                                                    .Where(x => x.Exercicio == exercicio
                                                                    && !unidadesGestorasQueNaoDevemAparecerEmSuprimentoDeFundos.Contains(x.UnidadeGestora))
                                                                    .Take(250).ToList();
            }
            else
            {
                suprimentoDeFundos = _dbPLL.DespesasSuprimentos.AsNoTracking()
                                                               .Where(x => x.UnidadeGestora == unidadeGestora
                                                               && x.Exercicio == exercicio
                                                               && !unidadesGestorasQueNaoDevemAparecerEmSuprimentoDeFundos.Contains(x.UnidadeGestora))
                                                               .Take(250).ToList();
            }

            var suprimentoDeFundosViewModel = SuprimentoDeFundosFactory.ConverterSuprimentosDeFundosParaSuprimentosDeFundosViewModel(suprimentoDeFundos);

            return suprimentoDeFundosViewModel;
        }

        private List<short?> ObterListaDeAnosAPartirDe2018AteOAnoAtual()
        {
            var anoInicial = 2018;

            var anoAtual = DateTime.Now.Year;

            var listaDeAnos = new List<short?>();

            for (var ano = anoInicial; ano <= anoAtual; ano++)
            {
                var anoShort = short.Parse(ano.ToString());

                listaDeAnos.Add(anoShort);
            }

            return listaDeAnos;
        }
    }
}