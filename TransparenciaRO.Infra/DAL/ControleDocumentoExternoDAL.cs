﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static System.Data.Entity.EntityState;

namespace TransparenciaRO.Infra.DAL
{
    public class ControleDocumentoExternoDAL
    {
        public DbTransparencia dbTransparencia;

        public ControleDocumentoExternoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }
        public List<ControleDoumentoExterno> BuscarDocumento(string numeroDocumento, string unidadeGestora)
        {
            var ugGuid = new Guid();
            if (unidadeGestora != "")
            {
                ugGuid = new Guid(unidadeGestora);
            }

            return dbTransparencia.ControleDoumentoExterno
                .Include(u => u.UnidadeGestora)
                .Where(x =>
                    (numeroDocumento == "" || x.NumeroProcessoDocumento.Contains(numeroDocumento))
                    && (unidadeGestora == "" || x.UnidadeGestoraId == ugGuid)
                    )
                    .OrderBy(s => s.ETipoStatus)
                    .ToList();
        }
        public ControleDoumentoExterno Buscar(Guid documentoId)
        {
            var retorno = dbTransparencia
                .ControleDoumentoExterno
                .Include(cc => cc.Arquivos)
                .FirstOrDefault(cc => cc.ControleDoumentoExternoId == documentoId);

            retorno.Arquivos = retorno.Arquivos.Where(a => a.DataRemocao == null).ToList();

            return retorno;
        }
        public ControleDoumentoExterno BuscarPorId(Guid id)
        {
            return dbTransparencia.ControleDoumentoExterno.FirstOrDefault(x => x.ControleDoumentoExternoId == id);
        }

        public void Save(ControleDoumentoExterno controleDocumento)
        {
            dbTransparencia.ControleDoumentoExterno.Add(controleDocumento);
            dbTransparencia.SaveChanges();
        }

        public void Update(ControleDoumentoExterno controleDoumentoExterno)
        {
            dbTransparencia.Entry(controleDoumentoExterno).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public Guid? AdicionarEditar(ControleDoumentoExterno controleDoumentoExterno, out string mensagemErro)
        {
            try
            {
                if (controleDoumentoExterno.ControleDoumentoExternoId == new Guid())
                {
                    //Inserção
                    controleDoumentoExterno.ControleDoumentoExternoId = Guid.NewGuid();
                    foreach (var arquivo in controleDoumentoExterno.Arquivos)
                    {
                        dbTransparencia.Arquivos.Attach(arquivo);
                        arquivo.Temporario = false;
                    }
                    dbTransparencia.ControleDoumentoExterno.Add(controleDoumentoExterno);
                }
                else
                {
                    //Alteração
                    var registroBD = dbTransparencia.ControleDoumentoExterno.Include(cc => cc.Arquivos).FirstOrDefault(cc => cc.ControleDoumentoExternoId == controleDoumentoExterno.ControleDoumentoExternoId);
                    dbTransparencia.Entry(dbTransparencia.ControleDoumentoExterno.FirstOrDefault(cc => cc.ControleDoumentoExternoId == controleDoumentoExterno.ControleDoumentoExternoId)).CurrentValues.SetValues(controleDoumentoExterno);
                    //Adiciona os novos arquivos
                    var arquivosAInserir = dbTransparencia.Arquivos.Where(a => a.ControleDoumentoExternoId == controleDoumentoExterno.ControleDoumentoExternoId && a.DataRemocao == null && a.Temporario);
                    foreach (var arquivoAInserir in arquivosAInserir)
                    {
                        if (dbTransparencia.Entry(arquivoAInserir).State == Detached)
                            dbTransparencia.Arquivos.Attach(arquivoAInserir);
                        registroBD.Arquivos.Add(arquivoAInserir);
                    }
                    //Remove os arquivos que foram desvinculados
                    var arquivosARemover = dbTransparencia.Arquivos.Where(a => a.ControleDoumentoExternoId == controleDoumentoExterno.ControleDoumentoExternoId && a.DataRemocao == null).Except(controleDoumentoExterno.Arquivos, a => a.ArquivoId).ToList();
                    arquivosARemover.ForEach(a => { dbTransparencia.Entry(a).State = Modified; a.DataRemocao = DateTime.Now; });

                }
                foreach (var arquivo in dbTransparencia.Arquivos.Where(a => a.ControleDoumentoExternoId == controleDoumentoExterno.ControleDoumentoExternoId))
                {
                    arquivo.Temporario = false;
                }
               dbTransparencia.SaveChanges();
                mensagemErro = "";
                return controleDoumentoExterno.ControleDoumentoExternoId;
            }
            catch (Exception e)
            {
                mensagemErro = e.ToString();
                return null;
            }
        }
    }
}