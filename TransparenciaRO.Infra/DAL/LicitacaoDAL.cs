﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using static System.Data.Entity.EntityState;
using System.Data.Entity;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;
using Z.EntityFramework.Extensions.Core.SchemaObjectModel;
using System.Runtime.CompilerServices;
using TransparenciaRO.Infra.Migrations.DbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class LicitacaoDAL : DALBaseComArquivos<Licitacao>
    {
        public LicitacaoDAL(DbTransparencia dbTransparencia) : base(dbTransparencia)
        {
            //
        }

        public IList<Licitacao> GetLicitacao(DateTime? dataInicial, DateTime? dataFinal, Guid? ug, EModalidadeLicitacao? modalidade, int? numeroLicitacao, string numeroProcesso)
        {
            if (!numeroProcesso.IsNullOrEmpty() || numeroLicitacao > 0)
            {
                dataInicial = null;
                dataFinal = null;
                modalidade = null;
            }

            return db.Licitacoes
                .Include(l => l.UnidadeGestora)
                .Where(l =>
                    (numeroLicitacao == null || l.NumeroLicitacao == numeroLicitacao)
                    && (dataInicial == null || l.DataDaPublicacao >= dataInicial)
                    && (dataFinal == null || l.DataDaPublicacao <= dataFinal)
                    && (ug == null || l.UnidadeGestoraId == ug)
                    && (numeroProcesso == "" || l.NumeroProcessoAdministrativo == numeroProcesso)
                    && (modalidade == 0 || l.ModalidadeLicitacaoString == modalidade.ToString()))
                .OrderByDescending(l => l.DataDaPublicacao)
                .ToList();
        }

        public IList<Licitacao> GetDispensaLicitacao(DateTime? dataInicial, DateTime? dataFinal, Guid? ug, EModalidadeLicitacao? modalidade, string numeroProcesso)
        {
            var consulta = db.Licitacoes.Include(licitacao => licitacao.UnidadeGestora);

            if ((ug != null && ug != Guid.Empty) && ug == Guid.Parse("66758e7f-7a7b-41ea-8154-0cef5077b5e1")) // Verifica se Unidade Gestora selecionada for a SESAU
            {
                var fundoEstadualDeSaudeId = Guid.Parse("c31f6bbf-1609-4d62-b60f-fd876e52f89f");

                consulta = consulta.Where(licitacao =>
                       (dataInicial == null || licitacao.DataDaPublicacao >= dataInicial)
                    && (dataFinal == null || licitacao.DataDaPublicacao <= dataFinal)
                    && (ug == null || licitacao.UnidadeGestoraId == ug || licitacao.UnidadeGestoraId == fundoEstadualDeSaudeId)
                    && (string.IsNullOrEmpty(numeroProcesso) || licitacao.NumeroProcessoAdministrativo.Contains(numeroProcesso))
                    && (modalidade == 0 || modalidade == null || licitacao.ModalidadeLicitacaoString == modalidade.ToString())
                );
            }
            else
            {
                consulta = consulta.Where(licitacao =>
                       (dataInicial == null || licitacao.DataDaPublicacao >= dataInicial)
                    && (dataFinal == null || licitacao.DataDaPublicacao <= dataFinal)
                    && (ug == null || licitacao.UnidadeGestoraId == ug)
                    && (string.IsNullOrEmpty(numeroProcesso) || licitacao.NumeroProcessoAdministrativo.Contains(numeroProcesso))
                    && (modalidade == 0 || modalidade == null || licitacao.ModalidadeLicitacaoString == modalidade.ToString())
                );
            }

            var licitacoes = consulta.OrderByDescending(l => l.DataDaPublicacao).ToList();

            return licitacoes;
        }

        public override List<Arquivo> ObterArquivos(Guid pId)
        {
            return db.Licitacoes.FirstOrDefault(l => l.LicitacaoId == pId)?.Arquivos.ToList();
        }

        public Licitacao Buscar(Guid pId)
        {
            var retorno = db.Licitacoes
                            .Include(cc => cc.Arquivos)
                            .Include(cc => cc.Fornecedores)
                            .Include(cc => cc.ItensLicitacao)
                            .FirstOrDefault(cc => cc.LicitacaoId == pId);

            retorno.UnidadeGestora = BuscarPorId(retorno.UnidadeGestoraId);

            retorno.Arquivos = retorno.Arquivos.Where(a => a.DataRemocao == null).OrderBy(a => a.DataUpload).ToList();

            return retorno;
        }

        private UnidadeGestora BuscarPorId(Guid pId)
        {
            return db
                .UnidadesGestoras
                .FirstOrDefault(x => x.unidadeGestoraID == pId);

        }

        public override Licitacao ObterPorId(Guid pId)
        {
            return db.Licitacoes.FirstOrDefault(l => l.LicitacaoId == pId);
        }

        public bool  LicitacaoExiste(string NumeroProcessoAdministrativo)
        {
            return db.Licitacoes.Any(l => l.NumeroProcessoAdministrativo == NumeroProcessoAdministrativo);
        }
        public override void AtribuirArquivo(Arquivo pArquivo, Guid pId)
        {
            pArquivo.LicitacaoId = pId;
        }

        public Guid? AdicionarDispensaLicitacao(Licitacao pLicitacao, out string pMensagemErro)
        {
            try
            {
                pLicitacao.LicitacaoId = Guid.NewGuid();

                pLicitacao.Fornecedores.ForEach(f => f.LicitacaoId = pLicitacao.LicitacaoId);

                if (pLicitacao.ItensLicitacao != null)
                    pLicitacao.ItensLicitacao.ForEach(il => il.LicitacaoId = pLicitacao.LicitacaoId);

                foreach (var arquivo in pLicitacao.Arquivos)
                {
                    db.Arquivos.Attach(arquivo);
                    arquivo.Temporario = false;
                }

                db.Licitacoes.Add(pLicitacao);

                db.SaveChanges();

                pMensagemErro = "";

                return pLicitacao.LicitacaoId;
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();

                return null;
            }
        }

        public Guid? EditarDispensaLicitacao(Licitacao pLicitacao, out string pMensagemErro)
        {
            try
            {
                //Alteração
                var registroBD = db.Licitacoes.Include(cc => cc.Arquivos).FirstOrDefault(cc => cc.LicitacaoId == pLicitacao.LicitacaoId);
                db.Entry(db.Licitacoes.FirstOrDefault(cc => cc.LicitacaoId == pLicitacao.LicitacaoId)).CurrentValues.SetValues(pLicitacao);


                if (pLicitacao.ItensLicitacao == null)
                    pLicitacao.ItensLicitacao = new List<ItemLicitacao>();

                //itens da licitação
                var itensLicitacaoDB = db.ItemLicitacao.Where(il => il.LicitacaoId == pLicitacao.LicitacaoId).ToList();
                AdicionarEditarItensLicitacao(itensLicitacaoDB, pLicitacao);
                RemoverItensLicitacaoExcluidos(itensLicitacaoDB, pLicitacao);
                pLicitacao.ItensLicitacao = null;

                //fornecedores
                var fornecedoresDB = db.FornecedorDispensa.Where(fdb => fdb.LicitacaoId == pLicitacao.LicitacaoId).ToList();
                AdicionarEditarFornecedoresDispensa(fornecedoresDB, pLicitacao);
                RemoverFornecedoresDispensaExcluidos(fornecedoresDB, pLicitacao);
                //ExcluirArquivosDosFornecedores(pLicitacao);                    

                //arquivos
                var arquivosAInserir = db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId && a.DataRemocao == null && a.Temporario);

                foreach (var arquivoAInserir in arquivosAInserir)
                {
                    if (db.Entry(arquivoAInserir).State == Detached)
                        db.Arquivos.Attach(arquivoAInserir);
                    registroBD.Arquivos.Add(arquivoAInserir);
                }

                //Remove os arquivos que foram desvinculados
                var arquivosARemover = db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId && a.DataRemocao == null).Except(pLicitacao.Arquivos, a => a.ArquivoId).ToList();
                arquivosARemover.ForEach(a => { db.Entry(a).State = Modified; a.DataRemocao = DateTime.Now; });

                foreach (var arquivo in db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId))
                {
                    arquivo.Temporario = false;
                }

                //AdicionarArquivosDosFornecedores(pLicitacao);

                db.SaveChanges();

                pMensagemErro = "";

                return pLicitacao.LicitacaoId;
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();

                return null;
            }
        }

        public Guid? AdicionarEditar(Licitacao pLicitacao, out string pMensagemErro)
        {
            try
            {
                if (pLicitacao.LicitacaoId == new Guid())
                {
                    //Inserção
                    pLicitacao.LicitacaoId = Guid.NewGuid();

                    pLicitacao.Fornecedores.ForEach(f => f.LicitacaoId = pLicitacao.LicitacaoId);

                    if (pLicitacao.ItensLicitacao != null)
                        pLicitacao.ItensLicitacao.ForEach(il => il.LicitacaoId = pLicitacao.LicitacaoId);

                    foreach (var arquivo in pLicitacao.Arquivos)
                    {
                        db.Arquivos.Attach(arquivo);
                        arquivo.Temporario = false;
                    }

                    db.Licitacoes.Add(pLicitacao);
                }
                else
                {
                    //Alteração
                    var registroBD = db.Licitacoes.Include(cc => cc.Arquivos).FirstOrDefault(cc => cc.LicitacaoId == pLicitacao.LicitacaoId);
                    db.Entry(db.Licitacoes.FirstOrDefault(cc => cc.LicitacaoId == pLicitacao.LicitacaoId)).CurrentValues.SetValues(pLicitacao);


                    if (pLicitacao.ItensLicitacao == null)
                        pLicitacao.ItensLicitacao = new List<ItemLicitacao>();

                    //itens da licitação
                    var itensLicitacaoDB = db.ItemLicitacao.Where(il => il.LicitacaoId == pLicitacao.LicitacaoId).ToList();
                    AdicionarEditarItensLicitacao(itensLicitacaoDB, pLicitacao);
                    RemoverItensLicitacaoExcluidos(itensLicitacaoDB, pLicitacao);
                    pLicitacao.ItensLicitacao = null;

                    //fornecedores
                    var fornecedoresDB = db.FornecedorDispensa.Where(fdb => fdb.LicitacaoId == pLicitacao.LicitacaoId).ToList();
                    AdicionarEditarFornecedoresDispensa(fornecedoresDB, pLicitacao);
                    RemoverFornecedoresDispensaExcluidos(fornecedoresDB, pLicitacao);
                    //ExcluirArquivosDosFornecedores(pLicitacao);                    

                    //arquivos
                    var arquivosAInserir = db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId && a.DataRemocao == null && a.Temporario);

                    foreach (var arquivoAInserir in arquivosAInserir)
                    {
                        if (db.Entry(arquivoAInserir).State == Detached)
                            db.Arquivos.Attach(arquivoAInserir);
                        registroBD.Arquivos.Add(arquivoAInserir);
                    }

                    //Remove os arquivos que foram desvinculados
                    var arquivosARemover = db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId && a.DataRemocao == null).Except(pLicitacao.Arquivos, a => a.ArquivoId).ToList();
                    arquivosARemover.ForEach(a => { db.Entry(a).State = Modified; a.DataRemocao = DateTime.Now; });

                }

                foreach (var arquivo in db.Arquivos.Where(a => a.LicitacaoId == pLicitacao.LicitacaoId))
                {
                    arquivo.Temporario = false;
                }

                //AdicionarArquivosDosFornecedores(pLicitacao);
                pLicitacao.Fornecedores = null;

                db.SaveChanges();

                pMensagemErro = "";

                return pLicitacao.LicitacaoId;
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();

                return null;
            }
        }

        public void AdicionarArquivosDosFornecedores(Licitacao pLicitacao)
        {
            foreach (var fornecedor in pLicitacao.Fornecedores)
                foreach (var arquivo in fornecedor.Arquivos)
                {
                    db.Arquivos.Attach(arquivo);
                    arquivo.Temporario = false;
                }

            var arquivosTemporariosDB = db.Arquivos.Where(a => a.Temporario == true);

            foreach (var at in arquivosTemporariosDB)
                if (pLicitacao.Fornecedores.Any(f => f.FornecedorId == at.FornecedorDispensaId))
                    at.Temporario = false;
        }

        //public void ExcluirArquivosDosFornecedores(Licitacao pLicitacao)
        //{
        //    foreach (var fornecedor in pLicitacao.Fornecedores)
        //    {
        //        var arquivosPorFornecedorDB = db.Arquivos.Where(a => a.FornecedorDispensaId == fornecedor.FornecedorDispensaId);

        //        foreach (var apf in arquivosPorFornecedorDB)
        //            if (!fornecedor.Arquivos.Any(a => a.FornecedorDispensaId == apf.FornecedorDispensaId))
        //            {
        //                db.Entry(apf).State = Modified;
        //                apf.DataRemocao = DateTime.Now;
        //            }
        //    }
        //}

        private void AdicionarEditarFornecedoresDispensa(List<FornecedorDispensa> fornecedoresDB, Licitacao pLicitacao)
        {
            foreach (var fornecedor in pLicitacao.Fornecedores)
            {
                if (!fornecedoresDB.Any(fdb => fdb.FornecedorId == fornecedor.FornecedorId))
                {
                    db.FornecedorDispensa.Add(fornecedor);
                }
                else
                {
                    for (var i = 0; i < fornecedoresDB.Count(); i++)
                    {
                        if (fornecedoresDB[i].FornecedorId == fornecedor.FornecedorId &&
                           (fornecedoresDB[i].CNPJ != fornecedor.CNPJ ||
                           fornecedoresDB[i].RazaoSocial != fornecedor.RazaoSocial ||
                           fornecedoresDB[i].NumeroEmpenho != fornecedor.NumeroEmpenho ||
                           fornecedoresDB[i].Valor != fornecedor.Valor ||
                           fornecedoresDB[i].LocalExecucao != fornecedor.LocalExecucao ||
                           fornecedoresDB[i].Vigencia != fornecedor.Vigencia ||
                           fornecedoresDB[i].Cancelado != fornecedor.Cancelado ||
                           fornecedoresDB[i].DataAssinatura != fornecedor.DataAssinatura))
                        {
                            fornecedoresDB[i].CNPJ = fornecedor.CNPJ;
                            fornecedoresDB[i].RazaoSocial = fornecedor.RazaoSocial;
                            fornecedoresDB[i].NumeroEmpenho = fornecedor.NumeroEmpenho;
                            fornecedoresDB[i].Valor = fornecedor.Valor;
                            fornecedoresDB[i].LocalExecucao = fornecedor.LocalExecucao;
                            fornecedoresDB[i].Vigencia = fornecedor.Vigencia;
                            fornecedoresDB[i].Cancelado = fornecedor.Cancelado;
                            fornecedoresDB[i].DataAssinatura = fornecedor.DataAssinatura;

                            break;
                        }
                    }
                }
            }
        }

        private void RemoverFornecedoresDispensaExcluidos(List<FornecedorDispensa> fornecedoresDB, Licitacao pLicitacao)
        {
            foreach (var fornecedorDB in fornecedoresDB)
                if (!pLicitacao.Fornecedores.Any(f => f.FornecedorId == fornecedorDB.FornecedorId))
                    db.FornecedorDispensa.Remove(fornecedorDB);
        }

        private void AdicionarEditarItensLicitacao(List<ItemLicitacao> itensLicitacaoDB, Licitacao pLicitacao)
        {
            foreach (var itemLicitacao in pLicitacao.ItensLicitacao)
            {
                if (!itensLicitacaoDB.Any(il => il.ItemLicitacaoId == itemLicitacao.ItemLicitacaoId))
                {
                    db.ItemLicitacao.Add(itemLicitacao);
                }
                else
                {
                    for (var i = 0; i < itensLicitacaoDB.Count(); i++)
                    {
                        if (itensLicitacaoDB[i].ItemLicitacaoId == itemLicitacao.ItemLicitacaoId &&(itensLicitacaoDB[i].Item != itemLicitacao.Item ||itensLicitacaoDB[i].Descricao != itemLicitacao.Descricao ||itensLicitacaoDB[i].NumeroEmpenho != itemLicitacao.NumeroEmpenho ||itensLicitacaoDB[i].Quantidade != itemLicitacao.Quantidade ||itensLicitacaoDB[i].ValorUnitario != itemLicitacao.ValorUnitario) ||itensLicitacaoDB[i].Status != itemLicitacao.Status)
                        {
                            itensLicitacaoDB[i].Item = itemLicitacao.Item;
                            itensLicitacaoDB[i].Descricao = itemLicitacao.Descricao;
                            itensLicitacaoDB[i].NumeroEmpenho = itemLicitacao.NumeroEmpenho;
                            itensLicitacaoDB[i].Quantidade = itemLicitacao.Quantidade;
                            itensLicitacaoDB[i].ValorUnitario = itemLicitacao.ValorUnitario;
                            itensLicitacaoDB[i].Status = itemLicitacao.Status;

                            break;
                        }
                    }
                }
            }
        }

        private void RemoverItensLicitacaoExcluidos(List<ItemLicitacao> itensLicitacaoDB, Licitacao pLicitacao)
        {
            foreach (var itemLicitacaoDB in itensLicitacaoDB)
                if (!pLicitacao.ItensLicitacao.Any(i => i.ItemLicitacaoId == itemLicitacaoDB.ItemLicitacaoId))
                    db.ItemLicitacao.Remove(itemLicitacaoDB);
        }
    }
}
