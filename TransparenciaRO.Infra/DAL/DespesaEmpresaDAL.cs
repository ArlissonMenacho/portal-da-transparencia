﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using EntityFramework.Extensions;

namespace TransparenciaRO.Infra.DAL
{
    public class DespesaEmpresaDAL
    {
        private readonly DbTransparencia dbTransparencia = new DbTransparencia();

        public DespesaEmpresaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(List<DespesaEmpresa> despesasEmpresas)
        {
            dbTransparencia.BulkInsert(despesasEmpresas);
            dbTransparencia.SaveChanges();
        }

        public void VerficarRemoverDados(DateTime data, EEmpresa empresa)
        {
            if (dbTransparencia.DespesasEmpresas.Any(x => x.PagamentoRecebimento.Year == data.Year && x.PagamentoRecebimento.Month == data.Month && x.Empresa == empresa))
            {
                dbTransparencia.DespesasEmpresas.Where(x => x.PagamentoRecebimento.Year == data.Year && x.PagamentoRecebimento.Month == data.Month && x.Empresa == empresa).Delete();
            }
        }

        public int AnoMax(EEmpresa empresa)
        {
            return dbTransparencia.DespesasEmpresas
                .Where(r => r.Empresa == empresa)
                .Max(r => (int?)r.PagamentoRecebimento.Year) ?? 0;
        }

        public int AnoMin(EEmpresa empresa)
        {
            return dbTransparencia.DespesasEmpresas
                .Where(r => r.Empresa == empresa)
                .Min(r => (int?)r.PagamentoRecebimento.Year) ?? 0;
        }

        public List<int> Meses(int ano, EEmpresa empresa)
        {
            var meses = dbTransparencia.DespesasEmpresas
                .Where(r => r.PagamentoRecebimento.Year == ano && r.Empresa == empresa)
                .GroupBy(r => r.PagamentoRecebimento.Month)
                .Select(r => new { MesInt = r.Key })
                .ToList();

            return meses
                .Select(r => r.MesInt)
                .ToList();
        }

        public Dictionary<int, decimal> DespesasAnual(EEmpresa empresa)
        {
            var despesaAnual = dbTransparencia.DespesasEmpresas
                .Where(r => r.Empresa == empresa)
                .GroupBy(r => r.PagamentoRecebimento.Year)
                .Select(r => new { Ano = r.Key, Valor = r.Sum(y => y.Valor) });

            return despesaAnual.ToDictionary(k => k.Ano, v => v.Valor);
        }

        public Dictionary<int, decimal> DespesaMensal(int ano, EEmpresa empresa)
        {
            var despesaAnual = dbTransparencia.DespesasEmpresas
                .Where(r => r.Empresa == empresa && r.PagamentoRecebimento.Year == ano)
                .GroupBy(r => r.PagamentoRecebimento.Month)
                .Select(r => new { Mes = r.Key, Valor = r.Sum(y => y.Valor) });

            return despesaAnual.ToDictionary(k => k.Mes, v => v.Valor);
        }

        public List<DespesaEmpresa> DespesaDetalhada(int ano, int mes, EEmpresa empresa)
        {
            return dbTransparencia.DespesasEmpresas
                .Where(r => r.PagamentoRecebimento.Year == ano && r.PagamentoRecebimento.Month == mes && r.Empresa == empresa)
                .ToList();
        }

    }
}
