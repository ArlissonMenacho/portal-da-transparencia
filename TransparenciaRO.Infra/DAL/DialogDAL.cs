﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class DialogDAL
    {
        public readonly DbTransparencia dbTransparencia;

        public DialogDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Add(Dialog dialog)
        {
            dbTransparencia.Dialog.Add(dialog);
            dbTransparencia.SaveChanges();
        }

        public void Update(Dialog dialog)
        {
            dbTransparencia.Entry(dialog).State = EntityState.Modified;
            dbTransparencia.SaveChanges();
        }

        public IList<Dialog> List()
        {
            return dbTransparencia.Dialog.ToList();
        }
    }
}