﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.DiariaSugesp;

namespace TransparenciaRO.Infra.DAL
{
    public class DiariasSugespeDAL : ReadOnlyDALBase<DiariasSugespe>
    {
        public DiariasSugespeDAL(DbTransparencia db) : base(db)
        {

        }

        public IEnumerable<DiariasSugespe> ObterPorFiltro(DateTime dataInicial, DateTime dataFinal, string nome, string unidadeGestora)
        {
            var dados = _db.DiariasSugespe.AsNoTracking()
                .Where(d => (dataInicial == null || d.DataIda >= dataInicial) &&
                            (dataFinal == null || d.DataVolta <= dataFinal)
                )
                .GroupBy(d => d.Solicitacao)
                .SelectMany(d => d.OrderByDescending(item => item.DataDocumento))
                .ToList().Take(1000);

            return dados;
        }

        public IEnumerable<DiariasSugespe> ObterPorCPF(string cpf)
        {
            var dados = _db.DiariasSugespe
                .Where(ds => ds.CpfPassageiro == cpf || ds.CpfMotorista == cpf)
                .OrderBy(ds => ds.Solicitacao);

            var diarias = new List<DiariasSugespe>();

            foreach (var ds in dados)
            {
                if (ds.CpfMotorista == cpf)
                    diarias.Add(new DiariasSugespe
                    {
                        Solicitacao = ds.Solicitacao,
                        MatriculaMotorista = ds.MatriculaMotorista,
                        Motorista = ds.Motorista,
                        Cargo = ds.Cargo,
                        UGMotorista = ds.UGMotorista,
                        DataIda = ds.DataIda,
                        DataVolta = ds.DataVolta,
                        QuantidadeDiarias = ds.QuantidadeDiarias,
                        ValorDiarias = ds.ValorDiarias,
                        ValorDiariaMotorista = ds.ValorDiariaMotorista,
                        TipoViagem = ds.TipoViagem,
                        Objetivo = ds.Objetivo,
                        CpfMotorista = ds.CpfMotorista
                    });

                else if (ds.CpfPassageiro == cpf)
                {
                    diarias.Add(new DiariasSugespe
                    {
                        Solicitacao = ds.Solicitacao,
                        MatriculaPassageiro = ds.MatriculaPassageiro,
                        Passageiro = ds.Passageiro,
                        CargoPassageiro = ds.CargoPassageiro,
                        UGPassageiro = ds.UGPassageiro,
                        DataIda = ds.DataIda,
                        DataVolta = ds.DataVolta,
                        QuantidadediariasPassageiro = ds.QuantidadediariasPassageiro,
                        ValorDiariasPassageiro = ds.ValorDiariasPassageiro,
                        TotalDiaria = ds.TotalDiaria,
                        TipoViagem = ds.TipoViagem,
                        ObjetivoPassageiro = ds.ObjetivoPassageiro,
                        CpfPassageiro = ds.CpfPassageiro
                    });
                }
            }

            var resultado = diarias
                            .GroupBy(x => x.Solicitacao)
                            .Select(y => y.First())
                            .OrderBy(z => z.Solicitacao)
                            .ToList();

            return resultado;
        }

        public IEnumerable<DiariaCSVViewModel> ObterPorAno(int ano)
        {
            var diariasDB = _db.DiariasSugespe
                               .AsNoTracking()
                               .Where(ds => (ds.DataIda.Value.Year == ano || ds.DataVolta.Value.Year == ano))
                               .OrderBy(ds => ds.Solicitacao)
                               .ToList();

            var solicitacoes = diariasDB.Select(ds => ds.Solicitacao).Distinct().ToList();

            var diariasRetorno = new List<DiariaCSVViewModel>();

            bool cadastrarMotorista;

            foreach (var solicitacao in solicitacoes)
            {
                cadastrarMotorista = true;

                var diariasPorSolicitacao = diariasDB.Where(d => d.Solicitacao == solicitacao)
                                                     .Distinct();

                foreach (var diaria in diariasPorSolicitacao)
                {
                    if (cadastrarMotorista && diaria.TipoViagem == "TERRESTRE")
                    {
                        diariasRetorno.Add(new DiariaCSVViewModel
                        {
                            Solicitacao = diaria.Solicitacao,
                            Matricula = diaria.MatriculaMotorista,
                            Nome = diaria.Motorista,
                            Cargo = diaria.Cargo,
                            UnidadeGestora = diaria.UGMotorista,
                            DataIda = diaria.DataIda,
                            DataVolta = diaria.DataVolta,
                            QuantidadeDiarias = diaria.QuantidadeDiarias,
                            ValorDiarias = diaria.ValorDiarias,
                            TotalDiarias = diaria.ValorDiariaMotorista,
                            TipoViagem = diaria.TipoViagem,
                            Objetivo = diaria.Objetivo
                        });

                        cadastrarMotorista = false;
                    }

                    diariasRetorno.Add(new DiariaCSVViewModel
                    {
                        Solicitacao = diaria.Solicitacao,
                        Matricula = diaria.MatriculaPassageiro,
                        Nome = diaria.Passageiro,
                        Cargo = diaria.CargoPassageiro,
                        UnidadeGestora = diaria.UGPassageiro,
                        DataIda = diaria.DataIda,
                        DataVolta = diaria.DataVolta,
                        QuantidadeDiarias = diaria.QuantidadediariasPassageiro,
                        ValorDiarias = diaria.ValorDiariasPassageiro,
                        TotalDiarias = diaria.TotalDiaria,
                        TipoViagem = diaria.TipoViagem,
                        Objetivo = diaria.ObjetivoPassageiro
                    });
                }
            }

            var diariasRetornoDistinct = diariasRetorno.OrderBy(dr => dr.Solicitacao)
                                 .Distinct()
                                 .ToList();

            return diariasRetornoDistinct;
        }
    }
}