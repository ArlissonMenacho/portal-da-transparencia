﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.Data.Entity;
using TransparenciaRO.Infra.Utils;
using static System.Data.Entity.EntityState;

namespace TransparenciaRO.Infra.DAL
{
    public class ContratoConvenioDAL
    {
        public DbTransparencia DbTransparencia;

        public ContratoConvenioDAL(DbTransparencia dbTransparencia)
        {
            DbTransparencia = dbTransparencia;
        }

        public IQueryable<ContratoConvenio> GetContratoConvenio(ETipoDocumento tipoDocumento, DateTime? dataInicial, DateTime? dataFinal, bool cancelado, bool sancionado, bool comArquivos, bool semArquivos, List<Guid> unidadesGestoras, string cnpj, string numeroProcesso, string numeroDocumento, string razaoSocial, string objeto)
        {
            if (unidadesGestoras.Count > 0 || !cnpj.IsNullOrEmpty() || !numeroProcesso.IsNullOrEmpty() || !numeroDocumento.IsNullOrEmpty() || !razaoSocial.IsNullOrEmpty())
            {
                dataInicial = null;
                dataFinal = null;
            }

            var contratosConvenios = DbTransparencia.ContratosConvenios.Include(cc => cc.Arquivos).Include(cc => cc.TermoFomento)
                            .Where(cc => cc.TipoDocumento == tipoDocumento &&
                                (dataInicial == null || cc.DataElaboracao >= dataInicial) &&
                                (dataFinal == null || cc.DataElaboracao <= dataFinal) &&
                                (cancelado && sancionado || (cancelado && cc.Cancelado) || (sancionado && !cc.Cancelado)) &&
                                (comArquivos && semArquivos || (comArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) != null) || (semArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) == null)) &&
                                (unidadesGestoras.Count == 0 || unidadesGestoras.Any(ug => cc.UnidadeGestoraId == ug)) &&
                                (cnpj == "" || cc.Cnpj.Contains(cnpj)) &&
                                (numeroProcesso == "" || cc.NumeroProcesso.Contains(numeroProcesso)) &&
                                (numeroDocumento == "" || cc.NumeroDocumento.Contains(numeroDocumento)) &&
                                (razaoSocial == "" || cc.Empresa.Contains(razaoSocial)) &&
                                (objeto == "" || cc.Objeto.Contains(objeto))
                            ).OrderBy(cc => cc.NumeroDocumento);

            return contratosConvenios;
        }

        public IQueryable<ContratoConvenio> GetContratoConvenioTipoContratoEContratoEmergencialCOVIDParaAPI(DateTime? dataInicial, DateTime? dataFinal, bool cancelado, bool sancionado, bool comArquivos, bool semArquivos, List<Guid> unidadesGestoras, string cnpj, string numeroProcesso, string numeroDocumento, string razaoSocial, string objeto)
        {
            if (unidadesGestoras.Count > 0 || !cnpj.IsNullOrEmpty() || !numeroProcesso.IsNullOrEmpty() || !numeroDocumento.IsNullOrEmpty() || !razaoSocial.IsNullOrEmpty())
            {
                dataInicial = null;
                dataFinal = null;
            }

            var contratosConvenios = DbTransparencia.ContratosConvenios.Include(cc => cc.Arquivos).Include(cc => cc.UnidadeGestora)
                            .Where(cc => (cc.TipoDocumento == ETipoDocumento.Contrato || cc.TipoDocumento == ETipoDocumento.ContratoEmergencialCOVID19) &&
                                (dataInicial == null || cc.DataElaboracao >= dataInicial) &&
                                (dataFinal == null || cc.DataElaboracao <= dataFinal) &&
                                (cancelado && sancionado || (cancelado && cc.Cancelado) || (sancionado && !cc.Cancelado)) &&
                                (comArquivos && semArquivos || (comArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) != null) || (semArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) == null)) &&
                                (unidadesGestoras.Count == 0 || unidadesGestoras.Any(ug => cc.UnidadeGestoraId == ug)) &&
                                (cnpj == "" || cc.Cnpj.Contains(cnpj)) &&
                                (numeroProcesso == "" || cc.NumeroProcesso.Contains(numeroProcesso)) &&
                                (numeroDocumento == "" || cc.NumeroDocumento.Contains(numeroDocumento)) &&
                                (razaoSocial == "" || cc.Empresa.Contains(razaoSocial)) &&
                                (objeto == "" || cc.Objeto.Contains(objeto))
                            ).OrderByDescending(cc => cc.DataElaboracao);

            return contratosConvenios;
        }

        public IQueryable<ContratoConvenio> GetContratoConvenioCOVID19(ETipoDocumento tipoDocumento, DateTime? dataInicial, DateTime? dataFinal, bool cancelado, bool sancionado, bool comArquivos, bool semArquivos, List<Guid> unidadesGestoras, string cnpj, string numeroProcesso, string numeroDocumento, string razaoSocial, string objeto)
        {
            if (unidadesGestoras.Count > 0 || !cnpj.IsNullOrEmpty() || !numeroProcesso.IsNullOrEmpty() || !numeroDocumento.IsNullOrEmpty() || !razaoSocial.IsNullOrEmpty())
            {
                dataInicial = null;
                dataFinal = null;
            }

            var contratosConvenios = DbTransparencia.ContratosConvenios.Include(cc => cc.Arquivos)
                            .Where(cc => cc.TipoDocumento == tipoDocumento &&
                                (dataInicial == null || cc.DataElaboracao >= dataInicial) &&
                                (dataFinal == null || cc.DataElaboracao <= dataFinal) &&
                                (cancelado && sancionado || (cancelado && cc.Cancelado) || (sancionado && !cc.Cancelado)) &&
                                (comArquivos && semArquivos || (comArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) != null) || (semArquivos && cc.Arquivos.FirstOrDefault(a => a.DataRemocao == null) == null)) &&
                                (unidadesGestoras.Count == 0 || unidadesGestoras.Any(ug => cc.UnidadeGestoraId == ug)) &&
                                (cnpj == "" || cc.Cnpj.Contains(cnpj)) &&
                                (numeroProcesso == "" || cc.NumeroProcesso.Contains(numeroProcesso)) &&
                                (numeroDocumento == "" || cc.NumeroDocumento.Contains(numeroDocumento)) &&
                                (razaoSocial == "" || cc.Empresa.Contains(razaoSocial)) &&
                                (objeto == "" || cc.Objeto.Contains(objeto))
                            ).OrderBy(cc => cc.NumeroDocumento);

            return contratosConvenios;
        }

        public IQueryable<ContratoConvenio> GetAllContratosConveniosCOVID19()
        {

            var contratosConvenios = DbTransparencia.ContratosConvenios.AsNoTracking()
                                                                       .Include(cc => cc.Arquivos)
                                                                       .Include(cc => cc.UnidadeGestora)
                                                                       .Where(cc => cc.TipoDocumento == ETipoDocumento.ContratoEmergencialCOVID19)
                                                                       .OrderBy(cc => cc.NumeroDocumento);

            return contratosConvenios;
        }

        public ContratoConvenio Buscar(Guid pContratoConvenioId)
        {
            var retorno = DbTransparencia
                .ContratosConvenios
                .Include(cc => cc.Arquivos)
                .Include(cc => cc.TermoFomento.PrestacaoDeContas)
                .Include(cc => cc.TermoFomento.Monitoramento)
                .Include(cc => cc.TermoFomento.RemuneracaoEquipe)
                .Include(cc => cc.UnidadeGestora)
                .FirstOrDefault(cc => cc.ContratoConvenioId == pContratoConvenioId);

            retorno.Arquivos = retorno.Arquivos.Where(a => a.DataRemocao == null).ToList();

            return retorno;
        }

        public Dictionary<int, decimal> DadosAnuais(ETipoDocumento tipoDocumento)
        {
            var contratosConvenios = DbTransparencia.ContratosConvenios
                .Where(x => x.TipoDocumento == tipoDocumento)
                .GroupBy(x => x.DataElaboracao.Year, x => x.Valor)
                .OrderByDescending(x => x.Key)
                .ToDictionary(k => k.Key, v => v.Sum());

            return contratosConvenios;
        }

        public Dictionary<int, decimal> DadosMensais(ETipoDocumento tipoDocumento, int ano)
        {
            var contratosConvenios = DbTransparencia.ContratosConvenios
                .Where(x => x.TipoDocumento == tipoDocumento && x.DataElaboracao >= new DateTime(ano, 1, 1) && x.DataElaboracao <= new DateTime(ano, 12, 31, 23, 59, 59))
                .GroupBy(x => x.DataElaboracao.Month, x => x.Valor)
                .OrderBy(x => x.Key)
                .ToDictionary(k => k.Key, v => v.Sum());

            return contratosConvenios;
        }

        public Dictionary<int, decimal> DadosAnuaisPorUnidadeGestora(ETipoDocumento tipoDocumento, List<Guid> unidadesGestoras)
        {
            var contratosConvenios = DbTransparencia.ContratosConvenios
                .Where(x => x.TipoDocumento == tipoDocumento && unidadesGestoras.Any(ug => ug == x.UnidadeGestoraId))
                .GroupBy(x => x.DataElaboracao.Year, x => x.Valor)
                .OrderBy(x => x.Key)
                .ToDictionary(k => k.Key, v => v.Sum());

            return contratosConvenios;
        }

        public Dictionary<int, decimal> DadosMensaisPorUnidadeGestora(ETipoDocumento tipoDocumento, int ano, List<Guid> unidadesGestoras)
        {
            var contratosConvenios = DbTransparencia.ContratosConvenios
                .Where(x => x.TipoDocumento == tipoDocumento && x.DataElaboracao >= new DateTime(ano, 1, 1) && x.DataElaboracao <= new DateTime(ano, 12, 31, 23, 59, 59) && unidadesGestoras.Any(ug => ug == x.UnidadeGestoraId))
                .GroupBy(x => x.DataElaboracao.Month, x => x.Valor)
                .OrderBy(x => x.Key)
                .ToDictionary(k => k.Key, v => v.Sum());

            return contratosConvenios;
        }

        public Dictionary<int, int> AnosDosContratosConvenios()
        {
            return DbTransparencia.ContratosConvenios
                .GroupBy(x => x.DataElaboracao.Year)
                .OrderBy(x => x.Key)
                .ToDictionary(k => k.Key, k => k.Key);
        }

        public int AnosFinalContratosConvenios()
        {
            return DbTransparencia.ContratosConvenios
                .Max(x => (int?)x.DataElaboracao.Year) ?? 0;
        }
        public Guid? AdicionarEditar(ContratoConvenio pContratoConvenio, out string pMensagemErro)
        {
            try
            {
                if (pContratoConvenio.ContratoConvenioId == new Guid())
                {
                    if (pContratoConvenio.TipoDocumento != ETipoDocumento.TermoFomento &&
                        ETipoDocumento.TermoDeCooperacao != pContratoConvenio.TipoDocumento)
                    {
                        //Inserção
                        pContratoConvenio.ContratoConvenioId = Guid.NewGuid();
                        foreach (var arquivo in pContratoConvenio.Arquivos)
                        {
                            DbTransparencia.Arquivos.Attach(arquivo);
                            arquivo.Temporario = false;
                        }
                        DbTransparencia.ContratosConvenios.Add(pContratoConvenio);
                    }

                    if (pContratoConvenio.TipoDocumento == ETipoDocumento.TermoFomento||
                        pContratoConvenio.TipoDocumento == ETipoDocumento.TermoDeCooperacao)
                    {
                        pContratoConvenio.ContratoConvenioId = Guid.NewGuid();
                        pContratoConvenio.DataElaboracao = pContratoConvenio.DataAssinatura ?? DateTime.Now;
                        pContratoConvenio.LicitacaoOrigem = pContratoConvenio.OrigemRecursos;

                        pContratoConvenio.TermoFomentoId = pContratoConvenio.TermoFomento.Id;

                        pContratoConvenio.TermoFomento.PrestacaoDeContasId = pContratoConvenio.TermoFomento.PrestacaoDeContas.Id;

                        if (pContratoConvenio.TermoFomento.RemuneracaoEquipe != null)
                        {
                            foreach (var item in pContratoConvenio.TermoFomento.RemuneracaoEquipe)
                            {
                                item.TermoFomentoId = pContratoConvenio.TermoFomento.Id;
                            }
                        }

                        if (pContratoConvenio.Arquivos != null)
                        {
                            foreach (var arquivo in pContratoConvenio.Arquivos)
                            {
                                DbTransparencia.Arquivos.Attach(arquivo);
                                arquivo.Temporario = false;
                            }
                        }
                        DbTransparencia.ContratosConvenios.Add(pContratoConvenio);

                    }
                }

                else
                {
                    pContratoConvenio.DataElaboracao = pContratoConvenio.DataAssinatura ?? DateTime.Now;
                    //Alteração
                    var registroBD = DbTransparencia.ContratosConvenios
                        .Include(cc => cc.Arquivos)
                        .FirstOrDefault(cc => cc.ContratoConvenioId == pContratoConvenio.ContratoConvenioId);
                    DbTransparencia.Entry(DbTransparencia.ContratosConvenios.FirstOrDefault(cc => cc.ContratoConvenioId == pContratoConvenio.ContratoConvenioId)).CurrentValues.SetValues(pContratoConvenio);

                    var fornecedoresDB = DbTransparencia.TermoFomento
                        .Where(fdb => fdb.Id == pContratoConvenio.TermoFomentoId).ToList();
                    //AdicionarEditarFornecedoresDispensa(fornecedoresDB, pLicitacao);
                    //RemoverFornecedoresDispensaExcluidos(fornecedoresDB, pLicitacao);

                    //arquivos
                    var arquivosAInserir = DbTransparencia.Arquivos.Where(a => a.ContratoConvenioId == pContratoConvenio.ContratoConvenioId && a.DataRemocao == null && a.Temporario);
                    foreach (var arquivoAInserir in arquivosAInserir)
                    {
                        if (DbTransparencia.Entry(arquivoAInserir).State == Detached)
                            DbTransparencia.Arquivos.Attach(arquivoAInserir);
                        registroBD.Arquivos.Add(arquivoAInserir);
                    }
                    //Remove os arquivos que foram desvinculados
                    var arquivosARemover = DbTransparencia.Arquivos.Where(a => a.ContratoConvenioId == pContratoConvenio.ContratoConvenioId && a.DataRemocao == null).Except(pContratoConvenio.Arquivos, a => a.ArquivoId).ToList();
                    arquivosARemover.ForEach(a => { DbTransparencia.Entry(a).State = Modified; a.DataRemocao = DateTime.Now; });

                }

                foreach (var arquivo in DbTransparencia.Arquivos.Where(a => a.ContratoConvenioId == pContratoConvenio.ContratoConvenioId))
                {
                    arquivo.Temporario = false;
                }
                DbTransparencia.SaveChanges();
                pMensagemErro = "";

                return pContratoConvenio.ContratoConvenioId;
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();
                return null;
            }
        }
    }
}