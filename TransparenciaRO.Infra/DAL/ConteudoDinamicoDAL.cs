﻿using System;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class ConteudoDinamicoDAL : ReadOnlyDALBase<ConteudoDinamico>
    {
        public ConteudoDinamicoDAL(DbTransparencia db) : base(db) { }
    }
}
