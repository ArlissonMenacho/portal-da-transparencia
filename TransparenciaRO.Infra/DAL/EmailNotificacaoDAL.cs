﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class EmailNotificacaoDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public EmailNotificacaoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        private bool CheckIfEmailAlreadyExists(string email)
        {
            var emailNotificacao = _dbTransparencia.EmailNotificacao.FirstOrDefault(e => e.Email.Equals(email));

            bool emailAlreadyExists = emailNotificacao != null;

            return emailAlreadyExists;
        }

        public void DisableNotificationsById(Guid id)
        {
            var emailNotificacao = _dbTransparencia.EmailNotificacao.FirstOrDefault(email => email.Id == id);

            emailNotificacao.DesabilitarNotificacoes();

            _dbTransparencia.Entry(emailNotificacao).State = EntityState.Modified;

            _dbTransparencia.SaveChanges();
        }

        public List<EmailNotificacao> GetEmailsToNotifyBoletimAdded()
        {
            var listOfEmailNotificacao = _dbTransparencia.EmailNotificacao.Where(email => email.NotificarBoletim).ToList();

            return listOfEmailNotificacao;
        }

        public List<EmailNotificacao> GetEmailsToNotifyDispensaLicitacaoAdded()
        {
            var listOfEmailNotificacao = _dbTransparencia.EmailNotificacao.Where(email => email.NotificarDispensaLicitacao).ToList();

            return listOfEmailNotificacao;
        }

        public List<EmailNotificacao> GetEmailsToNotifyIntegraProcessoAdded()
        {
            var listOfEmailNotificacao = _dbTransparencia.EmailNotificacao.Where(email => email.NotificarIntegraProcesso).ToList();

            return listOfEmailNotificacao;
        }

        public void Save(EmailNotificacao emailNotificacao)
        {
            bool emailExists = CheckIfEmailAlreadyExists(emailNotificacao.Email);

            if (emailExists)
                Update(emailNotificacao.Email, emailNotificacao);
            else
            {
                emailNotificacao.Id = Guid.NewGuid();

                _dbTransparencia.EmailNotificacao.Add(emailNotificacao);

                _dbTransparencia.SaveChanges();
            }
        }

        private void Update(string email, EmailNotificacao newDataEmailNotificao)
        {
            var emailNotificacao = _dbTransparencia.EmailNotificacao.FirstOrDefault(e => e.Email.Equals(email));

            emailNotificacao.AlterarDados(newDataEmailNotificao.Nome, newDataEmailNotificao.NotificarDispensaLicitacao, newDataEmailNotificao.NotificarIntegraProcesso, newDataEmailNotificao.NotificarBoletim);

            _dbTransparencia.Entry(emailNotificacao).State = EntityState.Modified;

            _dbTransparencia.SaveChanges();
        }
    }
}
