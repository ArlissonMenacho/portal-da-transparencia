﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class MateriaisDAL : ReadOnlyDALBase<Materiais>
    {
        public MateriaisDAL(DbTransparencia db) : base(db) { }

        public IEnumerable<object> ObterPorFiltro(string ug, string tipoMaterial, string dataInicial, string dataFinal, DateTime? dtInicial, DateTime? dtFinal, int qtdBusca)
        {
            var espDespesa = Convert.ToInt32(tipoMaterial);

            if (dataInicial == "" && dataFinal == "" && ug == "0")
            {
                var ultimos30Dias = DateTime.Now.AddDays(-30);
                switch (dtInicial == null && dtFinal == null ? 2020 : dtInicial != null ? dtInicial.Value.Year : dtFinal.Value.Year)
                {
                    case (2017):
                        return (from m in _db.Materiais2017.AsNoTracking()
                                where (m.DataDocumento >= ultimos30Dias) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2018):
                        return (from m in _db.Materiais2018.AsNoTracking()
                                where (m.DataDocumento >= ultimos30Dias) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2019):
                        return (from m in _db.Materiais2019.AsNoTracking()
                                where (m.DataDocumento >= ultimos30Dias) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    default:
                        return (from m in _db.Materiais2020.AsNoTracking()
                                where (m.DataDocumento >= ultimos30Dias) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
                }
            }

            if (dataInicial == "" && dataFinal == "" && ug != "0")
            {
                switch (dtInicial == null && dtFinal == null ? 2020 : dtInicial != null ? dtInicial.Value.Year : dtFinal.Value.Year)
                {
                    case (2017):
                        return (from m in _db.Materiais2017.AsNoTracking()
                                where (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2018):
                        return (from m in _db.Materiais2018.AsNoTracking()
                                where (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2019):
                        return (from m in _db.Materiais2019.AsNoTracking()
                                where (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    default:
                        return (from m in _db.Materiais2020.AsNoTracking()
                                where (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
                }
            }

            if (dataInicial != "" && dataFinal != "" && dtInicial == dtFinal)
            {
                switch (dtInicial == null && dtFinal == null ? 2020 : dtInicial != null ? dtInicial.Value.Year : dtFinal.Value.Year)
                {
                    case (2017):
                        return (from m in _db.Materiais2017.AsNoTracking()
                                where (m.DataDocumento == dtInicial) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2018):
                        return (from m in _db.Materiais2018.AsNoTracking()
                                where (m.DataDocumento == dtInicial) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    case (2019):
                        return (from m in _db.Materiais2019.AsNoTracking()
                                where (m.DataDocumento == dtInicial) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                    default:
                        return (from m in _db.Materiais2020.AsNoTracking()
                                where (m.DataDocumento == dtInicial) &&
                                      (ug == "0" || m.UG == ug) &&
                                      (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                                group m by new
                                {
                                    m.CodEspecificacaoDespesa,
                                    m.Credor,
                                    m.DataDocumento,
                                    m.EspecificacaoDespesa,
                                    m.Exercicio,
                                    m.NumEmpenho,
                                    m.UG,
                                    m.valorDespesa,
                                    m.Documento
                                }
                                  into myGroup
                                select new
                                {
                                    Exercicio = myGroup.Key.Exercicio,
                                    UG = myGroup.Key.UG,
                                    NumEmpenho = myGroup.Key.NumEmpenho,
                                    DataDocumento = myGroup.Key.DataDocumento,
                                    Credor = myGroup.Key.Credor,
                                    EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                    valorDespesa = myGroup.Key.valorDespesa,
                                    Documento = myGroup.Key.Documento
                                }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
                }
            }

            switch (dtInicial == null && dtFinal == null ? 2020 : dtInicial != null ? dtInicial.Value.Year : dtFinal.Value.Year)
            {
                case (2017):
                    return (from m in _db.Materiais2017.AsNoTracking()
                            where (m.DataDocumento >= dtInicial && m.DataDocumento <= dtFinal) &&
                                  (ug == "0" || m.UG == ug) &&
                                  (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                            group m by new
                            {
                                m.CodEspecificacaoDespesa,
                                m.Credor,
                                m.DataDocumento,
                                m.EspecificacaoDespesa,
                                m.Exercicio,
                                m.NumEmpenho,
                                m.UG,
                                m.valorDespesa,
                                m.Documento
                            }
                                  into myGroup
                            select new
                            {
                                Exercicio = myGroup.Key.Exercicio,
                                UG = myGroup.Key.UG,
                                NumEmpenho = myGroup.Key.NumEmpenho,
                                DataDocumento = myGroup.Key.DataDocumento,
                                Credor = myGroup.Key.Credor,
                                EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                                valorDespesa = myGroup.Key.valorDespesa,
                                Documento = myGroup.Key.Documento
                            }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                case (2018):
                    return (from m in _db.Materiais2018.AsNoTracking()
                            where (m.DataDocumento >= dtInicial && m.DataDocumento <= dtFinal) &&
                                  (ug == "0" || m.UG == ug) &&
                                  (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                            group m by new
                            {
                                m.CodEspecificacaoDespesa,
                                m.Credor,
                                m.DataDocumento,
                                m.EspecificacaoDespesa,
                                m.Exercicio,
                                m.NumEmpenho,
                                m.UG,
                                m.valorDespesa,
                                m.Documento
                            }
                                  into myGroup
                            select new
                            {
                                Exercicio = myGroup.Key.Exercicio,
                                UG = myGroup.Key.UG,
                                NumEmpenho = myGroup.Key.NumEmpenho,
                                DataDocumento = myGroup.Key.DataDocumento,
                                Credor = myGroup.Key.Credor,
                                EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,

                                valorDespesa = myGroup.Key.valorDespesa,
                                Documento = myGroup.Key.Documento
                            }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                case (2019):
                    return (from m in _db.Materiais2019.AsNoTracking()
                            where (m.DataDocumento >= dtInicial && m.DataDocumento <= dtFinal) &&
                                  (ug == "0" || m.UG == ug) &&
                                  (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                            group m by new
                            {
                                m.CodEspecificacaoDespesa,
                                m.Credor,
                                m.DataDocumento,
                                m.EspecificacaoDespesa,
                                m.Exercicio,
                                m.NumEmpenho,
                                m.UG,
                                m.valorDespesa,
                                m.Documento
                            }
                                  into myGroup
                            select new
                            {
                                Exercicio = myGroup.Key.Exercicio,
                                UG = myGroup.Key.UG,
                                NumEmpenho = myGroup.Key.NumEmpenho,
                                DataDocumento = myGroup.Key.DataDocumento,
                                Credor = myGroup.Key.Credor,
                                EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,

                                valorDespesa = myGroup.Key.valorDespesa,
                                Documento = myGroup.Key.Documento
                            }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);

                default:
                    return (from m in _db.Materiais2020.AsNoTracking()
                            where (m.DataDocumento >= dtInicial && m.DataDocumento <= dtFinal) &&
                                  (ug == "0" || m.UG == ug) &&
                                  (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                            group m by new
                            {
                                m.CodEspecificacaoDespesa,
                                m.Credor,
                                m.DataDocumento,
                                m.EspecificacaoDespesa,
                                m.Exercicio,
                                m.NumEmpenho,
                                m.UG,
                                m.valorDespesa,
                                m.Documento
                            }
                                  into myGroup
                            select new
                            {
                                Exercicio = myGroup.Key.Exercicio,
                                UG = myGroup.Key.UG,
                                NumEmpenho = myGroup.Key.NumEmpenho,
                                DataDocumento = myGroup.Key.DataDocumento,
                                Credor = myGroup.Key.Credor,
                                EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,

                                valorDespesa = myGroup.Key.valorDespesa,
                                Documento = myGroup.Key.Documento
                            }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
            }
        }

        public IEnumerable<Materiais> ObterPorFiltroDetalhado(short exercicio, string ug, string numEmpenho, long especificacaoDespesa, decimal valorDespesa, string documento)
        {
            switch (exercicio)
            {
                case (2017):
                    return (from m in _db.Materiais2017.AsNoTracking()
                            where (m.Exercicio == exercicio) &&
                                  (m.UG == ug) &&
                                  (m.NumEmpenho == numEmpenho) &&
                                  (m.EspecificacaoDespesa == especificacaoDespesa) &&
                                  (m.valorDespesa == valorDespesa) &&
                                  (m.Documento == documento)
                            select new Materiais()
                            {
                                Exercicio = m.Exercicio,
                                UG = m.UG,
                                NumEmpenho = m.NumEmpenho,
                                DataDocumento = m.DataDocumento,
                                CodEspecificacaoDespesa = m.CodEspecificacaoDespesa,
                                EspecificacaoDespesa = m.EspecificacaoDespesa,
                                Credor = m.Credor,
                                ValorEmpenhada = m.ValorEmpenhada,
                                ValorLiquidada = m.ValorLiquidada,
                                ValorPaga = m.ValorPaga,
                                valorDespesa = m.valorDespesa,
                                Descricao = m.Descricao,
                                Qtde = m.Qtde,
                                UnidadeMedida = m.UnidadeMedida,
                                VlrUnitario = m.VlrUnitario,
                                Documento = m.Documento
                            });

                case (2018):
                    return (from m in _db.Materiais2018.AsNoTracking()
                            where (m.Exercicio == exercicio) &&
                                  (m.UG == ug) &&
                                  (m.NumEmpenho == numEmpenho) &&
                                  (m.EspecificacaoDespesa == especificacaoDespesa) &&
                                  (m.valorDespesa == valorDespesa) &&
                                  (m.Documento == documento)
                            select new Materiais()
                            {
                                Exercicio = m.Exercicio,
                                UG = m.UG,
                                NumEmpenho = m.NumEmpenho,
                                DataDocumento = m.DataDocumento,
                                CodEspecificacaoDespesa = m.CodEspecificacaoDespesa,
                                EspecificacaoDespesa = m.EspecificacaoDespesa,
                                Credor = m.Credor,
                                ValorEmpenhada = m.ValorEmpenhada,
                                ValorLiquidada = m.ValorLiquidada,
                                ValorPaga = m.ValorPaga,
                                valorDespesa = m.valorDespesa,
                                Descricao = m.Descricao,
                                Qtde = m.Qtde,
                                UnidadeMedida = m.UnidadeMedida,
                                VlrUnitario = m.VlrUnitario,
                                Documento = m.Documento
                            });

                case (2019):
                    return (from m in _db.Materiais2019.AsNoTracking()
                            where (m.Exercicio == exercicio) &&
                                  (m.UG == ug) &&
                                  (m.NumEmpenho == numEmpenho) &&
                                  (m.EspecificacaoDespesa == especificacaoDespesa) &&
                                  (m.valorDespesa == valorDespesa) &&
                                  (m.Documento == documento)
                            select new Materiais()
                            {
                                Exercicio = m.Exercicio,
                                UG = m.UG,
                                NumEmpenho = m.NumEmpenho,
                                DataDocumento = m.DataDocumento,
                                CodEspecificacaoDespesa = m.CodEspecificacaoDespesa,
                                EspecificacaoDespesa = m.EspecificacaoDespesa,
                                Credor = m.Credor,
                                ValorEmpenhada = m.ValorEmpenhada,
                                ValorLiquidada = m.ValorLiquidada,
                                ValorPaga = m.ValorPaga,
                                valorDespesa = m.valorDespesa,
                                Descricao = m.Descricao,
                                Qtde = m.Qtde,
                                UnidadeMedida = m.UnidadeMedida,
                                VlrUnitario = m.VlrUnitario,
                                Documento = m.Documento
                            });

                default:
                    return (from m in _db.Materiais2020.AsNoTracking()
                            where (m.Exercicio == exercicio) &&
                                  (m.UG == ug) &&
                                  (m.NumEmpenho == numEmpenho) &&
                                  (m.EspecificacaoDespesa == especificacaoDespesa) &&
                                  (m.valorDespesa == valorDespesa) &&
                                  (m.Documento == documento)
                            select new Materiais()
                            {
                                Exercicio = m.Exercicio,
                                UG = m.UG,
                                NumEmpenho = m.NumEmpenho,
                                DataDocumento = m.DataDocumento,
                                CodEspecificacaoDespesa = m.CodEspecificacaoDespesa,
                                EspecificacaoDespesa = m.EspecificacaoDespesa,
                                Credor = m.Credor,
                                ValorEmpenhada = m.ValorEmpenhada,
                                ValorLiquidada = m.ValorLiquidada,
                                ValorPaga = m.ValorPaga,
                                valorDespesa = m.valorDespesa,
                                Descricao = m.Descricao,
                                Qtde = m.Qtde,
                                UnidadeMedida = m.UnidadeMedida,
                                VlrUnitario = m.VlrUnitario,
                                Documento = m.Documento
                            });
            }
        }

        #region COVID19

        public IEnumerable<object> ObterPorFiltroCOVID19(string ug, string tipoMaterial, string dataInicial, string dataFinal, DateTime? dtInicial, DateTime? dtFinal, int qtdBusca)
        {
            var espDespesa = Convert.ToInt32(tipoMaterial);

            if (dataInicial == "" && dataFinal == "")
            {
                return (from m in _db.VW_Materiais2020_COVID19.AsNoTracking()
                        where (ug == "0" || m.UG == ug) &&
                              (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                        group m by new
                        {
                            m.CodEspecificacaoDespesa,
                            m.Credor,
                            m.DataDocumento,
                            m.EspecificacaoDespesa,
                            m.Exercicio,
                            m.NumEmpenho,
                            m.UG,
                            m.valorDespesa,
                            m.Documento,
                            m.NomDespesa,
                            m.ValorEmpenhada,
                            m.ValorLiquidada,
                            m.ValorPaga,
                            m.Status
                        }
                          into myGroup
                        select new
                        {
                            Exercicio = myGroup.Key.Exercicio,
                            UG = myGroup.Key.UG,
                            NumEmpenho = myGroup.Key.NumEmpenho,
                            DataDocumento = myGroup.Key.DataDocumento,
                            Credor = myGroup.Key.Credor,
                            EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                            valorDespesa = myGroup.Key.valorDespesa,
                            Documento = myGroup.Key.Documento,
                            NomDespesa = myGroup.Key.NomDespesa,
                            ValorEmpenhada = myGroup.Key.ValorEmpenhada,
                            ValorLiquidada = myGroup.Key.ValorLiquidada,
                            ValorPaga = myGroup.Key.ValorPaga,
                            Status = myGroup.Key.Status
                        }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
            }

            if (dataInicial != "" && dataFinal != "" && dtInicial == dtFinal)
            {
                return (from m in _db.VW_Materiais2020_COVID19.AsNoTracking()
                        where (m.DataDocumento == dtInicial) &&
                              (ug == "0" || m.UG == ug) &&
                              (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                        group m by new
                        {
                            m.CodEspecificacaoDespesa,
                            m.Credor,
                            m.DataDocumento,
                            m.EspecificacaoDespesa,
                            m.Exercicio,
                            m.NumEmpenho,
                            m.UG,
                            m.valorDespesa,
                            m.Documento,
                            m.NomDespesa,
                            m.ValorEmpenhada,
                            m.ValorLiquidada,
                            m.ValorPaga,
                            m.Status
                        }
                          into myGroup
                        select new
                        {
                            Exercicio = myGroup.Key.Exercicio,
                            UG = myGroup.Key.UG,
                            NumEmpenho = myGroup.Key.NumEmpenho,
                            DataDocumento = myGroup.Key.DataDocumento,
                            Credor = myGroup.Key.Credor,
                            EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                            valorDespesa = myGroup.Key.valorDespesa,
                            Documento = myGroup.Key.Documento,
                            NomDespesa = myGroup.Key.NomDespesa,
                            ValorEmpenhada = myGroup.Key.ValorEmpenhada,
                            ValorLiquidada = myGroup.Key.ValorLiquidada,
                            ValorPaga = myGroup.Key.ValorPaga,
                            Status = myGroup.Key.Status
                        }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
            }

            return (from m in _db.VW_Materiais2020_COVID19.AsNoTracking()
                    where (m.DataDocumento >= dtInicial && m.DataDocumento <= dtFinal) &&
                          (ug == "0" || m.UG == ug) &&
                          (tipoMaterial == "0" || m.EspecificacaoDespesa == espDespesa)
                    group m by new
                    {
                        m.CodEspecificacaoDespesa,
                        m.Credor,
                        m.DataDocumento,
                        m.EspecificacaoDespesa,
                        m.Exercicio,
                        m.NumEmpenho,
                        m.UG,
                        m.valorDespesa,
                        m.Documento,
                        m.NomDespesa,
                        m.ValorEmpenhada,
                        m.ValorLiquidada,
                        m.ValorPaga,
                        m.Status
                    }
                          into myGroup
                    select new
                    {
                        Exercicio = myGroup.Key.Exercicio,
                        UG = myGroup.Key.UG,
                        NumEmpenho = myGroup.Key.NumEmpenho,
                        DataDocumento = myGroup.Key.DataDocumento,
                        Credor = myGroup.Key.Credor,
                        EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                        valorDespesa = myGroup.Key.valorDespesa,
                        Documento = myGroup.Key.Documento,
                        NomDespesa = myGroup.Key.NomDespesa,
                        ValorEmpenhada = myGroup.Key.ValorEmpenhada,
                        ValorLiquidada = myGroup.Key.ValorLiquidada,
                        ValorPaga = myGroup.Key.ValorPaga,
                        Status = myGroup.Key.Status
                    }).Take(qtdBusca).OrderByDescending(m => m.DataDocumento);
        }

        public IEnumerable<Materiais> ObterPorFiltroDetalhadoCOVID19(short exercicio, string ug, string numEmpenho, long especificacaoDespesa, decimal valorDespesa, string documento)
        {
            return (from m in _db.VW_Materiais2020_COVID19.AsNoTracking()
                    where (m.Exercicio == exercicio) &&
                          (m.UG == ug) &&
                          (m.NumEmpenho == numEmpenho) &&
                          (m.EspecificacaoDespesa == especificacaoDespesa) &&
                          (m.valorDespesa == valorDespesa) &&
                          (m.Documento == documento)
                    select new Materiais()
                    {
                        Exercicio = m.Exercicio,
                        UG = m.UG,
                        NumEmpenho = m.NumEmpenho,
                        DataDocumento = m.DataDocumento,
                        CodEspecificacaoDespesa = m.CodEspecificacaoDespesa,
                        EspecificacaoDespesa = m.EspecificacaoDespesa,
                        Credor = m.Credor,
                        ValorEmpenhada = m.ValorEmpenhada,
                        ValorLiquidada = m.ValorLiquidada,
                        ValorPaga = m.ValorPaga,
                        valorDespesa = m.valorDespesa,
                        Descricao = m.Descricao,
                        Qtde = m.Qtde,
                        UnidadeMedida = m.UnidadeMedida,
                        VlrUnitario = m.VlrUnitario,
                        Documento = m.Documento
                    });
        }

        public IEnumerable<object> ObterComprasCOVID19ParaAPI(int qtdBusca)
        {
            var consulta = (from m in _db.VW_Materiais2020_COVID19.AsNoTracking()
                            join ug in _db.UnidadesGestoras on m.UG equals ug.unidadeGestoraIDSIAFEM
                            select new { m, ug });

            var consultaAgrupada = consulta.GroupBy(c => new
            {
                c.m.CodEspecificacaoDespesa,
                c.m.Credor,
                c.m.DataDocumento,
                c.m.EspecificacaoDespesa,
                c.m.Exercicio,
                c.m.NumEmpenho,
                c.m.UG,
                c.m.valorDespesa,
                c.m.Documento,
                c.m.NomDespesa,
                c.ug.nomeUG
            }).Select(myGroup => new
            {
                Exercicio = myGroup.Key.Exercicio,
                UG = myGroup.Key.UG,
                UnidadeGestora = myGroup.Key.nomeUG,
                NumEmpenho = myGroup.Key.NumEmpenho,
                DataDocumento = myGroup.Key.DataDocumento,
                Credor = myGroup.Key.Credor,
                EspecificacaoDespesa = myGroup.Key.EspecificacaoDespesa,
                ValorDespesa = myGroup.Key.valorDespesa,
                Documento = myGroup.Key.Documento,
                NomDespesa = myGroup.Key.NomDespesa,
                Materiais = consulta.Where(c => (c.m.Exercicio == myGroup.Key.Exercicio) &&
                          (c.m.UG == myGroup.Key.UG) &&
                          (c.m.NumEmpenho == myGroup.Key.NumEmpenho) &&
                          (c.m.EspecificacaoDespesa == myGroup.Key.EspecificacaoDespesa) &&
                          (c.m.valorDespesa == myGroup.Key.valorDespesa) &&
                          (c.m.Documento == myGroup.Key.Documento))
                .Select(c => new
                {
                    Descricao = c.m.Descricao,
                    Qtde = c.m.Qtde,
                    UnidadeMedida = c.m.UnidadeMedida,
                    ValorUnitario = c.m.VlrUnitario
                })
            });

            return consultaAgrupada;
        }

        public IEnumerable<VW_Materiais2020_COVID19> ObterTodasComprasMateriaisCOVID19()
        {
            var consulta = (from compraMaterial in _db.VW_Materiais2020_COVID19.AsNoTracking()
                            join unidadeGestora in _db.UnidadesGestoras on compraMaterial.UG equals unidadeGestora.unidadeGestoraIDSIAFEM
                            select new { compraMaterial, unidadeGestora });

            var compras = new List<VW_Materiais2020_COVID19>();

            foreach(var c in consulta)
            {
                var compra = c.compraMaterial;

                compra.NomeUG = c.unidadeGestora.nomeUG;

                compras.Add(compra);
            }

            return compras;
        }

        #endregion
    }
}
