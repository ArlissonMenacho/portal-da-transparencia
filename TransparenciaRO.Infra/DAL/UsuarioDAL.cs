﻿using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Linq;
using System;
using TransparenciaRO.Infra.Utils;
using static System.Data.Entity.EntityState;
using System.Data.Entity;
namespace TransparenciaRO.Infra.DAL
{
    public class UsuarioDAL : DALBase<Usuario>
    {
        public UsuarioDAL(DbTransparencia db) : base(db) { }

        public void AtualizarSenha(Guid pId, string pSenha, bool pAlterarSenhaProximoLogin = false)
        {
            var usuario = _db.Usuarios.FirstOrDefault(u => u.UsuarioId == pId);
            usuario.Senha = pSenha.GenerateHash();
            usuario.AlterarSenhaProximoLogin = pAlterarSenhaProximoLogin;
            _db.SaveChanges();
        }

        public IQueryable<Usuario> ObterTodosComPermissoes()
        {
            return _db.Usuarios.Include("Permissoes");
        }

        public override Usuario ObterPorId(Guid pId)
        {
            return _db.Usuarios
                .Include(x => x.Permissoes)
                .Include(x => x.Pastas)
                .AsNoTracking()
                .FirstOrDefault(u => u.UsuarioId == pId);
        }

        public Usuario ObterPorCpf(long pCPF)
        {
            return _db.Usuarios.Include(p => p.Permissoes).Include(p => p.Pastas).FirstOrDefault(u => u.CPF == pCPF);
        }

        public Usuario ObterPorEmail(string pEmail)
        {
            return _db.Usuarios.Include(p => p.Permissoes).Include(p => p.Pastas).FirstOrDefault(u => u.Email == pEmail);
        }
        /// <summary>
        /// Atualiza os dados comuns do usuário. Para alterar a senha, o método AtualizarSenha deverá ser utilizado.
        /// </summary>
        /// <param name="pDados">Objeto do usuário a ser inserido ou atualizado</param>
        /// <returns></returns>
        public override Guid SalvarDados(Usuario pDados)
        {
            if (pDados.UsuarioId == new Guid())
            {
                //Insert
                pDados.DataCriacao = DateTime.Now;
                pDados.UsuarioId = Guid.NewGuid();
                _db.Usuarios.Add(pDados);
                foreach (var permissao in pDados.Permissoes)
                {
                    _db.Permissoes.Attach(permissao);
                }

                foreach (var pastaGuid in pDados.Pastas)
                {
                    var pasta = _db.Pastas.FirstOrDefault(p => p.PastaId == pastaGuid.PastaId);
                    _db.Pastas.Attach(pasta);
                }
            }
            else
            {
                var novasPermissoes = pDados.Permissoes.ToList();
                var usuarioBD = _db.Usuarios.Include(p => p.Permissoes).Include(p => p.Pastas).FirstOrDefault(u => u.UsuarioId == pDados.UsuarioId);
                //Verifica se o usuário precisa resetar a senha
                if (pDados.AlterarSenhaProximoLogin && !usuarioBD.AlterarSenhaProximoLogin)
                {
                    usuarioBD.AlterarSenhaProximoLogin = true;
                    usuarioBD.Senha = pDados.Senha;
                }
                usuarioBD.Nome = pDados.Nome;
                usuarioBD.Email = pDados.Email;
                usuarioBD.AlterarSenhaProximoLogin = pDados.AlterarSenhaProximoLogin;
                usuarioBD.Ativo = pDados.Ativo;
                usuarioBD.CPF = pDados.CPF;
                var permissoesADeletar = usuarioBD.Permissoes.Except(pDados.Permissoes, p => p.PermissaoId).ToList();
                var permissoesAInserir = pDados.Permissoes.Except(usuarioBD.Permissoes, p => p.PermissaoId).ToList();
                permissoesADeletar.ForEach(p => usuarioBD.Permissoes.Remove(p));

                foreach (var permissao in permissoesAInserir)
                {
                    if (_db.Entry(permissao).State == Detached)
                        _db.Permissoes.Attach(permissao);

                    usuarioBD.Permissoes.Add(permissao);
                }

                var pastaADeletar = usuarioBD.Pastas.Except(pDados.Pastas, p => p.PastaId).ToList();
                var pastaAInserir = pDados.Pastas.Except(usuarioBD.Pastas, p => p.PastaId).ToList();
                pastaADeletar.ForEach(p => usuarioBD.Pastas.Remove(p));

                foreach (var pasta in pastaAInserir)
                {
                    if (_db.Entry(pasta).State == Detached)
                        _db.Pastas.Attach(pasta);

                    usuarioBD.Pastas.Add(pasta);
                }
            }

            _db.SaveChanges();
            return pDados.UsuarioId;
        }

        public override void Remover(Guid pId)
        {
            var usuario = ObterPorId(pId);
            if (usuario == null)
                return;
            usuario.Ativo = false;
            _db.SaveChanges();
        }
    }
}