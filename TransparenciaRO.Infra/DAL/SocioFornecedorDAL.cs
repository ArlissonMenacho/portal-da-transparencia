﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class SocioFornecedorDAL
    {
        private readonly DbTransparencia dbTransparencia;

        public SocioFornecedorDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(SocioFornecedor socioFornecedor)
        {
            dbTransparencia.SocioFornecedores.Add(socioFornecedor);
            dbTransparencia.SaveChanges();
        }

        public int Remover(Guid socioFornecedorId)
        {
            var socioFornecedor = BuscarSocioFornecedor(socioFornecedorId);
            var codigoImpedimento = socioFornecedor.ImpedirFornecedorCodigo;

            dbTransparencia.SocioFornecedores.Remove(socioFornecedor);
            dbTransparencia.SaveChanges();

            return codigoImpedimento;
        }

        public SocioFornecedor BuscarSocioFornecedor(Guid socioFornecedorId)
        {
            return dbTransparencia.SocioFornecedores.Include(x => x.ImpedirFornecedor.Fornecedor).FirstOrDefault(x => x.SocioFornecedorId == socioFornecedorId);
        }

        public ICollection<SocioFornecedor> BurcarSocioFornecedores()
        {
            return dbTransparencia.SocioFornecedores.ToList();
        }

        public IList<SocioFornecedor> BuscarSocioFornecedorPorCodigoImpedimento(int codigoImpedimento)
        {
            var socios = dbTransparencia.SocioFornecedores.Where(x => x.ImpedirFornecedor.Codigo == codigoImpedimento);
            return socios.ToList();
        }
    }
}
