﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.DataDBPLL.Contexto;

namespace TransparenciaRO.Infra.DAL
{

    public abstract class ReadOnlyDALBase<T> where T : class
    {
        protected readonly DbTransparencia _db;
        protected readonly DbPLLContext _dbPLL;
        public ReadOnlyDALBase(DbTransparencia db, DbPLLContext dbPLLContext)
        {
            _db = db;
            _dbPLL = dbPLLContext;
        }
        public ReadOnlyDALBase(DbTransparencia db)
        {
            _db = db;
        }

        public IEnumerable<T> ObterTodos()
        {
            return _db.Set<T>();
        }

        public IQueryable<T> ObterTodosAsQueryable()
        {
            return _db.Set<T>();
        }
    }
    public abstract class DALBase<T> : ReadOnlyDALBase<T> where T : class
    {
        #region Métodos abstratos
        public abstract T ObterPorId(Guid pId);
        public abstract Guid SalvarDados(T pDados);
        public abstract void Remover(Guid pId);
        #endregion

        public DALBase(DbTransparencia db, DbPLLContext dbPLL) : base(db,dbPLL) { }
        public DALBase(DbTransparencia db) : base(db) { }
    }
}
