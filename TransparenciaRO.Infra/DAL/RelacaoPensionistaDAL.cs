﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class RelacaoPensionistaDAL
    {
        public DbTransparencia dbTransparencia;

        public RelacaoPensionistaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void AdicionarDados(List<RelacaoPensionista> relacaoPensionista)
        {
            dbTransparencia.BulkInsert(relacaoPensionista);
        }

        public void VerificarDadosRemover(int ano, int mes)
        {
            if (dbTransparencia.RelacaoPensionista.Any(x => x.Ano == ano && x.Mes == mes))
            {
                dbTransparencia.RelacaoPensionista.Where(x => x.Ano == ano && x.Mes == mes).Delete();
            }
        }

        public int Ano()
        {
            return dbTransparencia.RelacaoPensionista.FirstOrDefault() != null
                ? dbTransparencia.RelacaoPensionista.Max(x => x.Ano)
                : 0;
        }

        public int Mes(int ano)
        {
            return dbTransparencia.RelacaoPensionista.FirstOrDefault() != null
                ? dbTransparencia.RelacaoPensionista.Where(x => x.Ano == ano).Max(x => x.Mes)
                : 0;
        }

        public List<int> Meses(int ano)
        {
            var mesMaximo = ano == DateTime.Now.Year
                ? dbTransparencia.RelacaoPensionista.Where(s => s.Ano == ano).Max(s => s.Mes)
                : 12;

            return Enumerable.Range(1, mesMaximo).ToList();
        }

        public Dictionary<string, int> RelacaoLotacao(int ano, int mes)
        {
            var relacaoLotacoes = dbTransparencia.RelacaoPensionista
                .Include(rs => rs.Lotacao)
                .Where(rs => rs.Ano == ano && rs.Mes == mes)
                .GroupBy(rs => rs.Lotacao.Descricao.Trim())
                .Select(rs => new { rs.Key, Count = rs.Count() });

            return relacaoLotacoes
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> RelacaoCargo(string strLotacaoId, int ano, int mes)
        {
            var lotacaoId = String.IsNullOrEmpty(strLotacaoId)
                ? (Guid?)null
                : new Guid(strLotacaoId);

            var RelacaoPensionista = dbTransparencia.RelacaoPensionista
                .Where(rs => rs.Ano == ano && rs.Mes == mes && (lotacaoId == null || rs.LotacaoId == lotacaoId))
                .GroupBy(rs => rs.Cargo)
                .Select(rs => new { rs.Key, Count = rs.Count() });

            return RelacaoPensionista
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> RelacaoClassificacao(string strLotacaoId, int ano, int mes)
        {
            var lotacaoId = String.IsNullOrEmpty(strLotacaoId)
                ? (Guid?)null
                : new Guid(strLotacaoId);

            var RelacaoPensionista = dbTransparencia.RelacaoPensionista
                .Where(rs => rs.Ano == ano && rs.Mes == mes && (lotacaoId == null || rs.LotacaoId == lotacaoId))
                .GroupBy(rs => rs.Classificacao)
                .Select(rs => new { rs.Key, Count = rs.Count() });

            return RelacaoPensionista.ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, double> MediaIdadeUnidadeGestora(int ano, int mes)
        {
            var funcionario = dbTransparencia.RelacaoPensionista
                .Include(rs => rs.Lotacao)
                .Where(rs => rs.Ano == ano && rs.Mes == mes)
                .Select(rs => new { rs.Lotacao.Descricao, rs.DataNascimento });

            return funcionario
                .ToList()
                .GroupBy(f => f.Descricao)
                .ToDictionary(k => k.Key, v => v.Average(f => Idade(f.DataNascimento)));
        }

        public Dictionary<string, double> MediaIdadeCargo(string strLotacaoId, int ano, int mes)
        {
            var lotacaoId = String.IsNullOrEmpty(strLotacaoId)
                ? (Guid?)null
                : new Guid(strLotacaoId);

            var funcionario = dbTransparencia.RelacaoPensionista
                .Where(rs => rs.Ano == ano && rs.Mes == mes && (lotacaoId == null || rs.LotacaoId == lotacaoId))
                .Select(rs => new { rs.Cargo, rs.DataNascimento });

            return funcionario
                .ToList()
                .GroupBy(f => f.Cargo)
                .ToDictionary(k => k.Key, v => v.Average(f => Idade(f.DataNascimento)));
        }

        public Dictionary<string, double> MediaIdadeClassificacao(string strLotacaoId, int ano, int mes)
        {
            var lotacaoId = String.IsNullOrEmpty(strLotacaoId)
                ? (Guid?)null
                : new Guid(strLotacaoId);

            var funcionario = dbTransparencia.RelacaoPensionista
                .Where(rs => rs.Ano == ano && rs.Mes == mes && (lotacaoId == null || rs.LotacaoId == lotacaoId))
                .Select(rs => new { rs.Classificacao, rs.DataNascimento });

            return funcionario
                .ToList()
                .GroupBy(f => f.Classificacao)
                .ToDictionary(k => k.Key, v => v.Average(f => Idade(f.DataNascimento)));
        }

        public IQueryable<RelacaoPensionista> Funcionarios(int ano, int mes, string unidadeGestora, string cargo, string classificacao)
        {
            var lotacao = unidadeGestora == "s Unidades Gestoras"
                ? ""
                : unidadeGestora;

            var funcionarios = dbTransparencia.RelacaoPensionista
                .Include(r => r.Lotacao)
                .Where(rs => rs.Ano == ano
                             && rs.Mes == mes
                             && (lotacao == "" || rs.Lotacao.Descricao == lotacao.Trim())
                             && (cargo == "" || rs.Cargo == cargo.Trim())
                             && (classificacao == "" || rs.Classificacao == classificacao.Trim()));

            return funcionarios;
        }

        private int Idade(DateTime? dataNascimento)
        {
            var nascimento = dataNascimento ?? DateTime.Today;
            var today = DateTime.Today;
            var ano = 0;

            if (dataNascimento == null)
                return 0;

            if (today.Month > nascimento.Month)
                ano = today.Year - nascimento.Year;
            else if (today.Month == nascimento.Month)
                if (today.Day >= nascimento.Day)
                    ano = today.Year - nascimento.Year;
                else
                    ano = (today.Year - 1) - nascimento.Year;
            else
                ano = (today.Year - 1) - nascimento.Year;

            return ano;
        }

        public void RemoverDadosAnoMes(int ano, int mes)
        {
            dbTransparencia.Database.ExecuteSqlCommand("DELETE FROM RelacaoPensionista WHERE Ano = {0} AND Mes = {1}",
                ano, mes);
        }
    }
}
