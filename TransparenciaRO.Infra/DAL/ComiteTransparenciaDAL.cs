﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using TransparenciaRO.Infra.Factory;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.ComiteTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class ComiteTransparenciaDAL
    {
        public DbTransparencia _dbTransparencia;

        public ComiteTransparenciaDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public long AdicionarInscricao(InscricaoComiteTransparenciaViewModel inscricaoVM)
        {
            var inscricao = ComiteTransparenciaFactory.CriarInscricao(inscricaoVM.InscricaoId, inscricaoVM.RazaoSocial, inscricaoVM.Cnpj, inscricaoVM.RepresentanteLegal, inscricaoVM.Telefone);

            _dbTransparencia.InscricaoComiteTransparencia.Add(inscricao);

            _dbTransparencia.SaveChanges();

            return inscricao.Protocolo;
        }

        public List<InscricaoComiteTransparenciaViewModel> BuscarInscricoes()
        {
            var inscricoesComiteTransparencia = _dbTransparencia.InscricaoComiteTransparencia.Include(i => i.Arquivos).ToList();

            var inscricoesComiteTransparenciaVM = ComiteTransparenciaFactory.ConverterInscricoesComiteTransparenciaParaInscricoesComiteTransparenciaViewModel(inscricoesComiteTransparencia);

            return inscricoesComiteTransparenciaVM;
        }

        public List<InscricaoComiteTransparenciaViewModel> FiltrarInscricoes(Expression<Func<InscricaoComiteTransparencia, bool>> expression)
        {
            var inscricoesComiteTransparencia = _dbTransparencia.InscricaoComiteTransparencia.Include(i => i.Arquivos).Where(expression).ToList();

            var inscricoesComiteTransparenciaVM = ComiteTransparenciaFactory.ConverterInscricoesComiteTransparenciaParaInscricoesComiteTransparenciaViewModel(inscricoesComiteTransparencia);

            return inscricoesComiteTransparenciaVM;
        }

        public void ReomverInscrito(Guid id)
        {
            var inscricao = _dbTransparencia.InscricaoComiteTransparencia.FirstOrDefault(i => i.InscricaoId == id);

            _dbTransparencia.InscricaoComiteTransparencia.Remove(inscricao);

            _dbTransparencia.SaveChanges();
        }
    }
}
