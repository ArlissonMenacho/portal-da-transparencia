﻿using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Linq;
using System.Collections.Generic;
using System;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class DiariaDAL : ReadOnlyDALBase<Diaria>
    {
        public DiariaDAL(DbTransparencia db) : base(db) { }

        public IEnumerable<Diaria> ObterDiariasPorFiltro(DateTime pDataInicial, DateTime pDataFinal, string pNome, IEnumerable<string> CodigosOrgao)
        {
            var uGs = new List<string>();

            if (CodigosOrgao.Equals("110033") || CodigosOrgao.Equals("130031"))
                uGs.AddRange(new List<string>() { "110033", "130031" });
            else
                uGs.AddRange(CodigosOrgao);

            var dataFinal = pDataFinal.EndOfTheDay();


            return _db.Diarias.AsNoTracking().Where(d =>
                              (d.DataDocumento >= pDataInicial && d.DataDocumento <= dataFinal) &&
                              (uGs.Count() == 0 || uGs.Any(ug => ug.Equals(d.UG))) &&
                              (pNome.Trim() == "" || d.Nome.Contains(pNome)) &&
                              (d.ValorDocumento > 0))
                              .Take(1000)
                              .ToList();
                              
        }
    }
}