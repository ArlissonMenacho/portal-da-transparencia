﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class UnidadeGestoraDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public UnidadeGestoraDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public IEnumerable<UnidadeGestora> GetUnidadesGestoras()
        {
            return _dbTransparencia.UnidadesGestoras.OrderBy(ug => ug.sigla ?? ug.nomeUG)
                                                    .ToList();
        }

        public IEnumerable<UnidadeGestora> GetUnidadesGestorasParaLicitacao()
        {
            return _dbTransparencia.UnidadesGestoras.OrderBy(ug => ug.nomeUG).ToList()
                .Select(ug => new UnidadeGestora { sigla = ug.sigla, nomeUG = $"{ug.sigla} - {ug.nomeUG}" });
        }

        public IDictionary<string, string> UnidadesGestorasDictionary()
        {
            var unidadesGestoras = _dbTransparencia.UnidadesGestoras.GroupBy(ug => ug.nomeUG).ToList()
                .Select(ug => new { Descricao = (ug.Any(o => o.sigla.IsNullOrEmpty()) ? ug.Key.Trim() : ug.First().sigla + " - " + ug.Key.Trim()), Codigos = ug.Select(o => o.unidadeGestoraID) })
                .ToDictionary(k => String.Join(",", k.Codigos.First()), v => v.Descricao);

            return unidadesGestoras;
        }

        public IDictionary<string, string> UnidadesGestorasGuid()
        {
            return _dbTransparencia.UnidadesGestoras.GroupBy(ug => ug.nomeUG).ToList()
                .Select(ug => new { Descricao = (ug.Any(o => o.sigla.IsNullOrEmpty()) ? ug.Key.Trim() : ug.First().sigla + " - " + ug.Key.Trim()), Codigos = ug.Select(o => o.unidadeGestoraID) })
                .ToDictionary(k => String.Join(",", k.Codigos), v => v.Descricao);
        }

        public string GetNomeUg(string ug)
        {
            return _dbTransparencia.UnidadesGestoras.FirstOrDefault(u => u.unidadeGestoraIDSIAFEM == ug)?.nomeUG;
        }

        public string BuscarUnidadeGestoroaPorGuid(Guid ugID)
        {
            return _dbTransparencia.UnidadesGestoras.FirstOrDefault(u => u.unidadeGestoraID == ugID)?.nomeUG;
        }

        public IDictionary<string, string> BuscarUgPorGuid(Guid guid)
        {
            return _dbTransparencia.UnidadesGestoras
                .Where(x => x.unidadeGestoraID == guid)
                .GroupBy(ug => ug.nomeUG).ToList()
                .Select(ug => new
                {
                    Descricao = (ug.Any(o => o.sigla.IsNullOrEmpty())
                            ? ug.Key.Trim()
                            : ug.First().sigla + " - " + ug.Key.Trim()),
                    Codigos = ug.Select(o => o.unidadeGestoraID)
                })
                .ToDictionary(k => String.Join(",", k.Codigos.First()), v => v.Descricao);
        }
    }
}