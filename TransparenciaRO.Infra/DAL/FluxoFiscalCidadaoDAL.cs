﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class FluxoFiscalCidadaoDAL
    {
        private DbTransparencia _dbTransparencia;
        private FluxoFiscalCidadao _fluxoFiscalCidadao;

        public FluxoFiscalCidadaoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
            _fluxoFiscalCidadao = new FluxoFiscalCidadao();
        }

        public void SalvarFluxo(FluxoFiscalCidadao fluxoFiscalCidadao)
        {
            _dbTransparencia.FluxoFiscalCidadao.Add(fluxoFiscalCidadao);
            _dbTransparencia.SaveChanges();
            new FiscalCidadaoDAL(_dbTransparencia).Update(new FiscalCidadao
            {
                UnidadeGestoraId = fluxoFiscalCidadao.OrgaoDestinoId,
                ProtocoloId = fluxoFiscalCidadao.ProtocoloId,
                EStatus = ETipoStatus.EmAndamento
            });
        }

        public IList<FluxoFiscalCidadao> BuscarFluxos(long id)
        {
            return _dbTransparencia.FluxoFiscalCidadao
                .Include(x => x.OrgaoOrigem)
                .Include(x => x.OrgaoDestino)
                .Where(x => x.ProtocoloId == id).ToList();
        }
    }
}
