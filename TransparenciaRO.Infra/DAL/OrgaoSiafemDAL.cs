﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class OrgaoSiafemDAL
    {

        private readonly DbTransparencia db = new DbTransparencia();

        public List<OrgaoSiafem> getOrgaos()
        {
            return db.OrgaosSiafem.GroupBy(o => o.CGC).Select(o => o.FirstOrDefault()).OrderBy(i=>i.ORGAO).ToList();
        }
    }
}
