﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using EntityFramework.Extensions;

namespace TransparenciaRO.Infra.DAL
{
    public class ReceitaEmpresaDAL
    {
        private readonly DbTransparencia dbTransparencia = new DbTransparencia();

        public ReceitaEmpresaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public void Adicionar(List<ReceitaEmpresa> receitaEmpresas)
        {
            dbTransparencia.BulkInsert(receitaEmpresas);
            dbTransparencia.SaveChanges();
        }
        public void VerficarRemoverDados(DateTime data, EEmpresa empresa)
        {
            if (dbTransparencia.ReceitasEmpresas.Any(x => x.Data.Year == data.Year && x.Data.Month == data.Month && x.Empresa == empresa))
            {
                dbTransparencia.ReceitasEmpresas.Where(x => x.Data.Year == data.Year && x.Data.Month == data.Month && x.Empresa == empresa).Delete();
            }
        }
        public int AnoMax(EEmpresa empresa)
        {
            return dbTransparencia.ReceitasEmpresas
                .Where(r => r.Empresa == empresa)
                .Max(r => (int?)r.Data.Year) ?? 0;
        }

        public int AnoMin(EEmpresa empresa)
        {
            return dbTransparencia.ReceitasEmpresas
                .Where(r => r.Empresa == empresa)
                .Min(r => (int?)r.Data.Year) ?? 0;
        }

        public List<int> Meses(int ano, EEmpresa empresa)
        {
            var meses = dbTransparencia.ReceitasEmpresas
                .Where(r => r.Data.Year == ano && r.Empresa == empresa)
                .GroupBy(r => r.Data.Month)
                .Select(r => new { MesInt = r.Key})
                .ToList();

            return meses
                .Select(r => r.MesInt)
                .ToList();
        }

        public Dictionary<int, decimal> ReceitaAnual(EEmpresa empresa)
        {
            var receitaAnual = dbTransparencia.ReceitasEmpresas
                .Where(r => r.Empresa == empresa)
                .GroupBy(r => r.Data.Year)
                .Select(r => new { Ano = r.Key, Valor = r.Sum(y => y.Receita) });

            return receitaAnual.ToDictionary(k => k.Ano, v => v.Valor);
        }

        public Dictionary<int, decimal> ReceitaMensal(int ano, EEmpresa empresa)
        {
            var receitaAnual = dbTransparencia.ReceitasEmpresas
                .Where(r => r.Empresa == empresa && r.Data.Year == ano)
                .GroupBy(r => r.Data.Month)
                .Select(r => new { Mes = r.Key, Valor = r.Sum(y => y.Receita) });

            return receitaAnual.ToDictionary(k => k.Mes, v => v.Valor);
        }

        public List<ReceitaEmpresa> ReceitaDetalhada(int ano, int mes, EEmpresa empresa)
        {
            return dbTransparencia.ReceitasEmpresas
                .Where(r => r.Data.Year == ano && r.Data.Month == mes && r.Empresa == empresa)
                .ToList();
        }
    }
}
