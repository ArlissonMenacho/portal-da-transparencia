﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class FolhaPagamentoDAL
    {
        private readonly DbTransparencia dbTransparencia = new DbTransparencia();

        public IList<FolhaPagamento> getFolha(int ano, int mes, string nome, string cpf, string lotacao)
        {
            if (nome != string.Empty && cpf == string.Empty && lotacao == "0")
            {
                return dbTransparencia.FolhaPagamento.Where(i => i.Ano == ano && i.Mes == mes && i.Nome.Contains(nome)).OrderBy(i => i.Nome).ToList();
            }
            else if (nome != string.Empty && cpf == string.Empty && lotacao != "0")
            {
                return dbTransparencia.FolhaPagamento.Where(i => i.Ano == ano && i.Mes == mes && i.Lotacao == lotacao && i.Nome.Contains(nome)).OrderBy(i => i.Nome).ToList();
            }
            else if (nome == string.Empty && cpf != string.Empty && lotacao == "0")
            {
                return dbTransparencia.FolhaPagamento.Where(i => i.Ano == ano && i.Mes == mes && i.Cpf == cpf).ToList();
            }
            else if (nome == string.Empty && cpf != string.Empty && lotacao != "0")
            {
                return dbTransparencia.FolhaPagamento.Where(i => i.Ano == ano && i.Mes == mes && i.Cpf == cpf && i.Lotacao == lotacao).ToList();
            }
            else if (nome == string.Empty && cpf == string.Empty && lotacao != "0")
            {
                return dbTransparencia.FolhaPagamento.Where(i => i.Ano == ano && i.Mes == mes && i.Lotacao == lotacao).OrderBy(i => i.Nome).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<string> getLotacaoUG(short ano, byte mes)
        {
            return dbTransparencia.FolhaPagamento.Where(fp => fp.Ano == ano && fp.Mes == mes).Select(s => s.Lotacao).Distinct().ToList();
        }

        public FolhaPagamento getFolha(int ano, int mes, string matricula)
        {
            return dbTransparencia.FolhaPagamento.FirstOrDefault(fp => fp.Ano == ano && fp.Mes == mes && fp.Matricula == matricula);
        }
    }
}
