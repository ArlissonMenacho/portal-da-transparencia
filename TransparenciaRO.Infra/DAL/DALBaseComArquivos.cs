﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using static System.Data.Entity.EntityState;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public abstract class DALBaseComArquivos<T> where T : class, IClasseComArquivos
    {

        internal DbTransparencia db;

        public DALBaseComArquivos(DbTransparencia dbTransparencia)
        {
            db = dbTransparencia;
        }

        public Guid? SalvarDados(T pObjeto, out string pMensagemErro)
        {
            try
            {
                if (pObjeto.GetId() == new Guid())
                {
                    //Inserção
                    pObjeto.SetId(Guid.NewGuid());                    
                    db.Entry(pObjeto).State = Added;
                    foreach (var arquivo in pObjeto.Arquivos)
                    {
                        AtribuirArquivo(arquivo, pObjeto.GetId());
                        db.Arquivos.Attach(arquivo);
                        arquivo.Temporario = false;                   
                    }
                }
                else
                {
                    //Alteração
                    var registroBD = ObterPorId(pObjeto.GetId());
                    db.Entry(registroBD).CurrentValues.SetValues(pObjeto); // Atualiza os campos no BD

                    var arquivosRegistro = ObterArquivos(pObjeto.GetId());

                    //Inserção de novos arquivos (temporários)
                    arquivosRegistro.Where(a => a.Temporario && a.DataRemocao == null).ToList().ForEach(arquivoAVincular =>
                    {
                        if (db.Entry(arquivoAVincular).State == Detached)
                            db.Arquivos.Attach(arquivoAVincular);
                        arquivoAVincular.Temporario = false;
                    });

                    //Remoção de arquivos já vinculados (pelo BD) ao registro, mas que foram removidos da view e agora precisam ser removidos do BD
                    arquivosRegistro.Where(a => a.DataRemocao == null).Except(pObjeto.Arquivos, arq => arq.ArquivoId).ToList().ForEach(arquivoAExcluir =>
                    {
                        db.Entry(arquivoAExcluir).State = Modified;
                        arquivoAExcluir.DataRemocao = DateTime.Now;
                    });
                }

                db.SaveChanges();
                pMensagemErro = "";
                return pObjeto.GetId();
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();
                return null;
            }
        }

        public abstract T ObterPorId(Guid pId);
        public abstract List<Arquivo> ObterArquivos(Guid pId);
        public abstract void AtribuirArquivo(Arquivo pArquivo, Guid pId);
        

    }
}
