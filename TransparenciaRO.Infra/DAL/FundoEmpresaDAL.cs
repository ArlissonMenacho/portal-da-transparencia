﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class FundoEmpresaDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public FundoEmpresaDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public void Salvar(ICollection<FundoEmpresa> fundoEmpresas)
        {
            _dbTransparencia.BulkInsert(fundoEmpresas);
            _dbTransparencia.BulkSaveChanges();
        }

        public void VerificarDados(string numeroProcesso, EEmpresa eEmpresa)
        {
            if (_dbTransparencia.FundoEmpresa.Any(x => x.Processo.Contains(numeroProcesso) && x.EEmpresa == eEmpresa))
                    _dbTransparencia.FundoEmpresa.Where(x => x.Processo == numeroProcesso && x.EEmpresa == eEmpresa).Delete();
        }

        public IList<FundoEmpresa> BuscarTodos(EEmpresa empresa)
        {
            return _dbTransparencia.FundoEmpresa.Where(f => f.EEmpresa == empresa).ToList();
        }

        public IList<FundoEmpresa> BuscarFiltro(DateTime dataInicial, DateTime dataFinal, string cnpjCpf, string favorecido)
        {
            return _dbTransparencia.FundoEmpresa.Where(f => 
                f.DataPagamento >= dataInicial 
                && f.DataPagamento <= dataFinal
                && (cnpjCpf == "" || f.CnpjCpf.Contains(cnpjCpf))
                && (favorecido == "" || f.Favorecido.Contains(favorecido))
                ).ToList();
        }
    }
}