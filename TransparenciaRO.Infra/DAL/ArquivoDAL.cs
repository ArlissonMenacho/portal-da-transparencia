﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class ArquivoDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public ArquivoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }


        public List<string> AdicionarArquivosBoletim(Guid boletimId, HttpFileCollectionBase files)
        {
            var mensagemErros = new List<string>();

            for (var i = 0; i < files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(files[i]))
                {
                    var arquivo = new Arquivo()
                    {
                        ArquivoId = Guid.NewGuid(),
                        BoletimId = boletimId,
                        Extensao = Path.GetExtension(files[i].FileName),
                        Descricao = files[i].FileName,
                        PastaId = GuidUtils.GuidPastaBoletins(),
                        Temporario = false
                    };

                    if (ArquivoUtils.SalvarArquivo(files[i], arquivo.ArquivoId))
                        Adicionar(arquivo);
                    else
                        mensagemErros.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                }
                else
                    mensagemErros.Add($"Não foi possível fazer o upload do arquivo { files[i].FileName }. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
            }

            return mensagemErros;
        }


        public List<string> AdicionarArquivosInscricaoComiteTransparencia(Guid inscricaoId, HttpFileCollectionBase files)
        {
            var mensagemErros = new List<string>();

            for (var i = 0; i < files.Count; i++)
            {
                if (ArquivoUtils.ArquivoValido(files[i]))
                {
                    var arquivo = new Arquivo()
                    {
                        ArquivoId = Guid.NewGuid(),
                        InscricaoComiteTransparenciaId = inscricaoId,
                        TipoAnexo = (ETipoAnexo)System.Enum.Parse(typeof(ETipoAnexo), files.AllKeys[i]),
                        Extensao = Path.GetExtension(files[i].FileName),
                        Descricao = files[i].FileName,
                        PastaId = GuidUtils.GuidPastaComiteTransparencia(),
                        Temporario = true
                    };

                    if (ArquivoUtils.SalvarArquivo(files[i], arquivo.ArquivoId))
                        Adicionar(arquivo);
                    else
                        mensagemErros.Add($"Não foi possível fazer o upload do arquivo devido a um erro interno do sistema. Contate o administrador do sistema.");
                }
                else
                    mensagemErros.Add($"Não foi possível fazer o upload do arquivo { files[i].FileName }. Verifique se o mesmo possui menos de 10MB e uma das seguintes extensões: PDF, XLS, XLSX, DOC ou DOCX");
            }

            return mensagemErros;
        }

        public void Adicionar(Arquivo arquivo)
        {
            try
            {
                _dbTransparencia.Arquivos.Add(arquivo);
                _dbTransparencia.SaveChanges();
            }
            catch (Exception e)
            {
                var MensagemErro = e.Message;
                throw;
            }
        }

        public void Editar(Arquivo arquivo)
        {
            _dbTransparencia.Entry(arquivo).State = EntityState.Modified;
            _dbTransparencia.SaveChanges();
        }

        /// <summary>
        /// Remoção lógica do arquivo (apenas atribui data de remoção)
        /// </summary>
        /// <param name="arquivo"></param>
        public void Remover(Arquivo arquivo)
        {
            arquivo.DataRemocao = DateTime.Now;
            _dbTransparencia.Entry(arquivo).State = EntityState.Modified;
            _dbTransparencia.SaveChanges();
        }

        public Arquivo BuscarArquivo(Guid arquivoGuid)
        {
            return _dbTransparencia.Arquivos.Find(arquivoGuid);
        }

        public ICollection<Arquivo> BuscarArquivos()
        {
            return _dbTransparencia.Arquivos.ToList();
        }

        public ICollection<Arquivo> BuscarArquivoPorPasta(Guid pTipoServico)
        {
            return _dbTransparencia.Arquivos.Where(x => x.PastaId == pTipoServico && x.DataRemocao == null).ToList();
        }

        public ICollection<Arquivo> BuscarArquivoTemporarios()
        {
            return _dbTransparencia.Arquivos
                .Where(a => a.Temporario == true)
                .ToList();
        }

        public List<Arquivo> BuscarArquivoTemporarios(Guid fornecedorDispensaId)
        {
            return _dbTransparencia.Arquivos
                .Where(a => a.Temporario == true && a.FornecedorDispensaId == fornecedorDispensaId)
                .ToList();
        }

        /// <summary>
        /// Remoção dos arquivos arquivos temporarios
        /// </summary>
        /// <param name="arquivos"></param>
        public void RemoverTemporario(ICollection<Arquivo> arquivos)
        {
            foreach (var arquivo in arquivos)
            {
                if (arquivo.DataUpload.Date < DateTime.Now.Date)
                    _dbTransparencia.Arquivos.Remove(arquivo);
            }
            _dbTransparencia.SaveChanges();
        }

        public void RemoverArquivofornecedorDispensa(Guid arquivoId)
        {
            var arquivo = _dbTransparencia.Arquivos.FirstOrDefault(a => a.ArquivoId == arquivoId);

            if (arquivo.Temporario == true)
            {
                _dbTransparencia.Arquivos.Remove(arquivo);
                _dbTransparencia.SaveChanges();
            }

            else
                Remover(arquivo);
        }
    }
}