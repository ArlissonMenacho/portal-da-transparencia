﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class LicitacoesDAL : ReadOnlyDALBase<Licitacaoo>
    {
        public LicitacoesDAL(DbTransparencia dbTransparencia) : base(dbTransparencia) { }

        public IEnumerable<Licitacaoo> ObterPorFiltro(string ano, string modalidade, string numeroLicitacao, string numeroProcesso, int qtnBusca)
        {
            var dados = _db.Licitacaoo.AsNoTracking().Where(l =>
                 (ano == null || ano == "" || l.Pregao.Contains(ano)) &&
                 (modalidade == null || modalidade == "" || l.Modalidade == modalidade) &&
                 (numeroLicitacao == null || numeroLicitacao == "" || l.Pregao.Contains(numeroLicitacao)) &&
                 (numeroProcesso == null || numeroProcesso == "" || l.Processo.Contains(numeroProcesso))
                  )
                 .GroupBy(item => item.Pregao)
                 .SelectMany(grouping => grouping.OrderBy(item => item.Pregao).Take(1))
                 .OrderByDescending(item => item.Pregao).Take(qtnBusca)
                 .ToList();

            return dados;
        }

        public IEnumerable<Licitacaoo> ObterPorFiltro(string numeroLicitacao) =>
                   _db.Licitacaoo.AsNoTracking().Where(l =>
                   (numeroLicitacao == null || numeroLicitacao == "" || l.Pregao.Equals(numeroLicitacao))
                   )
                   .OrderByDescending(item => item.Pregao)
                   .ToList();
    }
}
