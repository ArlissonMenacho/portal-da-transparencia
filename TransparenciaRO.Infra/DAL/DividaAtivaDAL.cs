﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Linq;
using System.Linq.Dynamic;

namespace TransparenciaRO.Infra.DAL
{
    public class DividaAtivaDAL
    {
        public DbTransparencia dbTransparencia;

        public DividaAtivaDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public DividaAtiva Detalhar(int? Id)
        {
            return dbTransparencia.DividaAtivas.Find(Id);
        }

        public IList<DividaAtiva> BuscarDividaAtiva(string nomeRazaoSocial, string cpfCnpj)
        {
            var x = dbTransparencia.DividaAtivas
                .Where(d =>
                (nomeRazaoSocial == "" || d.Nome.Contains(nomeRazaoSocial)) &&
                (cpfCnpj == "" || d.CpfCnpj.Contains(cpfCnpj)))
                .Take(1000)
                .OrderBy(d => d.Nome)
                .ToList();

            return x;
        }
    }
}
