﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Factory;
using TransparenciaRO.Infra.Model.View;

namespace TransparenciaRO.Infra.DAL
{
    public class RegistroBoletimDal
    {
        public DbTransparencia _dbTransparencia;

        public RegistroBoletimDal(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public List<RegistroBoletimViewModel> ObterTodos()
        {
            var boletins = _dbTransparencia.RegistroBoletim
                                           .AsNoTracking()
                                           .OrderByDescending(b => b.Data)
                                           .ToList();

            foreach (var boletim in boletins)
                boletim.Arquivo = _dbTransparencia.Arquivos.FirstOrDefault(a => a.BoletimId == boletim.BoletimId);

            var boletinsVM = Factory.RegistroBoletimFactory.ConverterRegistrosBoletimsParaRegistrosBoletimsViewModel(boletins);

            return boletinsVM;
        }

        public void AdicionarBoletim(RegistroBoletimViewModel boletimVM)
        {
            var numeroDeBoletins = _dbTransparencia.RegistroBoletim.AsNoTracking().Count();

            var descricao = $"Boletim de Controle Interno Edição {(numeroDeBoletins + 1)}";

            var registroBoletim = RegistroBoletimFactory.RegistrarBoletim(boletimVM.BoletimId, boletimVM.Data, descricao);

            _dbTransparencia.RegistroBoletim.Add(registroBoletim);

            _dbTransparencia.SaveChanges();
        }
    }
}
