﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class TbPLLOrgaoDAL
    {
        public DbTransparencia dbTransparencia;

        public TbPLLOrgaoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public IEnumerable<Orgao> GetOrgaos()
        {
            return dbTransparencia.Orgaos.ToList();
        }
    }
}
