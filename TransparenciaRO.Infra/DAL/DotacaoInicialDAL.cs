﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class DotacaoInicialDAL
    {
        public DbTransparencia DbTransparencia;

        public DotacaoInicialDAL(DbTransparencia dbTransparencia)
        {
            this.DbTransparencia = dbTransparencia;
        }

        public IQueryable<DotacaoInicial> GetDotacaoInicial(IEnumerable<int> pCodUnidadesGestoras, IEnumerable<int> pExercicios)
        {
            return DbTransparencia.DotacaoInicial.Where(di => pCodUnidadesGestoras.Contains(Convert.ToInt32(di.CodigoOrgao)) && pExercicios.Contains(di.Exercicio));
        }
    }
}
