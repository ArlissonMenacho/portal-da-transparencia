﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Factory;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Model.View.DadosLicitacao;

namespace TransparenciaRO.Infra.DAL
{
    public class DadosLicitacaoDAL : ReadOnlyDALBase<DadosLicitacao>
    {
        public DadosLicitacaoDAL(DbTransparencia db) : base(db)
        {
            //
        }

        public List<DadosLicitacao> Filter(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .Where(licitacao =>
                                                   (string.IsNullOrEmpty(filtro.NumeroProcessoAdministrativo) || licitacao.NumeroProcessoAdministrativo.Contains(filtro.NumeroProcessoAdministrativo))
                                                   && (string.IsNullOrEmpty(filtro.CNPJ) || licitacao.CNPJ.Contains(filtro.CNPJ))
                                                   && (string.IsNullOrEmpty(filtro.RazaoSocial) || licitacao.RazaoSocial.Contains(filtro.RazaoSocial))
                                                   && (string.IsNullOrEmpty(filtro.Objeto) || licitacao.Objeto.Contains(filtro.Objeto))
                                               )
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .Take(filtro.QuantidadeResultados)
                                               .ToList();

            return licitacoes;
        }

        public List<DadosLicitacao> GetAll()
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .Take(50)
                                               .ToList();

            return licitacoes;
        }

        public List<DadosLicitacao> GetAllForCSV()
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .ToList();

            return licitacoes;
        }

        public IndexDadosLicitacaoViewModel GetDataToLoadIndexView()
        {
            var licitacoes = GetAll();

            var indexDadosLicitacaoViewModel = DadosLicitacaoFactory.CreateIndexDadosLicitacaoViewModel(licitacoes);

            return indexDadosLicitacaoViewModel;
        }

        #region aditivados

        public List<DadosLicitacao> GetAditivados()
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .Where(dl => dl.TermoAditivoTipoAnexo == ETipoAnexo.TermoAditivo)
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .ToList();

            foreach (var licitacao in licitacoes)
            {
                licitacao.ContratacaoAditivadas = _db.ContratacaoAditivadas
                                                     .AsNoTracking()
                                                     .Where(ca => ca.NumeroProcesso == licitacao.NumeroProcessoAdministrativo)
                                                     .ToList();

                licitacao.ArquivosTermosAditivos = _db.Licitacoes
                                                      .AsNoTracking()
                                                      .Include("Arquivos")
                                                      .Where(l => l.NumeroProcessoAdministrativo == licitacao.NumeroProcessoAdministrativo)
                                                      .FirstOrDefault()
                                                      .Arquivos.Where(a => a.TipoAnexo == ETipoAnexo.TermoAditivo && a.DataRemocao == null).OrderBy(a => a.DataUpload)
                                                      .ToList();
            }

            return licitacoes;
        }

        public IndexDadosLicitacaoViewModel GetDataToLoadAditivadosView()
        {
            var licitacoes = GetAditivados();

            var indexDadosLicitacaoViewModel = DadosLicitacaoFactory.CreateIndexDadosLicitacaoViewModel(licitacoes);

            return indexDadosLicitacaoViewModel;
        }

        public List<DadosLicitacao> FilterAditivados(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .Where(licitacao =>
                                                      (licitacao.TermoAditivoTipoAnexo == ETipoAnexo.TermoAditivo)
                                                   && (string.IsNullOrEmpty(filtro.NumeroProcessoAdministrativo) || licitacao.NumeroProcessoAdministrativo.Contains(filtro.NumeroProcessoAdministrativo))
                                                   && (string.IsNullOrEmpty(filtro.CNPJ) || licitacao.CNPJ.Contains(filtro.CNPJ))
                                                   && (string.IsNullOrEmpty(filtro.RazaoSocial) || licitacao.RazaoSocial.Contains(filtro.RazaoSocial))
                                                   && (string.IsNullOrEmpty(filtro.Objeto) || licitacao.Objeto.Contains(filtro.Objeto))
                                               )
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .Take(filtro.QuantidadeResultados)
                                               .ToList();

            foreach (var licitacao in licitacoes)
            {
                licitacao.ContratacaoAditivadas = _db.ContratacaoAditivadas
                                                     .AsNoTracking()
                                                     .Where(ca => ca.NumeroProcesso == licitacao.NumeroProcessoAdministrativo)
                                                     .ToList();

                licitacao.ArquivosTermosAditivos = _db.Licitacoes
                                                     .AsNoTracking()
                                                     .Include("Arquivos")
                                                     .Where(l => l.NumeroProcessoAdministrativo == licitacao.NumeroProcessoAdministrativo)
                                                     .FirstOrDefault()
                                                     .Arquivos.Where(a => a.TipoAnexo == ETipoAnexo.TermoAditivo && a.DataRemocao == null).OrderBy(a => a.DataUpload)
                                                     .ToList();
            }

            return licitacoes;
        }

        #endregion

        #region pendentes

        public List<DadosLicitacao> GetPendentes()
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .Where(dl => dl.Status == Enum.EStatusItemLicitacao.EntregaParcial || dl.Status == Enum.EStatusItemLicitacao.EmAtraso || dl.Status == Enum.EStatusItemLicitacao.AtrasoNotificado)
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .Take(50)
                                               .ToList();

            return licitacoes;
        }

        public IndexDadosLicitacaoViewModel GetDataToLoadPendentesView()
        {
            var licitacoes = GetPendentes();

            var indexDadosLicitacaoViewModel = DadosLicitacaoFactory.CreateIndexDadosLicitacaoViewModel(licitacoes);

            return indexDadosLicitacaoViewModel;
        }

        public List<DadosLicitacao> FilterPendentes(FiltroDadosLicitacaoViewModel filtro)
        {
            var licitacoes = _db.DadosLicitacao.AsNoTracking()
                                               .Where(licitacao =>
                                                      (licitacao.Status == Enum.EStatusItemLicitacao.EntregaParcial || licitacao.Status == Enum.EStatusItemLicitacao.EmAtraso || licitacao.Status == Enum.EStatusItemLicitacao.AtrasoNotificado)
                                                   && (string.IsNullOrEmpty(filtro.NumeroProcessoAdministrativo) || licitacao.NumeroProcessoAdministrativo.Contains(filtro.NumeroProcessoAdministrativo))
                                                   && (string.IsNullOrEmpty(filtro.CNPJ) || licitacao.CNPJ.Contains(filtro.CNPJ))
                                                   && (string.IsNullOrEmpty(filtro.RazaoSocial) || licitacao.RazaoSocial.Contains(filtro.RazaoSocial))
                                                   && (string.IsNullOrEmpty(filtro.Objeto) || licitacao.Objeto.Contains(filtro.Objeto))
                                               )
                                               .OrderBy(licitacao => licitacao.NumeroProcessoAdministrativo)
                                               .Take(filtro.QuantidadeResultados)
                                               .ToList();

            return licitacoes;
        }
        #endregion
    }
}
