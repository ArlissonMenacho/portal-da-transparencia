﻿using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class TbPLLModalidadeLicitacaoDAL
    {

        public DbTransparencia dbTransparencia;

        public TbPLLModalidadeLicitacaoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public IEnumerable<ModalidadeLicitacao> GetModalidades()
        {
            return dbTransparencia.ModalidadeLicitacao.ToList();
        }
    }
}
