﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class DespesaDAL : IDisposable
    {
        private readonly DbTransparencia _dbTransparencia;

        public DespesaDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        private IQueryable<DespesaPortal> GetDespesasBase(int? pCodUnidadeGestora, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            var ano = DateTime.Now.Year;
            var codUnidadeGestora = (pCodUnidadeGestora ?? 0).ToString("000000");
            var nomeCredor = String.IsNullOrWhiteSpace(pNomeCredor) ? "" : pNomeCredor.Trim();
            var docCredor = String.IsNullOrEmpty(pDocCredor) ? "" : pDocCredor.Trim();

            return _dbTransparencia.DespesasPortal
                .Where(d => (
                    (docCredor == "" || d.DocCredor == docCredor)
                    && ((pCodUnidadeGestora ?? 0) == 0 || d.UG == codUnidadeGestora)
                    && ((pNumeroEmpenho ?? "") == "" || (d.NumEmpenho.Contains(pNumeroEmpenho) || d.Documento.Contains(pNumeroEmpenho)))
                    && (nomeCredor == "" || d.Credor.Contains(nomeCredor))
                ));
        }

        private IQueryable<DespesaPortal> GetDespesasBase(List<string> pCodUnidadesGestoras, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            var ano = DateTime.Now.Year;
            var nomeCredor = String.IsNullOrWhiteSpace(pNomeCredor) ? "" : pNomeCredor.Trim();
            var docCredor = String.IsNullOrEmpty(pDocCredor) ? "" : pDocCredor.Trim();

            return _dbTransparencia.DespesasPortal
                .Where(d => (
                    (docCredor == "" || d.DocCredor == docCredor)
                    && (pCodUnidadesGestoras.Any(ug => ug.Equals(d.UG)))
                    && ((pNumeroEmpenho ?? "") == "" || (d.NumEmpenho.Contains(pNumeroEmpenho) || d.Documento.Contains(pNumeroEmpenho)))
                    && (nomeCredor == "" || d.Credor.Contains(nomeCredor))
                ));
        }

        public List<DespesaPortal> GetComprasMaterial(int? pCodUnidadeGestora, DateTime dataInicial, DateTime dataFinal)
        {
            var codUnidadeGestora = (pCodUnidadeGestora ?? 0).ToString("000000");
            return _dbTransparencia.DespesasPortal
                .Where(d => ((pCodUnidadeGestora ?? 0) == 0 || d.UG == codUnidadeGestora)
                && (d.DataDocumento >= dataInicial && d.DataDocumento <= dataFinal)
                && (d.EspecificacaoDespesa == 33903000 || d.EspecificacaoDespesa == 33503000 || d.EspecificacaoDespesa == 44903000 || d.EspecificacaoDespesa == 44905200)
                ).ToList();
        }

        private IQueryable<DespesaPortal> GetDespesasBase(int? pCodUnidadeGestora, string pNumeroEmpenho, string pNomeCredor, MonthYear pMesAno, string pDocCredor)
        {
            var ano = DateTime.Now.Year;
            var dataInicial = pMesAno.FirstDay();
            var dataFinal = pMesAno.LastSecond();
            return GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pDocCredor)
                .Where(d => d.DataDocumento >= dataInicial && d.DataDocumento <= dataFinal);
        }

        public List<DespesaPorAnoModel> GetDespesasPorAno(int? pCodUnidadeGestora, string pNumeroEmpenho, string pNomeCredor, string pDocCredor, bool pRestringirAno = false)
        {
            var ano = DateTime.Now.Year;
            var dataPadraoInicial = new DateTime(2014, 1, 1);

            if (pCodUnidadeGestora.Equals(110031) || pCodUnidadeGestora.Equals(130031) || pCodUnidadeGestora.Equals(110033))
            {
                var consulta = (from d in GetDespesasBase(new List<string>() { "110031", "130031", "110033" }, pNumeroEmpenho, pNomeCredor, pDocCredor)
                                where d.DataDocumento >= dataPadraoInicial || pRestringirAno == false
                                group d by new { d.DataDocumento.Year, d.UG } into grp
                                orderby grp.Key
                                select new
                                {
                                    Exercicio = grp.Key.Year,
                                    CodUnidadeGestora = grp.Key.UG,
                                    NumeroEmpenho = pNumeroEmpenho,
                                    ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                                    ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                                    ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                                    NomeCredor = pNomeCredor,
                                    DocCredor = pDocCredor
                                });

                var despesas = new List<DespesaPorAnoModel>();

                foreach (var c in consulta)
                {
                    int? codUnidade = Convert.ToInt32(c.CodUnidadeGestora);

                    despesas.Add(new DespesaPorAnoModel
                    {
                        Exercicio = c.Exercicio,
                        CodUnidadeGestora = codUnidade,
                        NumeroEmpenho = c.NumeroEmpenho,
                        ValorEmpenhada = c.ValorEmpenhada,
                        ValorLiquidada = c.ValorLiquidada,
                        ValorPaga = c.ValorPaga,
                        NomeCredor = c.NomeCredor,
                        DocCredor = c.DocCredor
                    });
                }

                return despesas;
            }
            else
                return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pDocCredor)
                        where d.DataDocumento >= dataPadraoInicial || pRestringirAno == false
                        group d by d.DataDocumento.Year into grp
                        orderby grp.Key
                        select new DespesaPorAnoModel
                        {
                            Exercicio = grp.Key,
                            CodUnidadeGestora = pCodUnidadeGestora,
                            NumeroEmpenho = pNumeroEmpenho,
                            ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                            ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                            ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                            NomeCredor = pNomeCredor,
                            DocCredor = pDocCredor
                        }).ToList();
        }

        public List<DespesaPortal> Api(short? ano, short? mes, string empenho, string credor, string cnpj, string ug)
        {
            return _dbTransparencia.DespesasPortal
                   .Where(d => (ano == null || d.Exercicio == ano.Value)
                        && (mes == null || d.DataDocumento.Month == mes)
                        && (empenho == "" || empenho == null || d.Documento == empenho) &&
                   (credor == "" || credor == null || d.Credor.Contains(credor)) &&
                   (cnpj == "" || cnpj == null || d.DocCredor == cnpj) &&
                   (ug == "" || ug == null || d.UG == ug))
                   .Take(1000).ToList();
        }

        public List<DespesaPorMesModel> GetDespesasMeses(int? pCodUnidadeGestora, int pAno, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            var ano = DateTime.Now.Year;
            return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pDocCredor)
                    where d.DataDocumento >= new DateTime(pAno, 1, 1) && d.DataDocumento <= new DateTime(pAno, 12, 31, 23, 59, 59)
                    group d by d.DataDocumento.Month into grp
                    orderby grp.Key
                    select new DespesaPorMesModel
                    {
                        Ano = new DespesaPorAnoModel { Exercicio = pAno, CodUnidadeGestora = pCodUnidadeGestora, NumeroEmpenho = pNumeroEmpenho, NomeCredor = pNomeCredor, DocCredor = pDocCredor },
                        Mes = grp.Key,
                        ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                        ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                        ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                    }).ToList();
        }

        public List<DespesaProgramaAcaoModel> GetDespesasProgramaAcao(MonthYear pMesAno, int? pCodUnidadeGestora, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pMesAno, pDocCredor)
                    group d by new { d.CodPrograma, d.Programa, d.CodAcao, d.Acao, d.CodProjeto } into grp
                    orderby grp.Key
                    select new DespesaProgramaAcaoModel
                    {
                        Mes = new DespesaPorMesModel
                        {
                            Mes = pMesAno.Month,
                            Ano = new DespesaPorAnoModel
                            {
                                Exercicio = pMesAno.Year,
                                CodUnidadeGestora = pCodUnidadeGestora,
                                NumeroEmpenho = pNumeroEmpenho,
                                NomeCredor = pNomeCredor,
                                DocCredor = pDocCredor
                            }
                        },
                        Acao = grp.Key.Acao,
                        CodAcao = grp.Key.CodAcao.Value,
                        CodPrograma = grp.Key.CodPrograma.Value,
                        CodProjeto = grp.Key.CodProjeto,
                        Programa = grp.Key.Programa,
                        ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                        ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                        ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                    }).ToList();
        }

        public List<DespesaFuncaoModel> GetDespesasFuncoes(MonthYear pMesAno, int? pCodUnidadeGestora, int pCodPrograma, int pCodAcao, int pCodProjeto, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pMesAno, pDocCredor)
                    where d.CodPrograma == pCodPrograma && d.CodAcao == pCodAcao && d.CodProjeto == pCodProjeto
                    group d by new { d.CodFuncao, d.NomFuncao, d.DescricaoSubFuncao, d.CodSubFuncao, d.Fonte, d.DesFonteRecurso } into grp
                    orderby new { grp.Key.CodFuncao, grp.Key.CodSubFuncao }
                    select new DespesaFuncaoModel
                    {
                        ProgramaAcao = new DespesaProgramaAcaoModel
                        {
                            CodAcao = pCodAcao,
                            CodPrograma = pCodPrograma,
                            CodProjeto = pCodProjeto,
                            Mes = new DespesaPorMesModel
                            {
                                Mes = pMesAno.Month,
                                Ano = new DespesaPorAnoModel
                                {
                                    Exercicio = pMesAno.Year,
                                    CodUnidadeGestora = pCodUnidadeGestora,
                                    NumeroEmpenho = pNumeroEmpenho,
                                    NomeCredor = pNomeCredor,
                                    DocCredor = pDocCredor
                                }
                            }
                        },
                        CodFuncao = grp.Key.CodFuncao.Value,
                        DescricaoFuncao = grp.Key.NomFuncao,
                        DescricaoSubFuncao = grp.Key.DescricaoSubFuncao,
                        CodFonte = grp.Key.Fonte.Value,
                        DescFonteRecursos = grp.Key.DesFonteRecurso,
                        CodSubFuncao = grp.Key.CodSubFuncao.Value,
                        ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                        ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                        ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                    }).ToList();
        }

        public List<DespesaNaturezaModel> GetDespesasNaturezas(MonthYear pMesAno, int? pCodUnidadeGestora, int pCodPrograma, int pCodAcao, int pCodProjeto, int pCodFonteRecursos, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pMesAno, pDocCredor)
                    where d.CodPrograma == pCodPrograma && d.CodAcao == pCodAcao && d.CodProjeto == pCodProjeto
                    && d.Fonte == pCodFonteRecursos
                    group d by new { d.EspecificacaoDespesa, d.NomDespesa } into grp
                    select new DespesaNaturezaModel
                    {
                        Funcao = new DespesaFuncaoModel
                        {
                            CodFonte = pCodFonteRecursos,
                            ProgramaAcao = new DespesaProgramaAcaoModel
                            {
                                CodAcao = pCodAcao,
                                CodPrograma = pCodPrograma,
                                CodProjeto = pCodProjeto,
                                Mes = new DespesaPorMesModel
                                {
                                    Mes = pMesAno.Month,
                                    Ano = new DespesaPorAnoModel
                                    {
                                        Exercicio = pMesAno.Year,
                                        CodUnidadeGestora = pCodUnidadeGestora,
                                        NumeroEmpenho = pNumeroEmpenho,
                                        NomeCredor = pNomeCredor,
                                        DocCredor = pDocCredor
                                    }
                                }
                            }
                        },
                        CodNaturezaDespesa = grp.Key.EspecificacaoDespesa.Value,
                        NaturezaDaDespesa =
                            grp.Key.EspecificacaoDespesa.ToString().Substring(0, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(1, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(2, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(3, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(4, 1) + " . " +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(5, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(6, 1) + "." +
                            grp.Key.EspecificacaoDespesa.ToString().Substring(7, 1) + " - " +
                            grp.Key.NomDespesa,
                        ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                        ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                        ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                    }).ToList();
        }

        public List<DespesaEmpenhoModel> GetDespesasEmpenhos(MonthYear pMesAno, int? pCodUnidadeGestora, int pCodPrograma, int pCodAcao, int pCodProjeto, int pCodFonteRecursos, long pCodEspecificacaoDespesa, string pNumeroEmpenho, string pNomeCredor, string pDocCredor)
        {
            return (from d in GetDespesasBase(pCodUnidadeGestora, pNumeroEmpenho, pNomeCredor, pMesAno, pDocCredor)
                    where d.CodPrograma == pCodPrograma && d.CodAcao == pCodAcao && d.CodProjeto == pCodProjeto
                    && d.Fonte == pCodFonteRecursos
                    && d.EspecificacaoDespesa == pCodEspecificacaoDespesa
                    group d by new { d.Credor, d.NomOrgao, d.UG, d.NumEmpenho, d.DataDocumento } into grp
                    select new DespesaEmpenhoModel
                    {
                        Natureza = new DespesaNaturezaModel
                        {
                            CodNaturezaDespesa = pCodEspecificacaoDespesa,
                            Funcao = new DespesaFuncaoModel
                            {
                                CodFonte = pCodFonteRecursos,
                                ProgramaAcao = new DespesaProgramaAcaoModel
                                {
                                    CodAcao = pCodAcao,
                                    CodPrograma = pCodPrograma,
                                    CodProjeto = pCodProjeto,
                                    Mes = new DespesaPorMesModel
                                    {
                                        Mes = pMesAno.Month,
                                        Ano = new DespesaPorAnoModel
                                        {
                                            Exercicio = pMesAno.Year,
                                            CodUnidadeGestora = pCodUnidadeGestora,
                                            NumeroEmpenho = pNumeroEmpenho,
                                            NomeCredor = pNomeCredor,
                                            DocCredor = pDocCredor,
                                        }
                                    }
                                }
                            }
                        },
                        Credor = grp.Key.Credor.Substring(0, 2) == "PF" ? grp.Key.NomOrgao : grp.Key.Credor ?? grp.Key.NomOrgao,
                        NumeroEmpenho = grp.Key.NumEmpenho,
                        UnidadeGestora = grp.Key.UG,
                        DataDocumento = grp.Key.DataDocumento,
                        ValorEmpenhada = grp.Sum(d => d.ValorEmpenhada.Value),
                        ValorLiquidada = grp.Sum(d => d.ValorLiquidada.Value),
                        ValorPaga = grp.Sum(d => d.ValorPaga.Value),
                        IsLinkEmpenho = grp.Sum(d => d.ValorEmpenhada) == 0 ? null : grp.Sum(d => d.ValorEmpenhada).ToString(),
                        IsLinkItem = grp.Sum(d => d.ValorPaga) == 0 ? null : grp.Sum(d => d.ValorPaga).ToString()
                    }
                 ).ToList();
        }

        public List<DespesaPortal> ApiIntervaloAno(short? anoIni, short? anoFim)
        {
            return _dbTransparencia.DespesasPortal
                   .Where(d => (anoIni == null || d.Exercicio >= anoIni) && (anoFim == null || d.Exercicio <= anoFim))
                   .Take(1000)
                   .ToList();
        }

        public List<VW_DespesasDiretas> GetDespesasDiretas()
        {
            var despesasDiretas = _dbTransparencia.VW_DespesasDiretas.AsNoTracking().ToList();

            return despesasDiretas;
        }

        public void Dispose()
        {
            _dbTransparencia.Dispose();
        }
    }
}
