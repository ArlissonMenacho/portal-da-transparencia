﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using TransparenciaRO.Infra.Model.DB;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class PastasDAL
    {
        public DbTransparencia DbTransparencia;

        public PastasDAL(DbTransparencia dbTransparencia)
        {
            this.DbTransparencia = dbTransparencia;
        }

        public void Adicionar(Pasta pasta)
        {
            DbTransparencia.Pastas.Add(pasta);
            DbTransparencia.SaveChanges();
        }

        public ICollection<Pasta> BuscarPastas()
        {
            ICollection<Pasta> pastas = new List<Pasta>();
            foreach (var pasta in DbTransparencia.Pastas.Where(p => p.PastaOrigemId == null && p.DateRemocao == null))
            {
                pastas.Add(pasta);
            }

            return pastas;
        }

        public Pasta BuscarPasta(Guid pastaGuid)
        {
            Pasta pasta = DbTransparencia.Pastas.Find(pastaGuid);
            return pasta;
        }

        public ICollection<Pasta> BuscarSubPastas(Guid pastaGuid)
        {
            ICollection<Pasta> pastas = new List<Pasta>();
            foreach (var pasta in DbTransparencia.Pastas.Include(p => p.SubPastas).Include(p => p.Arquivos).Where(p => p.PastaOrigemId == pastaGuid && p.DateRemocao == null))
            {
                pastas.Add(pasta);
            }

            return pastas;
        }

        public ICollection<Arquivo> BuscarArquivosPorPasta(Guid pastaGuid)
        {
            ICollection<Arquivo> arquivos = new List<Arquivo>();
            foreach (var arquivo in DbTransparencia.Arquivos.Where(arquivo => arquivo.PastaId == pastaGuid && arquivo.DataRemocao == null))
            {
                arquivos.Add(arquivo);
            }

            return arquivos;
        }

        public Pasta BuscarPastaComSubPastasEArquivos(Guid pastaGuid)
        {
            Pasta pasta = new Pasta
            {
                SubPastas = new List<Pasta>(),
                Arquivos = new List<Arquivo>()
            };

            pasta = BuscarPasta(pastaGuid);
            pasta.SubPastas = BuscarSubPastas(pastaGuid);
            pasta.Arquivos = BuscarArquivosPorPasta(pastaGuid);

            return pasta;
        }

        public void Editar(Pasta pasta)
        {
            DbTransparencia.Entry(pasta).State = EntityState.Modified;
            DbTransparencia.SaveChanges();
        }

        public bool Remover(Guid pastaGuid)
        {
            Pasta pasta = BuscarPasta(pastaGuid);
            pasta.DateRemocao = DateTime.Now;
            var pastasASeremExcluidas = new List<Guid>() { pastaGuid };
            foreach (var subPasta in BuscarSubPastas(pastaGuid))
            {
                if (subPasta.Arquivos.Count > 0)
                    return false;

                pastasASeremExcluidas.Add(subPasta.PastaId);
                subPasta.SubPastas = BuscarSubPastas(subPasta.PastaId);

                if (!Remover(subPasta.PastaId)) return false;
            }

            DbTransparencia.Pastas.Where(p => pastasASeremExcluidas.Contains(p.PastaId)).ToList()
                .ForEach(p => p.DateRemocao = DateTime.Now);

            DbTransparencia.SaveChanges();

            return true;
        }
    }
}
