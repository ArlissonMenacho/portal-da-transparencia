﻿using System.Collections.Generic;
using System.Linq;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class EmpresaRemuneracaoDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public EmpresaRemuneracaoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public void VerficarRemoverDados(int ano, int mes, EEmpresa eEmpresa)
        {
            if (_dbTransparencia.EmpresaRemuneracao.Any(x => x.Ano == ano && x.Mes == mes && x.EEmpresa == eEmpresa))
            {
                _dbTransparencia.EmpresaRemuneracao.Where(x => x.Ano == ano && x.Mes == mes && x.EEmpresa == eEmpresa).Delete();
            }
        }

        public void AdicionarDados(List<EmpresaRemuneracao> remuneracoesEmpresa)
        {
            _dbTransparencia.BulkInsert(remuneracoesEmpresa);
            _dbTransparencia.BulkSaveChanges();
        }
        public int AnoMax(EEmpresa empresa)
        {
            return _dbTransparencia.EmpresaRemuneracao
                       .Where(r => r.EEmpresa == empresa)
                       .Max(r => (int?)r.Ano) ?? 0;
        }
        public int AnoMin(EEmpresa empresa)
        {
            return _dbTransparencia.EmpresaRemuneracao
                .Where(r => r.EEmpresa == empresa)
                .Min(r => (int?)r.Ano) ?? 0;
        }

        public List<int> Meses(int ano, EEmpresa empresa)
        {
            var meses = _dbTransparencia.EmpresaRemuneracao
                .Where(r => r.Ano == ano && r.EEmpresa == empresa)
                .GroupBy(r => r.Mes)
                .Select(r => new { MesInt = r.Key })
                .ToList();

            return meses
                .Select(r => r.MesInt)
                .ToList();
        }

        public Dictionary<string, int> RelacaoCargo(int ano, int mes, EEmpresa empresa, string tipoCargo)
        {
            var relacaoServidorEmpresa = _dbTransparencia.EmpresaRemuneracao
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa && (tipoCargo == "" || re.TipoCargo == tipoCargo))
                .GroupBy(re => re.Cargo)
                .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> RelacaoDepartamento(int ano, int mes, EEmpresa empresa)
        {
            var relacaoServidorEmpresa = _dbTransparencia.EmpresaRemuneracao
                 .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                 .GroupBy(re => re.Departamento)
                 .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> FaixasSalariais(int ano, int mes, EEmpresa empresa)
        {
            return _dbTransparencia.EmpresaRemuneracao
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                .Select(re => re.Salario)
                .ToList()
                .GroupBy(new FaixasSalariaisUtils().FaixaSalarialDescricao)
                .ToDictionary(k => k.Key, v => v.Count());
        }

        public Dictionary<string, int> RelacaoTipoCargo(int ano, int mes, EEmpresa empresa)
        {
            var relacaoTipoCargo = _dbTransparencia.EmpresaRemuneracao
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                .ToList()
                .GroupBy(re => re.TipoCargo);

            return relacaoTipoCargo
                .ToDictionary(k => k.Key, v => v.Count());
        }

        public IList<EmpresaRemuneracao> Funcionarios(int ano, int mes, EEmpresa empresa, string cargo, string tipoCargo, string departamento, string faixaSalarial)
        {
            var valoresSalarias = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);
            var funcionario = _dbTransparencia.EmpresaRemuneracao
                .Where(re => re.Ano == ano &&
                    re.Mes == mes &&
                    re.EEmpresa == empresa &&
                    (cargo == "" || re.Cargo == cargo) &&
                    (tipoCargo == "" || re.TipoCargo == tipoCargo) &&
                    (departamento == "" || re.Departamento == departamento) &&
                    (faixaSalarial == "" || (re.Salario > valoresSalarias.Item1 && re.Salario <= valoresSalarias.Item2))
                    )
                .ToList();

            return funcionario;
        }

        public EmpresaRemuneracao RemuneracaoServidor(int ano, int mes, string matricula)
        {
            return _dbTransparencia.EmpresaRemuneracao.FirstOrDefault(er => er.Ano == ano && er.Mes == mes && er.Matricula == matricula);
        }

    }
}