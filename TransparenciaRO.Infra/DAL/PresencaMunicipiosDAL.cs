﻿using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Utils;
using System.Collections.Generic;
using System.Linq;

namespace TransparenciaRO.Infra.DAL
{
    public class PresencaMunicipiosDAL
    {
        public DbTransparencia DbTransparencia;

        public PresencaMunicipiosDAL(DbTransparencia dbTransparencia)
        {
            DbTransparencia = dbTransparencia;
        }

        public IEnumerable<PresencaMunicipiosModel> GetPresencaMunicipiosPorExercicio(int pExercicio)
        {

            var retorno = DbTransparencia
                .EmpenhosMunicipios
                .AsNoTracking()
                .Where(emp => emp.EXERCICIO == pExercicio)
                .ToList()
                .GroupBy(emp => new { emp.CodigoMunicipio, emp.NomeMunicipio, })
                .Select(emp => new PresencaMunicipiosModel
                {
                    Empenhado = emp.Sum(e => e.ValorEmpenhado ?? 0),
                    EmpenhadoALiquidar = emp.Sum(e => e.ValorEmpenhadoALiquidar ?? 0),
                    EmpenhadoAPagar = emp.Sum(e => e.ValorEmpenhadoAPagar ?? 0),
                    Municipio = emp.Key.NomeMunicipio,
                    Liquidado = emp.Sum(e => e.ValorLiquidado ?? 0),
                    LiquidadoAPagar = emp.Sum(e => e.ValorLiquidadoAPagar ?? 0),
                    Pago = emp.Sum(e => e.ValorPago ?? 0),
                    Exercicio = pExercicio,
                    pEncCodMunicipio = emp.Key.CodigoMunicipio.ToString().ToEncrypted()
                }).OrderBy(emp => emp.Municipio);
            return retorno;
        }

        public IEnumerable<PresencaMunicipiosPorFuncao> GetPresencaMunicipiosPorFuncao(int pExercicio, int pCodMunicipio)
        {
            return DbTransparencia
                .EmpenhosMunicipios
                .AsNoTracking()
                .Where(emp => emp.EXERCICIO == pExercicio && emp.CodigoMunicipio == pCodMunicipio)
                .Select(emp => new PresencaMunicipiosPorFuncao
                {
                    Empenhado = emp.ValorEmpenhado ?? 0,
                    EmpenhadoALiquidar = emp.ValorEmpenhadoALiquidar ?? 0,
                    EmpenhadoAPagar = emp.ValorEmpenhadoAPagar ?? 0,
                    Investimento = emp.NomFuncao,
                    Liquidado = emp.ValorLiquidado ?? 0,
                    LiquidadoAPagar = emp.ValorLiquidadoAPagar ?? 0,
                    Pago = emp.ValorPago ?? 0
                });
        }
    }
}
