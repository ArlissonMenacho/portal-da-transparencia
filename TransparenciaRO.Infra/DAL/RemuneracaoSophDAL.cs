﻿using System.Collections.Generic;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.Linq;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class RemuneracaoSophDAL
    {
        private DbTransparencia _dbTransparencia;

        public RemuneracaoSophDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }
        
        public int AnoMax(EEmpresa empresa)
        {
            return _dbTransparencia.RemuneracaoSoph
                       .Where(r => r.EEmpresa == empresa)
                       .Max(r => (int?)r.Ano) ?? 0;
        }

        public int AnoMin(EEmpresa empresa)
        {
            return _dbTransparencia.RemuneracaoSoph
                       .Where(r => r.EEmpresa == empresa)
                       .Min(r => (int?)r.Ano) ?? 0;
        }

        public List<int> Meses(int ano, EEmpresa empresa)
        {
            var meses = _dbTransparencia.RemuneracaoSoph
                .Where(r => r.Ano == ano && r.EEmpresa == empresa)
                .GroupBy(r => r.Mes)
                .Select(r => new { MesInt = r.Key })
                .ToList();

            return meses
                .Select(r => r.MesInt)
                .ToList();
        }

        public IList<RemuneracaoSoph> Funcionarios(int ano, int mes, EEmpresa empresa, string cargo, string departamento, string faixaSalarial)
        {
            var valoresSalarias = new FaixasSalariaisUtils().FaixaSalarialValor(faixaSalarial);
            var funcionario = _dbTransparencia.RemuneracaoSoph
                .Where(re => re.Ano == ano &&
                             re.Mes == mes &&
                             re.EEmpresa == empresa &&
                             (cargo == "" || re.Cargo == cargo) &&
                             (departamento == "" || re.Departamento == departamento) &&
                             (faixaSalarial == "" || (re.TotalProventos > valoresSalarias.Item1 && re.TotalProventos <= valoresSalarias.Item2))
                )
                .ToList();
            return funcionario;
        }

        public Dictionary<string, int> FaixasSalariais(int ano, int mes, EEmpresa empresa)
        {
            return _dbTransparencia.RemuneracaoSoph
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                .Select(re => re.TotalProventos)
                .ToList()
                .GroupBy(new FaixasSalariaisUtils().FaixaSalarialDescricao)
                .ToDictionary(k => k.Key, v => v.Count());
        }
        public Dictionary<string, int> RelacaoCargo(int ano, int mes, EEmpresa empresa, string tipoCargo)
        {
            var relacaoServidorEmpresa = _dbTransparencia.RemuneracaoSoph
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                .GroupBy(re => re.Cargo)
                .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }

        public Dictionary<string, int> RelacaoDepartamento(int ano, int mes, EEmpresa empresa)
        {
            var relacaoServidorEmpresa = _dbTransparencia.RemuneracaoSoph
                .Where(re => re.Ano == ano && re.Mes == mes && re.EEmpresa == empresa)
                .GroupBy(re => re.Departamento)
                .Select(re => new { re.Key, Count = re.Count() });

            return relacaoServidorEmpresa
                .ToDictionary(k => k.Key, v => v.Count);
        }
    }
}