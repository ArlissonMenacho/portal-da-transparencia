﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class ViewEmpenhosDal
    {
        public readonly DbTransparencia dbTransparencia;

        public ViewEmpenhosDal(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        //Método deixou de ser utilizado em 28/03/2019 - Ancelmo Luiz
        public IEnumerable<Empenho> GetEmpenhos(int? pModalidadeCompra, List<String> pLstUnidadesGestoras, DateTime pDataInicial, DateTime pDataFinal, string pEmpenho)
        {
            return dbTransparencia.Empenhos.Where(emp => emp.DataEmpenho >= pDataInicial && emp.DataEmpenho <= pDataFinal
            && (pModalidadeCompra == null || emp.CodModalidadeCompra == pModalidadeCompra)
            && (pEmpenho == "" || emp.NumeroEmpenho == pEmpenho)
            && (pLstUnidadesGestoras.Count == 0 || pLstUnidadesGestoras.Contains(emp.CodUnidadeGestora.ToString())));
        }

        public IEnumerable<Empenho> GetEmpenhos(int? pModalidadeCompra, List<String> pLstUnidadesGestoras, string pEmpenho)
        {
            return dbTransparencia.Empenhos.Where(emp => (pModalidadeCompra == null || emp.CodModalidadeCompra == pModalidadeCompra)
            && (pEmpenho == "" || emp.NumeroEmpenho == pEmpenho)
            && (pLstUnidadesGestoras.Count == 0 || pLstUnidadesGestoras.Contains(emp.CodUnidadeGestora.ToString())));
        }

        public IEnumerable<ComprasContratacoes> GetComprasContratacoes(int pModalidadeCompra, string codUG, string pDataInicial, string pDataFinal, string pEmpenho)
        {
            var dtInicial = (string.IsNullOrEmpty(pDataInicial) ? (DateTime?)null : Convert.ToDateTime(pDataInicial).Date);
            var dtFinal = (string.IsNullOrEmpty(pDataFinal) ? (DateTime?)null : Convert.ToDateTime(pDataFinal).Date);
            int codMod = pModalidadeCompra == 0 ? 0 : pModalidadeCompra;

            return dbTransparencia.ComprasContratacoess
                .Where(emp => (dtInicial == null || emp.DataDocumento >= dtInicial)
                              && (dtFinal == null || emp.DataDocumento <= dtFinal)
                              && emp.CodModalidadeCompra == codMod
                              && emp.UG == codUG)
                .Distinct()
                .OrderByDescending(emp => emp.DataDocumento)
                .Take(100);
        }

        public IEnumerable<ComprasContratacoesEmergenciaisCovid19> GetComprasContratacoesCOVID19(string codUG, string pDataInicial, string pDataFinal, string credor)
        {
            var dtInicial = (string.IsNullOrEmpty(pDataInicial) ? (DateTime?)null : Convert.ToDateTime(pDataInicial).Date);
            var dtFinal = (string.IsNullOrEmpty(pDataFinal) ? (DateTime?)null : Convert.ToDateTime(pDataFinal).Date);

            return dbTransparencia.ComprasContratacoesEmergenciaisCovid19.AsNoTracking()
                                  .Where(ccec => (codUG == "0" || ccec.UG == codUG) &&
                                                 (dtInicial == null || ccec.DataDocumento >= dtInicial) &&
                                                 (dtFinal == null || ccec.DataDocumento <= dtFinal) &&
                                                 (string.IsNullOrEmpty(credor) || ccec.Credor.Contains(credor)))
                                  .Distinct()
                                  .OrderByDescending(cc => cc.DataDocumento)
                                  .ToList();
        }

        public IEnumerable<ComprasContratacoesEmergenciaisCovid19> GetComprasContratacoesCOVID19ParaAPI()
        {
            var consulta = (from ccec in dbTransparencia.ComprasContratacoesEmergenciaisCovid19.AsNoTracking()
                            join o in dbTransparencia.Orgaos on ccec.UG equals o.CodOrgao.ToString()
                            select new { compra = ccec, NomOrgao = o.NomOrgao })
                            .Distinct()
                            .OrderByDescending(c => c.compra.DataDocumento);

            var compras = new List<ComprasContratacoesEmergenciaisCovid19>();

            foreach (var c in consulta)
            {
                c.compra.UnidadeGestora = c.NomOrgao;

                compras.Add(c.compra);
            }

            return compras;
        }

        public DetalheEmpenho GetDetalhesEmpenho(string numeroEmpenho, string unidadeGestora)
        {
            var ug = Convert.ToInt32(unidadeGestora);
            return dbTransparencia.DetalheEmpenhos.FirstOrDefault(emp => emp.DocumentoNE == numeroEmpenho && emp.UG == ug);
        }

        public dynamic GetEmpenhosAPI(int? pModalidadeCompra, List<String> pLstUnidadesGestoras, DateTime pDataInicial, DateTime pDataFinal, string pEmpenho, string cnpj, int pagina, int registrosPorPagina)
        {
            var dados = dbTransparencia.Empenhos_API.AsNoTracking()
                                                    .Distinct()
                                                    .Where(emp => (emp.DataEmpenho >= pDataInicial && emp.DataEmpenho <= pDataFinal)
                                                    && (cnpj == null || emp.DocCredor == cnpj)
                                                    && (pModalidadeCompra == null || emp.CodModalidadeCompra == pModalidadeCompra)
                                                    && (pEmpenho == "" || emp.NumeroEmpenho == pEmpenho)
                                                    && (pLstUnidadesGestoras.Count == 0 || pLstUnidadesGestoras.Contains(emp.CodUnidadeGestora.ToString())))
                                                    .OrderBy(d => d.DataEmpenho).ToList();

            int quantidadeRegistros = dados.Count();

            int totalPaginas = (int)Math.Ceiling(quantidadeRegistros / Convert.ToDecimal(registrosPorPagina));

            var registrosPaginados = dados.Skip(registrosPorPagina * ((int)pagina - 1))
                                          .Take(registrosPorPagina)
                                          .Select(d => new
                                          {
                                              d.Credor,
                                              d.DocCredor,
                                              DataArquivo = d.DataArquivo.Value.ToString("dd/MM/yyyy"),
                                              DataEmpenho = d.DataEmpenho.Value.ToString("dd/MM/yyyy"),
                                              d.Descricao,
                                              d.Exercicio,
                                              d.Item,
                                              d.ModalidadeCompra,
                                              d.NumeroEmpenho,
                                              d.NumeroProcesso,
                                              d.Processo,
                                              d.Quantidade,
                                              d.TipoEmpenho,
                                              d.UnidadeGestora,
                                              d.UnidadeMedida,
                                              ValorItem = d.ValorItem?.ToString("c2")
                                          });

            var retorno = new
            {
                meta = new
                {
                    paginaAtual = (int)pagina,
                    totalPaginas = totalPaginas,
                    qtdRegistros = registrosPaginados.Count(),
                    qtdRegistrosEncontrados = quantidadeRegistros
                },
                ComprasContratacoes = registrosPaginados
            };

            return retorno;
        }
    }
}