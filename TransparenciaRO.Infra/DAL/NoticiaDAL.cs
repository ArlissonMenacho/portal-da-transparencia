﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using System.Data.Entity;
using static System.Data.Entity.EntityState;

namespace TransparenciaRO.Infra.DAL
{
    public class NoticiaDAL : DALBase<Noticia>
    {
        public NoticiaDAL(DbTransparencia db) : base(db) { }

        public override Noticia ObterPorId(Guid pId)
        {
            return _db.Noticias.FirstOrDefault(n => n.NoticiaId == pId);
        }

        public IQueryable<Noticia> ObterComUsuarios()
        {
            return _db.Noticias.Include(n => n.UsuarioCriador);
        }

        public override void Remover(Guid pId)
        {
            var noticia = ObterPorId(pId);
            _db.Set<Noticia>().Attach(noticia);
            noticia.DataExclusao = DateTime.Now;
            _db.SaveChanges();
        }

        public override Guid SalvarDados(Noticia pDados)
        {
            if (pDados.NoticiaId == new Guid())
            {
                //Insert                
                pDados.DataUltimaAtualizacao = pDados.DataCriacao = DateTime.Now;
                pDados.NoticiaId = Guid.NewGuid();                
                _db.Noticias.Add(pDados);
            }
            else
            {
                //Update: Obtém dados do BD e atualiza apenas os registros da view
                var registroBD = ObterPorId(pDados.NoticiaId);
                _db.Entry(registroBD).State = EntityState.Modified;
                if (!registroBD.HTMLNoticia.SequenceEqual(pDados.HTMLNoticia))
                {
                    // Somente altera a data de última atualização da notícia, caso o seu conteúdo seja alterado
                    registroBD.HTMLNoticia = pDados.HTMLNoticia;
                    registroBD.DataUltimaAtualizacao = DateTime.Now;
                }
                registroBD.Publicado = pDados.Publicado;
                registroBD.Titulo = pDados.Titulo;
                registroBD.Popup = pDados.Popup;
            }                        
            _db.SaveChanges();
            return pDados.NoticiaId;
        }

        public Noticia GetNoticiaPoPup()
        {
            Noticia destaque = _db.Noticias.FirstOrDefault(n => n.Popup);
            if (destaque == null)
            {
                destaque = _db.Noticias.ToList().First();
            }
            return destaque;
        }
    }
}
