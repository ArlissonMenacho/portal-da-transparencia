﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class PagamentoFornecedorRPVsDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public PagamentoFornecedorRPVsDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public void AddList(ICollection<PagamentoFornecedorRPV> pagamentoFornecedorRpvs)
        {

            var dataMax = pagamentoFornecedorRpvs.Max(x => x.DataOb);
            var dataMin = pagamentoFornecedorRpvs.Min(x => x.DataOb);
            var pg = _dbTransparencia.PagamentoFornecedorRpvs.Where(x => x.DataOb >= dataMin && x.DataOb <= dataMax).ToList();

            if (pg.Any())
            {
                var add = pagamentoFornecedorRpvs.NotContains(pg, rpv => rpv.DocumentoOB).ToList();

                if (add.Any())
                    _dbTransparencia.PagamentoFornecedorRpvs.AddRange(add);

                _dbTransparencia.SaveChanges();
            }
            else
            {
                _dbTransparencia.PagamentoFornecedorRpvs.AddRange(pagamentoFornecedorRpvs);
                _dbTransparencia.SaveChanges();
            }
        }

        public void UpdateList(ICollection<PagamentoFornecedorRPV> pagamentoFornecedorRpvs)
        {
            var dataMax = pagamentoFornecedorRpvs.Max(x => x.DataOb);
            var dataMin = pagamentoFornecedorRpvs.Min(x => x.DataOb);
            var pg = _dbTransparencia.PagamentoFornecedorRpvs.Where(x => x.DataOb >= dataMin && x.DataOb <= dataMax).ToList();

            if (pg.Any())
            {
                var update = pagamentoFornecedorRpvs.Contains(pg, rpv => rpv.DocumentoOB).ToList();

                if (update.Any())
                {
                    foreach (var i in update)
                    {
                        var pagamentoRpv = _dbTransparencia.PagamentoFornecedorRpvs.Where(x => x.DocumentoOB == i.DocumentoOB && x.Movimentacao == i.Movimentacao && (x.StatusOb != i.StatusOb || x.ValorPaga != i.ValorPaga)).FirstOrDefault();

                        if(pagamentoRpv != null)
                        {
                            pagamentoRpv.StatusOb = i.StatusOb;
                            pagamentoRpv.ValorPaga = i.ValorPaga;
                            pagamentoRpv.DataAtualizacao = DateTime.Now;

                            _dbTransparencia.Entry(pagamentoRpv).State = EntityState.Modified;
                            _dbTransparencia.SaveChanges();
                        }
                    }
                }

            }
        }

        public List<PagamentoFornecedorRPV> PagamentoFornecedorRpvs(DateTime dataInicial, DateTime dataFinal, string credor, string docCredor)
        {
            credor = credor.IsNullOrEmpty() ? null : credor;
            docCredor = docCredor.IsNullOrEmpty() ? null : docCredor;

            return (from pf in _dbTransparencia.PagamentoFornecedorRpvs
                    where (pf.DataOb >= dataInicial && pf.DataOb <= dataFinal) &&
                          (credor == null || pf.Credor.Contains(credor)) &&
                          (docCredor == null || pf.DocCredor.Contains(docCredor)) &&
                          (pf.DocumentoOB != "" && pf.DocumentoOB != null)                          
                    select pf).ToList();
        }

        public PagamentoFornecedorRPV RPVDataAtualizacao()
        {
            return _dbTransparencia.PagamentoFornecedorRpvs.OrderByDescending(f => f.DataAtualizacao).FirstOrDefault();
        }
    }
}
