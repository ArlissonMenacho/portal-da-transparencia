﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Data.Entity;

// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class LotacaoDAL
    {
        public DbTransparencia dbTransparencia;

        public LotacaoDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public List<Lotacao> ObterTodos()
        {
            return dbTransparencia.Lotacoes
                .OrderBy(l => l.Descricao)
                .ToList();
        }

        public Lotacao BuscarLotacao(Guid lotacaoId)
        {
            return dbTransparencia.Lotacoes
                .FirstOrDefault(x => x.LotacaoId == lotacaoId);
        }

        public Lotacao BuscaoLotacaoPorDescricao(string descricao)
        {
            return dbTransparencia.Lotacoes
                .FirstOrDefault(l => l.Descricao == descricao);
        }

        public Lotacao BuscarLotacaoPorLot(int lot)
        {
            return dbTransparencia.Lotacoes
                .FirstOrDefault(l => l.Lot == lot);
        }

        public void AdicionarDados(List<Lotacao> lotacoes)
        {
            dbTransparencia.BulkInsert(lotacoes);
        }

        public IQueryable<Lotacao> BuscarLotacaoesPorAnoMes(int ano, int mes)
        {
            return dbTransparencia.RelacaoServidores
                .Where(r => r.Ano == ano && r.Mes == mes)
                .Select(r => r.Lotacao)
                .Distinct()
                .OrderBy(r => r.Descricao);
        }

        public void Salvar()
        {
            dbTransparencia.SaveChanges();
        }
    }
}
