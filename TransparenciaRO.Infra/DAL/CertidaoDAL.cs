﻿using System;
using System.Data.Entity;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class CertidaoDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public CertidaoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public void Adicionar(Certidao certidao)
        {
            _dbTransparencia.Certidoes.Add(certidao);
            _dbTransparencia.SaveChanges();
        }

        public Certidao VerificarAutenticidadeDaCertidao(Guid certidaoId, DateTime dataEmissao, string cpfCnpj)
        {
            var autenticarCertidao = _dbTransparencia.Certidoes
                .Include(x=>x.Fornecedor)
                .FirstOrDefault(c => c.CertidaoId == certidaoId && c.DataDeEmissao == dataEmissao && c.Fornecedor.CpfCnpj == cpfCnpj);

            if (autenticarCertidao == null) return null;

            return autenticarCertidao.DataDeEmissao.AddDays(30).Date > DateTime.Now.Date ? autenticarCertidao : null;
        }
    }
}