﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Infra.DAL
{
    public class PagamentoFornecedorDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public PagamentoFornecedorDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }

        public List<PagamentoFornecedor> GetEmpenhosFornecedores(DateTime dataInicial, DateTime dataFinal, string credor, string docCredor, string unidadeGestora)
        {
            credor = credor.IsNullOrEmpty() ? null : credor;
            docCredor = docCredor.IsNullOrEmpty() ? null : docCredor;
            unidadeGestora = unidadeGestora.IsNullOrEmpty() ? null : unidadeGestora;

            var retorno = _dbTransparencia.PagamentosFornecedores.Where(pf =>
                (pf.DataDocumento >= dataInicial && pf.DataDocumento <= dataFinal) &&
                (unidadeGestora == null || pf.CodUnidadeGestora == unidadeGestora) &&
                (credor == null || pf.Credor.Contains(credor)) &&
                ((docCredor == null && (pf.DocCredor.Length == 11 || pf.DocCredor.Length == 14)) ||
                 pf.DocCredor.Contains(docCredor)) &&
                (pf.DocumentoOB != "" && pf.DocumentoOB != null) &&
                (pf.ValorPaga > 0M)).Take(1000).ToList();
            
            var resultado = retorno.GroupBy(x => new { x.Projeto, x.DocumentoOB, x.Exercicio, x.Programa })
                .Select(a => a.FirstOrDefault()).ToList();

            return resultado;
        }

        public List<PagamentoFornecedor> GetEmpenhosUG(string empenho, string unidadeGestora, DateTime pDataInicial)
        {
            return _dbTransparencia.PagamentosFornecedores
                .Where(d => d.DataDocumento == pDataInicial
                            && d.CodUnidadeGestora == unidadeGestora
                            && d.NumEmpenho == empenho
                            && d.ValorPaga > 0M)
                .Take(1000)
                .ToList();
        }

        public PagamentoFornecedor DetalhaEmpenhoFornecedor(string docCredor, string pNumEmpenho, string documentoNE, long? especificacaoDespesa, string processo, decimal? valorDespesa)
        {
            return _dbTransparencia.PagamentosFornecedores
                .FirstOrDefault(d => d.DocCredor == docCredor
                                     && d.NumEmpenho == pNumEmpenho
                                     && d.DocumentoNE == documentoNE
                                     && (especificacaoDespesa == null || d.EspecificacaoDespesa == especificacaoDespesa)
                                     && d.Processo == processo
                                     && (valorDespesa == null || d.ValorPaga == valorDespesa));
        }
    }
}
