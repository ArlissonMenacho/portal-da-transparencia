﻿using System;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class IndiceDAL : DALBase<Indice>
    {
        public IndiceDAL(DbTransparencia db) : base(db) { }

        public IQueryable<Indice> Buscar(string pBusca)
        {
            var termoBusca = String.Join(" OR ", pBusca.Split(' ').Select(t => $"formsof(INFLECTIONAL,\"{t}\")"));
            return _db.Indice.SqlQuery($"select i.* from Indice i inner join CONTAINSTABLE(Indice,PalavrasChave,'{termoBusca}',language 'PORTUGUESE') as r on i.indiceid = r.[key] order by r.rank desc;").AsQueryable();
        }

        public override Indice ObterPorId(Guid pId)
        {
            if (pId == new Guid())
                return null;
            return _db.Indice.FirstOrDefault(i => i.IndiceId == pId);
        }

        public override Guid SalvarDados(Indice pDados)
        {
            var indiceBD = ObterPorId(pDados.IndiceId);
            if (indiceBD == null)
            {
                //Inserir
                if (pDados.IndiceId == new Guid())
                    pDados.IndiceId = Guid.NewGuid();
                _db.Indice.Add(pDados);
            }
            else
            {
                indiceBD.Href = pDados.Href;
                indiceBD.PalavrasChave = pDados.PalavrasChave;
                //indiceBD.PreviaResultado = pDados.PreviaResultado;
                indiceBD.Titulo = pDados.Titulo;
                indiceBD.Agrupamento = pDados.Agrupamento;
            }
            _db.SaveChanges();
            return pDados.IndiceId;
        }

        public override void Remover(Guid pId)
        {
            throw new NotImplementedException();
        }
    }
}
