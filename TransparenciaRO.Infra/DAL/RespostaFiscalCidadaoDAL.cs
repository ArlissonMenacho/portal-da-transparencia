﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class RespostaFiscalCidadaoDAL
    {
        private DbTransparencia _dbTransparencia;
        private FiscalCidadaoDAL _fiscalCidadaoDal;

        public RespostaFiscalCidadaoDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
            _fiscalCidadaoDal = new FiscalCidadaoDAL(_dbTransparencia);
        }


        public void SalvarResposta(RespostaFiscalCidadao resposta)
        { 
            _dbTransparencia.RespostaFiscalCidadao.Add(resposta);
            _dbTransparencia.SaveChanges();
            _fiscalCidadaoDal.Update(new FiscalCidadao
            {
                ProtocoloId = resposta.ProtocoloId,
                EStatus = ETipoStatus.EmAndamento
            });
        }

    }
}
