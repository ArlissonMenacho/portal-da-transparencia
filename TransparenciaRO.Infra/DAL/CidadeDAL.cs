﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
// ReSharper disable InconsistentNaming

namespace TransparenciaRO.Infra.DAL
{
    public class CidadeDAL
    {
        private readonly DbTransparencia dbTransparencia;

        public CidadeDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }
        
        public List<Cidade> BuscarCidades()
        {
            return dbTransparencia.Cidades.ToList();
        }

        public List<Cidade> BuscarCidadePorEstado(EEstado estado)
        {
            return new List<Cidade>(dbTransparencia.Cidades.Where(c => c.EstadoId == estado));
        }

        public Cidade BuscarCidade(Guid cidadeId)
        {
            return dbTransparencia.Cidades.FirstOrDefault(x => x.CidadeId == cidadeId);
        }
    }
}
