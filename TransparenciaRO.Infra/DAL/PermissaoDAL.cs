﻿using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using System.Linq;
using System;
using TransparenciaRO.Infra.Utils;
using static System.Data.Entity.EntityState;

namespace TransparenciaRO.Infra.DAL
{
    public class PermissaoDAL : ReadOnlyDALBase<Permissao>
    {
        public PermissaoDAL(DbTransparencia db) : base(db) { }
    }
}
