﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Utils;
using static System.Data.Entity.EntityState;

namespace TransparenciaRO.Infra.DAL
{
    public class PortariaResolucaoDAL
    {
        public DbTransparencia DbTransparencia;

        public PortariaResolucaoDAL(DbTransparencia dbTransparencia)
        {
            DbTransparencia = dbTransparencia;
        }

        public Guid? AdicionarEditar(PortariaResolucao pPortariaResolucao, out string pMensagemErro)
        {
            try
            {
                if (pPortariaResolucao.PortariaResolucaoId == new Guid())
                {
                    //Inserção
                    pPortariaResolucao.PortariaResolucaoId = Guid.NewGuid();
                    foreach (var arquivo in pPortariaResolucao.Arquivos)
                    {
                        DbTransparencia.Arquivos.Attach(arquivo);
                        arquivo.Temporario = false;
                    }
                    DbTransparencia.PortariaResolucao.Add(pPortariaResolucao);
                }
                else
                {
                    //Alteração
                    var registroBD = DbTransparencia.PortariaResolucao.Include(cc => cc.Arquivos).FirstOrDefault(cc => cc.PortariaResolucaoId == pPortariaResolucao.PortariaResolucaoId);
                    DbTransparencia.Entry(DbTransparencia.PortariaResolucao.FirstOrDefault(cc => cc.PortariaResolucaoId == pPortariaResolucao.PortariaResolucaoId)).CurrentValues.SetValues(pPortariaResolucao);
                    //Adiciona os novos arquivos
                    //var arquivosAInserir = DbTransparencia.Arquivos.Where(a => a.ContratoConvenioId == pContratoConvenio.ContratoConvenioId && a.DataRemocao == null).Except(pContratoConvenio.Arquivos, a => a.ArquivoId);
                    var arquivosAInserir = DbTransparencia.Arquivos.Where(a => a.PortariaResolucaoId == pPortariaResolucao.PortariaResolucaoId && a.DataRemocao == null && a.Temporario);
                    foreach (var arquivoAInserir in arquivosAInserir)
                    {
                        if (DbTransparencia.Entry(arquivoAInserir).State == Detached)
                            DbTransparencia.Arquivos.Attach(arquivoAInserir);
                        registroBD.Arquivos.Add(arquivoAInserir);
                    }
                    //Remove os arquivos que foram desvinculados
                    var arquivosARemover = DbTransparencia.Arquivos.Where(a => a.PortariaResolucaoId == pPortariaResolucao.PortariaResolucaoId && a.DataRemocao == null).Except(pPortariaResolucao.Arquivos, a => a.ArquivoId).ToList();
                    arquivosARemover.ForEach(a => { DbTransparencia.Entry(a).State = Modified; a.DataRemocao = DateTime.Now; });

                }
                foreach (var arquivo in DbTransparencia.Arquivos.Where(a => a.PortariaResolucaoId == pPortariaResolucao.PortariaResolucaoId))
                {
                    arquivo.Temporario = false;
                }
                DbTransparencia.SaveChanges();
                pMensagemErro = "";
                return pPortariaResolucao.PortariaResolucaoId;
            }
            catch (Exception e)
            {
                pMensagemErro = e.ToString();
                return null;
            }
        }

        public PortariaResolucao Buscar(Guid pPortariaResolucaoId)
        {
            var retorno = DbTransparencia
                .PortariaResolucao
                .Include(cc => cc.Arquivos)
                .FirstOrDefault(cc => cc.PortariaResolucaoId == pPortariaResolucaoId);

            retorno.Arquivos = retorno.Arquivos.Where(a => a.DataRemocao == null).ToList();

            return retorno;
        }

        public IList<PortariaResolucao> BuscarPortariaResolucao(DateTime? dataInicial, DateTime? dataFinal, Guid? ug, int? numeroDocumento, ETipoDocumento tipoDocumento)
        {
            if (ug != null || numeroDocumento > 0)
            {
                dataInicial = null;
                dataFinal = null;
            }
            var dados = DbTransparencia.PortariaResolucao
                .Include(a => a.Arquivos)
                .Include(x => x.UnidadeGestora)
                .Where(x => (numeroDocumento == null || x.Numero == numeroDocumento)
                            && (dataInicial == null || x.DataPublicacao >= dataInicial)
                            && (dataFinal == null || x.DataPublicacao <= dataFinal)
                            && (ug == null || x.UnidadeGestoraId == ug)
                            && (x.ETipoDocumento == tipoDocumento)
                )
                .OrderBy(l => l.Numero)
                .ToList();
            return dados;
        }
    }
}
