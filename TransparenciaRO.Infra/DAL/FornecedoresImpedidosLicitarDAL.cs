﻿using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class FornecedoresImpedidosLicitarDAL
    {
        public readonly DbTransparencia dbTransparencia;

        public FornecedoresImpedidosLicitarDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public IList<FornecedoresImpedidosLicitar> GetlAll()
        {
            return dbTransparencia.FornecedoresImpedidosLicitar.ToList();
        }
        public IList<FornecedoresImpedidosLicitar> GetlByCpfCnpj(string cpjCnpj)
        {
            return dbTransparencia.FornecedoresImpedidosLicitar.Where(x => x.CpfCnpj.Equals(cpjCnpj)).ToList();
        }
    }
}