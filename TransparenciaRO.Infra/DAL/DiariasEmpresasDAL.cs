﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using EntityFramework.Extensions;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class DiariasEmpresasDAL
    {
        public DbTransparencia dbTransparencia;

        public DiariasEmpresasDAL(DbTransparencia _dbTransparencia)
        {
            this.dbTransparencia = _dbTransparencia;
        }

        public void VerificarRemoverDadosExistente(int ano, int mes, EEmpresa eEmpresa)
        {
            if (dbTransparencia.DiariasEmpresas.Any(x => x.Ano == ano && x.Mes == mes && x.Empresa == eEmpresa))
            {
                dbTransparencia.DiariasEmpresas.Where(x => x.Ano == ano && x.Mes == mes && x.Empresa == eEmpresa).Delete();
            }
        }

        public void AdicionarDados(IList<DiariasEmpresa> diariasEmpresas)
        {
            dbTransparencia.BulkInsert(diariasEmpresas);
            dbTransparencia.SaveChanges();
        }

        public IList<DiariasEmpresa> BuscarDiariasEmpresas(int ano, int? mes, string nome, EEmpresa empresa)
        {
            return dbTransparencia.DiariasEmpresas
                .Where(x => (nome == "" || x.Nome.Contains(nome))
                            && x.Ano == ano
                            && (mes == null || x.Mes == mes)
                            && x.Empresa == empresa)
                .OrderBy(x => x.NumeroProcesso)
                .ToList();
        }
        public DiariasEmpresa DetalharDiariaEmpresa(string numeroProcesso, string nome, EEmpresa empresa)
        {
            return dbTransparencia.DiariasEmpresas
                .FirstOrDefault(x => x.NumeroProcesso == numeroProcesso
                                     && x.Nome == nome
                                     && x.Empresa == empresa);

        }

        public IList<DiariasEmpresa> BuscarServidoresDiariasEmpresas(string numeroProcesso, string nome, EEmpresa empresa)
        {
            return dbTransparencia.DiariasEmpresas
                .Where(x => x.NumeroProcesso == numeroProcesso
                            && x.Empresa == empresa
                            && x.Nome != nome)
                .OrderBy(x => x.Nome)
                .ToList();
        }

        public int Ano()
        {
            return dbTransparencia.DiariasEmpresas.FirstOrDefault() != null
                ? dbTransparencia.DiariasEmpresas.Max(x => x.Ano)
                : 0;
        }

        public int Mes(int ano)
        {
            var retorno = dbTransparencia.DiariasEmpresas.FirstOrDefault(x => x.Ano == ano) != null
                ? dbTransparencia.DiariasEmpresas.Where(x => x.Ano == ano).Max(x => x.Mes)
                : 12;
            return retorno;
        }
    }
}
