﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class OrgaoDAL
    {
        public DbTransparencia dbTransparencia;

        public OrgaoDAL(DbTransparencia _dbTransparencia)
        {
            this.dbTransparencia = _dbTransparencia;
        }

        public IEnumerable<Orgao> GetOrgaos()
        {
            return dbTransparencia.Orgaos.ToList();
        }

        /// <summary>
        /// Retorna um dicionário de órgãos, onde a chave são os códigos do órgão com mesmo nome (separados por vírgula) e o valor é o nome propriamente dito
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetOrgaosConsolidadosPorNome()
        {
            return dbTransparencia.Orgaos.GroupBy(org => org.NomOrgao).ToList()
                .Select(org => new
                {
                    Descricao = org.Key.Trim() + " (" + (org.Select(o => o.CodOrgao).Distinct().Count() > 1 ? "Códigos " + String.Join(",", org.Select(o => o.CodOrgao).Distinct()) : "Código " + org.FirstOrDefault().CodOrgao) + ")",
                    Codigos = String.Join(",", org.Select(o => o.CodOrgao).Distinct())
                })
                .GroupBy(o => o.Codigos)
                .ToDictionary(k => k.Key, v => v.First().Descricao);
        }
    }
}