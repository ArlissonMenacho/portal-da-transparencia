﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class ContatoInstitucionalDAL
    {
        public DbTransparencia dbTransparencia;

        public ContatoInstitucionalDAL(DbTransparencia dbTransparencia)
        {
            this.dbTransparencia = dbTransparencia;
        }

        public List<ContatoInstitucional> BuscarInstituicoes()
        {
            return dbTransparencia.Instituicoes.Include(x => x.CargosInstituicoes).ToList();
        }
    }
}
