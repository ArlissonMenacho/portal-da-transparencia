﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Infra.DAL
{
    public class AcrescimoRendimentosDAL
    {
        private readonly DbTransparencia _dbTransparencia;

        public AcrescimoRendimentosDAL(DbTransparencia dbTransparencia)
        {
            _dbTransparencia = dbTransparencia;
        }
        public IEnumerable<AcrescimoRendimentos> ObterVantagensPorCpf(string pCPF, int ano, int mes)
        {
            var cpfConvertido = Convert.ToInt64(pCPF);
            var anoConvertido = ano.ToString();
            var mesConvertido = mes.ToString().Length == 1 ? "0" + mes.ToString() : mes.ToString();

            return _dbTransparencia.Vantagens.Where(v => (v.CPF == cpfConvertido && v.Ano == anoConvertido && v.Mes == mesConvertido)).Distinct().ToList();
        }
        public IEnumerable<AcrescimoRendimentos> ObterTemporariasPorCpf(string pCPF, int ano, int mes)
        {
            var cpfConvertido = Convert.ToInt64(pCPF);
            var anoConvertido = ano.ToString();
            var mesConvertido = mes.ToString().Length == 1 ? "0" + mes.ToString() : mes.ToString();

            return _dbTransparencia.Temporarias.Where(t => (t.CPF == cpfConvertido && t.Ano == anoConvertido && t.Mes == mesConvertido)).Distinct().ToList();
        }
    }
}
