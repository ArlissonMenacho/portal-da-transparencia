﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Migrations.ConteudosDinamicos
{
    public static class api
    {
        public static string html = @"
            <div class='container'>
                <p>Abaixo está a descrição detalha das APIs de receitas e despesas</p>
                <div class='panel panel-default'>
                    <div class='panel-heading'>Despesas</div>
                    <div class='panel-body'>
                        Pesquisa despesas por empenho, CNPJ, nome do credor, ano ou mês<br />
                        Todos os parâmetros são opcionais, e o número máximo de registros retornados é de <b>1000</b>.
                        <b>Método: </b>GET<br />
                        <b>URL: </b><a href='http://transparencia.ro.gov.br/Api/DespesaApi'>http://transparencia.ro.gov.br/Api/DespesaApi</a>
                        <table class='table table-striped'>
                            <thead>
                                <tr>
                                    <td><b>Parâmetros de entrada</b></td>
                                    <td><b>Descrição</b></td>
                                    <td><b>Exemplo</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>ano</b></td>
                                    <td>O ano para qual o detalhamento da despesa deverá ser retornado.</td>
                                    <td>2016</td>
                                </tr>
                                <tr>
                                    <td><b>mes</b></td>
                                    <td>O mês para qual o detalhamento da despesa deverá ser retornado.</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td><b>empenho</b></td>
                                    <td>O número de empenho para qual o detalhamento da despesa deverá ser retornado <b>(a consulta é canse-insensitive)</b></td>
                                    <td>2010NE01979</td>
                                </tr>
                                <tr>
                                    <td><b>credor</b></td>
                                    <td>O nome do credor para qual o detalhamento da despesa deverá ser retornado. <b>Poderá ser preechido parcialmente</b>.</td>
                                    <td>NET<small> (retorna todas as despesas cujo credor contém NET no nome)</small></td>
                                </tr>
                                <tr>
                                    <td><b>cnpj</b></td>
                                    <td>O CNPJ do credor para qual o detalhamento da despesa deverá ser retornado. <b>Poderá ser preenchido ou não com pontos e traços, o sistema ignorará tudo o que não for numérico</b>.</td>
                                    <td>11111111111111 ou 11.111.111/0001-00<small></small></td>
                                </tr>
                            </tbody>
                        </table>
                        <b>Exemplo de chamada: </b><a href='http://transparencia.ro.gov.br/Api/DespesaApi?ano=2016&mes=12'>http://transparencia.ro.gov.br/Api/DespesaApi?ano=2016&mes=12</a><br />
                        <b>Exemplo de chamada intervalo entre anos: </b><a href='http://transparencia.ro.gov.br/api/despesaApi/getintervaloano?anoIni=2010&anoFim=2011'>http://transparencia.ro.gov.br/Api/DespesaApi/GetIntervaloAno?anoIni=2010&anoFim=2011</a><br />
                    </div>
                </div>
                <div class='panel panel-default'>
                    <div class='panel-heading'>Receitas</div>
                    <div class='panel-body'>
                        <b>Ano</b><br/>
                        Método para detalhar as receitas do governo referentes a um ano específico<br/>
                        <b>Método: </b>GET<br/>
                        <b>URL: </b><a href='http://transparencia.ro.gov.br/Api/ReceitaApi?anoInicial=2010&anoFinal=2016&consolidacao=0&ug=0'>http://transparencia.ro.gov.br/Api/ReceitaApi?anoInicial=2010&anoFinal=2016&consolidacao=0&ug=0</a>
                        <table class='table table-striped'>
                            <thead>
                            <tr>
                                <td><b>Parâmetros de entrada</b></td>
                                <td><b>Descrição</b></td>
                                <td><b>Exemplo</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><b>ano</b></td>
                                <td>O ano para qual o detalhamento da receita deverá ser retornado.</td>
                                <td>2016</td>
                            </tr>
                            <tr>
                                <td><b>tipoconsulta</b></td>
                                <td>
                                    O tipo de consulta a ser efetuado.
                                    <ul>
                                        <li><b>0: </b>Total - A somatória de todas as receitas, ignorando os descontos</li>
                                        <li><b>1: </b>Líquida - A somatória de todas as receitas, contemplando também os descontos</li>
                                        <li><b>2: </b>Receita corrente</li>
                                        <li><b>3: </b>Receita capital</li>
                                        <li><b>4: </b>Receita intra-orçamentária</li>
                                    </ul>
                                </td>
                                <td>0 à 4</td>
                            </tr>
                            </tbody>
                        </table>
                        <b>Exemplo de chamada: </b><a href='http://transparencia.ro.gov.br/Api/ReceitaApi?anoInicial=2010&anoFinal=2016&consolidacao=0&ug=0'>http://transparencia.ro.gov.br/Api/ReceitaApi?anoInicial=2010&anoFinal=2016&consolidacao=0&ug=0</a><br/>
                        <hr/>
                        <b>Mes</b><br/>
                        Método para detalhar as receitas do governo referentes a um ano e mês específico<br/>
                        <b>Método: </b>GET<br/>
                        <b>URL: </b><a href='http://transparencia.ro.gov.br/api/receitaapi/mes?mes=12&ano=2016&tipoconsulta=0&ug=0'>http://transparencia.ro.gov.br/api/receitaapi/mes?mes=12&ano=2016&tipoconsulta=0&ug=0</a>
                        <table class='table table-striped'>
                            <thead>
                            <tr>
                                <td><b>Parâmetros de entrada</b></td>
                                <td><b>Descrição</b></td>
                                <td><b>Exemplo</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><b>ano</b></td>
                                <td>O ano para qual o detalhamento da receita deverá ser retornado.</td>
                                <td>2016</td>
                            </tr>
                            <tr>
                                <td><b>mes</b></td>
                                <td>O mês para qual o detalhamento da receita deverá ser retornado.</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td><b>tipoconsulta</b></td>
                                <td>
                                    O tipo de consulta a ser efetuado.
                                    <ul>
                                        <li><b>0: </b>Total - A somatória de todas as receitas, ignorando os descontos</li>
                                        <li><b>1: </b>Líquida - A somatória de todas as receitas, contemplando também os descontos</li>
                                        <li><b>2: </b>Receita corrente</li>
                                        <li><b>3: </b>Receita capital</li>
                                        <li><b>4: </b>Receita intra-orçamentária</li>
                                    </ul>
                                </td>
                                <td>0 à 4</td>
                            </tr>
                            </tbody>
                        </table>
                        <b>Exemplo de chamada: </b><a href='http://transparencia.ro.gov.br/api/receitaapi/mes?mes=12&ano=2016&tipoconsulta=0&ug=0'>http://transparencia.ro.gov.br/api/receitaapi/mes?mes=12&ano=2016&tipoconsulta=0&ug=0</a><br/>
                    </div>
                </div>
                <div class='panel panel-default'>
                    <div class='panel-heading'>Compras e Contratações</div>
                    <div class='panel-body'>
                        Pesquisar compras e contratações por unidades gestora, modalidade de compra, data inicial e data final<br />
                        Todos os parâmetros são opcionais, e o número máximo de registros retornados é de <b>1000</b>.
                        <b>Método: </b>GET<br />
                        <b>URL: </b><a href='http://transparencia.ro.gov.br/Api/ComprasContratacoesApi'>http://transparencia.ro.gov.br/Api/ComprasContratacoesApi</a>
                        <table class='table table-striped'>
                            <thead>
                                <tr>
                                    <td><b>Parâmetros de entrada</b></td>
                                    <td><b>Descrição</b></td>
                                    <td><b>Exemplo</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>dataInicial</b></td>
                                    <td>A data inicial para qual o detalhamento de compras e contratações deverá ser retornado.</td>
                                    <td>01/01/2017</td>
                                </tr>
                                <tr>
                                    <td><b>dataFinal</b></td>
                                    <td>A data final para qual o detalhamento de compras e contratações deverá ser retornado.</td>
                                    <td>13/02/2017</td>
                                </tr>
                                <tr>
                                    <td><b>unidadeGestora</b></td>
                                    <td>O código da unidade gestora para qual o detalhamento de compras e contratações deverá ser retornado.</td>
                                    <td>QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L</td>
                                </tr>
                                <tr>
                                    <td><b>codModalidadeCompra</b></td>
                                    <td>O código da modalidade de compra para qual o detalhamento de compras e contratações deverá ser retornado.</td>
                                    <td>QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L</td>
                                </tr>
                            </tbody>
                        </table>
                        <b>Exemplo de chamada: </b><a href='http://transparencia.ro.gov.br/Api/ComprasContratacoesApi?dataInicial=01/01/2017&dataFinal=13/02/2017&unidadeGestora=QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L&modalidadeCompra=QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L'>http://transparencia.ro.gov.br/Api/ComprasContratacoesApi?dataInicial=01/01/2017&dataFinal=13/02/2017&unidadeGestora=QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L&modalidadeCompra=QMR4ZdomqcS83Fb3ZT3aeA3QU4L3QU4L</a><br />
                    </div>
                </div>
            </div>
            ";
    }
}
