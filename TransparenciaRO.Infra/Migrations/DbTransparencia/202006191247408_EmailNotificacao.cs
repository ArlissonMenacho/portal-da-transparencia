namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailNotificacao : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailNotificacao",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 160, unicode: false),
                        Email = c.String(nullable: false, maxLength: 160, unicode: false),
                        NotificarIntegraProcesso = c.Boolean(nullable: false),
                        NotificarBoletim = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailNotificacao");
        }
    }
}
