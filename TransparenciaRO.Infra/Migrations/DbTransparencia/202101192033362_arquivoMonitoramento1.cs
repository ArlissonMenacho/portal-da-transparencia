namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class arquivoMonitoramento1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Arquivos", name: "Monitoramento_Id", newName: "MonitoramentoId");
            RenameIndex(table: "dbo.Arquivos", name: "IX_Monitoramento_Id", newName: "IX_MonitoramentoId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Arquivos", name: "IX_MonitoramentoId", newName: "IX_Monitoramento_Id");
            RenameColumn(table: "dbo.Arquivos", name: "MonitoramentoId", newName: "Monitoramento_Id");
        }
    }
}
