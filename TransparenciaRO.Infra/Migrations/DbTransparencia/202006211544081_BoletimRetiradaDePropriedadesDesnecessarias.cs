namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoletimRetiradaDePropriedadesDesnecessarias : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RegistroBoletim", "ArquivoId");
            DropColumn("dbo.RegistroBoletim", "Mensagem");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RegistroBoletim", "Mensagem", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.RegistroBoletim", "ArquivoId", c => c.Guid(nullable: false));
        }
    }
}
