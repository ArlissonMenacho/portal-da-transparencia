namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DividaAtiva : DbMigration
    {
        public override void Up()
        {
            
            AddColumn("dbo.DividaAtiva", "ValorTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DividaAtiva", "InscricaoEstadual", c => c.Int(nullable: false));
            AddColumn("dbo.DividaAtiva", "InscricaoCDA", c => c.Int(nullable: false));
            AddColumn("dbo.DividaAtiva", "Cartorio", c => c.String(maxLength: 150, unicode: false));
            AddColumn("dbo.DividaAtiva", "DocumentoCartorio", c => c.Int(nullable: false));
            AddColumn("dbo.DividaAtiva", "DataConfirmacaoCartorio", c => c.DateTime(nullable: false, defaultValueSql: "GETDATE()"));
            AddColumn("dbo.DividaAtiva", "Acao", c => c.String(maxLength: 30, unicode: false));
            AddColumn("dbo.DividaAtiva", "Situacao", c => c.Int(nullable: false));
            AddColumn("dbo.DividaAtiva", "CodigoReceita", c => c.Int(nullable: false));
            AddColumn("dbo.DividaAtiva", "GuiaLancamento", c => c.Int(nullable: false));
            DropColumn("dbo.DividaAtiva", "ValorCancelado");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DividaAtiva", "ValorCancelado", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.DividaAtiva", "GuiaLancamento");
            DropColumn("dbo.DividaAtiva", "CodigoReceita");
            DropColumn("dbo.DividaAtiva", "Situacao");
            DropColumn("dbo.DividaAtiva", "Acao");
            DropColumn("dbo.DividaAtiva", "DataConfirmacaoCartorio");
            DropColumn("dbo.DividaAtiva", "DocumentoCartorio");
            DropColumn("dbo.DividaAtiva", "Cartorio");
            DropColumn("dbo.DividaAtiva", "InscricaoCDA");
            DropColumn("dbo.DividaAtiva", "InscricaoEstadual");
            DropColumn("dbo.DividaAtiva", "ValorTotal");
        }
    }
}
