namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tipodocumentoFomento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Arquivos", "TipoAnexoTermoFomento", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Arquivos", "TipoAnexoTermoFomento");
        }
    }
}
