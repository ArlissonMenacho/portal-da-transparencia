namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NovosCamposPagamentoFornecedorRPV : DbMigration
    {
        public override void Up()
        {           
            AddColumn("dbo.PagamentoFornecedorRPV", "CpfCnpj", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.PagamentoFornecedorRPV", "CpfCnpjCessionario", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.PagamentoFornecedorRPV", "Cessionario", c => c.String(maxLength: 8000, unicode: false));            
        }
        
        public override void Down()
        {                       
            DropColumn("dbo.PagamentoFornecedorRPV", "Cessionario");
            DropColumn("dbo.PagamentoFornecedorRPV", "CpfCnpjCessionario");
            DropColumn("dbo.PagamentoFornecedorRPV", "CpfCnpj");            
        }
    }
}
