namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ComiteTransparenciaProtocolo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InscricaoComiteTransparencia", "Protocolo", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InscricaoComiteTransparencia", "Protocolo");
        }
    }
}
