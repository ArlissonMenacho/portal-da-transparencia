namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DispensaFornecedores2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FornecedorDispensa", "NumeroEmpenho", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AlterColumn("dbo.FornecedorDispensa", "CNPJ", c => c.String(nullable: false, maxLength: 18, unicode: false));
            AlterColumn("dbo.FornecedorDispensa", "RazaoSocial", c => c.String(nullable: false, maxLength: 250, unicode: false));
            DropColumn("dbo.FornecedorDispensa", "NotaEmpenho");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FornecedorDispensa", "NotaEmpenho", c => c.String(nullable: false, maxLength: 8000, unicode: false));
            AlterColumn("dbo.FornecedorDispensa", "RazaoSocial", c => c.String(nullable: false, maxLength: 8000, unicode: false));
            AlterColumn("dbo.FornecedorDispensa", "CNPJ", c => c.String(nullable: false, maxLength: 8000, unicode: false));
            DropColumn("dbo.FornecedorDispensa", "NumeroEmpenho");
        }
    }
}
