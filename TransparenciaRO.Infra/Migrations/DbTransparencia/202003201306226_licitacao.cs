namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System.Data.Entity.Migrations;

    public partial class licitacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Licitacoes", "Valor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Licitacoes", "NumeroLicitacao", c => c.Int());
            AlterColumn("dbo.Licitacoes", "Ano", c => c.Int());
            AlterColumn("dbo.Licitacoes", "EFusoHorario", c => c.Int());
        }

        public override void Down()
        {
            AlterColumn("dbo.Licitacoes", "EFusoHorario", c => c.Int(nullable: false));
            AlterColumn("dbo.Licitacoes", "Ano", c => c.Int(nullable: false));
            AlterColumn("dbo.Licitacoes", "NumeroLicitacao", c => c.Int(nullable: false));
            DropColumn("dbo.Licitacoes", "Valor");
        }
    }
}
