namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DispensaFornecedores : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FornecedorDispensa",
                c => new
                    {
                        FornecedorId = c.Guid(nullable: false),
                        CNPJ = c.String(nullable: false, maxLength: 8000, unicode: false),
                        RazaoSocial = c.String(nullable: false, maxLength: 8000, unicode: false),
                        NotaEmpenho = c.String(nullable: false, maxLength: 8000, unicode: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LicitacaoId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.FornecedorId)
                .ForeignKey("dbo.Licitacoes", t => t.LicitacaoId)
                .Index(t => t.LicitacaoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FornecedorDispensa", "LicitacaoId", "dbo.Licitacoes");
            DropIndex("dbo.FornecedorDispensa", new[] { "LicitacaoId" });
            DropTable("dbo.FornecedorDispensa");
        }
    }
}
