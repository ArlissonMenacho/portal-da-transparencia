namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class termoFomento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TermoFomento",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PrestacaoDeContasId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PrestacaoDeContas", t => t.PrestacaoDeContasId)
                .Index(t => t.PrestacaoDeContasId);
            
            CreateTable(
                "dbo.PrestacaoDeContas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DataPrevista = c.DateTime(nullable: false),
                        DataApresentacao = c.DateTime(nullable: false),
                        PrazoAnalise = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RemuneracaoEquipe",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nome = c.String(maxLength: 8000, unicode: false),
                        Cpf = c.String(maxLength: 8000, unicode: false),
                        Rg = c.String(maxLength: 8000, unicode: false),
                        Cargo = c.String(maxLength: 8000, unicode: false),
                        ValorTotal = c.Double(nullable: false),
                        ValorIndividual = c.Double(nullable: false),
                        TermoFomento_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TermoFomento", t => t.TermoFomento_Id)
                .Index(t => t.TermoFomento_Id);
            
            CreateTable(
                "dbo.TBContratacaoAditivadas",
                c => new
                    {
                        NumeroProcesso = c.String(nullable: false, maxLength: 128, unicode: false),
                        NumeroContrato = c.String(nullable: false, maxLength: 128, unicode: false),
                        TipoFormalizacao = c.String(maxLength: 8000, unicode: false),
                        TipoAlteracao = c.String(maxLength: 8000, unicode: false),
                        DataAlteracao = c.String(maxLength: 8000, unicode: false),
                        DataVigencia = c.String(maxLength: 8000, unicode: false),
                        Quantidade = c.String(maxLength: 8000, unicode: false),
                        ValorAcrescido = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.NumeroProcesso, t.NumeroContrato });
            
            AddColumn("dbo.ContratosConvenios", "TermoFomentoId", c => c.Guid(nullable: false));
            AddColumn("dbo.VW_DadosLicitacao2020", "ModalidadeLicitacao", c => c.Int(nullable: false));
            AddColumn("dbo.VW_DadosLicitacao2020", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.VW_DadosLicitacao2020", "Ano", c => c.Int());
            AddColumn("dbo.VW_DadosLicitacao2020", "NumeroLicitacao", c => c.Int());
            AddColumn("dbo.VW_DadosLicitacao2020", "TermoAditivoArquivoId", c => c.Guid());
            AddColumn("dbo.VW_DadosLicitacao2020", "TermoAditivoTipoAnexo", c => c.Int());
            CreateIndex("dbo.ContratosConvenios", "TermoFomentoId");
            AddForeignKey("dbo.ContratosConvenios", "TermoFomentoId", "dbo.TermoFomento", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContratosConvenios", "TermoFomentoId", "dbo.TermoFomento");
            DropForeignKey("dbo.RemuneracaoEquipe", "TermoFomento_Id", "dbo.TermoFomento");
            DropForeignKey("dbo.TermoFomento", "PrestacaoDeContasId", "dbo.PrestacaoDeContas");
            DropIndex("dbo.RemuneracaoEquipe", new[] { "TermoFomento_Id" });
            DropIndex("dbo.TermoFomento", new[] { "PrestacaoDeContasId" });
            DropIndex("dbo.ContratosConvenios", new[] { "TermoFomentoId" });
            DropColumn("dbo.VW_DadosLicitacao2020", "TermoAditivoTipoAnexo");
            DropColumn("dbo.VW_DadosLicitacao2020", "TermoAditivoArquivoId");
            DropColumn("dbo.VW_DadosLicitacao2020", "NumeroLicitacao");
            DropColumn("dbo.VW_DadosLicitacao2020", "Ano");
            DropColumn("dbo.VW_DadosLicitacao2020", "Status");
            DropColumn("dbo.VW_DadosLicitacao2020", "ModalidadeLicitacao");
            DropColumn("dbo.ContratosConvenios", "TermoFomentoId");
            DropTable("dbo.TBContratacaoAditivadas");
            DropTable("dbo.RemuneracaoEquipe");
            DropTable("dbo.PrestacaoDeContas");
            DropTable("dbo.TermoFomento");
        }
    }
}
