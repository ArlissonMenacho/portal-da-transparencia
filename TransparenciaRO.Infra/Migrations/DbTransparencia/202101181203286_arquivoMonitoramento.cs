namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class arquivoMonitoramento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Arquivos", "Monitoramento_Id", c => c.Guid());
            CreateIndex("dbo.Arquivos", "Monitoramento_Id");
            AddForeignKey("dbo.Arquivos", "Monitoramento_Id", "dbo.Monitoramento", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Arquivos", "Monitoramento_Id", "dbo.Monitoramento");
            DropIndex("dbo.Arquivos", new[] { "Monitoramento_Id" });
            DropColumn("dbo.Arquivos", "Monitoramento_Id");
        }
    }
}
