namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resolvendoConflitosFornecedorId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Arquivos", "FornecedorId", "dbo.FornecedorDispensa");
            DropIndex("dbo.Arquivos", new[] { "FornecedorId" });
            //DropColumn("dbo.Arquivos", "FornecedorId");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.Arquivos", "FornecedorId", c => c.Guid());
            CreateIndex("dbo.Arquivos", "FornecedorId");
            AddForeignKey("dbo.Arquivos", "FornecedorId", "dbo.FornecedorDispensa", "FornecedorId");
        }
    }
}
