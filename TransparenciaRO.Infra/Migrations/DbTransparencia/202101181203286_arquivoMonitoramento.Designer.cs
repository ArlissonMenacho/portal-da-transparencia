// <auto-generated />
namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class arquivoMonitoramento : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(arquivoMonitoramento));
        
        string IMigrationMetadata.Id
        {
            get { return "202101181203286_arquivoMonitoramento"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
