namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterTypeColumnDividaAtiva : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DividaAtiva", "DocumentoCartorio", c => c.String(maxLength: 30, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DividaAtiva", "DocumentoCartorio", c => c.Int(nullable: false));
        }
    }
}
