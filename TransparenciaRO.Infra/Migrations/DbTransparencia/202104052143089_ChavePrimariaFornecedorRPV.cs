namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChavePrimariaFornecedorRPV : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.PagamentoFornecedorRPV");
            AddColumn("dbo.PagamentoFornecedorRPV", "PagamentoFornecedorRPVId", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.PagamentoFornecedorRPV", "DocumentoOB", c => c.String(maxLength: 8000, unicode: false));            
            AddPrimaryKey("dbo.PagamentoFornecedorRPV", "PagamentoFornecedorRPVId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.PagamentoFornecedorRPV");            
            AlterColumn("dbo.PagamentoFornecedorRPV", "DocumentoOB", c => c.String(nullable: false, maxLength: 128, unicode: false));
            DropColumn("dbo.PagamentoFornecedorRPV", "PagamentoFornecedorRPVId");
            AddPrimaryKey("dbo.PagamentoFornecedorRPV", new[] { "DocumentoOB", "Movimentacao" });
        }
    }
}
