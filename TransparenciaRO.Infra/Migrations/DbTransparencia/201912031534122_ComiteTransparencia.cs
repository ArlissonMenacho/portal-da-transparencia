namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ComiteTransparencia : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InscricaoComiteTransparencia",
                c => new
                {
                    InscricaoId = c.Guid(nullable: false),
                    RazaoSocial = c.String(nullable: false, maxLength: 100, unicode: false),
                    Cnpj = c.String(nullable: false, maxLength: 18, unicode: false),
                    RepresentanteLegal = c.String(nullable: false, maxLength: 100, unicode: false),
                    Telefone = c.String(nullable: false, maxLength: 15, unicode: false),
                })
                .PrimaryKey(t => t.InscricaoId);

            AddColumn("dbo.Arquivos", "InscricaoComiteTransparenciaId", c => c.Guid());
            AddColumn("dbo.Arquivos", "TipoAnexo", c => c.Int());
            CreateIndex("dbo.Arquivos", "InscricaoComiteTransparenciaId");
            AddForeignKey("dbo.Arquivos", "InscricaoComiteTransparenciaId", "dbo.InscricaoComiteTransparencia", "InscricaoId");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Arquivos", "InscricaoComiteTransparenciaId", "dbo.InscricaoComiteTransparencia");
            DropIndex("dbo.Arquivos", new[] { "InscricaoComiteTransparenciaId" });
            DropColumn("dbo.Arquivos", "TipoAnexo");
            DropColumn("dbo.Arquivos", "InscricaoComiteTransparenciaId");
            DropTable("dbo.InscricaoComiteTransparencia");
        }
    }
}
