namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DividaAtiva1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DividaAtiva",
                c => new
                {
                    Id                      = c.Int(nullable: false),
                    Nome                    = c.String(nullable: false, unicode: false, maxLength: 240),
                    CpfCnpj                 = c.String(nullable: false, unicode: false, maxLength: 14),
                    ValorTotal              = c.Decimal(nullable: false, precision: 18, scale: 2),
                    ValorPago               = c.Decimal(nullable: false, precision: 18, scale: 2),
                    ValorAPagar             = c.Decimal(nullable: false, precision: 18, scale: 2),
                    InscricaoEstadual       = c.String(nullable: true, unicode: false, maxLength: 240),
                    InscricaoCDA            = c.String(nullable: true, unicode: false, maxLength: 240),
                    Cartorio                = c.String(nullable: true, unicode: false, maxLength: 240),
                    DocumentoCartorio       = c.String(nullable: true, unicode: false, maxLength: 240),
                    DataConfirmacaoCartorio = c.DateTime(nullable: true),
                    Acao                    = c.String(nullable: true, unicode: false, maxLength: 240),
                    Situacao                = c.String(nullable: true, unicode: false, maxLength: 240),
                    CodigoReceita           = c.String(nullable: true, unicode: false, maxLength: 240),
                    GuiaLancamento          = c.String(nullable: true, unicode: false, maxLength: 240),
                })
            .PrimaryKey(t => new { t.Id });
        }
        
        public override void Down()
        {
            DropTable("dbo.DividaAtiva");
        }
    }
}
