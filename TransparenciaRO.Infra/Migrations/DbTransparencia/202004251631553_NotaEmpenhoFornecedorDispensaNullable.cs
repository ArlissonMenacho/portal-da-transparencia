namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotaEmpenhoFornecedorDispensaNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FornecedorDispensa", "NumeroEmpenho", c => c.String(maxLength: 20, unicode: false));
            
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FornecedorDispensa", "NumeroEmpenho", c => c.String(nullable: false, maxLength: 20, unicode: false));
        }
    }
}
