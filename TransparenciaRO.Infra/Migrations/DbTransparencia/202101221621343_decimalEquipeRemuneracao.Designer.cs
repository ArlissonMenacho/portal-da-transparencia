// <auto-generated />
namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class decimalEquipeRemuneracao : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(decimalEquipeRemuneracao));
        
        string IMigrationMetadata.Id
        {
            get { return "202101221621343_decimalEquipeRemuneracao"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
