namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System.Data.Entity.Migrations;

    public partial class Status_ItemLicitacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemLicitacao", "Status", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.ItemLicitacao", "Status");
        }
    }
}
