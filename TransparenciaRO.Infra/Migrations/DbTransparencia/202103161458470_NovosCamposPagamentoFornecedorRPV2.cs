namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NovosCamposPagamentoFornecedorRPV2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PagamentoFornecedorRPV", "CpfCnpj");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PagamentoFornecedorRPV", "CpfCnpj", c => c.String(maxLength: 18, unicode: false));
        }
    }
}
