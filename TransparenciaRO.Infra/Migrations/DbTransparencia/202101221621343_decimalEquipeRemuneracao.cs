namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class decimalEquipeRemuneracao : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RemuneracaoEquipe", "ValorIndividual", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RemuneracaoEquipe", "ValorIndividual", c => c.Double(nullable: false));
        }
    }
}
