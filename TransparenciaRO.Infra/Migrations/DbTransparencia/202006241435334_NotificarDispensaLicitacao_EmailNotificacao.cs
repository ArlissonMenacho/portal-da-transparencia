namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificarDispensaLicitacao_EmailNotificacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailNotificacao", "NotificarDispensaLicitacao", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailNotificacao", "NotificarDispensaLicitacao");
        }
    }
}
