namespace TransparenciaRO.Infra.Migrations.DbTransparencia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LicitacaoSigla : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Licitacoes", "Sigla", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Licitacoes", "Sigla");           
        }
    }
}
