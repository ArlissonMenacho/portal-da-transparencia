﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransparenciaRO.Infra.Constantes
{
    public class ConstantesDiversas
    {
        public static string[] TermosComuns = new[]
        {
            "de",
            "da",
            "sobre",
            "no",
            "na",
            "ou",
            "ao",
            "do",
            "é",
            "a",
            "que",
            "para",
            "em",
            "com",
            "um",
            "uma",
            "e",
            "por",
            "quê",
            "pois",
            "o",
            "ter",
            "os",
            "as",
            "como",
            "pelo",
            "pela",
            "sua",
            "seu",
            "além",
            "todos",
            "todas",
            "às",
            "à",
            "ao",
            "aos",
            "pode",
            "das",
            "dos"
        };

        public static string EmailCge => "transparencia.cge.ro@gmail.com";
    }
}
