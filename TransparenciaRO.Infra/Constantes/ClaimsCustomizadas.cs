﻿
namespace TransparenciaRO.Infra.Constantes
{
    public static class ClaimsCustomizadas
    {
        public const string IdUsuario = "IdUsuario";
        public const string UgID = "UgID";
        public const string PrecisaTrocarSenhaNoProximoLogin = "PrecisaTrocarSenhaNoProximoLogin";
        public const string Administrador = "Administrador";
        public const string Pastas = "Pastas";
        public const string CPF = "CPF";
    }
}
