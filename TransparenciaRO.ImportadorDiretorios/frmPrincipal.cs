﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Collections;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.DAL;
using static System.Environment;

namespace TransparenciaRO.ImportadorDiretorios
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }
        private string diretorioSelecionado = "";
        private Guid pastaBDPaiSelecionada;
        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            if (!RotinaSelecaoPasta())
            {
                MessageBox.Show("Uma pasta precisa ser selecionada para a varredura!");
                Close();
            }
        }
        private void RotinaImportacao()
        {
            if (MessageBox.Show("Tem certeza que deseja importar as pastas e arquivos para o BD?", "Confirmação", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var pastas = MontarEstruturaArquivos(diretorioSelecionado, pastaBDPaiSelecionada, true);
                SalvarAlteracoes(pastas.ToList());
            }
        }
        private bool RotinaSelecaoPasta()
        {
            var fsd = new FolderBrowserDialog();
            fsd.Description = "Escolha um diretório raíz para que os arquivos sejam varridos";
            if (fsd.ShowDialog() == DialogResult.OK)
            {
                Logar("Carregando estrutura de pastas...");
                var pastasBD = MontarEstruturaPastaBD();
                Logar("Carregamento concluído");
                tvPastasBD.Nodes.Clear();
                tvPreviaPastasAInserir.Nodes.Clear();
                diretorioSelecionado = fsd.SelectedPath;
                Logar($"Diretório a ter seu conteúdo importado: {diretorioSelecionado}");
                tvPastasBD.Nodes.AddRange(pastasBD.Select(p => ToTreeNode(p)).ToArray());
                tvPreviaPastasAInserir.Nodes.AddRange(MontarEstruturaArquivos(diretorioSelecionado, new Guid(), false).Select(p => ToTreeNode(p)).ToArray());
                return true;
            }
            else
            {
                
                return false;
            }
        }
        Guid lastGuid;
        private Guid GetNewGuid()
        {
            lastGuid = Guid.NewGuid();
            return lastGuid;
        }

        private void Logar(string pMensagem)
        {
            txtLog.Text = DateTime.Now.ToString("HH:mm:ss") + " - " + pMensagem + System.Environment.NewLine + txtLog.Text;
        }

        private void SalvarAlteracoes(List<Pasta> pPastas)
        {
            using (var dbTransparencia = new DbTransparencia())
            {
                new PastaDAL(dbTransparencia).SalvarPastaEArquivos(pPastas);
            }
        }

        private CustomTreeNode ToTreeNode(Pasta pPasta)
        {
            return new CustomTreeNode(pPasta.Nome, pPasta.PastaId.ToString(), pPasta.SubPastas.Select(p => ToTreeNode(p)).ToArray());
        }

        private IList<Pasta> MontarEstruturaPastaBD()
        {
            using (var dbTransparencia = new DbTransparencia())
            {
                var pastaDAL = new PastaDAL(dbTransparencia);
                return pastaDAL.GetPastasEArquivos(null, true).ToList();
            }
        }

        /// <summary>
        /// Cria uma lista com as pastas, sub-pastas e seus respectivos arquivos
        /// </summary>
        /// <param name="pDiretorio"></param>
        /// <param name="pIdPastaPai"></param>
        /// <param name="pAlterarNomeArquivos"></param>
        /// <returns></returns>
        private IList<Pasta> MontarEstruturaArquivos(string pDiretorio, Guid pIdPastaPai, bool pAlterarNomeArquivos)
        {
            Logar($"Montar estrutura para diretório: {pDiretorio}");
            try
            {
                return new List<DirectoryInfo>(new DirectoryInfo(pDiretorio).GetDirectories()).Select(f => new Pasta
                {
                    Nome = f.Name,
                    PastaId = GetNewGuid(),
                    IconeFa = Infra.Enum.EFontAwesome.fa_folder,
                    PastaOrigemId = pIdPastaPai,
                    Arquivos = f.GetFiles().Select(a => CriarArquivo(a, !pAlterarNomeArquivos)).ToList(),
                    Administravel = false,
                    SubPastas = MontarEstruturaArquivos(f.FullName, lastGuid, pAlterarNomeArquivos),
                    Descricao = f.Name

                }).ToList();
            }
            catch
            {
                return null;
            }
        }

        public Arquivo CriarArquivo(FileInfo a, bool ReadOnly = true)
        {
            var guidArquivo = Guid.NewGuid();
            var retorno =  new Arquivo
            {
                ArquivoId = guidArquivo,
                DataUpload = DateTime.Now,
                Descricao = String.Join(".", a.Name.Split('.').Reverse().Skip(1).Reverse()),
                Extensao = a.Name.Split('.').Last(),
                PastaId = lastGuid
            };
            if (!ReadOnly)
            { 
                File.Copy(a.FullName, diretorioSelecionado + @"\" + guidArquivo.ToString());
                Logar($"Processando cópia de {a.FullName} para {guidArquivo.ToString()}...");
                this.Refresh();
            }

            return retorno;
        }

        private void tvPastasBD_AfterSelect(object sender, TreeViewEventArgs e)
        {
            lblPastaPaiSelecionada.Text = $"Pasta selecionada: {tvPastasBD.SelectedNode.Text} (PastaId: {tvPastasBD.SelectedNode.Tag})";
            pastaBDPaiSelecionada = new Guid(tvPastasBD.SelectedNode.Tag.ToString());
        }

        private void btnSelecionarOutraPasta_Click(object sender, EventArgs e)
        {
            RotinaSelecaoPasta();
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            RotinaImportacao();
        }
    }

    public partial class CustomTreeNode : TreeNode
    {
        public CustomTreeNode(string pText, string pTag, TreeNode[] pNodes) : base(
            pText, pNodes)
        {
            this.Tag = pTag;
        }
    }
}




