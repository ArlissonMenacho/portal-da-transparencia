﻿namespace TransparenciaRO.ImportadorDiretorios
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvPastasBD = new System.Windows.Forms.TreeView();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.btnImportar = new System.Windows.Forms.Button();
            this.lblPastaPaiSelecionada = new System.Windows.Forms.Label();
            this.tvPreviaPastasAInserir = new System.Windows.Forms.TreeView();
            this.btnSelecionarOutraPasta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tvPastasBD
            // 
            this.tvPastasBD.Location = new System.Drawing.Point(268, 29);
            this.tvPastasBD.Name = "tvPastasBD";
            this.tvPastasBD.Size = new System.Drawing.Size(250, 378);
            this.tvPastasBD.TabIndex = 1;
            this.tvPastasBD.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvPastasBD_AfterSelect);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(524, 29);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(243, 378);
            this.txtLog.TabIndex = 2;
            // 
            // btnImportar
            // 
            this.btnImportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportar.Location = new System.Drawing.Point(12, 413);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(755, 50);
            this.btnImportar.TabIndex = 3;
            this.btnImportar.Text = "Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // lblPastaPaiSelecionada
            // 
            this.lblPastaPaiSelecionada.Location = new System.Drawing.Point(13, 3);
            this.lblPastaPaiSelecionada.Name = "lblPastaPaiSelecionada";
            this.lblPastaPaiSelecionada.Size = new System.Drawing.Size(754, 23);
            this.lblPastaPaiSelecionada.TabIndex = 4;
            this.lblPastaPaiSelecionada.Text = "...";
            // 
            // tvPreviaPastasAInserir
            // 
            this.tvPreviaPastasAInserir.Location = new System.Drawing.Point(12, 29);
            this.tvPreviaPastasAInserir.Name = "tvPreviaPastasAInserir";
            this.tvPreviaPastasAInserir.Size = new System.Drawing.Size(250, 322);
            this.tvPreviaPastasAInserir.TabIndex = 5;
            // 
            // btnSelecionarOutraPasta
            // 
            this.btnSelecionarOutraPasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelecionarOutraPasta.Location = new System.Drawing.Point(12, 357);
            this.btnSelecionarOutraPasta.Name = "btnSelecionarOutraPasta";
            this.btnSelecionarOutraPasta.Size = new System.Drawing.Size(250, 50);
            this.btnSelecionarOutraPasta.TabIndex = 6;
            this.btnSelecionarOutraPasta.Text = "Selecionar outra pasta";
            this.btnSelecionarOutraPasta.UseVisualStyleBackColor = true;
            this.btnSelecionarOutraPasta.Click += new System.EventHandler(this.btnSelecionarOutraPasta_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 466);
            this.Controls.Add(this.btnSelecionarOutraPasta);
            this.Controls.Add(this.tvPreviaPastasAInserir);
            this.Controls.Add(this.lblPastaPaiSelecionada);
            this.Controls.Add(this.btnImportar);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.tvPastasBD);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.Text = "TransparenciaRO - Importação de pastas";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TreeView tvPastasBD;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.Label lblPastaPaiSelecionada;
        private System.Windows.Forms.TreeView tvPreviaPastasAInserir;
        private System.Windows.Forms.Button btnSelecionarOutraPasta;
    }
}