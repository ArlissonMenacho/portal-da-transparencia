﻿using System.Collections.Generic;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Business
{
    public class FolhaPagamentoBusiness
    {
        public IList<FolhaPagamento> getFolha(int ano, int mes,string nome, string cpf, string lotacao)
        {
            return new FolhaPagamentoDAL().getFolha(ano, mes,nome, cpf, lotacao);
        }

        public List<string> getLotacaoUG(short ano, byte mes)
        {
            var uGs = new FolhaPagamentoDAL().getLotacaoUG(ano, mes);
            return uGs;
        }

        public FolhaPagamento getFolha(int ano, int mes, string matricula)
        {
            return new FolhaPagamentoDAL().getFolha(ano, mes, matricula);
        }

    }
}
