﻿using System.Collections.Generic;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Business
{
    public class UnidadeGestoraBusiness
    {
        public IEnumerable<UnidadeGestora> GetUnidadesGestoras(DbTransparencia dbTransparencia)
        {
            return new UnidadeGestoraDAL(dbTransparencia).GetUnidadesGestoras();
        }

    }
}
