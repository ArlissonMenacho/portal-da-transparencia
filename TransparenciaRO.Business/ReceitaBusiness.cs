﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.View;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;
using TransparenciaRO.Infra.Enum;
using TransparenciaRO.Infra.Utils;

namespace TransparenciaRO.Business
{
    public class ReceitaBusiness
    {
        private static IQueryable<ReceitaAcumuladaSiafem> QueryReceita(DbTransparencia db, int anoinicial, int anofinal, ETipoConsultaReceita pTipoConsulta, string pUG, bool pMostrarDetalhes = false)
        {
            int codUG = int.TryParse(pUG, out codUG) ? codUG : 0;

            var codigosAPartirDe2021String = new List<string> { "1", "2", "7" };

            return db
                    .ReceitaAcumuladaSiafem
                    .Where(r =>
                        (
                            r.Exercicio >= anoinicial && r.Exercicio <= anofinal /* Inicialmente, escopa os registros dentro do período informado  */
                        ) &&
                        (
                            (r.Exercicio >= 2021 && (pMostrarDetalhes || codigosAPartirDe2021String.Contains(r.CodEspecificacaoReceita.ToString().Substring(0, 1)))) ||    /* Se exercício for >= 2021, deverá considerar apenas códigos 1, 2, 7, verificar 8 e 9 */
                            (r.Exercicio >= 2013 && r.Exercicio < 2021 && (pMostrarDetalhes || new List<long> { 10000000, 20000000, 70000000, 90000000 }.Contains(r.CodEspecificacaoReceita))) ||    /* Se exercício for >= 2013, deverá considerar apenas códigos 10000000, 20000000, 70000000, 90000000 */
                            (r.Exercicio <= 2012 && r.CodEspecificacaoReceita > 0 && (pMostrarDetalhes || r.CodEspecificacaoReceita % 10000000 == 0)) /* Se exercício <= 2012, deverá considerar todos os códigos terminados em 0000000 */
                        ) &&
                        (
                            codUG == 0 || r.CodAdministracao == codUG
                        ) &&
                        (
                            (pTipoConsulta == ETipoConsultaReceita.Liquida) || /* Se o tipo de consulta for líquida, apenas soma todos os registros encontrados (os tributos, por já serem negativos, automaticamente subtrairão da soma) */

                            (pTipoConsulta == ETipoConsultaReceita.Capital &&
                            ((r.Exercicio == 2021 && r.CodEspecificacaoReceita.ToString().Substring(0, 1) == "2") ||
                             (r.CodEspecificacaoReceita == (long)EEspecificacaoReceita.ReceitaCapital))) || /* Consulta capital: Soma registros com código de especificação 20000000 */

                            (pTipoConsulta == ETipoConsultaReceita.Corrente &&
                            ((r.Exercicio == 2021 && r.CodEspecificacaoReceita.ToString().Substring(0, 1) == "1") ||
                             (r.CodEspecificacaoReceita == (long)EEspecificacaoReceita.ReceitaCorrente))) || /* Consulta corrente: Soma registros com código de especificação 10000000 */

                            (pTipoConsulta == ETipoConsultaReceita.IntraOrcamentaria &&
                            ((r.Exercicio == 2021 && r.CodEspecificacaoReceita.ToString().Substring(0, 1) == "7") ||
                             (r.CodEspecificacaoReceita == (long)EEspecificacaoReceita.ReceitaIntraOrcamentaria))) || /* Consulta intra-orçamentária: Soma registros com código de especificação 70000000 */

                            (pTipoConsulta == ETipoConsultaReceita.Total &&
                            ((r.Exercicio == 2021 && r.CodEspecificacaoReceita.ToString().Substring(0, 1) != "9") ||
                             (r.CodEspecificacaoReceita != (long)EEspecificacaoReceita.Tributos))) /* Consulta total: Soma todos os registros, mas não subtrai os tributos */
                        )
                    );
        }

        public static List<ReceitaModel> DetalharAno(int pAno, string pUG, ETipoConsultaReceita pTipoConsulta)
        {
            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAno, pAno, pTipoConsulta, pUG, true);

                var queryGroup = queryFiltro
                    .GroupBy(r => new { r.CodEspecificacaoReceita, r.Descricao })
                    .OrderBy(r => r.Key.CodEspecificacaoReceita)
                    .Select(r => new
                    {
                        CodReferencia = r.Key.CodEspecificacaoReceita,
                        DescricaoReferencia = r.Key.Descricao,
                        ValorArrecadado = r.Sum(rr => rr.ValorMensal ?? 0),
                        ValorAcumulado = r.Where(rr => rr.Mes == r.Max(rrr => rrr.Mes)).Sum(rr => rr.ValorAcumulado) ?? 0,
                        ValorPrevisto = r.Where(rr => rr.Mes == r.Max(rrr => rrr.Mes)).Sum(rr => rr.ValorSaldoFinal) ?? 0,
                        Ano = pAno
                    }).OrderBy(r => r.CodReferencia).ToList();

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroup)
                {
                    receitas.Add(new ReceitaModel
                    {
                        Referencia = r.CodReferencia.ToString("0-0-0-0-00-00").Replace('-', '.') + " - " + r.DescricaoReferencia,
                        ValorArrecadado = r.ValorArrecadado,
                        ValorAcumulado = r.ValorAcumulado,
                        ValorPrevisto = r.ValorPrevisto,
                        Ano = r.Ano
                    });
                }

                return receitas;
            }
        }

        public static List<ReceitaModel> DetalharMes(int pMes, int pAno, string pUG, ETipoConsultaReceita pTipoConsulta)
        {
            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAno, pAno, pTipoConsulta, pUG, true);

                var queryGroup = queryFiltro
                    .Where(r => r.Mes == pMes)
                    .GroupBy(r => new { r.CodEspecificacaoReceita, r.Descricao })
                    //.ToList()
                    .OrderBy(r => r.Key.CodEspecificacaoReceita)
                    .Select(r => new
                    {
                        CodReferencia = r.Key.CodEspecificacaoReceita,
                        DescricaoReferencia = r.Key.Descricao,
                        ValorArrecadado = r.Sum(rr => rr.ValorMensal ?? 0),
                        ValorAcumulado = r.Sum(rr => rr.ValorAcumulado ?? 0),
                        ValorPrevisto = r.Sum(rr => rr.ValorSaldoFinal ?? 0),
                        Mes = pMes,
                        Ano = pAno
                    });

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroup)
                {
                    receitas.Add(new ReceitaModel
                    {
                        Referencia = r.CodReferencia.ToString("0-0-0-0-00-00").Replace('-', '.') + " - " + r.DescricaoReferencia,
                        ValorArrecadado = r.ValorArrecadado,
                        ValorAcumulado = r.ValorAcumulado,
                        ValorPrevisto = r.ValorPrevisto,
                        Ano = r.Ano
                    });
                }

                return receitas;
            }
        }

        public static List<ReceitaModel> DetalharQuadrimestre(string pQuadrimestre, int pAno, string pUg, ETipoConsultaReceita pTipoConsulta)
        {
            var receitaUtils = new ReceitaUtils().QuadrimestreDetalhes(pQuadrimestre);

            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAno, pAno, pTipoConsulta, pUg);

                var queryGroup = queryFiltro
                    .Where(r => r.Mes >= receitaUtils.Item1 && r.Mes <= receitaUtils.Item2 && r.Exercicio == pAno)
                    .GroupBy(r => new { r.CodEspecificacaoReceita, r.Descricao })
                    //.ToList()
                    .OrderBy(r => r.Key.CodEspecificacaoReceita)
                    .Select(r => new
                    {
                        CodReferencia = r.Key.CodEspecificacaoReceita,
                        DescricaoReferencia = r.Key.Descricao,
                        ValorArrecadado = r.Sum(m => m.ValorMensal ?? 0),
                        ValorPrevisto = r.Sum(rr => rr.ValorSaldoFinal ?? 0)
                    });

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroup)
                {
                    receitas.Add(new ReceitaModel
                    {
                        Referencia = r.CodReferencia.ToString("0-0-0-0-00-00").Replace('-', '.') + " - " + r.DescricaoReferencia,
                        ValorArrecadado = r.ValorArrecadado,
                        ValorPrevisto = r.ValorPrevisto
                    });
                }

                return receitas;
            }
        }

        public static List<ReceitaModel> GetReceitaAno(int pAnoInicial,
            int pAnoFinal,
            ETipoConsolidacao pConsolidarPor,
            ETipoConsultaReceita pTipoConsulta,
            string pUG,
            bool pIncluirReceitaPrevista)
        {
            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAnoInicial, pAnoFinal, pTipoConsulta, pUG);

                var queryGroup = queryFiltro
                     .GroupBy(r => new { Ano = r.Exercicio })
                     .OrderBy(r => r.Key.Ano)
                     .Select(s => new
                     {
                         Ano = s.Key.Ano,
                         ValorArrecadado = s.Sum(m => m.ValorMensal ?? 0),
                         ValorPrevisto = pIncluirReceitaPrevista ? s.Where(d => d.Mes == s.Where(sss => sss.Exercicio == d.Exercicio).Max(sss => sss.Mes)).Sum(v => v.ValorSaldoFinal ?? 0) : 0
                     });

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroup)
                {
                    var receita = new ReceitaModel
                    {
                        Referencia = r.Ano.ToString(),
                        ValorArrecadado = r.ValorArrecadado,
                        ValorPrevisto = r.ValorPrevisto,
                        Ano = r.Ano
                    };

                    receitas.Add(receita);
                }

                return receitas;
            }
        }

        public static List<ReceitaModel> GetReceitaMes(int pAnoInicial,
            int pAnoFinal,
            ETipoConsolidacao pConsolidarPor,
            ETipoConsultaReceita pTipoConsulta,
            string pUG,
            bool pIncluirReceitaPrevista)
        {
            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAnoInicial, pAnoFinal, pTipoConsulta, pUG);

                var queryGroup = queryFiltro
                     .GroupBy(r => new { Mes = r.Mes, Ano = r.Exercicio })
                     .OrderBy(r => r.Key.Ano).ThenBy(r => r.Key.Mes)
                     .Select(s => new
                     {
                         ValorArrecadado = s.Sum(m => m.ValorMensal ?? 0),
                         ValorPrevisto = pIncluirReceitaPrevista ? s.Sum(v => v.ValorSaldoFinal ?? 0) : 0,
                         Ano = s.Key.Ano,
                         Mes = s.Key.Mes,
                     });

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroup)
                {
                    var receita = new ReceitaModel
                    {
                        Referencia = r.Mes.ToString() + "/" + r.Ano.ToString(),
                        ValorArrecadado = r.ValorArrecadado,
                        ValorPrevisto = r.ValorPrevisto,
                        Ano = r.Ano,
                        Mes = r.Mes
                    };

                    receitas.Add(receita);
                }

                return receitas;
            }
        }

        public static List<ReceitaModel> GetReceitaQuadrimestre(int pAnoInicial, int pAnoFinal, ETipoConsolidacao pConsolidarPor, ETipoConsultaReceita pTipoConsulta, string pUG, bool pIncluirReceitaPrevista)
        {
            var primeiroQuadrimestre = new int[] { 1, 2, 3, 4 };
            var segundoQuadrimestre = new int[] { 5, 6, 7, 8 };


            using (var db = new DbTransparencia())
            {
                var queryFiltro = QueryReceita(db, pAnoInicial, pAnoFinal, pTipoConsulta, pUG);

                var queryGroupMes = queryFiltro
                    .GroupBy(r => new
                    {
                        Mes = r.Mes,
                        Ano = r.Exercicio
                    })
                    .OrderBy(r => r.Key.Ano)
                    .ThenBy(r => r.Key.Mes)
                    .Select(s => new
                    {
                        Quadrimestre = s.Key.Mes <= 4 ? "1º Quadr/" + s.Key.Ano.ToString() :
                                       s.Key.Mes >= 5 && s.Key.Mes <= 8 ? "2º Quadr/" + s.Key.Ano.ToString() :
                                       "3º Quadr/" + s.Key.Ano.ToString(),
                        ValorArrecadado = s.Sum(m => m.ValorMensal ?? 0),
                        Ano = s.Key.Ano,
                        Mes = s.Key.Mes
                    });

                var queryGroupQuadrimestre = queryGroupMes
                    .GroupBy(r => new
                    {
                        Quadrimestre = r.Quadrimestre,
                        Ano = r.Ano
                    })
                    .OrderBy(r => r.Key.Ano)
                    .Select(s => new
                    {
                        Quadrimestre = s.Key.Quadrimestre,
                        ValorArrecadado = s.Sum(r => r.ValorArrecadado),
                        Ano = s.Key.Ano
                    });

                var receitas = new List<ReceitaModel>();

                foreach (var r in queryGroupQuadrimestre)
                {
                    var receita = new ReceitaModel
                    {
                        Referencia = r.Quadrimestre,
                        ValorArrecadado = r.ValorArrecadado,
                        Ano = r.Ano
                    };

                    receitas.Add(receita);
                }

                return receitas;
            }
        }
    }
}
