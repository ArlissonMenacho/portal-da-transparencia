﻿using System.Collections.Generic;
using TransparenciaRO.Infra.DAL;
using TransparenciaRO.Infra.Model.DB.dbTransparencia;

namespace TransparenciaRO.Business
{
    public class TbPllOrgaoSiafemBusiness
    {
        public List<OrgaoSiafem> getOrgaos()
        {
            return new OrgaoSiafemDAL().getOrgaos();
        }
    }
}
